/*
    count continous allocations, all ways should be used
usage: command nprog nway
*/

#include <iostream>
#include <algorithm>

using namespace std;

uint64_t verify(int nprog, int nway){
    int up = nprog*2;
    int tmask = (1<<nway)-1;
    int alloc[up+1];
    fill(alloc, alloc+up, 0);

    uint64_t rt = 0;
    while(1){
        int mask = 0;
        for(int i = 0; i < nprog; i++){
            if(alloc[i*2] > alloc[i*2+1]){
                mask = 0;
                break;
            }
            for(int j = alloc[i*2]; j <= alloc[i*2+1]; j++)
                mask |= (1 << j);
        }
        //cout << mask << endl;
        if(mask == tmask)
            rt++;

        int pos = 0;
        alloc[pos]++;
        while(alloc[pos] == nway){
            alloc[pos++] = 0;
            alloc[pos]++;
        }
        if(pos == up) break;
    }
    return rt;
}

double DP(int nprog, int nway){
    int up = 1<<nway;
    double* res[nprog+1]; // can be optimized to O(nway*3^nprog)

    for(int i = 0; i <= nprog; i++){
        res[i] = new double[up];
        fill(res[i], res[i]+up, 0.0);
    }

    res[0][0] = 1;
    for(int i = 0; i < nprog; i++)
        for(int j = 0; j < up; j++){
            if(!res[i][j]) continue;
            for(int k1 = 0; k1 < nway; k1++){
                int mask = 0;
                for(int k2 = k1; k2 < nway; k2++){
                    mask |= 1 << k2;
                    res[i+1][j|mask] += res[i][j];
                }
            }
        }
    return res[nprog][up-1];
}

int main(int argc, char** argv){
    cout << "Usage : command nprog nway" << endl;
    int nprog = atoi(argv[1]);
    int nway = atoi(argv[2]);
    
    cout << DP(nprog, nway) << endl;
//    cout << verify(nprog, nway) << endl;
    return 0;
}
