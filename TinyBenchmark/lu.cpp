#include <iostream>
#include <stdint.h>
#include <random>

using namespace std;

double** getMatrix(int nr){
    double **rt = new double*[nr];
    for(int i = 0; i < nr; i++)
        rt[i] = new double[nr];
    return rt;
}

void initMatrix(double **a, int nra){
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<> dis(1, 1000000);
    for(int i = 0; i < nra; i++)
        for(int j = 0; j < nra; j++)
            a[i][j] = dis(gen);
}

void LUDecompose(double **a, int nra, int* &P){
    if(P == NULL)
        P = new int[nra];
    const double eps = 1e-7;
    for(int i = 0; i < nra; i++)
        P[i] = i;
    for(int i = 0; i < nra; i++){
        for(int j = i; j < nra; j++){
            if(fabs(a[P[j]][i]) > eps)
                swap(P[j], P[i]);
        }
        if(fabs(a[P[i]][i]) < eps){
            cerr << "matrix degerneation" << endl;
            return;
        }
        for(int j = i+1; j < nra; j++){
            a[P[j]][i] /= a[P[i]][i];
            double *pa = a[P[j]];
            for(int k = i+1; k < nra; k++){
                pa[k] -= pa[i] * a[P[i]][k];
            }
        }
    }
}

int main(int argc, char **argv){
    int nra;
    int itr = 10;
    nra = atoi(argv[1]);
    itr = atoi(argv[2]);

    double **a = getMatrix(nra);
    initMatrix(a, nra);
    int *P = NULL;
    for(int i = 0; i < itr; i++)
        LUDecompose(a, nra, P);

    cout << nra*(uint64_t)nra*nra*itr << "\t" << a[nra-1][nra-1] << endl;
    return 0;
}
