#ifndef __SearchTransientMask_H
#define __SearchTransientMask_H

#include <ga/GASimpleGA.h>
#include <ga/std_stream.h>
#include <ga/GA1DBinStrGenome.h>

#include "DPTransientMask.h"



//enumerate all options
double MinMissRatioSamll(double h, double l, int* ret = NULL){

  int64_t num = 1ll << fpds.size();
  int nf = fpds.size();
  if(ret == NULL)
    ret = new int[nf];

  int64_t sol;
  double mmr = 1;

  for(int64_t i = 0; i < num; i++){
    for(int j = 0; j < nf; j++){
      ret[j] = (i & (1ll<<j)) ? 1 : 0;
    }
    double mr = GetMR(h, l, ret);
    if(mr < mmr){
      mmr = mr;
      sol = i;
    }
  }

  if(ret){
    for(int i = 0; i < fpds.size(); i++)
        ret[i] = (sol & (1<<i)) ? 1 : 0;
  };
  return mmr;
}

static double h;
static double l;
float Objective(GAGenome &g){
  GA1DBinaryStringGenome & genome = (GA1DBinaryStringGenome &)g;
  int ret[fpds.size()];
  for(int i = 0; i < genome.length(); i++){
    ret[i] = genome.gene(i);
  }
  double mr = GetMR(h, l, ret);
  return 1-mr;
}

double GeneticAlgorithm(double _h, double _l, int* ret = NULL){
  GA1DBinaryStringGenome genome(fpds.size(), Objective);
  h = _h;
  l = _l;

  GASimpleGA ga(genome);
  ga.populationSize(32);
  ga.nGenerations(512);
  ga.pMutation(0.001);
  ga.pCrossover(0.9);
  ga.evolve();
  const GA1DBinaryStringGenome &g = (const GA1DBinaryStringGenome&)(ga.statistics().bestIndividual());
  for(int i = 0; i < g.length(); i++)
    ret[i] = g.gene(i);
  //cout << "The GA found:\n" << g << endl;
  //cout << "Best Population : " << ga.statistics().bestPopulation() << endl;
  //cout << "Minimal Miss Ratio : " << 1-ga.statistics().maxEver() << endl;
  return 1-ga.statistics().maxEver();
}

#endif
