#pragma once

#include <execinfo.h>

void BackTrace(){
    int nptrs;
    void *buffer[1024];
    char **strings;

    nptrs = backtrace(buffer, 1024);
    cout << "backtrace() return " << nptrs << " addresses" << endl;
    strings = backtrace_symbols(buffer, nptrs);
    for(int i = 0; i < nptrs; i++)
        cout << strings[i] << endl;
    free(strings);
}
