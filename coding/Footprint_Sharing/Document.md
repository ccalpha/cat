##Document

---
####  LRUStack.h  ... Files

  1.  ***LRUStack*** : general LRU stack, implemented by list and map.
    * **MoveToHead(p)** : Move p-th in the link to the head of stack
    * **LRUStack(size)** : Specify the maximal capacity of stack
    * **int push(int data)** : push a data into stack, return information of this push : LRUAccess::DataHit, LRUAccess::DataFill or the evictee
    * **GetHead()** : get LRU data
    * **GetTail()** : get MRU data
    * **bool IsInStack(int datum)** : if a data is in the stack
    * **IsFull()** : if the stack is full
---

####  CacheSimulator.h : Used for validation. Simulate two kinds of cache architecture and three kinds of trace interleaving.

  1.  ***CacheStack*** : Virtual class of LRUCache and PSCache(split LRU)
  2.  ***LRUCache*** : Simulator of full-associative LRU cache
    * **LRUCache(int _size)** : Specify the capacity of cache size
    * **int push(int datum)** : the same as **LRUStack::push**
  3.  ***PSCache*** : Simulator of partial shared cache(split LRU cache)
    * **PSCache(int _ss, int _ds)** : Specify the capacity of shared and dedicated parts
    * **int push(int datum, int IsDedicate)** : specify the push to stacks, if IsDedicate=True, push to dedicated stack, otherwise to shared stack. Return value is the same as **LRUStack::push**
  4.  **double RandomInterleave(CacheStack *CS, Trace *T[], int N, int *pushTo = NULL)** : return miss ratio. *N* traces sotred in *T*, are going to be pushed into cache *CS*, *pushTo[i] = 1* means i-th trace will be pushed into dedicated parts(split LRU), otherwise shared parts.
  The traces are arbitrary interleaved.
  5.  **RoundRobinInterleave** : same as above, but Round-Robin interleaved.  
  6.  **ContinuousInterleave** : same as above, but traces are concatenated as a longer trace, in asceding order.
---

#### TraceGenerator.h : Generate viraus of traces

  1.  **Trace* RandomTrace(int n, int m)** : Generate random trace with *n* accesses and *m* distinct data.
  2.  **Trace* StreamTrace(int n, int m)** : Generate stream trace, with *n* accesses and *m* distinct data.
  3.  **OutputFPDist(FPDist *F)** : display footprint distribution *F*
  4.  **OutputTrace(Trace *T)** : display trace *T*

---

#### footprint.h : definition of trace, footprint distribution and three different algorithm of different footprints.

  1.  ***FPDist*** : statistic of average footprint with different windows. Not all windows are recorded due to memory capacity, only part windows are selected
    * *W* : lengths of selected windows
    * *FP* : average footprint of related indows
    * *N* : number of total windows
    * *M* : number of selected windows
    * **FPDist(int _N)** : initialize with amount of window
    * **GetW** : select windows which to be recorded
    * **GetMissRatio(int c)** : return miss ratio of cache with capacity *c*
    * **FPDist operator+(const FPDist& lhs)** : return accumulated *FPDist* of two *FPDist*
  2.  ***Trace*** : Definition of trace
    * *A* : the accesses of the trace
    * *N* : the length of trace
    * **Trace(int _N)** : initialize trace with length *N*
    * **getM** : return amount of distinct accesses
    * **getN** : return *N*
    * **GetDistincAccess** : return a set of distinct access
  3.  **int\* GetNext(Trace *T, direction d)** :  Get forward or backward index of the  data
  4.  **GetFootprint** : get conventional footprint
  5.  **GetSFootprint** : get sfp, chencheng's implementation of lfp, O(N<sup>2</sup>), given a windows, remove the data which appear in H most recent succeed data outside of the window
  6.  **GetMissRateAR(FPDist\* d[], int N, int csize, int \*AR = NULL, int \*epos=NULL)** : Useless?
  7.  **GetLFootprint** : get lfp, hao's algorithm
---

#### footprint.cpp

  1.  **GetMW**
  2.  **GetRW**
  3.  **GetOV**
  4.  **GetLFootprint_Naive** : A naive implementation of **GetLFootprint**, O(N<sup>2</sup>), for validation
  5.  **VerifyLemma3** : for validation
  6.  **VerfiyHaoAlgo** : for validation, call **GetLFootprint_Naive** and **GetLFootprint**
---

###Example

Initialize traces:
```c++
Trace **T = new Trace*[2];
T[1] = RandomTrace(n, m);
T[0] = StreamTrace(n, m);
```

Setup objective of traces, impact both H and L or only L:
```c++
int *TraceType = new int[2];
TraceType[0] = 0;   // only L
TraceType[1] = 1;   // H and L
```

Simulation of the traces, *rt* is the miss ratio:
```c++
CacheStack *PSLRU = new PSCache(ssize, dsize);
double rt = RandomInterleave(PSLRU, T, 2, TraceType);
```

Get footprint of the traces:
```c++
FPDist **d = new FPDist*[2];
d[0] = GetFootprint(T[0]);
d[1] = GetSFootprint(T[1], ssize, dsize);
```

Figure out miss ratio by footprint:
```c++
int *AR = new int[2];
AR[0] = T[0]->N;
AR[1] = T[1]->N;
int *pos = new int[2];
// overall miss ratio
double rt = GetMissRateAR(d, 2, ssize, AR, pos);
```
