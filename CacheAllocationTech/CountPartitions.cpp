#include <iostream>
#include <cstdlib>

using namespace std;

size_t nChoosek(size_t n, size_t k){
    if(k > n) return 0;
    if(k*2 >n) k = n-k;
    if(k == 0) return 1;

    size_t rt = n;
    for(size_t i = 2; i <= k; i++){
        rt *= (n-i+1);
        rt /= i;
    }
    return rt;
}

int main(int argc, char** argv){
    cout << "Usage : command cachesize programs" << endl;
    size_t c = atoi(argv[1]);
    size_t prog = atoi(argv[2]);
    cout << "Cache size: " << c << "\t#Programs: " << prog << endl;
    cout << "#Partitions: " << nChoosek(c+prog-1,prog-1) << endl;

    return 0;
}
