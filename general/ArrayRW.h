#ifndef __ArraySequence_h
#define __ArraySequence_h

#include <fstream>
#include <stdint.h>
#include <string>
#include <iostream>

using namespace std;

enum MemOper {MALLOC=0, RETURN, FREE, REALLOC, CALLOC};

static const int ntype = 5;
static const string types[ntype] = {"malloc", "return", "free", "realloc", "calloc"};
static const int npar[ntype] = {2, 1, 1, 3, 3};

class ArrayReader{
    private:
        fstream fin;
    public:
        uint64_t info[8];    
        ArrayReader(string filename);
        bool GetNext();
        ~ArrayReader();
};

ArrayReader::~ArrayReader(){
    fin.close();
}

ArrayReader::ArrayReader(string filename){
    fin.open(filename.c_str());
    assert(fin.good() && "Open File Error");
}

bool ArrayReader::GetNext(){
    string s;
    if(!(fin >> s)) return false;
    for(info[0] = 0; ; info[0]++){
        if(types[info[0]] == s)
            break;
    }
    for(int i = 1; i <= npar[info[0]]; i++)
        fin >> info[i];
    return true;
}

class ArrayWriter{
private:
    ofstream fout;
public:
    ArrayWriter(string filename){
        fout.open(filename.c_str());
        assert(fout.good());
    }

    void Write(MemOper m, uint64_t callee, uint64_t size){
        assert(m == MALLOC);
        fout << types[m] << " " << callee << " " << size << endl;
    }

    void Write(MemOper m, uint64_t callee, uint64_t num, uint64_t size){
        assert(m == CALLOC || m == REALLOC);
        fout << types[m] << " " << callee << " " << num << " " << size << endl;
    }

    void Write(MemOper m, uint64_t addr){
        assert(m == RETURN || m == FREE);
        if(m == RETURN)
            fout << "\t";
        fout << types[m] << " " << addr << endl;
    }

    ~ArrayWriter(){
        fout.flush();
        fout.close();
    }

};
#endif
