#include <iostream>
#include <string>
#include <sstream>
#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters
#include "pin_cache.H"
#include "footprint.h"
#include "array_register.h"

#if defined(TARGET_MAC)
#define MALLOC "_malloc"
#define FREE "_free"
#else
#define MALLOC "malloc"
#define FREE "free"
#endif


KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "FootprintDist", "Output file");
KNOB<UINT64> KnobRefCount(KNOB_MODE_WRITEONCE, "pintool", "n", "1000000000000", "Reference count of Program");
vector<FPDist*> fpds;
ArrayRegister ary_reg;
UINT64 refCount = 0;
const UINT32 floor2CacheLine = 7;


LOCALFUN VOID Fini(int code, VOID * v)
{
  for(unsigned int i = 0; i < fpds.size(); i++){
    fpds[i]->FinishCalculation(refCount);
    std::stringstream ss;
    ss << KnobOutput.Value() << "." << i;
    fpds[i]->OutputToFile(ss.str());
  }
}


LOCALFUN VOID MemRefMulti(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
  refCount++;
  int id = ary_reg.GetID(addr);
  if(id == (int)fpds.size()){
    fpds.push_back(new FPDist(KnobRefCount.Value()));
  }
  fpds[id]->Access(addr >> floor2CacheLine, refCount);
}

LOCALFUN VOID MemRefSingle(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
  refCount++;
  int id = ary_reg.GetID(addr);
  if(id+1 == (int)fpds.size()){
    fpds.push_back(new FPDist(KnobRefCount.Value()));
  }
  fpds[id]->Access(addr >> floor2CacheLine, refCount);
}

LOCALFUN VOID Instruction(INS ins, VOID *v)
{
    if (INS_IsMemoryRead(ins) && INS_IsStandardMemop(ins))
    {
        const UINT32 size = INS_MemoryReadSize(ins);
        const AFUNPTR countFun = (size <= 4 ? (AFUNPTR) MemRefSingle : (AFUNPTR) MemRefMulti);

        // only predicated-on memory instructions access D-cache
        INS_InsertPredicatedCall(
            ins, IPOINT_BEFORE, countFun,
            IARG_MEMORYREAD_EA,
            IARG_MEMORYREAD_SIZE,
            IARG_UINT32, CACHE_BASE::ACCESS_TYPE_LOAD,
            IARG_END);
    }

    if (INS_IsMemoryWrite(ins) && INS_IsStandardMemop(ins))
    {
        const UINT32 size = INS_MemoryWriteSize(ins);
        const AFUNPTR countFun = (size <= 4 ? (AFUNPTR) MemRefSingle : (AFUNPTR) MemRefMulti);

        // only predicated-on memory instructions access D-cache
        INS_InsertPredicatedCall(
            ins, IPOINT_BEFORE, countFun,
            IARG_MEMORYWRITE_EA,
            IARG_MEMORYWRITE_SIZE,
            IARG_UINT32, CACHE_BASE::ACCESS_TYPE_STORE,
            IARG_END);
    }
}

VOID Arg1Before(CHAR * name, ADDRINT size)
{
  ary_reg.BeforeMalloc(size);
}

VOID MallocAfter(ADDRINT ret)
{
  ary_reg.AfterMalloc(ret);
}

VOID Image(IMG img, VOID *v)
{
    // Instrument the malloc() and free() functions.  Print the input argument
    // of each malloc() or free(), and the return value of malloc().
    //
    //  Find the malloc() function.
    RTN mallocRtn = RTN_FindByName(img, MALLOC);
    if (RTN_Valid(mallocRtn))
    {
        RTN_Open(mallocRtn);

        // Instrument malloc() to print the input argument value and the return value.
        RTN_InsertCall(mallocRtn, IPOINT_BEFORE, (AFUNPTR)Arg1Before,
                       IARG_ADDRINT, MALLOC,
                       IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                       IARG_END);
        RTN_InsertCall(mallocRtn, IPOINT_AFTER, (AFUNPTR)MallocAfter,
                       IARG_FUNCRET_EXITPOINT_VALUE, IARG_END);

        RTN_Close(mallocRtn);
    }

    // Find the free() function.
    RTN freeRtn = RTN_FindByName(img, FREE);
    if (RTN_Valid(freeRtn))
    {
        RTN_Open(freeRtn);
        // Instrument free() to print the input argument value.
        RTN_InsertCall(freeRtn, IPOINT_BEFORE, (AFUNPTR)Arg1Before,
                       IARG_ADDRINT, FREE,
                       IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                       IARG_END);
        RTN_Close(freeRtn);
    }
}

GLOBALFUN int main(int argc, char *argv[])
{
    PIN_InitSymbols();
    PIN_Init(argc, argv);

    IMG_AddInstrumentFunction(Image, 0);
    INS_AddInstrumentFunction(Instruction, 0);
    PIN_AddFiniFunction(Fini, 0);

    // Never returns
    PIN_StartProgram();

    return 0; // make compiler happy
}
