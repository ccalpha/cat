#include "../general/TraceWriter.h"
#include "../general/randomArray.h"
#include "../general/ArrayRW.h"

void GetStreamRandom(string filename, uint64_t ssize, uint64_t rsize, uint64_t itr, uint32_t num_chunk=4){
    TraceWriter tw(filename+".trace", (ssize+rsize)*itr, num_chunk);
    ArrayWriter aw(filename+".array");

    uint64_t addr_s = 0x1000000;
    uint64_t strip = 4;
    
    aw.Write(MemOper::MALLOC, 0, ssize*strip);
    aw.Write(MemOper::RETURN, addr_s);
    aw.Write(MemOper::MALLOC, 0, rsize*strip);
    aw.Write(MemOper::RETURN, addr_s+ssize*strip);

    uint32_t* rarray = InplaceRandomArray(rsize);
    
    for(int i = 0; i < num_chunk; i++)
        for(int j = 0; j < itr; j++){
            uint64_t addr = addr_s;
            for(int k = 0; k < ssize; k++, addr += strip)
                tw.Write(addr, strip);
            for(int k = 0; k < rsize; k++)
                tw.Write(addr+rarray[k]*strip);
        }
}

int main(){
    GetStreamRandom("genstreamrandom", 2<<20, 1<<18, 4, 4);
    return 0;
}
