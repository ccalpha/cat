#!/usr/bin/ruby

traces=File.readlines("names.txt")
traces = ["bzip2","astar","bwaves"]

names = "../Traces_continous/bzip2.trace ../Traces_continous/astar.trace ../Traces_continous/bwaves.trace"
for i in 18...28
    for j in 18...28
        system("time ./runtest.sh Exclusive_Simple -l2 #{2**i} -l3 #{2**j} -t ./traces")
    end
end

exit

for i in traces
    #puts i
    name = i.strip 
    name = "../Traces_continous/" + name + ".trace"
    if(!File.exist?(name))
        puts name
        next
    end
    system("time ./runtest.sh footprint_simple -t #{name}")
end
