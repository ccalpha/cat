#include <iostream>
#include <cstdint>
#include <cstring>

#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters

#include "pin_cache.H"
//#include "../general/Caches.h"
//#include "../general/CacheConfig.h"
#include <map>
#include <queue>
#include <functional>
#include <vector>
#include <algorithm>
#include <assert.h>

KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "stdout", "Output file");
KNOB<string> KnobFillTimes(KNOB_MODE_WRITEONCE, "pintool", "f", "ftlist", "List file of fill times");

std::ostream *fout;

UINT64 inst_count = 0; //logical time
vector<double> fts;
map<UINT64, UINT64> prev_access;
priority_queue<pair<UINT64, UINT64>, vector<pair<UINT64, UINT64>>, greater<pair<UINT64, UINT64>>> evict[128]; //<prev_access,address>
map<UINT64, bool> dirty[128];

UINT64 *mcount; // misses count
UINT64 *wbcount; // write back count

LOCALFUN VOID Fini(int code, VOID * v)
{
    *fout << "Number of accesses : " << inst_count << "\n";
    *fout << "Read Accesses : \n";
    for(size_t i = 0; i < fts.size(); i++)
        *fout << mcount[i] << " ";
    *fout << "\n";

    *fout << "Write Accesses : \n";
    for(size_t i = 0; i < fts.size(); i++)
        *fout << wbcount[i] << " ";
    *fout << "\n";
    fout->flush();
}

inline void Access(ADDRINT a, bool IsWrite = false){
    inst_count++;
    UINT64 x;
    if(prev_access.find(a) == prev_access.end())
        x = INT_MAX;
    else x = inst_count - prev_access[a];
    for(size_t i = 0; i < fts.size(); i++){
        if(x > fts[i])
            //mcount[i]++; //memory read
            *fout << "R " << (a<<6) << "\n";
    }


    for(size_t i = 0; i < fts.size(); i++){
        if(evict[i].size() != 0){
            while(prev_access[evict[i].top().second] != evict[i].top().first)
                evict[i].pop();
            auto &y = evict[i].top();
            x = inst_count - y.first;
            if(x > fts[i]){
                if(dirty[i][y.second])
                    //wbcount[i]++; //memory write
                    *fout << "W " << (y.second << 6) << "\n";
                dirty[i][y.second] = false;
                evict[i].pop();
            }
        }
        evict[i].push(make_pair(inst_count, a));    
        if(IsWrite)
            dirty[i][a] = true;
    }


    prev_access[a] = inst_count;
}

LOCALFUN VOID MemRefMulti(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    ADDRINT tmp = addr+size;
    while(addr < tmp){
        Access(addr>>6, accessType == CACHE_BASE::ACCESS_TYPE_STORE);
        addr+=64;
    }
}

LOCALFUN VOID MemRefSingle(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    Access(addr>>6, accessType == CACHE_BASE::ACCESS_TYPE_STORE);
}


LOCALFUN VOID Instruction(INS ins, VOID *v)
{
    if ((INS_IsMemoryRead(ins) || INS_IsMemoryWrite(ins)) && INS_IsStandardMemop(ins)){
        const UINT32 size = INS_MemoryReadSize(ins);
        const AFUNPTR countFun = (size <= 4 ? (AFUNPTR) MemRefSingle : (AFUNPTR) MemRefMulti);

        bool flag = INS_IsMemoryRead(ins);
        // only predicated-on memory instructions access D-cache
        INS_InsertPredicatedCall(
                ins, IPOINT_BEFORE, countFun,
                flag?IARG_MEMORYREAD_EA:IARG_MEMORYWRITE_EA,
                flag?IARG_MEMORYREAD_SIZE:IARG_MEMORYWRITE_SIZE,
                IARG_UINT32, flag?CACHE_BASE::ACCESS_TYPE_LOAD:CACHE_BASE::ACCESS_TYPE_STORE,
                IARG_END);
    }
}

void MyInit(){
    if(KnobOutput.Value() == "stdout")
        fout = &(std::cout);
    else
        fout = new ofstream(KnobOutput.Value().c_str());

    ifstream fin(KnobFillTimes.Value().c_str());
    assert(fin.good() && "failed to open list file of fill times");
    double t;
    while(fin >> t)
        fts.push_back(t);
    fin.close();

    mcount = new UINT64[fts.size()];
    fill(mcount, mcount + fts.size(), 0);
    wbcount = new UINT64[fts.size()];
    fill(wbcount, wbcount + fts.size(), 0);
}

GLOBALFUN int main(int argc, char *argv[])
{
    PIN_Init(argc, argv);

    MyInit();
    INS_AddInstrumentFunction(Instruction, 0);
    PIN_AddFiniFunction(Fini, 0);

    // Never returns
    PIN_StartProgram();

    return 0; // make compiler happy
}
