#include <iostream>

#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters

#include "pin_cache.H"
#include "../general/Caches.h"
#include "../general/CacheConfig.h"
#include <set>
#include <boost/timer.hpp>

KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "stdout", "Output file");
KNOB<UINT64> KnobMod(KNOB_MODE_WRITEONCE, "pintool", "m", "100000", "record every m accesses");
KNOB<UINT64> KnobBound(KNOB_MODE_WRITEONCE, "pintool", "b", "1000000000000", "bound");
KNOB<UINT64> KnobWarmup(KNOB_MODE_WRITEONCE, "pintool", "w", "0", "warmup");
std::ostream *fout;

LOCALFUN ITLB::CACHE itlb("ITLB", ITLB::cacheSize, ITLB::lineSize, ITLB::associativity);
LOCALVAR DTLB::CACHE dtlb("DTLB", DTLB::cacheSize, DTLB::lineSize, DTLB::associativity);
LOCALVAR IL1::CACHE il1("L1 Instruction Cache", IL1::cacheSize, IL1::lineSize, IL1::associativity);
LOCALVAR DL1::CACHE dl1("L1 Data Cache", DL1::cacheSize, DL1::lineSize, DL1::associativity);
LOCALVAR LRUCACHE<UL2::allocation> ul2("L2 Unified Cache : LRU", UL2::cacheSize, UL2::lineSize, UL2::associativity);
LOCALVAR LRUFilter<UL3::allocation> ul3("L3 Unified Cache : LRU", UL3::cacheSize, UL3::lineSize, UL3::associativity);

UINT64 ins_count = 0;
UINT64 ref_count = 0;
std::set<ADDRINT> wss_count;
std::set<UINT64> dirty;

UINT64 misses_count = 0;
UINT64 writeback_count = 0;

boost::timer mytimer;
UINT64 bound = 0;
UINT64 warmup = 0;
UINT64 mymod = 100000;
LOCALFUN VOID OutputInfo(){
    *fout << "Instruments : " << ins_count << "\n";
    *fout << "References : " << ref_count << "\n";
    *fout << "Working set size : " << wss_count.size() << "\n";
    *fout << "Misses : " << misses_count << "\n";
    *fout << "Write back : " << writeback_count << "\n";
    fout->flush();
}

LOCALFUN VOID Fini(int code, VOID * v)
{
    *fout << "*****END*****\n";
    OutputInfo();
//    *fout << itlb;
//    *fout << dtlb;
    *fout << dl1;
    *fout << ul2;
    *fout << ul3;
    fout->flush();
}


LOCALFUN VOID Ul2Access(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    // second level unified cache
    const BOOL ul2Hit = ul2.Access(addr, size, accessType);

    // third level unified cache
    if ( ! ul2Hit) {
        ADDRINT victim = 0;
        if(!ul3.AccessSingleLine(addr, accessType, victim)){
            //addr read miss
            //*fout << "R " << addr << endl;
            misses_count++;
            if(dirty.find(victim) != dirty.end()){
                dirty.erase(victim);
                //*fout << "W " << victim << endl;
                writeback_count++;
            }
        }
    }
}



LOCALFUN VOID MemRefSingle(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    wss_count.insert(addr>>6);
    ref_count++;
    
    if(ref_count < warmup)
        return;
    
    //about 2 min per billion
    if(ref_count %  mymod == 0){
        //cout << ref_count << " " << mytimer.elapsed() << endl;
        mytimer.restart();
        *fout << "Ref_count : " << ref_count << endl;
        *fout << "Dirty Blocks : " << dirty.size() << endl;
        OutputInfo();
    }

    if(ref_count == bound)
        PIN_ExitApplication(0);

    if(accessType == CACHE_BASE::ACCESS_TYPE_STORE)
        dirty.insert(addr>>6<<6);
    const BOOL dl1Hit = dl1.AccessSingleLine(addr, accessType);
    if ( ! dl1Hit) Ul2Access(addr, size, accessType);
}

LOCALFUN VOID MemRefMulti(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType){
    ADDRINT up = addr + size; 
    do{
        MemRefSingle(addr, size, accessType);
        addr = (addr >> 6 << 6) + 64;
    }while(addr < up);
}

LOCALFUN VOID Instruction(INS ins, VOID *v)
{

    if (INS_IsMemoryRead(ins) && INS_IsStandardMemop(ins))
    {
        const UINT32 size = INS_MemoryReadSize(ins);
        const AFUNPTR countFun = (size <= 4 ? (AFUNPTR) MemRefSingle : (AFUNPTR) MemRefMulti);

        INS_InsertPredicatedCall(
            ins, IPOINT_BEFORE, countFun,
            IARG_MEMORYREAD_EA,
            IARG_MEMORYREAD_SIZE,
            IARG_UINT32, CACHE_BASE::ACCESS_TYPE_LOAD,
            IARG_END);
    }

    if (INS_IsMemoryWrite(ins) && INS_IsStandardMemop(ins))
    {
        const UINT32 size = INS_MemoryWriteSize(ins);
        const AFUNPTR countFun = (size <= 4 ? (AFUNPTR) MemRefSingle : (AFUNPTR) MemRefMulti);

        INS_InsertPredicatedCall(
            ins, IPOINT_BEFORE, countFun,
            IARG_MEMORYWRITE_EA,
            IARG_MEMORYWRITE_SIZE,
            IARG_UINT32, CACHE_BASE::ACCESS_TYPE_STORE,
            IARG_END);
    }
}

GLOBALFUN int main(int argc, char *argv[])
{
    PIN_Init(argc, argv);
    if(KnobOutput.Value() == "stdout")
      fout = &(std::cout);
    else
      fout = new ofstream(KnobOutput.Value().c_str());

    mymod = KnobMod.Value();
    bound = KnobBound.Value();
    warmup = KnobWarmup.Value();

    INS_AddInstrumentFunction(Instruction, 0);
    PIN_AddFiniFunction(Fini, 0);

    // Never returns
    PIN_StartProgram();

    return 0; // make compiler happy
}
