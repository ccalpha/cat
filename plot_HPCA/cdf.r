library(ggplot2)
require(scales)

mydata<- read.table("./errors.data", header=TRUE)
df <- data.frame(x = c(mydata$VFP, mydata$FP, mydata$Even, mydata$Proportional), g = gl(4,378))
ggplot(df) +
  stat_ecdf(aes(x, color = factor(g, labels=c("VFP", "FP", "Even", "Proportional"))))+
  scale_x_continuous(labels=percent)+
  scale_y_continuous(labels=percent)+
  scale_linetype_manual(values=c("solid", "dotted", "dashed", "dotdash"))+
  theme_gray() +
  theme(legend.justification=c(1,0),
      legend.position=c(1,0),
      text = element_text(size=20))+
  labs(x="Absolute Prediction Error", y="Percentage") +
  guides(color=guide_legend(title = NULL))
