/*
count the number of allocations
discontinous
*/
#include <iostream>
#include <algorithm>

using namespace std;

uint64_t Pow(int a, int x){
    uint64_t ret = 1;
    while(x--)
        ret *= a;
    return ret;
}

uint64_t GCD(uint64_t N, uint64_t M){
    if(N < M) return GCD(M, N);
    return M == 0 ? N : GCD(M, N%M);
}

uint64_t Choose(uint64_t N, uint64_t M){
    uint64_t numerator = 1;
    uint64_t dominator = 1;
    for(int i = M; i; i--){
        numerator *= N;
        dominator *= M;
        uint64_t t = GCD(numerator, dominator);
        numerator /= t;
        dominator /= t;
        --N, --M;
    }
    return numerator;
}

uint64_t CountAllocations(int N, int P){
//do not consider isomorphic, C(N+2^P-1-1, N)*N!
    uint64_t ret = Choose(N+(1<<P)-2, N);
    for(int i = 2; i <= N; i++)
        ret *= i;
    return ret;
}

int main(int argc, char **argv){
    cout << "Usage : command programs ways" << endl;
    int P = atoi(argv[1]);
    int N = atoi(argv[2]);
    uint64_t count = CountAllocations(N, P);
    cout << "Number of Distinct Allocations : " << count << endl;
    return 0;
}
