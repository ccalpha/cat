#include <iostream>

#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters

#include "pin_cache.H"
#include "../../general/footprint_reborn.h"
#include "../../general/footprintManager_reborn.h"


KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "FootprintDist", "Output file");
KNOB<UINT64> KnobLineSize(KNOB_MODE_WRITEONCE, "pintool", "l", "6", "log2 of cache line size, default 6");

FPBuilder fpBuilder;
uint64_t cnt = 0;

LOCALFUN VOID Fini(int code, VOID * v)
{
  FPDist &fp = *fpBuilder.Finalize();
  string file = KnobOutput.Value();
  fp.serialize(file);
}

UINT64 floor2CacheLine = 6;

LOCALFUN VOID MemRefMulti(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    cnt++;
    if(cnt % 1000000000 == 0)
        cout << "Inst processed : " << cnt/1000000000 << endl;
//  fpBuilder.OnReference((uint64_t)(addr>>floor2CacheLine));
}

LOCALFUN VOID MemRefSingle(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    cnt++;
    if(cnt % 1000000000 == 0)
        cout << "Inst processed : " << cnt/1000000000 << endl;
//  fpBuilder.OnReference((uint64_t)(addr>>floor2CacheLine));
}

LOCALFUN VOID Instruction(INS ins, VOID *v)
{
    if (INS_IsMemoryRead(ins) && INS_IsStandardMemop(ins))
    {
        const UINT32 size = INS_MemoryReadSize(ins);
        const AFUNPTR countFun = (size <= 4 ? (AFUNPTR) MemRefSingle : (AFUNPTR) MemRefMulti);

        // only predicated-on memory instructions access D-cache
        INS_InsertPredicatedCall(
            ins, IPOINT_BEFORE, countFun,
            IARG_MEMORYREAD_EA,
            IARG_MEMORYREAD_SIZE,
            IARG_UINT32, CACHE_BASE::ACCESS_TYPE_LOAD,
            IARG_END);
    }
    
    
    if (INS_IsMemoryWrite(ins) && INS_IsStandardMemop(ins))
    {
        const UINT32 size = INS_MemoryWriteSize(ins);
        const AFUNPTR countFun = (size <= 4 ? (AFUNPTR) MemRefSingle : (AFUNPTR) MemRefMulti);

        // only predicated-on memory instructions access D-cache
        INS_InsertPredicatedCall(
            ins, IPOINT_BEFORE, countFun,
            IARG_MEMORYWRITE_EA,
            IARG_MEMORYWRITE_SIZE,
            IARG_UINT32, CACHE_BASE::ACCESS_TYPE_STORE,
            IARG_END);
    }
    
}

GLOBALFUN int main(int argc, char *argv[])
{
    PIN_Init(argc, argv);
    floor2CacheLine = KnobLineSize.Value();

    INS_AddInstrumentFunction(Instruction, 0);
    PIN_AddFiniFunction(Fini, 0);

    // Never returns
    PIN_StartProgram();

    return 0; // make compiler happy
}
