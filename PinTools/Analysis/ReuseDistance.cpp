#include <iostream>

#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters

#include "pin_cache.H"
#include "../../general/footprint.h"
#include "../../general/TraceReader.h"
#include "../../general/ReuseDistance.h"

KNOB<string> KnobTrace(KNOB_MODE_WRITEONCE, "pintool", "t", "trace", "trace file");
KNOB<UINT64> KnobCacheSize(KNOB_MODE_WRITEONCE, "pintool", "c", "16384", "number of cache blocks");
KNOB<UINT64> KnobLineSize(KNOB_MODE_WRITEONCE, "pintool", "l", "6", "log2 of cache line size, default 7");
ReuseDistance *rd;

LOCALFUN VOID Fini(int code, VOID * v)
{
  cout << "Number of cache blocks : " << KnobCacheSize.Value() << endl;
  cout << "Size of cache block : " << KnobLineSize.Value() << endl;
  cout << "MissRatio : " << rd->GetMissRatio() * 100.0 << "%" << endl;
  //fpd->OutputToFile(KnobOutput.Value());
}

UINT64 floor2CacheLine = 6;

LOCALFUN VOID MemRefSingle(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType = CACHE_BASE::ACCESS_TYPE_LOAD)
{
  rd->Access(addr >> floor2CacheLine);
}

GLOBALFUN int main(int argc, char *argv[])
{
    PIN_Init(argc, argv);
    rd = new ReuseDistance(KnobCacheSize.Value());
    floor2CacheLine = KnobLineSize.Value();

    TraceReader trace(KnobTrace.Value());
    for(int i = 0; i < trace.GetNumChunk(); i++){
      cout << "Working on chunk " << i << endl;
      MemRef* refs = trace.GetChunk(i);
      for(uint64_t j = 0; j < trace.GetChunkSize(); j++)
        MemRefSingle(refs[j].address, refs[j].size);
    }

    Fini(0, NULL);

    return 0; // make compiler happy
}
