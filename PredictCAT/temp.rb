#!/usr/bin/ruby

x = File.readlines("opt_5M").to_a
groups = File.open("opt_groups", "w")
allocs = File.open("opt_allcos", "w")
missratio = File.open("opt_missratio", "w")
for i in 0...x.length
    if(i%3==0)
        groups.write(x[i])
    end
    if(i%3==2)
        allocs.write(x[i])
    end
    if(i%3==1)
        missratio.write(x[i])
    end
end

groups.close()
allocs.close()
missratio.close()


