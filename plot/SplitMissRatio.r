#!/usr/bin/env Rscript

library(ggplot2)
library(scales)

data <- read.table("catmissratio.data",header=T)
colnames(data) = c("ID","Name","Ways","Loads","Stores","L3Misses","Prediction")
data$Measured = data$L3Misses/(data$Loads+data$Stores)

mrcurve <- ggplot(data)+
    geom_line(aes(x=1:nrow(data),y=Prediction,color="a",linetype="a", group=Name),size=2)+
    geom_line(aes(x=1:nrow(data),y=Measured,color="b",linetype="b", group=Name),size=2)+
    facet_grid(.~Name,scales="free")+
    scale_color_manual(name=element_blank(), values=c('#3399FF','#FF6600'),labels=c("Prediction","Measurement"))+
    scale_linetype_manual(name=element_blank(), values=c("dashed", "solid"),labels=c("Prediction","Measurement"))+
    xlab(label="Programs and cache allocations(from 1.25MB to 25MB, in 1.25MB step)")+
    ylab(label="Miss Ratios")+
    scale_y_continuous(labels=percent,limits=c(0,0.2))+
    scale_x_continuous(breaks=seq(1,nrow(data),by=20),labels=data$Name[seq(1,nrow(data),by=20)])+
    theme(axis.text.x=element_text(angle=90,hjust=1,vjust=2))+
    theme(text=element_text(size=32))+
    theme(legend.position="top")+
    theme(legend.key.width=unit(1.5,"cm"))
  
    
pdf("Rplots.pdf", 24,8)
mrcurve
dev.off()