#!/usr/bin/ruby

#usage : selection.rb numofcorun time
names = []
File.readlines("names.txt").each{|x| names << x.strip}
groups = File.readlines("groups")
allocs = File.readlines("alloc_share")


ROOT="../Intel_FullTrace"

for i in 0...groups.length
    gid = groups[i].split
    #alloc = allocs[i].strip
    alloc = allocs[0]
    puts "working on co-run group : #{gid.join(" ")}"
    puts "allocation : #{alloc}"
    fps = "";
    for i in gid
        fps += "#{ROOT}/#{names[i.to_i]}.trace\n"
    end

    system("echo \"#{fps}\" > traces")
    system("echo \"#{alloc}\" > alloc")
    system("./runtest.sh CAT -t ./traces -a ./alloc")
end
