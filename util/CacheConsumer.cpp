#include <stdint.h>

#include <boost/program_options.hpp>

#include "../general/randomArray.h"

int main(int argc, char** argv){
    uint64_t size;
    namespace po = boost::program_options;
    
    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "show help messages")
        ("size,s", po::value<uint64_t>(&size)->required(), "array sizes, byte")
    ;
    po::positional_options_description p;
    p.add("size", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    if(vm.count("help")){
        cout << desc << endl;
        return 0;
    }
    po::notify(vm);

    size /= sizeof(uint32_t);
    //uint32_t *ary = InplaceRandomArray(size/sizeof(uint32_t));
    uint32_t *ary = new uint32_t[size];
    for(int i = 0; i < size; i++)
        ary[i] = i+1;
    ary[size-1] = 0;

    uint32_t ptr = ary[0];
    while(1){
        ptr = ary[ptr];
    }
    return 0;
}
