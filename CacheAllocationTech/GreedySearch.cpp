/*
Greedy search for the best next step/steps.
Given an occupancy, try all of the possible allocation of next way/ways, find the one which is legal and optimal. If there is n programs, the complexity is 2^n of each step. If the view is longer, say evaluate x ways at one time, the complexity is 2^xn
*/

#include <iostream>
#include <boost/program_options.hpp>
#include <vector>
#include "CAT.h"
#include "footprint.h"
#include "general.h"
#include "OptimalPartition.h"

using namespace std;

bool IsOptExist(vector<FPDist> &fps, int nways, double c, double thresh=0);
bool IsOccupationExist(vector<double>);
bool AllocationOfOccupation(vector<double>);
bool GreedSearch(vector<FPDist*> &fps, double c, vector<double> ocu, double thresh=0);

//command -w nways -c c -s s program1 program2 ...
int main(int argc, char** argv){
    int nways;
    int steps;
    double c;
    vector<FPDist> fps;
    
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "show help message")
        ("nways,w", po::value<int>(&nways)->default_value(8), "number of ways")
        ("capacity,c", po::value<double>(&c)->default_value(16384), "capacity of each way")
        ("step,s", po::value<int>(&steps)->default_value(1), "number of ways at each step")
        ("footprint,f", po::value<vector<string> >()->required(), "file of footprints")
    ;
    po::positional_options_description p;
    p.add("footprint", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(),vm);
    //a little trick, if help is behind po::notify, fp would be required even -h is given
    if(vm.count("help")){
        cout << desc << endl;
        return 0;
    }
    po::notify(vm);

    
    for(auto i : vm["footprint"].as<vector<string> >()){
        fps.push_back(FPDist(i));
    }
    cout << IsOptExist(fps, nways, c) << endl;
    return 0;
}

bool IsOptExist(vector<FPDist> &fps, int nways, double c, double thresh){
    vector<double> optpar = OptimalPartition(fps, nways*c);
    vector<FPDist*> x;
    for(auto i : fps)
        x.push_back(&i);
    return GreedSearch(x, c, optpar, thresh);
}

bool IsOccupationExist(vector<double> ocu){
    return false;
}

bool AllocationOfOccupation(vector<double> ocu){
    return false;
}

bool GreedSearch(vector<FPDist*> &fps, double c, vector<double> ocu, double thresh){
    if(IsOccupationExist(ocu))
        return AllocationOfOccupation(ocu);
    vector<double> fts;
    for(int i = 0; i < ocu.size(); i++)
        fts.push_back(fps[i]->GetFT(ocu[i]));

    size_t upper = 1;
    upper <<= fps.size();
    for(size_t i = 1; i < upper; i++){
        vector<int> needed;
        for(int j = 0; j < fps.size(); j++){
            if((i>>j)&1)
                needed.push_back(j);
        }
        double lft = 0, rft = 1e20;
        while(rft-lft>1e-3){
            double mlf = (rft+lft)/2;
            for(auto i : fts)
                if(mlf>i) mlf = i;
            double sum = 0;
            for(auto i : needed){
                sum += ocu[i] - fps[i]->GetFP(fts[i]-mlf);
            }
            if(sum < c) lft = mlf;
            else rft = mlf;
        }
        vector<double> nextstep(ocu);
        for(auto i : needed)
            nextstep[i] -= fps[i]->GetFP(fts[i]-rft);
        if(GreedSearch(fps, c, nextstep))
            return true;
    }
    return false;
}
/*
CAT GreedSearch(vector<FPDist> fps, int nways, double c, int steps){
    CAT mycat(c, fps);
    size_t nalloc = 1;
    nalloc <<= fps.size();

    for(int i = 0; i < nways; i++){
        size_t opt_alloc = 1;
        double opt_mcount = 1e20;
        for(size_t j = 1; j < (1<<fps.size()); j++){
            vector<double> ocu = mycat.TryAddWay(j);
            vector<double> mr = mycat.TryAddOccupancy(ocu);
            double mcount = 0;
            for(int k = 0; k < mr.size(); k++)
                mcount += mr[k]*fps[i].access_count;
            if(mcount < opt_mcount){
                opt_alloc = j;
                opt_mcount = mcount;
            }
        }
        mycat.AddWay(opt_alloc);
    }
    auto rt = mycat.GetOccupancies();
    PrintVector(rt, "Final Occupancy : ");
    return mycat;
}
*/
