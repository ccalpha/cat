#include <iostream>
#include "footprint.h"

using namespace std;

int main(){
    const int sz = 1<<20;
    for(int i = 0; i < 10; i++){
        int *a = new int[sz];
        for(int j = 0; j < sz; j+=i+1)
            a[j] = j;
    }
//    return 0;

    int idx = 0;
    while(1){
        FPDist *f = new FPDist("FootprintDist", idx);
        if(f->IsGood()){
            cout << f->W[0] << " " << f->W[f->M-1] << " " << f->FP[0] << " " << f->FP[f->M-1] << endl;
        }
        else break;
        if(idx == 20) break;
        idx++;
    }
    return 0;
}

