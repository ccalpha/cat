#pragma once

#include <iostream>
#include <map>
#include <stdint.h>
#include <set>

using namespace std;

class ReuseDistance{
  uint64_t cachesize;
  uint64_t ref_cnt;
  uint64_t miss_cnt;
  map<uint64_t, uint64_t> prev;
  set<uint64_t> pos;
public:
  ReuseDistance(uint64_t cs);
  void Access(uint64_t addr);
  uint64_t GetRefCount();
  uint64_t GetMissCount();
  double GetMissRatio();
};

ReuseDistance::ReuseDistance(uint64_t cs){
  cachesize = cs;
  ref_cnt = 0;
  miss_cnt = 0;
}

uint64_t ReuseDistance::GetRefCount(){
  return ref_cnt;
}

uint64_t ReuseDistance::GetMissCount(){
  return miss_cnt;
}

double ReuseDistance::GetMissRatio(){
  return GetMissCount() / (double)GetRefCount();
}

void ReuseDistance::Access(uint64_t addr){
  ref_cnt++;
  if(prev.find(addr) == prev.end() || pos.end() - pos.find(prev[addr]) > cachesize)
    miss_cnt++;
  if(prev.find(addr) != prev.end())
    pos.erase(pos.find(prev[addr]));
  prev[addr] = ref_cnt;
  pos.insert(ref_cnt);
}
