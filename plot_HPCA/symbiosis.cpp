//Figure out all of the symbiosis
//
#include <iostream>
#include <fstream>
#include <set>

using namespace std;

class _data{
public:
    int group_id;
    int id[256];
    double value;
    uint64_t mask;
    _data(){
        mask = 0;
    }
}data[1024];

bool IsOverlap(int a, int b, int c){
    set<int> visited;
    for(int i = 0; i < 3; i++){
        visited.insert(data[a].id[i]);
        visited.insert(data[b].id[i]);
        visited.insert(data[c].id[i]);
    }
    return visited.size() != 9;
}

/*
bool SearchSymbiosis(int deep, int id){
    if(deep == 0) return true;
    --deep;
    while(--id >= 0){
        if(SearchSymbiosis(deep, id))
            cout << 
    }
}
*/

int main(){
    int ndata = 0;
    int ssize = 4; // symbiosis size
    ifstream fin("./degregation_4.data");
    while(fin >> data[ndata].id[0]){
        _data &p = data[ndata];
        for(int i= 1; i < ssize; i++)
            fin >> p.id[i];
        for(int i = 0; i < ssize; i++)
            p.mask |= (uint64_t)1 << p.id[i];

        fin >> p.value;
        p.group_id = ndata;
        ndata++;
    }
    for(int i = 0; i < ndata; i++)
        for(int j = i+1; j < ndata; j++){
            if(data[i].mask & data[j].mask) continue;
            cout << data[i].group_id << " " << data[j].group_id << " ";
            cout << data[i].value + data[j].value << endl;
        }

    /*
    for(int i = 0; i < ndata; i++)
        for(int j = i+1; j < ndata; j++)
            for(int k = j+1; k < ndata; k++){
                if(IsOverlap(i,j,k)) continue;
                cout << data[i].group_id << " " << data[j].group_id << " " << data[k].group_id << " ";
                cout << data[i].value + data[j].value + data[k].value << endl;
            }
    */

    return 0;
}
