#include <iostream>
#include <fstream>
#include <string>
#include "TraceReader.h"

using namespace std;

int main(int argc, char **argv){
    string str(argv[1]);
    TraceReader tr(str);
    cout << "Num of Inst : " << tr.GetNumInst() << endl;
    cout << "Num of Chunk : " << tr.GetNumChunk() << endl;
    cout << "Chunk Size : " << tr.GetChunkSize() << endl;
    
    return 0;
    cout << "Begin Outputting Chunk" << endl;
    for(int i = 0; i < tr.GetNumChunk(); i++){
        MemRef *rt = tr.GetChunk(i);
        for(int j = 0; j < tr.GetChunkSize(); j++){
            cout << rt[j].address << "\t" << rt[j].size << endl;
        }
    }
    return 0;
}
