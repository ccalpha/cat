#!/usr/bin/ruby

ROOT = "../PinTools/SelectedFP"
list = File.readlines("flist")
ar = File.readlines("./AccessRate")

for i in 0..list.length-1
    list[i] = list[i].strip
    ar[i] = ar[i].strip
end

for i in 0..list.length-1
    for j in i+1..list.length-1
        for k in j+1..list.length-1
            system("./PredictExclusive -H 32768 -L 131072 #{ROOT}/#{list[i]}.footprint #{ROOT}/#{list[j]}.footprint #{ROOT}/#{list[k]}.footprint") 
        end
    end
end
