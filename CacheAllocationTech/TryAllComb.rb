#!/usr/bin/ruby

FP_HOME = "../PinTools/AMD_Traces_32MChunk"
list = File.readlines("./list")
for i in 0..list.length-1
    list[i] = list[i].strip
end

a = (0..list.length-1).to_a
a = a.combination(6).to_a
for i in a
    str = ""
    for j in i
        str += " #{FP_HOME}/#{list[j]}.footprint"
    end
    system("./ApproximateDP -w 20 -c 12288 #{str} -u 100")
end

