#include <iostream>
#include <string>
#include <algorithm>
#include <numeric>

#include <boost/program_options.hpp>

#include "footprint.h"
#include "OptimalPartition.h"
#include "general.h"

using namespace std;

int main(int argc, char **argv){
    double c;
    double roundto;
    vector<string> names;
    vector<double> ar;

    namespace po = boost::program_options;
    po::options_description desc("Allowed Options");
    desc.add_options()
        ("help,h", "This message")
        ("capacity,c", po::value<double>(&c)->required(), "capacity of cache")
        ("accessrate,a", po::value<vector<double>>()->multitoken(), "access rates")
        ("footprint,f", po::value<vector<string>>()->multitoken(), "footprints")
        ;
    po::positional_options_description p;
    p.add("footprint", -1);
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    if(vm.count("help")){
        cout << desc << endl;
        return 0;
    }
    if(vm.count("accessrate")){
        ar = vm["accessrate"].as<vector<double>>();
        if(ar.size()  != vm["footprint"].as<vector<string>>().size()){
        cout << "#accessrate and #footprint don't match" << endl;
        cout << ar.size() << " " << vm["footprint"].as<vector<string>>().size() << endl;
        return 1;
        }
    }
    po::notify(vm);
    
    vector<FPDist*> fpds;
    for(auto i : vm["footprint"].as<vector<string>>())
        fpds.push_back(new FPDist(i));
    if(ar.size() == 0)
        ar = vector<double>(fpds.size(), 1.0);
    double ft = FTofComposedFP(c, fpds, ar);
    double omr = 0;
    cout << "Individual Missratio:";
    for(int i = 0; i < fpds.size(); i++){
        double tmp = fpds[i]->GetFP(ft*ar[i]+1)-fpds[i]->GetFP(ft*ar[i]);
        cout << " " << tmp;
        omr += tmp*ar[i];
    }
    cout << endl;

    cout << "Individual Occupantion:";
    for(int i = 0; i < fpds.size(); i++){
        cout << " " << fpds[i]->GetFP(ft*ar[i]) << " " << ft*ar[i];
    }
    cout << endl;

    cout << "Overall Missratio: " << omr << endl;

    return 0;
}
