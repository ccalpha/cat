/*
 * This is used to validate other implementation of footprint
  Given specifc size of window, return the count of footprint
 */

#pragma once

#include <iostream>
#include <queue>
#include <map>
#include <stdint.h>

using namespace std;

class ExhustedFPDist{
private:
  map<int64_t, int64_t> prev;
  queue<int64_t> window;
  int64_t access_count;
  int64_t cfp;
  int64_t w;
  int64_t wss;
public:
  ExhustedFPDist(int64_t _w){
    access_count = 0;
    w = _w;
    cfp = 0;
    wss = 0;
  };

  void StartNewChunk(int64_t ref_count = 0){
    while(access_count < ref_count)
      AccessPlaceHolder();
    access_count = 0;
    wss = 0;
    prev.clear();
    while(!window.empty())
      window.pop();
  }

  void AccessPlaceHolder(){
    window.push(0);

    access_count++;
    if(access_count <= w)
      return;

    int64_t x = window.front();
    window.pop();
    if(x && (prev.find(x) == prev.end() || prev[x] <= access_count-w))
      wss--;
    cfp += wss;
  }

  void Access(int64_t addr, int64_t ref_count = 0){
    ref_count--;
    while(access_count < ref_count){
      AccessPlaceHolder();
    }
    access_count++;

    window.push(addr);
    if(access_count <= w){
      if(prev.find(addr) == prev.end())
        wss++, cfp++;
      prev[addr] = access_count;
      return;
    }

    //y
    if(prev.find(addr) == prev.end() || prev[addr] < access_count-w)
      wss++;
    prev[addr] = access_count;

    //x
    int64_t x = window.front();
    window.pop();
    if(prev.find(x) == prev.end() || prev[x] <= access_count-w)
      wss--;
    cfp += wss;
  }

  void FinishCalcuation(int64_t ref_count = 0){
    while(access_count < ref_count)
      AccessPlaceHolder();
  }

  int64_t GetCFP(){
    return cfp;
  }
};
