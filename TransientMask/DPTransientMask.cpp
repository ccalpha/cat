#include <iostream>
#include <algorithm>
#include "DPTransientMask.h"
#include "MassArrays.h"
#include "SearchTransientMask.h"
using namespace std;

double MinMissRatio(double h, double l, int *ret = NULL, double g = 64.0){

  if(ret == NULL) ret = new int[fpds.size()];
  //return Test1(h, l, ret, g);

  double c = h+l;
  if(fpds.size() <= 20)
    return MinMissRatioSamll(h, l, ret);
  else  // n has to be large enough! Say, 100..
    //return MassArrays(h, l, ret, g);
    return GeneticAlgorithm(h, l, ret);
}

int main(int argc, char** argv){
	CommandLineParse(argc, argv);
  GetFPDS(fps.getValue());

  double h, l, cl, asso;
  if(CacheConfig.isSet()){
    if(CacheConfig.getValue() == "Intel"){
      cl = 64;
      asso = 16;
      h = (1 << 20) / cl;
      l = (1 << 20) / cl;
    }
    else if(CacheConfig.getValue() == "Power8"){
      cl = 128;
      asso = 8;
      h = (8<<20) / cl;
      l = (8<<20) / cl;
    }
    h = h / asso * (asso-1);
    l = l / asso;
  }else{
    h = HSize.getValue() / CacheLine.getValue();
    l = LSize.getValue() / CacheLine.getValue();
  }

  int* sol = new int[fpds.size()];
  fill(sol, sol + fpds.size(), 0);
  cout << "Without Transient : " << GetMR(h, l, sol) << endl;
  double mr;
  if(fpds.size() >= 25) mr = MinMissRatio(h, l, sol, 1.0);
  else mr = MinMissRatioSamll(h, l, sol);

  cout << "Best Transient Mask Ever : " << mr << endl;
  cout << "Mask of Arrays : " << endl;
  cout << "\t";
  for(int i = 0; i < fpds.size(); i++)
    cout << sol[i] << " ";
  cout << endl;
  OutputMask(sol);
  return 0;
}
