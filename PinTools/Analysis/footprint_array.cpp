#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters

#include "pin_cache.H"
#include "../../general/footprint.h"
#include "../../general/TraceReader.h"
#include "../general/array_register.h"
#include "../../general/ArrayRW.h"

using namespace std;

KNOB<string> KnobArrays(KNOB_MODE_WRITEONCE, "pintool", "a", "array", "array file");
KNOB<string> KnobTrace(KNOB_MODE_WRITEONCE, "pintool", "t", "trace", "trace file");
KNOB<UINT64> KnobLineSize(KNOB_MODE_WRITEONCE, "pintool", "l", "6", "log2 of cache line size, default 7");
KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "fpdist", "file of output");
vector<FPDist*> fpds;
//ArrayRegister ary_reg;
//MergeEngine ary_reg;
ArrayEngine ary_reg;
UINT64 refCount = 0;
UINT64 floor2CacheLine = 6;
UINT64 mallocCount = 0;
UINT64 chunkCount = 0;

LOCALFUN VOID Fini(UINT64 chunks)
{
    cout << "Number of Arrays : " << ary_reg.GetNumberOfArray() << endl;
    cout << "Number of fpds : " << fpds.size() << endl;
    for(unsigned int i = 0; i < fpds.size(); i++){
        fpds[i]->FinishCalculation(refCount, chunks);
        fpds[i]->OutputToFile(KnobOutput.Value().c_str());
    }
}


LOCALFUN VOID MemRefSingle(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType = CACHE_BASE::ACCESS_TYPE_LOAD)
{
    refCount++;
    int id = ary_reg.GetID(addr);
    while((int)fpds.size() <= id){
        fpds.push_back(new FPDist());
    }
    fpds[id]->Access(addr >> floor2CacheLine, refCount);

}

VOID MallocBefore(ADDRINT callee, ADDRINT size)
{
    ary_reg.BeforeMalloc(size);
}

VOID FreeBefore(ADDRINT ptr){
    ary_reg.FreeArray(ptr);
}

VOID MallocAfter(ADDRINT ret)
{
    ary_reg.AfterMalloc(ret);
}

VOID CallocBefore(ADDRINT callee, ADDRINT size, ADDRINT num){
    ary_reg.BeforeMalloc(size * num);
}

VOID ReallocBefore(ADDRINT callee, ADDRINT ptr, ADDRINT size){
    ary_reg.FreeArray(ptr);
    ary_reg.BeforeMalloc(size);
}

void PrepareArrays(){
    ArrayReader ai(KnobArrays.Value());
    while(ai.GetNext()){
        switch(ai.info[0]){
            case MALLOC: MallocBefore(ai.info[1], ai.info[2]); break;
            case RETURN: MallocAfter(ai.info[1]); break;
            //case FREE: FreeBefore(ai.info[1]); break;
            //case REALLOC: ReallocBefore(ai.info[1], ai.info[2], ai.info[3]); break;
            //case CALLOC: CallocBefore(ai.info[1], ai.info[2], ai.info[3]); break;
        }
    }
}

GLOBALFUN int main(int argc, char *argv[])
{
    PIN_Init(argc, argv);
    PrepareArrays();
    floor2CacheLine = KnobLineSize.Value();

    TraceReader trace(KnobTrace.Value());
    trace.ShowInfo();

    for(uint64_t i = 0; i < fpds.size(); i++)
        fpds[i]->InitCalculation();
    for(int64_t i = 0; i < trace.GetNumChunk(); i++){
    //for(int64_t i = 1; i < 2; i++){
        chunkCount++;
        cout << "Working on chunk " << i << endl;
        for(uint64_t j = 0; j < fpds.size(); j++)
            fpds[j]->StartNewChunk(refCount);
        refCount = 0;
        MemRef* refs = trace.GetChunk(i);
        for(uint64_t j = 0; j < trace.GetChunkSize(); j++)
            MemRefSingle(refs[j].address, refs[j].size);
        delete refs;
    }
    Fini(chunkCount);
    return 0; // make compiler happy
}
