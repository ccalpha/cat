#include "../general/TraceWriter.h"
#include "../general/randomArray.h"

void GetStreamRandom(string filename, uint64_t rsize, uint64_t itr, uint32_t num_chunk=4){
    TraceWriter tw(filename, (rsize)*itr, num_chunk);
    uint32_t* rarray = InplaceRandomArray(rsize);

    for(int i = 0; i < num_chunk; i++)
        for(int j = 0; j < itr; j++){
            uint64_t addr = 0x1000000;
            for(int k = 0; k < rsize; k++)
                tw.Write(addr+rarray[k]*4);
        }
}

int main(){
    GetStreamRandom("genrandom.trace", 2<<20, 4, 4);
    return 0;
}
