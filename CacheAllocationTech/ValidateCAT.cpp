/*
Vlidate prediction of CAT by using specific cases
*/

#include <iostream>

#include "footprint.h"
#include "CAT.h"
#include "general.h"

using namespace std;

void TestCase1(vector<FPDist> fpds){
    //Input Program: bzip2 and lbm
    int nways = 2;
    double c = 1024;
    cout << "Test 1: " << endl;
    cout << "\tnways: " << nways << "\tc: " << c << endl;
    cout << "\tnprogram: " << fpds.size() << endl;
    cout << "\tallocation : 1 3" << endl;
    CAT mycat(c);
    for(auto i : fpds)
        mycat.AddProgram(i);
    mycat.AddWay(1);
    PrintVector(mycat.GetOccupancies(), "Occupancy of 1st way: ");
    
    mycat.AddWay(3);
    PrintVector(mycat.GetOccupancies(), "Occupancy of 2nd way: ");
    //should be 1208.75 839.254
    cout << "Test 1 finish\n" << endl;
}

void TestCase2(vector<FPDist> fpds){
    double c = 1024;
    vector<int> alloc = {1, 2, 3};
    cout << "Test 2: " << endl;
    cout << "\tnways: " << alloc.size() << "\tc: " << c << endl; 
    cout << "\tnprogram: " << fpds.size() << endl;
    cout << "\tallocation : ";
    PrintVector(alloc);
    
    CAT mycat(c);
    for(auto i : fpds)
        mycat.AddProgram(i);
    for(int i = 0; i < alloc.size(); i++){
        mycat.AddWay(alloc[i]);
        cout << "Occupancy of #" << i << " way: ";
        PrintVector(mycat.GetOccupancies());
    }
    //should be 1229.82 1842.18
    cout << "Test 2 finish\n" << endl;
}

void TestCaseBase(vector<FPDist> fpds, vector<int> alloc, int id = -1, double c=1024){
    cout << "Test " << id << ":" << endl;
    cout << "\tnways: " << alloc.size() << "\tc: " << c << endl;
    cout << "\tnprograms: " << fpds.size() << endl;
    cout << "\tallocation: ";
    PrintVector(alloc);

    CAT mycat(c);
    for(auto i : fpds)
        mycat.AddProgram(i);
    for(int i = 0; i < alloc.size(); i++){
        mycat.AddWay(alloc[i]);
        cout << "Occupancy of #" << i << " ways: ";
        PrintVector(mycat.GetOccupancies());
    }
    cout << "Test " << id << " finish\n" << endl;
}

int main(int argc, char** argv){
    vector<FPDist> fpds;
    for(int i = 1; i < argc; i++){
        string name(argv[i]);
        FPDist fp(name);
        fpds.push_back(fp);
    }

    //bzip2, lbm
    TestCaseBase(fpds, {1,3}, 1); //should be 1208.75 839.254
    TestCaseBase(fpds, {1, 2, 3}, 2);  //should be 1229.82 1842.18
    TestCaseBase(fpds, {1, 2, 3, 3}, 3);
    TestCaseBase(fpds, {1, 2, 3, 3, 1}, 4);
    TestCaseBase(fpds, {1, 3, 3}, 5);
    return 0;
}
