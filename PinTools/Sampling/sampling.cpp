#include <pin.H>
#include <ctime>
#include <cstdlib>
#include <map>
#include <iostream>
#include <fstream>

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters
#include "pin_cache.H"

using namespace std;

KNOB<UINT64> KnobChunkSize(KNOB_MODE_WRITEONCE, "pintool", "c", "8388608", "ChunkSize");
KNOB<string> KnobROI(KNOB_MODE_WRITEONCE, "pintool", "roi", "ROIFile", "flags of region of interesting");
KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "SampledTrace", "Output file of trace");
KNOB<string> KnobArray(KNOB_MODE_WRITEONCE, "pintool", "a", "Arrays", "Output file of arrays");
KNOB<double> KnobRate(KNOB_MODE_WRITEONCE, "pintool", "rate", "0.001", "Output file of arrays");
KNOB<UINT64> KnobSize(KNOB_MODE_WRITEONCE, "pintool", "size", "2147483648", "Output file of arrays"); //8G

UINT64 maxSize;
UINT64 chunkSize;
double srate;
map<string, int> rois;
ofstream traceout;
ofstream arrayout;

UINT64 instcount = 0;
BOOL isSampling = 0;
UINT64 nextSampling = 8388608;

UINT64 traceFileSize = 0;
ADDRINT CountDown(){
    instcount++;
    if(isSampling) return 1;
    if(traceFileSize >= maxSize){
        cout << "File Size : " << traceFileSize << " " << maxSize << endl;
        PIN_ExitApplication(0);
        return 0;
    }
    return --nextSampling == 0;
}

UINT64 tmpChunkSize = 1;
LOCALFUN VOID MemRef(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType){
    isSampling = true;
    if(--tmpChunkSize == 0){
        traceFileSize += chunkSize*(sizeof(addr)+sizeof(size));
        tmpChunkSize = chunkSize;
        isSampling = false;
        double x = ((rand() % 10000)+5000)/10000.0; //the interval between chunks is 0.5~1.5 times of chunksize/srate
        nextSampling = chunkSize / srate * x;
        traceout.flush();
        cout << "Start New Chunk: " <<  instcount << " nextSampling:" << nextSampling << endl;
        return;
    }
    traceout.write((char*)&addr, sizeof(addr));
    traceout.write((char*)&size, sizeof(size));
}

LOCALFUN VOID Instruction(INS ins, VOID *v){
    if(INS_IsStandardMemop(ins) && (INS_IsMemoryRead(ins) || INS_IsMemoryWrite(ins))){
        INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)CountDown, IARG_END);
        INS_InsertThenCall(ins, IPOINT_BEFORE, (AFUNPTR)MemRef,
                INS_IsMemoryRead(ins)?IARG_MEMORYREAD_EA : IARG_MEMORYWRITE_EA,
                INS_IsMemoryRead(ins)?IARG_MEMORYREAD_SIZE : IARG_MEMORYWRITE_SIZE,
                IARG_UINT32, CACHE_BASE::ACCESS_TYPE_LOAD,
                IARG_END);
    }
}

#if defined(TARGET_MAC)
#define MALLOC "_malloc"
#define FREE "_free"
#define CALLOC "_calloc"
#define REALLOC "_realloc"
#else
#define MALLOC "malloc"
#define FREE "free"
#define CALLOC "calloc"
#define REALLOC "realloc"
#endif

VOID MallocBefore(ADDRINT callee, ADDRINT size){
    arrayout << MALLOC << "\t" << callee << "\t" << size << endl;
}

VOID FreeBefore(ADDRINT ptr){
    arrayout << FREE << "\t" << ptr << endl;
}

VOID MallocAfter(ADDRINT ret){
    arrayout << "\treturn\t" << ret << endl;
}

VOID CallocBefore(ADDRINT callee, ADDRINT size, ADDRINT num){
    arrayout << CALLOC << "\t" << callee << "\t" << size << "\t" << num << endl;
}

VOID ReallocBefore(ADDRINT callee, ADDRINT ptr, ADDRINT size){
    arrayout << REALLOC << "\t" << callee << "\t" << ptr << "\t" << size << endl;
}

VOID Image(IMG img, VOID *v)
{
    RTN mallocRtn = RTN_FindByName(img, MALLOC);
    if (RTN_Valid(mallocRtn))
    {
        RTN_Open(mallocRtn);
        RTN_InsertCall(mallocRtn, IPOINT_BEFORE, (AFUNPTR)MallocBefore,
                IARG_ADDRINT, RTN_Address(mallocRtn),
                IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                IARG_END);
        RTN_InsertCall(mallocRtn, IPOINT_AFTER, (AFUNPTR)MallocAfter,
                IARG_FUNCRET_EXITPOINT_VALUE, IARG_END);
        RTN_Close(mallocRtn);
    }

    RTN freeRtn = RTN_FindByName(img, FREE);
    if (RTN_Valid(freeRtn)){
        RTN_Open(freeRtn);
        RTN_InsertCall(freeRtn, IPOINT_BEFORE, (AFUNPTR)FreeBefore,
                IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                IARG_END);
        RTN_Close(freeRtn);
    }

    RTN callocRtn = RTN_FindByName(img, CALLOC);
    if(RTN_Valid(callocRtn)){
        RTN_Open(callocRtn);
        RTN_InsertCall(callocRtn, IPOINT_BEFORE, (AFUNPTR)CallocBefore,
                IARG_ADDRINT, RTN_Address(callocRtn),
                IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                IARG_FUNCARG_ENTRYPOINT_VALUE, 1,
                IARG_END);
        RTN_InsertCall(callocRtn, IPOINT_AFTER, (AFUNPTR)MallocAfter,
                IARG_FUNCRET_EXITPOINT_VALUE, IARG_END);
        RTN_Close(callocRtn);
    }

    RTN reallocRtn = RTN_FindByName(img, REALLOC);
    if(RTN_Valid(reallocRtn)){
        RTN_Open(reallocRtn);
        RTN_InsertCall(reallocRtn, IPOINT_BEFORE, (AFUNPTR)ReallocBefore,
                IARG_ADDRINT, RTN_Address(reallocRtn),
                IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                IARG_FUNCARG_ENTRYPOINT_VALUE, 1,
                IARG_END);
        RTN_InsertCall(reallocRtn, IPOINT_AFTER, (AFUNPTR)MallocAfter,
                IARG_FUNCRET_EXITPOINT_VALUE, IARG_END);
        RTN_Close(reallocRtn);
    }
}

LOCALFUN VOID Fini(int code, VOID * v){
    cout << "Inst Count: " << instcount << endl;
    cout << "nextSampling: " << nextSampling << endl;
    traceout.write((char*)&maxSize, sizeof(maxSize));
    traceout.write((char*)&chunkSize, sizeof(chunkSize));
    traceout.write((char*)&srate, sizeof(srate));
    UINT64 x = traceFileSize / (chunkSize*12);
    traceout.write((char*)&x, sizeof(x)); //number of chunk
    traceout.write((char*)&instcount, sizeof(instcount)); //number of Store/Load inst
    cout << sizeof(instcount) << endl;
    traceout.close();
    //arrayout.close();
}

LOCALFUN VOID Init(){
    cout << "BEGIN TO SAMPLE" << endl;
    srand(time(NULL));
    chunkSize = KnobChunkSize.Value();
    maxSize = KnobSize.Value();
    srate = KnobRate.Value();
    traceout.open(KnobOutput.Value().c_str(), ofstream::out);
    //arrayout.open(KnobArray.Value().c_str(), ofstream::out);
    tmpChunkSize = chunkSize;
    ifstream fin(KnobROI.Value().c_str());
    if(fin.good()){
        string a, b;
        while(fin >> a >> b){
            rois[a] = 0;
            rois[b] = 1;
        }
        fin.close();
    }
}

GLOBALFUN int main(int argc, char *argv[])
{
    PIN_InitSymbols();
    PIN_Init(argc, argv);
    Init();
    //IMG_AddInstrumentFunction(Image, 0);
    INS_AddInstrumentFunction(Instruction, 0);
    PIN_AddFiniFunction(Fini, 0);

    PIN_StartProgram();

    return 0; // make compiler happy
}
