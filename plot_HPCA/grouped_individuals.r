library(ggplot2)
library(data.table)
require(scales)

groups <- read.table("./grouped_individuals.data", header=TRUE)
#groups$Actual <- NA
groups_melt <- melt(groups)

Results = factor(groups_melt$variable, labels=c("Actual Miss Ratio", "Absolute Error of VFP", "Absolute Error of HOTL"))
groups_melt$Program <- factor(groups_melt$Program, levels = groups_melt$Program)

groups_plot <- ggplot(groups_melt, aes(x=Program,y=value, color=Results, group=Results, shape=Results)) +
  geom_line() +
  geom_point()+
  #scale_y_continuous(labels=percent, sec.axis=dup_axis(name="Absolute Error")) +
  scale_y_continuous("Miss Ratio", labels=percent, sec.axis = dup_axis(name="Absolute Error"))+
  labs(x="Benchmarks") +
  theme_gray()+
  theme(axis.text.x = element_text(angle = 90,vjust=0.5,hjust=1)) +
  theme(text = element_text(size=16))+
  theme(legend.position="top")+
  theme(legend.title=element_blank())+
  coord_fixed(ratio=80)

groups_plot
