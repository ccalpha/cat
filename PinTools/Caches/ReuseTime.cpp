#include <iostream>
#include <cstdint>
#include <cstring>

#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters

#include "pin_cache.H"
#include "../general/Caches.h"
#include "../general/CacheConfig.h"
#include <map>

KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "stdout", "Output file");

std::ostream *fout;

UINT64 ref_count = 0;
map<ADDRINT, ADDRINT> prev_access;
UINT64 *hist;
const int lhist = 256*128;

inline UINT32 GetID(UINT64 value){
    const UINT32 SUBLOG_BITS = 8;
    UINT32 msb = 63 - __builtin_clzll(value);
    if(msb < SUBLOG_BITS) return value;
    UINT32 shift = msb - SUBLOG_BITS;
    UINT32 index = value >> shift;
    index = (shift << SUBLOG_BITS) + index;
    return index;
}

LOCALFUN VOID Fini(int code, VOID * v)
{
    for(int i = 1; i < lhist; i++)
        hist[i] += hist[i-1];
    
    *fout << ref_count << "\t" << prev_access.size() << "\n";
    UINT64 hcount = 0;
    UINT64 pos = 0;
    for(; pos < 256; pos++)
        *fout << pos << " " << hist[hcount++] << "\n";

    for(UINT64 i = 1; pos <= ref_count ; i<<=1){
        for(UINT64 j = 0; j < 256 && pos <= ref_count; j++, pos += i)
            *fout << pos << " " << hist[hcount++] << "\n";
    }
    fout->flush();
}

inline void  AddData(ADDRINT a){
    if(ref_count % 1000000000 == 0)
        cout << "ref_count: " << ref_count << endl;
    ref_count++;
    UINT64 x = ref_count-prev_access[a];
    prev_access[a] = ref_count;
    x = GetID(x);
    hist[x]++;
}

LOCALFUN VOID MemRefMulti(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    ADDRINT tmp = addr+size;
    while(addr < tmp){
        AddData(addr>>6);
        addr+=64;
    }
}

LOCALFUN VOID MemRefSingle(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    AddData(addr>>6);
}


LOCALFUN VOID Instruction(INS ins, VOID *v)
{
    if ((INS_IsMemoryRead(ins) || INS_IsMemoryWrite(ins)) && INS_IsStandardMemop(ins)){
        const UINT32 size = INS_MemoryReadSize(ins);
        const AFUNPTR countFun = (size <= 4 ? (AFUNPTR) MemRefSingle : (AFUNPTR) MemRefMulti);

        bool flag = INS_IsMemoryRead(ins);
        // only predicated-on memory instructions access D-cache
        INS_InsertPredicatedCall(
                ins, IPOINT_BEFORE, countFun,
                flag?IARG_MEMORYREAD_EA:IARG_MEMORYWRITE_EA,
                flag?IARG_MEMORYREAD_SIZE:IARG_MEMORYWRITE_SIZE,
                IARG_UINT32, CACHE_BASE::ACCESS_TYPE_LOAD,
                IARG_END);
    }
}

GLOBALFUN int main(int argc, char *argv[])
{
    hist = new UINT64[lhist];
    fill(hist, hist+lhist, 0);
    PIN_Init(argc, argv);
    if(KnobOutput.Value() == "stdout")
        fout = &(std::cout);
    else
        fout = new ofstream(KnobOutput.Value().c_str());

    INS_AddInstrumentFunction(Instruction, 0);
    PIN_AddFiniFunction(Fini, 0);

    // Never returns
    PIN_StartProgram();

    return 0; // make compiler happy
}
