#!/usr/bin/ruby
require 'thread'

queue = Queue.new

traces = [
/localdisk/cye/LFPInput/mcf,
/localdisk/cye/LFPInput/sphinx3,
/localdisk/cye/LFPInput/astar,
/localdisk/cye/LFPInput/soplex,
/localdisk/cye/LFPInput/bzip2,
/localdisk/cye/LFPInput/lbm,
/localdisk/cye/LFPInput/tonto,
/localdisk/cye/LFPInput/wrf,
/localdisk/cye/LFPInput/zeusmp,
/localdisk/cye/LFPInput/omnetpp,
/localdisk/cye/LFPInput/povray,
/localdisk/cye/LFPInput/gcc,
/localdisk/cye/LFPInput/cactusADM,
/localdisk/cye/LFPInput/h264ref,
/localdisk/cye/LFPInput/libquantum,
/localdisk/cye/LFPInput/dealII,
/localdisk/cye/LFPInput/namd,
/localdisk/cye/LFPInput/bwaves,
/localdisk/cye/LFPInput/gobmk,
/localdisk/cye/LFPInput/hmmer,
/localdisk/cye/LFPInput/leslie3d,
/localdisk/cye/LFPInput/sjeng,
/localdisk/cye/LFPInput/GemsFDTD,
/localdisk/cye/LFPInput/specrand,
/localdisk/cye/LFPInput/calculix,
/localdisk/cye/LFPInput/millc,
/localdisk/cye/LFPInput/gromacs,
/localdisk/cye/LFPInput/xalancbmk
]

traces.each do |i|
  queue << i
end

threads = []
5.times do |i|
    threads << Thread.new do
      while (e = queue.pop(true) rescue nil)
        system("./LFP 1 1 #{e} >> Precision#{i}")
      end
    end
end

threads.each{|t| t.join}
