#!/usr/bin/ruby
#!/usr/bin/ruby

#usage : selection.rb numofcorun
nco = ARGV[0].to_i

names = []
File.readlines("names.txt").each{|x| names << x.strip}
#mrs = File.readlines("./missrate").map{|x| x.to_f}

candidates = (0..names.size-1).to_a
#candidates = (0..9).to_a
#candidates = [1,2,3,4,5,9,10,15,18,19,22,24]
candidates = [1,2,3,4,9,10,15,18,22,24]
candidates = candidates.combination(nco).to_a
cnt = 0
#candidates = [[3,9,10,22]]
#cnt = 157

ac=File.readlines("AccessRate2")
#ROOT="../PinTools/Intel_FullTrace/"
ROOT="../PinTools/AMD_FullTrace/"

puts "Usage : command Ncorun"
#HOTL Prediction
for i in candidates
    s = i.join(" ")
    cmd = ""
    for j in i
        cmd += " #{ROOT}/#{names[j]}.footprint"
#        system("./ShowFootprint -s #{ROOT}/#{names[j]}.footprint")
    end
    puts "working on co-run group : #{s}"
    puts cnt
    system("./NaturalPartition -c 196608 -f #{cmd} -a #{ac[cnt].strip}")
    cnt=cnt+1
end

exit
#Even Partition
cnt = 0
for i in candidates
    for j in i
       system("./ShowFootprint -c 32768 #{ROOT}/#{names[j]}.footprint")
    end
    cnt=cnt+1
end

