library(ggplot2)
#library(reshape)
library(data.table)
library(plyr)
library(scales)

overhead <- read.table("./overhead.data", header=TRUE)

attach(overhead)
overhead_agg <- aggregate(overhead[, c("Time")], by=list(Method, Decomposition), FUN=sum, na.rm=TRUE)
detach(overhead)

options(percision=0)

overhead_agg$Group.2 <- factor(overhead_agg$Group.2, levels=c("Profile", "Modeling", "bwaves+astar", "bwaves+bzip2", "astar+bzip2"))
overhead_agg$Group.1 <- factor(overhead_agg$Group.1, levels=c("Simulation", "VFP", "Simulation_2", "VFP_2"), labels=c("Simulation(3)", "VFP(3)", "Simulation(2)", "VFP(2)"))

overhead_agg <- ddply(overhead_agg, .(Group.1), transform, pos = cumsum(x) - (0.5 * x))

overhead_agg

ggplot(overhead_agg) +
    aes(x=Group.1,y=overhead_agg$x, fill=Group.2,label=paste0(overhead_agg$x,"s"))+
    geom_bar(stat="identity") +
    geom_text(aes(label=x, y=pos), size=3)+
    labs(x="Workload", y="Estimated Time(s)")+
    theme(aspect.ratio=0.25)+
    theme(text = element_text(size=16))+
    theme(legend.position="top")+
    guides(fill=guide_legend(title = element_blank()))+
    coord_flip()
