#include <iostream>
#include <stdint.h>

#include <boost/program_options.hpp>

#include "footprint.h"
#include "TraceReader.h"

using namespace std;

int main(int argc, char **argv){
	namespace po = boost::program_options;
    
    int log2l = 64;
    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "show help messages")
        ("trace,t", po::value<string>()->required(), "compute footprint of continous sampled trace")
		("output,o", po::value<string>()->required(), "output to files")
        ("cacheline,l", po::value<int>(&log2l), "length of cache line")
    ;
    po::positional_options_description p;
    p.add("trace", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    if(vm.count("help")){
        cout << desc << endl;
        return 0;
    }
    po::notify(vm);

	SimpleTraceReader st(vm["trace"].as<string>());
	FPDist fp;
    for(int i = 0; ; i++){
        if(!log2l){
            log2l = --i; break;
        }
        log2l>>=1;
    }
	uint64_t x = st.GetNumRef();
	for(uint64_t i = 0; i < x; i++){
		fp.Access((st.GetNextRef())>>log2l);
    }
    fp.ShowInfo();
	fp.FinishCalculation();
    fp.ShowInfo();
	fp.OutputToFile(vm["output"].as<string>());
	return 0;
}
