/*
 * Copyright (c) 2007 Mark D. Hill and David A. Wood
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __MEM_RUBY_STRUCTURES_DIPPOLICY_HH__
#define __MEM_RUBY_STRUCTURES_DIPPOLICY_HH__

#include "mem/ruby/structures/AbstractReplacementPolicy.hh"
using namespace std;

class DIPPolicy : public AbstractReplacementPolicy
{
  public:
    DIPPolicy(int64 num_sets, int64 assoc);
    ~DIPPolicy();

    void touch(int64 set, int64 way, Tick time);
    int64 getVictim(int64 set) const;
  private:
    const int64 LRUSets = 64;
    const int64 BIPSets = 128;
    const int eps = 31;
    mutable int64 IsLRU; //>= 0, LRU
    int64 getMRU(int64 set) const;
    int64 getLRU(int64 set) const;
    mutable int lfsr = 0xACE1u;
    int myrand() const;
};

inline
DIPPolicy::DIPPolicy(int64 num_sets, int64 assoc)
    : AbstractReplacementPolicy(num_sets, assoc)
{
  IsLRU = 0;
}

inline
DIPPolicy::~DIPPolicy()
{
}

inline void
DIPPolicy::touch(int64 set, int64 index, Tick time)
{
    assert(index >= 0 && index < m_assoc);
    assert(set >= 0 && set < m_num_sets);

    m_last_ref_ptr[set][index] = time;
}

inline int64 DIPPolicy::getMRU(int64 set) const{
  Tick time, largest_time;
  int64 largest_index;

  largest_index = 0;
  largest_time = m_last_ref_ptr[set][0];

  for (unsigned i = 0; i < m_assoc; i++) {
      time = m_last_ref_ptr[set][i];
      if (time > largest_time) {
          largest_index = i;
          largest_time = time;
      }
  }
  return largest_index;
}

inline int64 DIPPolicy::getLRU(int64 set) const{
  Tick time, smallest_time;
  int64 smallest_index;

  smallest_index = 0;
  smallest_time = m_last_ref_ptr[set][0];

  for (unsigned i = 0; i < m_assoc; i++) {
      time = m_last_ref_ptr[set][i];
      if (time < smallest_time) {
          smallest_index = i;
          smallest_time = time;
      }
  }
  return smallest_index;
}

int inline DIPPolicy::myrand() const{
  int bit = ((lfsr >> 0) ^ (lfsr >> 2) ^ (lfsr >> 3) ^ (lfsr >> 5) ) & 1;
  return lfsr =  (lfsr >> 1) | (bit << 15);
}


inline int64 DIPPolicy::getVictim(int64 set) const{
  if(set < LRUSets){
    IsLRU++;
    return getLRU(set);
  }
  else if(set < BIPSets){
    IsLRU--;
    if(myrand()&eps)
      return getLRU(set);
    else
      return getMRU(set);
  }
  else{
    if(IsLRU >= 0 || (myrand()&eps))
      return getLRU(set);
    else
      return getMRU(set);
  }
}

#endif // __MEM_RUBY_STRUCTURES_DIPPOLICY_HH__
