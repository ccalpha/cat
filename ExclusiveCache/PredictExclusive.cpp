#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <boost/program_options.hpp>

#include "footprint.h"
#include "CAT.h"
#include "general.h"

using namespace std;

vector<double> GetMR(vector<VFPDist> &vfps, size_t H, size_t L, vector<double> &AccessRate){
    if(AccessRate.size() == 0)
        AccessRate = vector<double>(vfps.size(), 1);
    double sum = accumulate(AccessRate.begin(), AccessRate.end(), 0.0);
    for(auto &i : AccessRate)
        i /= sum;

    int counter = 0;
    double l = 0;
    double r = 1e20;
    while(r-l > 1e-5){
        if(++counter >= 1000) //fp is not monotonic
            break;
        double mid = (r+l)/2;
        double c = 0;
        for(int i = 0; i < vfps.size(); i++)
            c += vfps[i].GetVFP(H, mid*AccessRate[i]);
        if(c < L)
            l = mid;
        else r = mid;
    }

    vector<double> rt;
    for(int i = 0; i < vfps.size(); i++)
        rt.push_back(vfps[i].GetVFP(H,l*AccessRate[i]+1)-vfps[i].GetVFP(H,l*AccessRate[i]));
    return rt;
}

//command H L fp1 fp2 ...
int main(int argc, char** argv){
    size_t H,L;

    namespace po = boost::program_options;
    po::options_description desc("Allowed Options");
    desc.add_options()
        ("help,h", "This message")
        ("higher,H", po::value<size_t>(&H)->required(), "Capacity of higher partition")
        ("lower,L", po::value<size_t>(&L)->required(), "Capacity of lower partition")
        ("footprint,f", po::value<vector<string>>(), "footprints") 
        ("accessrate,a", po::value<vector<double>>()->multitoken(), "individual access rates")
    ;
    po::positional_options_description p;
    p.add("footprint", -1);
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    if(vm.count("help")){
        cout << desc << endl;
        return 0;
    }
    po::notify(vm);

    vector<double> ar;
    if(vm.count("accessrate"))
        ar = vm["accessrate"].as<vector<double>>();
    if(ar.size() != 0 && ar.size() != vm["footprint"].as<vector<string>>().size()){
        cerr << "Number of access rates and footprints are not the same" << endl;
        return 1;
    }
    vector<FPDist> fpds;
    for(auto i : vm["footprint"].as<vector<string>>()){
        fpds.push_back(FPDist(i));
    }
    vector<VFPDist> vfpds;
    for(auto i : fpds)
        vfpds.push_back(VFPDist(i));
    vector<double> rt = GetMR(vfpds, H, L, ar);
    double smr = 0;
    for(int i = 0; i < rt.size(); i++)
        smr += rt[i]*ar[i];
    cout << "Overall MR: " << smr << endl;
    vector<FPDist*> pfpds;
    for(int i = 0; i < fpds.size(); i++)
        pfpds.push_back(&fpds[i]);
    smr += ColdMissRatio(pfpds);
    cout << "Cold MR: " << smr << endl;
    PrintVector(rt, "Individual Miss Ratios: ");
    return 0;
}
