#include <iostream>
#include <fstream>
#include <set>
#include <string>
#include <vector>

#include <ga/GASimpleGA.h>
#include <ga/std_stream.h>
#include <ga/GA1DBinStrGenome.h>

using namespace std;

const int nprog = 19;
double abs_error[nprog][nprog];
double rel_error[nprog][nprog];
int minprog = 2;

vector<string> removed = {"bzip2","milc","leslie3d","povray","sjeng","GemsFDTD","libquantum","h264ref","lbm","wrf"};
set<string> IsRemoved(removed.begin(), removed.end());

double Evaluate(vector<int> &id, double x[nprog][nprog]){
    double rt = 0;
    for(int i =0; i < id.size(); i++)
        for(int j = i+1; j < id.size(); j++)
            rt += x[id[i]][id[j]]; 
    return rt;
}

float Objective(GAGenome &g){
  GA1DBinaryStringGenome & genome = (GA1DBinaryStringGenome &)g;
  vector<int> selected;
  for(int i = 0; i < genome.length(); i++){
    if(genome.gene(i) == 1)
      selected.push_back(i);
  }
  if(selected.size() < minprog)
      return 0;
  double value = Evaluate(selected, rel_error);
  value *= 2.0;
  value /= selected.size() * (selected.size()-1);
  return 100-value;
}

vector<int> GeneticAlgorithm(){
  GA1DBinaryStringGenome genome(nprog, Objective);

  GASimpleGA ga(genome);
  ga.populationSize(128);
  ga.nGenerations(1024*256);
  ga.pMutation(0.001);
  ga.pCrossover(0.9);
  ga.evolve();
  const GA1DBinaryStringGenome &g = (const GA1DBinaryStringGenome&)(ga.statistics().bestIndividual());
  cout << "Best Population : " << ga.statistics().bestPopulation() << endl;
  cout << "Minimal Relative Error : " << 100-ga.statistics().maxEver() << endl;
  vector<int> rt;
  for(int i = 0; i < g.length(); i++)
      if(g.gene(i) == 1)
          rt.push_back(i);
  return rt;
}


int main(int argc, char**argv){
    if(argc == 2)
        minprog = atoi(argv[1]);
    cout << "The subset contains at least " << minprog << " programs" << endl;
    ifstream fin("result_error");
    int num = 0;
    string a, b;
    double c, d;
    for(int i = 0; i < nprog; i++)
      for(int j = i+1; j < nprog; j++){
        fin >> a >> b >> abs_error[i][j] >> rel_error[i][j];
        if(IsRemoved.find(a) != IsRemoved.end() || IsRemoved.find(b) != IsRemoved.end())
            continue;
        abs_error[j][i] = abs_error[i][j];
        rel_error[j][i] = rel_error[i][j];
      }
    fin.close();

    vector<int> rt = GeneticAlgorithm();
    cout << rt.size() << endl;

    ifstream fnamein("names.txt");
    string names[nprog];
    for(int i = 0; i < nprog; i++){
        fnamein >> names[i];
        if(IsRemoved.find(names[i]) != IsRemoved.end())
            i--;
    }

    for(int i = 0; i < rt.size(); i++)
        cout << names[rt[i]] << " ";
    cout << endl;

    for(int i = 0; i < rt.size(); i++)
        cout << rt[i] << " ";
    cout << endl;

    double dm = rt.size() * (rt.size()-1)/2.0;
    cout << "Relative Error : " << Evaluate(rt, rel_error)/dm << endl;
    cout << "Abs Error : " << Evaluate(rt, abs_error)/dm << endl;
    return 0;
}
