#include <iostream>
#include <algorithm>
#include <stdint.h>
#include "../general/roi.h"
using namespace std;

int main(int argc, char** argv){
    cout << "Usage : stream size(M) itr" << endl;
    int len = atoi(argv[1]);
    int itr = atoi(argv[2]);
    
    len <<= 20; //data size
    len /= sizeof(int);
    int *A = new int[len];
    for(int i = 0; i < len; i++)
        A[i] = i;
    ROIBegin();
    volatile uint64_t sum = 0;
    for(int j = 0; j < itr; j++)
    for(int i = 0; i < len; i++)
        sum += A[i]++;
    ROIEnd();
    cout << sum << endl;
    
    return 0;
}
