#include <iostream>

#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters

#include "pin_cache.H"
#include "../general/Caches.h"
#include "../general/CacheConfig.h"
#include "../../general/TraceReader.h"
#include "../../general/general.h"

#include "../../general/Interleaving.h"

KNOB<string> KnobTrace(KNOB_MODE_WRITEONCE, "pintool", "t", "traces", "list of traces");
KNOB<string> KnobAR(KNOB_MODE_WRITEONCE, "pintool", "r", "", "list of access rates");
KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "stdout", "Output file");
KNOB<string> KnobAlloc(KNOB_MODE_WRITEONCE, "pintool", "a", "alloc", "allocation of programs");
KNOB<UINT64> KnobCacheSize(KNOB_MODE_WRITEONCE, "pintool", "c", "20971520", "cache size in bytes");
KNOB<UINT64> KnobCacheWay(KNOB_MODE_WRITEONCE, "pintool", "w", "20", "number of cache ways");
std::ostream *fout;

//LOCALFUN vector<ITLB::CACHE> itlbs(128, ITLB::CACHE("ITLB", ITLB::cacheSize, ITLB::lineSize, ITLB::associativity));
//LOCALVAR vector<DTLB::CACHE> dtlbs(128, DTLB::CACHE("DTLB", DTLB::cacheSize, DTLB::lineSize, DTLB::associativity));
//LOCALVAR vector<IL1::CACHE> il1s(128,IL1::CACHE(("L1 Instruction Cache", IL1::cacheSize, IL1::lineSize, IL1::associativity)));
LOCALVAR vector<DL1::CACHE> dl1s(128,DL1::CACHE("L1 Data Cache", DL1::cacheSize, DL1::lineSize, DL1::associativity));
LOCALVAR vector<LRUCACHE<UL2::allocation>> ul2s(128, LRUCACHE<UL2::allocation>("L2 Unified Cache : LRU", UL2::cacheSize, UL2::lineSize, UL2::associativity));
CATCACHE<UL3::allocation> *ul3; //("CAT Cache", UL3::cacheSize, UL3::lineSize, UL3::associativity);
vector<TraceReader*> trl;
vector<double> accressRate;

vector<uint64_t> count_access;
vector<uint64_t> count_misses;

LOCALFUN VOID Init(){
    ifstream fin(KnobAlloc.Value().c_str());
    vector<uint64_t> alloc;

    //read alloc
    uint64_t x;
    while(fin >> x)
        alloc.push_back(x);
    fin.close();


    //inital cache
    ul3 = new CATCACHE<UL3::allocation>("CAT CACHE", KnobCacheSize.Value(), UL3::lineSize, KnobCacheWay.Value(), alloc);

    //read traces
    fin.open(KnobTrace.Value().c_str());
    string tmp;
    while(fin>>tmp){
        trl.push_back(new TraceReader(tmp));
    }
    fin.close();
    //read ar
    if(KnobAR.Value() != ""){
        fin.open(KnobAR.Value().c_str());
        accressRate.clear();
        double x;
        while(fin >> x)
            accressRate.push_back(x);
        fin.close();
    }
    else{
        for(auto i : trl)
            accressRate.push_back(i->GetNumChunk());
    }
    count_access = vector<uint64_t>(trl.size(), 0);
    count_misses = vector<uint64_t>(trl.size(), 0);
}

LOCALFUN VOID Fini(int code, VOID * v)
{
    PrintVector(count_access, "Accesses Count : ");
    PrintVector(count_misses, "Misses Count : ");
    *fout << "Info of L3 Caches\n";
    *fout << *ul3;
}

GLOBALFUN int main(int argc, char *argv[])
{
    PIN_Init(argc, argv);
    if(KnobOutput.Value() == "stdout")
      fout = &(std::cout);
    else
      fout = new ofstream(KnobOutput.Value().c_str());
    Init();

    UniformInterleave ui(trl, accressRate);
    MemRef* p;
    uint64_t inst_count = 0;
    while((p = ui.GetNext()) != NULL){
        inst_count++;
        //if((inst_count>>25<<25)==inst_count)
            //cout << inst_count << endl;
        uint8_t pid = ui.GetNextID();
        count_access[pid]++;
        if(!dl1s[pid].AccessSingleLine(p->address, CACHE_BASE::ACCESS_TYPE_LOAD))
          if(!ul2s[pid].Access(p->address, p->size, CACHE_BASE::ACCESS_TYPE_LOAD))
            if(!ul3->Access(pid, p->address, p->size, CACHE_BASE::ACCESS_TYPE_LOAD))
              count_misses[pid]++;
    }
    Fini(0, NULL);
    return 0;
}
