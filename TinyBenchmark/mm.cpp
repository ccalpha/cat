#include <iostream>
#include <stdint.h>

using namespace std;

void initMatrix(int **a, int nr, int nc){
  while(nr--){
    for(int i = 0; i < nc; i++)
      a[nr][i] = i;
  }
}

int** getMatrix(int nr, int nc){
  int **rt = new int*[nr];
  while(nr--)
    rt[nr] = new int[nc];
  return rt;
}


int** MatrixMul(int **a, int **b, int nra, int nrb, int ncb, int **c = NULL){
  if(c == NULL)
    c = getMatrix(nra, ncb);
  for(int i = 0; i < nra; i++)
    for(int j = 0; j < ncb; j++)
      c[i][j] = 0;

    for(int i = 0; i < nra; i++)
      for(int j = 0; j < nrb; j++)
        for(int k = 0; k < ncb; k++)
          c[i][k] += a[i][j] * b[j][k];
  return c;
}

int main(int argc, char **argv){
  int nra, nrb, ncb;
  int itr = 10;
  nra = atoi(argv[1]);
  nrb = atoi(argv[2]);
  ncb = atoi(argv[3]);
  itr = atoi(argv[4]);

  int **a = getMatrix(nra, nrb);
  int **b = getMatrix(nrb, ncb);
  int **c = getMatrix(nra, ncb);

  initMatrix(a, nra, nrb);
  initMatrix(b, nrb, ncb);
  for(int i = 0; i < nra; i++)
    for(int j = 0; j < ncb; j++)
      c[i][j] = 0;

  uint64_t sum = 0;
  for(int i = 0; i < itr; i++){
    MatrixMul(a, b, nra, nrb, ncb, c);
    sum += c[nra-1][ncb-1];
  }

  cout << nra*nrb*(uint64_t)ncb*itr << "\t" << sum << endl;
  return 0;
}

