#include <iostream>

#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters

#include "pin_cache.H"
#include "../general/Caches.h"
#include "../general/CacheConfig.h"
#include "../../general/TraceReader.h"
#include "../../general/general.h"

#include <vector>
KNOB<string> KnobTrace(KNOB_MODE_WRITEONCE, "pintool", "t", "traces", "list of traces");
KNOB<string> KnobAR(KNOB_MODE_WRITEONCE, "pintool", "a", "", "list of access rates");
KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "stdout", "Output file");
KNOB<UINT64> KnobL2Size(KNOB_MODE_WRITEONCE, "pintool", "l2", "0", "l2 size");
KNOB<UINT64> KnobL3Size(KNOB_MODE_WRITEONCE, "pintool", "l3", "0", "l3 size");
std::ostream *fout;

vector<double> accessRate;
vector<SimpleTraceReader*> trl;
vector<LRUFilter<UL2::allocation>*> ul2s;
//LOCALVAR CATCACHE<UL3::allocation> ul3("L3 Unified Cache : LRU", UL3::cacheSize, UL3::lineSize, UL3::associativity);
CATCACHE<UL3::allocation> *ul3;
vector<UINT64> count_access;
UINT64 L2Size;
UINT64 L3Size;

LOCALFUN VOID InitProcess(){
    ifstream fin(KnobTrace.Value().c_str());
    assert(fin.good() && "List file error");
    string tmp;
    while(fin >> tmp){
        trl.push_back(new SimpleTraceReader(tmp));
        count_access.push_back(0);
    }
    fin.close();
    for(auto i : trl)
        i->ShowInfo();
    
    L2Size = KnobL2Size.Value();
    L2Size = L2Size == 0 ? UL2::cacheSize : L2Size;
    L3Size = KnobL3Size.Value();
    L3Size = L3Size == 0 ? UL3::cacheSize : L3Size;
    cout << "Cache sizes : " << L2Size << " " << L3Size << endl;
    ul3 = new CATCACHE<UL3::allocation>("L3 Cache : LRU", L3Size, UL3::lineSize, UL3::associativity);
    for(uint32_t i = 0; i < trl.size(); i++)
        ul2s.push_back(new LRUFilter<UL2::allocation>("L2 Dedicated Cache : LRU", L2Size, UL2::lineSize, UL2::associativity));
    if(KnobAR.Value() == ""){
        for(uint32_t i = 0; i < trl.size(); i++)
            accessRate.push_back((double)(trl[i]->GetNumRef()));
    }
    else{
        fin.open(KnobAR.Value().c_str());
        double tval;
        while(fin >> tval)
            accessRate.push_back(tval);
        fin.close();
    }
    double sum = accumulate(accessRate.begin(), accessRate.end(), 0.0);
    for(auto &i : accessRate)
        i /= sum;
    PrintVector(accessRate, "Access Rates: ");
}

uint64_t cnt1 = 0;
LOCALFUN VOID Fini(int code, VOID * v)
{
    PrintVector(count_access, "Accesses Count : ");
    *fout << "Info of L2 Caches\n";
    for(uint32_t i = 0; i < trl.size(); i++){
        *fout << "Trace : " << trl[i]->fname << "\n";
        *fout << *ul2s[i];
    }
    *fout << "Info of L3 Caches\n";
    *fout << *ul3;
    *fout << cnt1 << "\n";
}

LOCALFUN VOID Ul2Access(int pid, ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType = CACHE_BASE::ACCESS_TYPE_LOAD)
{
    count_access[pid]++;
    ADDRINT x;
    BOOL l2hit = ul2s[pid]->Access(addr, accessType, x);
    if(!l2hit){
        cnt1 += ul3->RemoveSingleLine(pid, addr, accessType);
        ul3->AccessSingleLine(pid, x, accessType);
    }
}

UINT64 acnt = 1;
LOCALFUN VOID MemRefSingle(int pid, ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType = CACHE_BASE::ACCESS_TYPE_LOAD)
{
    if(acnt++ % 4000000000UL == 0){
        PIN_ExitApplication(0);
        //cout << acnt << " accesses have been processed" << endl;
    }
    Ul2Access(pid, addr, size, accessType);
}


GLOBALFUN int main(int argc, char *argv[])
{
    PIN_Init(argc, argv);
    if(KnobOutput.Value() == "stdout")
      fout = &(std::cout);
    else
      fout = new ofstream(KnobOutput.Value().c_str());
    InitProcess();
    vector<double> rrcounter(trl.size(), 0.0);
    vector<UINT64> refcounter(trl.size(), 0);
    BOOL flag = true;
    while(flag){
      for(uint32_t i = 0; i < trl.size(); i++){
        //if(floor(rrcounter[i]+accessRate[i]) != floor(rrcounter[i])){
          if(refcounter[i]++ == trl[i]->GetNumRef()){
            flag = false;
            break;
          }
          ADDRINT x = trl[i]->GetNextRef();
          MemRefSingle(i, x, 1);
        //}
        //rrcounter[i] += accessRate[i];
      }
    }
    PrintVector(refcounter, "RefCounter : ");
    Fini(0, NULL);
    return 0; // make compiler happy
}
