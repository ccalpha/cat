#include <iostream>

#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters

#include "pin_cache.H"
#include "../general/Caches.h"
#include "../general/CacheConfig.h"
#include "../../general/TraceReader.h"
#include "../../general/general.h"

#include "../../general/Interleaving.h"

KNOB<string> KnobTrace(KNOB_MODE_WRITEONCE, "pintool", "t", "traces", "list of traces");
KNOB<string> KnobAR(KNOB_MODE_WRITEONCE, "pintool", "r", "", "list of access rates");
KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "stdout", "Output file");
KNOB<string> KnobAlloc(KNOB_MODE_WRITEONCE, "pintool", "a", "alloc", "allocation of programs");
KNOB<UINT64> KnobCacheSize(KNOB_MODE_WRITEONCE, "pintool", "c", "20971520", "cache size in bytes");
KNOB<UINT64> KnobCacheWay(KNOB_MODE_WRITEONCE, "pintool", "w", "20", "number of cache ways");
std::ostream *fout;

LOCALVAR vector<LRUCACHE<UL2::allocation>> ul2s(128, LRUCACHE<UL2::allocation>("L2 Unified Cache : LRU", UL2::cacheSize, UL2::lineSize, UL2::associativity));
CATCACHE<UL3::allocation> *ul3; //("CAT Cache", UL3::cacheSize, UL3::lineSize, UL3::associativity);
vector<SimpleTraceReader*> trl;
vector<double> accressRate;

vector<uint64_t> count_access;
vector<uint64_t> count_misses;

LOCALFUN VOID Init(){
    ifstream fin(KnobAlloc.Value().c_str());
    vector<uint64_t> alloc;

    //read alloc
    uint64_t x;
    while(fin >> x)
        alloc.push_back(x);
    fin.close();


    //inital cache
    ul3 = new CATCACHE<UL3::allocation>("CAT CACHE", KnobCacheSize.Value(), UL3::lineSize, KnobCacheWay.Value(), alloc);

    //read traces
    fin.open(KnobTrace.Value().c_str());
    string tmp;
    while(fin>>tmp){
        trl.push_back(new SimpleTraceReader(tmp));
    }
    fin.close();

    //read ar
    if(KnobAR.Value() != ""){
        fin.open(KnobAR.Value().c_str());
        accressRate.clear();
        double x;
        while(fin >> x)
            accressRate.push_back(x);
        fin.close();
    }
    else{
        accressRate = vector<double>(trl.size(), 1);
    }
    double sum = accumulate(accressRate.begin(), accressRate.end(), 0.0);
    for(auto &i : accressRate)
        i /= sum;
    PrintVector(accressRate, "Access Rate:");
    count_access = vector<uint64_t>(trl.size(), 0);
    count_misses = vector<uint64_t>(trl.size(), 0);
}

LOCALFUN VOID Fini(int code, VOID * v)
{
    for(size_t i = 0; i < trl.size(); i++)
        *fout << ul2s[i]; 
    PrintVector(count_access, "Accesses Count : ");
    PrintVector(count_misses, "Misses Count : ");
    *fout << "Info of L3 Caches\n";
    *fout << *ul3;
}

GLOBALFUN int main(int argc, char *argv[])
{
    PIN_Init(argc, argv);
    if(KnobOutput.Value() == "stdout")
        fout = &(std::cout);
    else
        fout = new ofstream(KnobOutput.Value().c_str());
    Init();

    UniformSimpleInterleave ui(accressRate);
    while(1){
        uint32_t pid = ui.GetNext();
        if(count_access[pid]++ >= trl[pid]->GetNumRef())
            break;
        //if(!dl1s[pid].AccessSingleLine(p->address, CACHE_BASE::ACCESS_TYPE_LOAD))
        ADDRINT adr = trl[pid]->GetNextRef();
        if(!ul2s[pid].Access(adr, 1, CACHE_BASE::ACCESS_TYPE_LOAD))
            if(!ul3->Access(pid, adr, 1, CACHE_BASE::ACCESS_TYPE_LOAD))
                count_misses[pid]++;
    }
    Fini(0, NULL);
    return 0;
}
