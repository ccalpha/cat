library(ggplot2)
library(data.table)
library("gridExtra")
library("cowplot")
require(scales)

errors_2<- read.table("./errors_2.data", header=TRUE)
#median(errors_2$VFP)
#median(errors_2$Even)
#median(errors_2$FP)
#quantile(errors_2$VFP)
#sum(errors_2$VFP <= 0.01)
#sum(errors_2$Even <= 0.01)
#sum(errors_2$FP <= 0.01)

errors_2_dat <- data.frame(Error = c(errors_2$VFP, errors_2$FP,errors_2$Even, errors_2$Proportional_MissRatio, errors_2$Proportional_MissRate),
                  Model = rep(c("VFP", "HOTL", "Even", "Proportional(MissRatio)", "Proportional(MissRate)"), each=length(errors_2$VFP)))
errors_2_dat$Model <- factor(errors_2_dat$Model, levels = c("VFP","HOTL","Even","Proportional(MissRatio)", "Proportional(MissRate)"))

errors_2_plot <- ggplot(errors_2_dat) +
  stat_ecdf(aes(x = Error, color = Model, linetype = Model), lwd=0.8) +
  geom_vline(xintercept = median(errors_2$VFP), linetype="longdash") +
  theme_grey() +
  theme(legend.justification=c(0,1), legend.position=c(0.02,0.98)) +
  theme(legend.title=element_blank()) +
  theme(aspect.ratio=0.6)+
  theme(axis.text.y = element_text(colour="black",angle = 45,size=22))+
  theme(axis.text.x = element_text(colour="black",size=22))+
  theme(text = element_text(size=22))+
  #scale_x_log10(labels=scales::percent) +
  scale_x_log10(labels=scales::percent) +
  scale_y_continuous(labels=scales::percent) +
  labs(y="Cumulative Percent",x="errors of 2 benchmark co-run groups")
plot(errors_2_plot)

errors_3<- read.table("./errors_3.data", header=TRUE)
attach(errors_3)
errors_3 <- errors_3[which(ID1 != 19 & ID2 != 19 & ID3 != 19 & ID1 != 22 & ID2 != 22 & ID3 != 22), ]
detach(errors_3)
median(errors_3$VFP)
median(errors_3$Even)
median(errors_3$FP)
#quantile(errors_3$VFP)
sum(errors_3$VFP <= errors_3$Even+0.001)
sum(errors_3$VFP <= errors_3$FP+0.001)
sum(errors_3$VFP <= 0.01)
sum(errors_3$Even <= 0.01)
sum(errors_3$FP <= 0.01)

errors_3_dat <- data.frame(Error = c(errors_3$VFP, errors_3$FP,errors_3$Even, errors_3$Proportional_MissRatio, errors_3$Proportional_MissRate),
                  Model = rep(c("VFP", "HOTL", "Even", "Proportional(MissRatio)", "Proportional(MissRate)"), each=length(errors_3$VFP)))
errors_3_dat$Model <- factor(errors_3_dat$Model, levels = c("VFP","HOTL","Even","Proportional(MissRatio)", "Proportional(MissRate)"))

errors_3_plot <- ggplot(errors_3_dat) +
  stat_ecdf(aes(x = Error, color = Model, linetype = Model), lwd=0.8) +
  geom_vline(xintercept = median(errors_3$VFP), linetype="longdash") +
  theme_grey(20) +
  theme(legend.justification=c(0,1), legend.position=c(0.02,0.98)) +
  theme(legend.title=element_blank()) +
  theme(aspect.ratio=0.6)+
  theme(text = element_text(size=22))+
  theme(axis.text.y = element_text(colour="black",angle = 45,size=22))+
  theme(axis.text.x = element_text(colour="black",size=22))+
  scale_x_log10(labels=scales::percent,breaks=c(0.00001,0.0001,0.001,0.01,0.1)) +
  scale_y_continuous(labels=scales::percent) +
  labs(y='',x="errors of 3 benchmark co-run groups")
#grid.arrange(errors_2_plot, errors_3_plot, ncol=1)
plot(errors_3_plot)

errors_4<- read.table("./errors_4.data", header=TRUE)
attach(errors_4)
#errors_3 <- errors_3[which(ID1 != 19 & ID2 != 19 & ID3 != 19 & ID1 != 22 & ID2 != 22 & ID3 != 22), ]
detach(errors_4)
print("errors 4")
mean(errors_4$VFP)
mean(errors_4$Even)
mean(errors_4$FP)
sum(errors_4$VFP <= errors_4$Even+0.001)
sum(errors_4$VFP <= errors_4$FP+0.001)
sum(errors_4$VFP <= 0.01)
sum(errors_4$Even <= 0.01)
sum(errors_4$FP <= 0.01)

errors_4_dat <- data.frame(Error = c(errors_4$VFP, errors_4$FP,errors_4$Even),
                  Model = rep(c("VFP", "HOTL", "Even"), each=length(errors_4$VFP)))
errors_4_dat$Model <- factor(errors_4_dat$Model, levels = c("VFP","HOTL","Even"))


errors_4_plot <- ggplot(errors_4_dat) +
  stat_ecdf(aes(x = Error, color = Model, linetype = Model), lwd=0.8) +
  geom_vline(xintercept = median(errors_4$VFP), linetype="longdash") +
  theme_grey(20) +
  theme(legend.justification=c(0,1), legend.position=c(0.02,0.98)) +
  theme(legend.title=element_blank()) +
  theme(aspect.ratio=0.6)+
  theme(text = element_text(size=22))+
  theme(axis.text.y = element_text(colour="black",angle = 45,size=22))+
  theme(axis.text.x = element_text(colour="black",size=22))+
  #scale_x_log10(labels=scales::percent) +
  scale_x_log10(labels=scales::percent,breaks=c(0.00001,0.0001,0.001,0.01,0.1)) +
  scale_y_continuous(labels=scales::percent) +
  scale_color_manual(values=c("#F67771", "#A4A22B", "#26BD81"))+
  labs(y='',x="errors of 4 benchmark co-run groups")
plot(errors_4_plot)


#hist(errors_2$VFP_AbsError)
#hist(errors_2$FP_AbsError)
#d<-density(errors_2$VFP_AbsError)
#plot(d)
#d<-density(errors_2$FP_AbsError)
#plot(d)
print("output:")

mean(errors_2$VFP)
mean(errors_2$Even)
mean(errors_2$FP)
mean(errors_3$VFP)
mean(errors_3$Even)
mean(errors_3$FP)
mean(errors_4$VFP)
mean(errors_4$Even)
mean(errors_4$FP)
