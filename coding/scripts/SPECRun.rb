#!/usr/bin/ruby
require 'thread'

queue = Queue.new

dirs = File.read("dirs.txt").split
commands = File.read("commands.txt").split("\n")
inputs = File.read("inputs.txt").split("\n")

gem5 = "/localdisk/cye/gem5/build/X86/gem5.opt "

for i in 0..dirs.length-1
	#Dir.chdir dirs[i]
	x = commands[i].split
	cmd = x[0]
	x.shift
	opts = x.join(" ")
	commands[i] = gem5 + "--outdir=/localdisk/cye/gem5/results/#{i} -r --stdout-file=/localdisk/cye/gem5/results/#{i}/stdout -e --stderr-file=/localdisk/cye/gem5/results/#{i}/stderr /localdisk/cye/gem5/gem5-stable/configs/example/se.py --ruby --l2_size=4MB --mem-size=8GB -c #{cmd} --options=\"#{opts}\""
	if inputs[i].length != 0
		commands[i] += "--input=#{inputs[i]}"
	end
	queue << i
end

threads = []
16.times do |i|
    threads << Thread.new do
      while (e = queue.pop(true) rescue nil)
	pid = spawn("cd #{dirs[e]}; #{commands[e]}")
	Process.wait pid
        #system("#{e}")
	#puts e
      end
    end
end

threads.each{|t| t.join}
