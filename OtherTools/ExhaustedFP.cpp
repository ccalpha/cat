#include <iostream>
#include <iomanip>
#include "ExhaustedFP.h"
#include "../general/TraceReader.h"

using namespace std;
int main(int argc, char** argv){
    cout << "Usage : efp trace cachelines(16384)" << endl;
    string s(argv[1]);
    int cachesize = 16384;
    if(argc == 3)
        cachesize = atoi(argv[2]);

    ExhustedFPDist efp(cachesize);

    TraceReader trace(s);
    trace.ShowInfo();
    for(int i = 0; i < trace.GetNumChunk(); i++){
    //for(int i = 0; i < 2; i++){
        cout << "Working on trace " << i << endl;
        MemRef* refs = trace.GetChunk(i);
        for(uint64_t j = 0; j < trace.GetChunkSize(); j++){
          efp.Access(refs[j].address >> 6);
        }
        efp.StartNewChunk();
    }
    cout << setprecision(10) << efp.GetCFP() << " " << efp.GetCFP()/(double)((trace.GetChunkSize()-cachesize+1)*trace.GetNumChunk()) << endl;
    return 0;
}
