//
//  footprint.cpp
//  Footprint_Sharing
//
//  Created by yechencheng on 5/8/15.
//  Copyright (c) 2015 yechencheng. All rights reserved.
//

#include <iostream>
#include <numeric>
#include <map>
#include <algorithm>
#include "footprint.h"
#include "LRUStack.h"
#include <climits>

using namespace std;

map<Trace*, FPDist*> LFPS;
map<Trace*, FPDist*> HOTLS;

int64_t* Trace::GetBackward(){
    if(bw == NULL)
        bw = GetNext(this, direction::backward);
    return bw;
}

int64_t* Trace::GetForward(){
    if(fd == NULL)
        fd = GetNext(this, direction::forward);
    return fd;
}

//Figure out the next identical accesses
int64_t* GetNext(Trace *T, direction d){
    int64_t *fd = new int64_t[T->getN()];
    int64_t nullValue = 0;
    
    map<int64_t, int64_t> previous;
    
    int64_t pos;
    int64_t step;
    switch (d) {
        case direction::backward:
            pos = 0;
            step = 1;
            nullValue = -1;
            break;
        case direction::forward:
            pos = T->getN() - 1;
            step = -1;
            nullValue = INT_MAX;
            break;
    }
    
    for(int64_t i = 0; i < T->getN(); i++, pos += step){
        if(previous.count(T->A[pos]) == 0){
            fd[pos] = nullValue;
        }
        else{
            fd[pos] = previous[T->A[pos]];
        }
        previous[T->A[pos]] = pos;
    }
    return fd;
}

//Figure out distribution of footprint
FPDist* GetFootprint(Trace *T){
    int64_t n = T->getN();
    //int64_t m = T->getM();
    int64_t *fd = T->GetForward();
    int64_t *bw = T->GetBackward();
    
    FPDist *d = new FPDist(n);
    
    for(int64_t i = 0; i < d->M; i++){
        int64_t w = d->W[i];
        int64_t fp_count = 0;
        for(int64_t j = 0; j < w; j++)
            if(bw[j] == -1)
                fp_count++;
        int64_t cfp_count = (int)fp_count;
        for(int64_t j = w; j < d->N; j++){
            if(fd[j-w] >= j)
                cfp_count--;
            if(bw[j] <= j-w)
                cfp_count++;
            fp_count += cfp_count;
        }
        d->FP[i] = (double)fp_count / (d->N-w+1);
    }
    return d;
}

//SSize = Sharing Size
//DSize = dedicate size
//Sharing model = ____SSize____|___DSize___ -->Tail
FPDist* GetSFootprint(Trace *T, int64_t DSize){
    int64_t n = T->getN();
    int64_t *fd = T->GetForward();
    int64_t *bw = T->GetBackward();
    FPDist *d = new FPDist(T->N);
    
    for(int64_t i = 0; i < d->M; i++){
        int64_t w = d->W[i];
        int64_t dupper = w;
        for(int64_t j = 0; j < DSize && dupper <= T->N; j++){
            if(bw[dupper] >= w)
                j--;
            dupper++;
        }
        if(dupper > T->N){
            d->FP[i] = 0;
            continue;
        }
        int64_t fp_count = 0;
        for(int64_t j = 0; j < d->W[i]; j++){
            if(fd[j] >= dupper)
                fp_count++;
        }
        int64_t cfp_count = (int)fp_count;
        int64_t scount = 1;
        
        
        for(int64_t j = w; j < n; j++){
            if(fd[j] >= dupper)
                cfp_count++;
            if(fd[j-w] >= dupper)
                cfp_count--;

            
            bool introduced = false;
            if(fd[j] >= dupper){
                introduced = true;
                while(dupper < n && bw[dupper] > j)
                    dupper++;
                if(dupper == n){
                    //break;
                }
                else{
                    dupper++;
                }
            }
         
            if(bw[dupper-1] <= j && bw[dupper-1] > j-w && introduced){
                //if(bw[bw[dupper-1]] <= j-w)
                    cfp_count--;
            }

            fp_count += cfp_count;
            scount++;
        }
        d->FP[i] = fp_count / (double)(n-w+1);
    }
    return d;
}

int64_t GetWPos(FPDist *d, int64_t w){
    return upper_bound(d->W, d->W+d->M, w) - d->W;
}

double GetMissRateAR(FPDist** d, int64_t N, int64_t csize, int64_t *AR, double *winds){
    if(AR == NULL){
        AR = new int64_t[N];
        for(int64_t i = 0; i < N; i++)
            AR[i] = d[i]->N;
    }
    if(winds == NULL){
        winds = new double[N];
    }
    
    int64_t sAR = accumulate(AR, AR+N, 0);
    double *NormAR = new double[N];
    for(int64_t i = 0; i < N; i++)
        NormAR[i] = AR[i] / (double)sAR;
    double upper = sAR;
    double lower = 0;
    
    while(upper - lower >= 1e-7){
        double mid = (upper + lower) / 2;
        double sFP = 0;
        for(int64_t i = 0; i < N; i++){
            sFP += (*d[i])[mid*NormAR[i]];
        }
        if(sFP >= csize)
            upper = mid;
        else
            lower = mid;
    }
    
    for(int i = 0; i < N; i++)
        winds[i] = upper * NormAR[i];
    
    double Misses = 0;
    double sN = 0;
    for(int i = 0; i < N; i++){
        Misses += ((*d[i])[winds[i]+1] - (*d[i])[winds[i]]) * d[i]->N;
        sN += d[i]->N;
    }
    return Misses / (double)sN;
}

int64_t* GetMW(Trace *T, int64_t H){
    int64_t n = T->N;
    int64_t *ret = new int64_t[n+1];
    fill(ret, ret+n+1, 0);
    int64_t *fd = T->GetForward();
    int64_t *bw = T->GetBackward();
    
    int64_t wss = 0;
    int64_t left = 0, right = 0;
    while(right < T->N && (wss != H || bw[right] != -1)){
        if(bw[right] == -1)
            wss++;
        right++;
    }
    
    ret[right-left]++;
    
    while(right < n){
        if(bw[right++] >= left)
            continue;
        while(fd[left++] < right);
        while(right < n && bw[right] >= left)
            right++;
        ret[right-left]++;
    }
    return ret;
}

int64_t* GetRW(Trace *T, int64_t H){
    int64_t n = T->N;
    int64_t *ret = new int64_t[n+1];
    fill(ret, ret+n+1, 0);
    
    int64_t *bw = T->GetBackward();
    int64_t *fd = T->GetForward();
    
    LRUStack L(H+1);
    for(int64_t i = 0; i < n; i++){
        int64_t stat = L.push(T->A[i]);
        if(stat != LRUAccess::DataHit)
            continue;
        ret[i-bw[i]-1]++;
        
        /*
        //if it is maximal window
        if(i + 1 < n && bw[i] > 0){
            if(bw[i+1] < bw[i] && fd[bw[i]-1] > i)
                ret[i-bw[i]+1]++;
        }
        else
            ret[i-bw[i]+1]++;
         */
    }
    
    for(int64_t i = 1, wss = 1; i < n && wss <= H; i++){
        if(bw[i] != -1) continue;
        ret[i]++;
        wss++;
    }
    
    for(int64_t i = n-2, wss = 1; i >= 0 && wss <= H; i--){
        if(fd[i] != INT_MAX) continue;
        ret[n-i-1]++;
        wss++;
    }
    
    return ret;
}

int64_t* GetOV(Trace *T, int64_t H){
    int64_t n = T->N;
    int64_t *ret = new int64_t[n+1];
    fill(ret, ret+n+1, 0);
    int64_t *fd = T->GetForward();
    int64_t *bw = T->GetBackward();
    
    int64_t wss = 0;
    int64_t left = 0, right = 0;
    int64_t pright;
    
    while(wss != H && right < T->N){
        if(bw[right] == -1)
            wss++;
        right++;
    }
    pright = right;
    
    while(right < n){
        if(bw[right++] >= left)
            continue;
        while(fd[left++] < right);
        while(right < n && bw[right] >= left)
            right++;
        if(left < pright)
            ret[pright-left]++;
        pright = right;
    }
    return ret;
}

void OutputArray(int64_t *a, int64_t n){
    for(int64_t i = 0; i < n; i++){
        cout << a[i] << " ";
    }
    cout << endl;
}

int64_t* ProceedAccumulate(int64_t *a, int64_t n){
    for(int64_t i = n-1; i >= 0; i--)
        a[i] += a[i+1];
    for(int64_t i = n-1; i >= 0; i--){
        int64_t x = a[i] + a[i+1];
        if((x^a[i]) < 0 && (x^a[i+1]) < 0){
            cout << "ERROR" << endl;
        }
        a[i] += a[i+1];
    }
    return a;
}




FPDist* GetLFootprint(Trace *T,  int64_t H){
    //if(LFPS.find(T) != LFPS.end())
    //    return LFPS[T];
    
    int64_t n = T->N;
    //int64_t *mw0 = GetMW(T, H);
    //int64_t *ov = GetOV(T, H);
    int64_t *mw1 = GetMW(T, H-1);
    int64_t *rw = GetRW(T, H-2);
    
    //mw0 = ProceedAccumulate(mw0, n);
    //ov = ProceedAccumulate(ov, n);
    mw1 = ProceedAccumulate(mw1, n);
    rw = ProceedAccumulate(rw, n);
    
    /*
    cout << "MW0 : " << endl;
    OutputArray(mw0, n+1);
    cout << "OV : " << endl;
    OutputArray(ov, n+1);
    cout << "MW1 : " << endl;
    OutputArray(mw1, n+1);
    cout << "RW : " << endl;
    OutputArray(rw, n+1);
    */
    
    FPDist *_ret = GetHOTLFootprint(T);
    FPDist *ret = new FPDist(*_ret);
    
    //int64_t tw = upper_bound(ret->W, ret->W+ret->M, H) - ret->W;
    //int64_t tw = upper_bound(ret->FP, ret->FP+ret->M, (double)H) - ret->FP;
    int64_t tw = 0;
    
    //int64_t dec = ret->W[tw];
    int64_t dec = 0;
    int64_t i = 0;
    for(; tw < ret->M; i++, tw++){
        int64_t w = ret->W[tw];
        //ret->FP[i] *= n-w+1;
        //ret->FP[i] -= (mw0[w] - ov[w]) * H - mw1[w] - rw[w];
        //ret->FP[i] -= (n - w + 1 - mw0[w] + ov[w]) * H;
        ret->FP[i] = ret->FP[tw] - (H - (mw1[w] + rw[w]) / (double)(n-w+1));
        ret->W[i] = w - dec;
        //ret->FP[i] /= (n-w+1);
    }
    ret->M = i;
    LFPS[T] = ret;
    
    delete mw1;
    delete rw;
    return ret;
}

FPDist* GetLFootprint_Naive(Trace *T, int64_t H){
    int64_t n = T->getN();
    //int64_t m = T->getM();
    int64_t *fd = GetNext(T, direction::forward);
    int64_t *bw = GetNext(T, direction::backward);
    
    FPDist *d = new FPDist(n);
    
    for(int64_t i = 0; i < d->M; i++){
        int64_t w = d->W[i];
        int64_t fp_count = 0;
        for(int64_t j = 0; j < w; j++)
            if(bw[j] == -1)
                fp_count++;
        int64_t cfp_count = (int)fp_count;
        fp_count = max(fp_count - H, (int64_t)0);
        
        for(int64_t j = w; j < d->N; j++){
            if(fd[j-w] >= j || fd[j-w] == -1)
                cfp_count--;
            if(bw[j] <= j-w)
                cfp_count++;
            fp_count += max(cfp_count-H, (int64_t)0);
        }
        d->FP[i] = (double)fp_count / (d->N-w+1);
    }
    return d;
}

FPDist* GetHOTLFootprint(Trace *T){
    if(HOTLS.find(T) != HOTLS.end())
        return HOTLS[T];
    
    int64_t n = T->N;
    FPDist *ret = new FPDist(n);
    int64_t *fd = T->GetForward();
    int64_t *bw = T->GetBackward();
    int64_t *cnt = new int64_t[n+1];
    fill(cnt, cnt+n+1, 0);
    for(int64_t i = 0; i < n; i++){
        if(fd[i] == INT_MAX) continue;
        cnt[fd[i]-i]++;
    }
    int64_t *cnt2 = new int64_t[n+1];
    fill(cnt2, cnt2+n+1, 0);
    
    for(int64_t i = 0; i < n; i++){
        if(bw[i] == -1)
            cnt2[i+1]++;
        if(fd[i] == INT_MAX)
            cnt2[n-i]++;
    }
    
    for(int64_t i = n-1; i >= 0; i--)
        cnt2[i] += cnt2[i+1];
    for(int64_t i = n-1; i >= 0; i--)
        cnt2[i] += cnt2[i+1];
    
    for(int64_t i = n-1; i >= 0; i--)
        cnt[i] += cnt[i+1];
    for(int64_t i = n-1; i >= 0; i--)
        cnt[i] += cnt[i+1];
    
    
    

    for(int64_t i = 0; i < ret->M; i++){
        int64_t w = ret->W[i];
        if(w == n) ret->FP[i] = T->getM();
        else{
            double x = n-w+1;
            ret->FP[i] = T->getM() - cnt2[w+1]/x - cnt[w+1]/x;
        }
    }
    
    HOTLS[T] = ret;
    
    return ret;
}

void AdjustWindowLFootprint(FPDist *fpd, int64_t dsize){
    int64_t cnt = 0;
    /*
    while(cnt < fpd->M && fpd->W[cnt] < dsize)
        cnt++;
    */
    while(cnt < fpd->M && fpd->FP[cnt] <= 1e-7)
        cnt++;
    
    
    dsize = fpd->W[cnt];
    int64_t i = 0;
    while(cnt < fpd->M){
        fpd->W[i] = fpd->W[cnt] - dsize; //Should I plus 1?
        fpd->FP[i] = fpd->FP[cnt];
        i++;
        cnt++;
    }
    fpd->M = i;
}

map<Trace*, FPDist*> CCFPS;
//CC's algorithm of footprint
FPDist* GetCFootprint(Trace *t){
    if(CCFPS.find(t) != CCFPS.end())
        return CCFPS[t];
    
    int64_t n = t->N;
    FPDist *fpd = new FPDist(n);
    int64_t *fw = t->GetForward();
    int64_t *bd = t->GetBackward();
    int64_t *cnt = new int64_t[n+1];
    fill(cnt, cnt+n+1, 0);
    
    //Get sum of rt
    for(int64_t i = 0; i < n; i++){
        if(fw[i] == INT_MAX)
            cnt[n-i]++;
        else
            cnt[fw[i]-i]++;
    }
    
    int64_t sum = n;
    int64_t prev = 0;
    for(int64_t i = 1; i <= n; i++){
        int64_t _prev = cnt[i];
        cnt[i] = cnt[i-1] + sum;
        prev = _prev;
        sum -= prev;
    }
    
    //WSS
    sum = 0;
    int64_t wss = 0;
    for(int64_t i = 0; i < n; i++){
        cnt[i+1] -= sum;
        if(bd[i] == -1)
            wss++;
        sum += wss;
    }
    
    for(int64_t i = 0; i < fpd->M; i++){
        int64_t w = fpd->W[i];
        fpd->FP[i] = cnt[w] / (double)(n-w+1);
    }
    
    delete cnt;
    CCFPS[t] = fpd;
    return fpd;
}


int64_t* GetHWindow(Trace *t, int64_t H){
    int64_t n = t->N;
    int64_t* ret = new int64_t[n];
    map<int64_t, int64_t> pos;
    LRUStack lru(H);
    
    for(int64_t i = n-1; i >= 0; i--){
        int64_t stat = lru.push(t->A[i]);
        if(stat == LRUAccess::DataFill)
            ret[i] = n;
        else if(stat == LRUAccess::DataHit)
            ret[i] = ret[i+1];
        else{
            ret[i] = pos[stat] - 1;
        }
        pos[t->A[i]] = i;
    }
    return ret;
}

int64_t* GetNHWindow(Trace *t, int64_t H, int64_t *hw){
    int64_t n = t->N;
    int64_t *ret = new int64_t[n];
    int64_t *fd = t->GetForward();
    
    for(int64_t i = 0; i < n; i++){
        if(fd[i] == INT_MAX)
            ret[i] = n;
        else{
            int64_t left = i;
            int64_t right = fd[i]-1;
            while(left < right){
                int64_t mid = (left+right)/2;
                if(fd[i] <= hw[mid])
                    right = mid;
                else
                    left = mid+1;
            }
            ret[i] = right;
        }
    }
    
    
    int64_t wss = 0;
    for(int64_t i = n-1; i >= 0 && wss < H; i--){
        if(fd[i] == INT_MAX)
            wss++;
        ret[i] = i;
    }
    return ret;
}

map<Trace*, FPDist*> SFP2S;
FPDist* GetSFootprint2(Trace *t, int64_t H){
    if(SFP2S.find(t) != SFP2S.end())
        return SFP2S[t];
    int64_t n = t->N;
    FPDist *ret = new FPDist(n);

    //h-window
    int64_t *hw = GetHWindow(t, H);
    int64_t *nh = GetNHWindow(t, H, hw);
    
    int64_t *cnt = new int64_t[n+1];
    fill(cnt, cnt+n+1, 0);
    for(int i = 0;i < n; i++)
        cnt[nh[i]-i]++;
    
    int64_t sum = n - cnt[0];
    cnt[0] = 0;
    int64_t prev = 0;
    for(int64_t i = 1; i <= n; i++){
        int64_t _prev = cnt[i];
        cnt[i] = cnt[i-1] + sum;
        prev = _prev;
        sum -= prev;
    }
    
    //WSS
    int64_t *tmp = hw;
    fill(tmp, tmp+n, 0);
    sum = 0;
    int64_t wss = 0;
    for(int64_t i = 0; i < n; i++){
        cnt[i+1] -= sum;
        wss++;
        tmp[nh[i]]--;
        wss += tmp[i];
        sum += wss;
    }
    
    for(int64_t i = 0; i < ret->M; i++){
        int64_t w = ret->W[i];
        ret->FP[i] = cnt[w] / (double)(n-w+1);
    }
    
    SFP2S[t] = ret;
    return ret;
}


map<Trace*, VFPDist*> VFPS;
VFPDist* GetVFootprint(Trace *t){
    if(VFPS.find(t) != VFPS.end())
        return VFPS[t];
    VFPS[t] = new VFPDist(*GetHOTLFootprint(t));
    return VFPS[t];
}

//LEMMA : MW(H,x) = OV(H+1,x) + RW(H,x)
//return : -1 means consist, otherwise the maximal inconsist index
int64_t VerifyLemma3(Trace *T, int64_t H){
    int64_t *mw = GetMW(T, H);
    int64_t *rw = GetRW(T, H);
    int64_t *ov = GetOV(T, H+1);
    for(int64_t i = 0; i < T->N; i++)
        mw[i] -= rw[i];
    for(int64_t i = T->N - 1; i >= 0; i--){
        if(mw[i] != ov[i])
            return i;
    }
    return -1;
}

//Hao Algo : Theorem 2
int64_t VerfiyHaoAlgo(Trace *T, int64_t H){
    FPDist *lfp1 = GetLFootprint_Naive(T, H);
    FPDist *lfp2 = GetLFootprint(T, H);
    for(int64_t i = lfp1->M-1; i >= 0; i--){
        if(lfp1->W[i] != lfp2->W[i])
            return i;
    }
    return -1;
}
