#!/usr/bin/ruby

require 'optparse'
require 'pp'
require 'optparse/time'
require 'ostruct'

options = OpenStruct.new

opt_parser = OptionParser.new do |opts|
    opts.banner = "Usage : command [options]"

    opts.on("-s", "--separator SEP", "the separator") do |s|
      options.sep = s
    end

    opts.on("-c", "--column c", "selected column") do |c|
      options.col = c
    end
end
opt_parser.parse!(ARGV)
options

while line=gets
  puts line.split(options.sep)[options.col.to_i]
end
