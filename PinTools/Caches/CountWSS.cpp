#include <iostream>
#include <cstdint>
#include <cstring>
#include <set>

#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters

#include "pin_cache.H"
#include "../general/Caches.h"
#include "../general/CacheConfig.h"
#include <map>

KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "stdout", "Output file");

std::ostream *fout;

UINT64 ref_count = 0;
set<ADDRINT> wss_count;
const int lhist = 256*128;

LOCALFUN VOID Fini(int code, VOID * v)
{
    fout->flush();
}


void AddData(ADDRINT addr){
    ref_count++;
    if(ref_count % 1000000 == 0){
        *fout << wss_count.size() << "\n";
        wss_count.clear();
    }
    wss_count.insert(addr);
}

LOCALFUN VOID MemRefMulti(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    ADDRINT tmp = addr+size;
    while(addr < tmp){
        AddData(addr>>6);
        addr+=64;
    }
}

LOCALFUN VOID MemRefSingle(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    AddData(addr>>6);
}


LOCALFUN VOID Instruction(INS ins, VOID *v)
{
    if ((INS_IsMemoryRead(ins) || INS_IsMemoryWrite(ins)) && INS_IsStandardMemop(ins)){
        const UINT32 size = INS_MemoryReadSize(ins);
        const AFUNPTR countFun = (size <= 4 ? (AFUNPTR) MemRefSingle : (AFUNPTR) MemRefMulti);

        bool flag = INS_IsMemoryRead(ins);
        // only predicated-on memory instructions access D-cache
        INS_InsertPredicatedCall(
                ins, IPOINT_BEFORE, countFun,
                flag?IARG_MEMORYREAD_EA:IARG_MEMORYWRITE_EA,
                flag?IARG_MEMORYREAD_SIZE:IARG_MEMORYWRITE_SIZE,
                IARG_UINT32, CACHE_BASE::ACCESS_TYPE_LOAD,
                IARG_END);
    }
}

GLOBALFUN int main(int argc, char *argv[])
{
    PIN_Init(argc, argv);
    if(KnobOutput.Value() == "stdout")
        fout = &(std::cout);
    else
        fout = new ofstream(KnobOutput.Value().c_str());

    INS_AddInstrumentFunction(Instruction, 0);
    PIN_AddFiniFunction(Fini, 0);

    // Never returns
    PIN_StartProgram();

    return 0; // make compiler happy
}
