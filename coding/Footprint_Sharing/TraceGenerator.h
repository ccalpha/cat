//
//  TraceGenerator.h
//  Footprint_Sharing
//
//  Created by yechencheng on 5/8/15.
//  Copyright (c) 2015 yechencheng. All rights reserved.
//

#ifndef Footprint_Sharing_TraceGenerator_h
#define Footprint_Sharing_TraceGenerator_h

#include <iostream>
#include <random>
#include <algorithm>
#include "footprint.h"

using namespace std;

static int64_t acc = 0;

Trace* RandomTrace(int64_t n, int64_t m){
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<int64_t> dis(0,m-1);
    
    Trace* retval = new Trace(n);
    for(int64_t i = 0; i < n; i++)
        retval->A[i] = dis(gen) + acc;
    acc += m;
    return retval;
}

Trace* StreamTrace(int64_t n, int64_t m){
    Trace* retval = new Trace(n);
    for(int64_t i = 0; i < n; i++)
        retval->A[i] = (i % m) + acc;
    acc += m;
    return retval;
}

//Test case 1, arguments are used to keep consistent with generators only
//FP and SFP have different results
Trace* TestCase1(int64_t n, int64_t m){
    //ss = 1, ds = 2
    n = 10;
    Trace* retval = new Trace(n);
    
    int64_t A[10] = {4,1,2,2,4,3,4,1,1,0};
    for(int64_t i = 0; i < n; i++){
        retval->A[i] = A[i];
    }
    return retval;
}

//Test case 1, arguments are used to keep consistent with generators only
Trace* TestCase2(int64_t n, int64_t m){
    //ss = 1, ds = 2
    n = 10;
    Trace* retval = new Trace(n);
    
    int64_t A[10] = {1,3,3,2,3,1,1,3,1,1};
    for(int64_t i = 0; i < n; i++){
        retval->A[i] = A[i];
    }
    return retval;
}

//Example in LFP draft
Trace* TestTrace3(){
    string a = "abcaaaaabc";
        Trace* t = new Trace(a.length());
    for(int64_t i = 0; i < a.length(); i++)
        t->A[i] = a[i];
    return t;
}

void OutputFPDist(FPDist *F){
    /*
    for(int64_t i = 0; i < F->M; i++){
        cout << F->W[i] << "\t" << F->FP[i] << endl;
    }
    */
    for(int64_t i = 0; i < F->M; i++){
        cout << F->W[i] << " ";
    }
    cout << endl;
    for(int64_t i = 0; i < F->M; i++){
        cout << F->FP[i] << " ";
    }
    cout << endl;
}

void OutputTrace(Trace *T){
    for(int64_t i = 0; i < T->N; i++){
        cout << T->A[i] << " ";
    }
    cout << endl;
}

#endif
