#include <stdio.h>
#include <malloc.h>
#include <unistd.h>
#include <sys/stat.h>
/*
gcc malloc_hook.c -shared -fPIC -o malloc_hook.so  -w
*/
FILE* pFile;
/* Prototypes for our hooks.  */
static void my_init_hook(void);
static void *my_malloc_hook(size_t, const void *);
static void my_free_hook(void*, const void*);
static void *my_calloc_hook(size_t, const void *);

/* Variables to save original hooks. */
static void *(*old_malloc_hook)(size_t, const void *);
static void (*old_free_hook)(void*, const void*);
static void *(*old_calloc_hook)(size_t, const void *);

/* Override initializing hook from the C library. */
void (*__MALLOC_HOOK_VOLATILE __malloc_initialize_hook) (void) = my_init_hook;

static void my_init_hook(void)
{
  /*
	char fname[256];
	for(int i = 0; ; i++){
		sprintf(fname, "/localdisk/cye/cat/OtherTools/results/%d.malloc", i);
		struct stat st;
		int result = stat(fname, &st);
		if(result==0) continue;
		printf("%s\n", fname);
		pFile = fopen(fname, "w");
		break;
	}
  */
  old_malloc_hook = __malloc_hook;
  __malloc_hook = my_malloc_hook;
  old_free_hook = __free_hook;
  __free_hook = my_free_hook;
  old_calloc_hook = __calloc_hook;
  __calloc_hook = my_calloc_hook;
}

static void *
my_malloc_hook(size_t size, const void *caller)
{
  void *result;

  /* Restore all old hooks */
  __malloc_hook = old_malloc_hook;
__free_hook = old_free_hook;
  /* Call recursively */
  result = malloc(size);

  /* Save underlying hooks */
  old_malloc_hook = __malloc_hook;
old_free_hook = __free_hook;
  /* printf() might call malloc(), so protect it too. */
  fprintf(stdout,"malloc %u %llu %llu\n",(unsigned int) size, result, caller);
  /* Restore our own hooks */
  __malloc_hook = my_malloc_hook;
__free_hook=my_free_hook;
  return result;
}

static void *
my_calloc_hook(size_t size, const void *caller)
{
  void *result;

  /* Restore all old hooks */
  __calloc_hook = old_calloc_hook;
  __free_hook = old_free_hook;
  /* Call recursively */
  result = calloc(size);

  /* Save underlying hooks */
  old_calloc_hook = __calloc_hook;
  old_free_hook = __free_hook;
  /* printf() might call malloc(), so protect it too. */
  fprintf(stdout,"calloc %u %llu %llu\n",(unsigned int) size, result, caller);
  /* Restore our own hooks */
  __calloc_hook = my_calloc_hook;
  __free_hook=my_free_hook;
  return result;
}

static void my_free_hook(void* ptr, const void* caller){
  __free_hook = old_free_hook;
  __malloc_hook = old_malloc_hook;
  free(ptr);
  old_free_hook = __free_hook;
  old_malloc_hook = __malloc_hook;
  fprintf(stdout, "free %llu\n", ptr);
 __malloc_hook = my_malloc_hook;
 __free_hook=my_free_hook;
}
