#ifndef __TraceWriter_h
#define __TraceWriter_h

#include <fstream>
#include <assert.h>

using namespace std;

class TraceWriter{
    private:
        ofstream fout;
    public:
        uint64_t maxsize;
        uint64_t chunksize;
        double srate;
        uint64_t num_chunk;
        uint64_t num_inst;
        string filename;
        //chunksize and num_chunk are neccessary, others does hurt the trace
        TraceWriter(string _filename, uint64_t _chunksize, uint64_t _num_chunk, \
                uint64_t _maxsize = 0, double _srate = 0, uint64_t _num_inst = 0){
            maxsize = _maxsize;
            chunksize = _chunksize;
            srate = _srate;
            num_chunk = _num_chunk;
            num_inst = _num_inst;
            filename = _filename;
            fout.open(filename.c_str());
            assert(fout.good() && "Open output file error");
        }

        void Write(uint64_t addr, uint32_t size=1){
            fout.write((char*)&addr, sizeof(addr));
            fout.write((char*)&size, sizeof(size));
        }

        ~TraceWriter(){
            fout.write((char*)&maxsize, sizeof(maxsize));
            fout.write((char*)&chunksize, sizeof(chunksize));
            fout.write((char*)&srate, sizeof(srate));
            fout.write((char*)&num_chunk, sizeof(num_chunk));
            fout.write((char*)&num_inst, sizeof(num_inst));
            fout.flush();
            fout.close();
        }


};

#endif
