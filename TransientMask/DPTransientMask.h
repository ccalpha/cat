#ifndef __DPTransientMask_H
#define __DPTransientMask_H

#include <fstream>
#include <tclap/CmdLine.h>
#include <string>
#include <vector>
#include "footprint.h"

TCLAP::ValueArg<string> fps("f","footprints","File to footprints, without id suffix",true,"FPDist","string");
TCLAP::ValueArg<int> HSize("H", "hsize", "H partition size", false, 7340032, "integer");
TCLAP::ValueArg<int> LSize("L", "lsize", "L partition size", false, 1048576, "integer");
TCLAP::ValueArg<int> CacheLine("C", "cacheline", "size of cache line", false, 128, "integer");
TCLAP::ValueArg<int> Associativity("A", "associativity", "associativity of cache", false, 8, "integer");
TCLAP::ValueArg<string> MaskFile("m", "maskfile", "Transient Mask for Arrays", false, "FPDist.mask", "string");
TCLAP::ValueArg<string> CacheConfig("", "config", "Cache configuration : Intel or Power8", false, "Power8", "string");

void CommandLineParse(int argc, char** argv){
  TCLAP::CmdLine cmd("Command description message", ' ', "0.9");
  cmd.add(fps);
  cmd.add(HSize);
  cmd.add(LSize);
  cmd.add(CacheLine);
  cmd.add(MaskFile);
  cmd.add(CacheConfig);
  cmd.parse( argc, argv );
}


vector<FPDist*> fpds;
vector<int> fpdsID;
int64_t CacheBlock;
int64_t MaxWindow;

inline int GetIdx(double a, double granulairty = 64.0){
      return floor(a/granulairty);
}

void MergeFPDS(FPDist *fp1, FPDist *fp2){
  for(int64_t i = 0; i < fp1->M; i++){
    fp1->FP[i] += fp2->FP[i];
  }
}

int AssignFPDSID(FPDist *fp){
  return fpds.size();
  /*
  if(fp->GetDataCount() < 128*1024)
    return 0;
  else
    return fpds.size();
  */
}

void GetFPDS(string fname){
  int idx = 0;
    while(1){
        FPDist *f = new FPDist(fname, idx);
        if(!f->IsGood())
            break;
        fpds.push_back(f);
        fpdsID.push_back(idx);
        idx++;
    }

  /*
  string meta_name = fname + ".";
  while(1){
    string fn = meta_name + to_string(idx++);
    fin.open(fn.c_str());
    if(fin.good()){
      fin.close();
      FPDist *f = new FPDist(fn);
      int id = AssignFPDSID(f);
      if(id == fpds.size())
        fpds.push_back(f);
      else
        MergeFPDS(fpds[id], f);
      fpdsID.push_back(id);
    }else{
      break;
    }
  }
  */
  cout << "Number of Traces : " << fpds.size() << endl;
}

void OutputMask(int* sol){
  ofstream fout(MaskFile.getValue());
  for(int i = 0; i < fpdsID.size(); i++)
    fout << sol[fpdsID[i]] << endl;
  fout.close();
}

//
double GetTH(double c, int H[], int n){
    if(n == 0) return 0;
  double l = 0;
  double r = fpds[H[0]]->W[fpds[H[0]]->M-1];
  double s;
  while(r-l > 1e-5){
    double mid = (l+r)/2;
    s = 0;
    for(int i = 0; i < n; i++)
      s += (*fpds[H[i]])[mid];
    if(s < c) l = mid;
    else r = mid;
  }
  return r;
}

double GetW(double hh, double ll, double th, int H[], int a, int L[], int b){
  double l = 0;
  double r = a == 0 ? fpds[L[0]]->W[fpds[L[0]]->M-1] : fpds[H[0]]->W[fpds[H[0]]->M-1];

  while(r-l > 1e-5){
    double mid = (l+r)/2;
    double s = 0;
    for(int i = 0; i < a; i++)
      s += (*fpds[H[i]])[mid+th];
    s = max(hh, s);
    for(int i = 0; i < b; i++)
      s += (*fpds[L[i]])[mid];
    if(s < hh+ll) l = mid;
    else r = mid;
  }
  return r;
}

double GetMR(double h, double l, int* ret){
  int *H = new int[fpds.size()];
  int *L = new int[fpds.size()];
  int nH = 0;
  int nL = 0;
  for(int i = 0; i < fpds.size(); i++){
    if(ret[i] == 0)
      H[nH++] = i;
    else
      L[nL++] = i;
  }
  double th = GetTH(h, H, nH);
  double w = GetW(h, l, th, H, nH, L, nL);
  double mr = 0;
  for(int j = 0; j < nH; j++){
    double tmp = (*fpds[H[j]])[th+w+1] - (*fpds[H[j]])[th+w];
    mr += tmp;
    if(mr > 1) cout << "H : " << j << " " << mr << " " << tmp << endl;
    assert(mr <= 1);
  }
  for(int j = 0; j < nL; j++){
    double tmp = (*fpds[L[j]])[w+1] - (*fpds[L[j]])[w];
    mr += tmp;
    if(mr > 1) cout << "L : " << j << " " << mr << " " << tmp << endl;
    assert(mr <= 1);
  }
  delete H;
  delete L;
  return mr;
}

double GetMR(double c){
    int *H = new int[fpds.size()];
    for(int i = 0; i < fpds.size(); i++)
        H[i] = i;
    double th = GetTH(c, H, fpds.size());
    double mr = 0;
    for(int i = 0; i < fpds.size(); i++){
        mr += (*fpds[H[i]])[th+1]-(*fpds[H[i]])[th];
    }
    delete H;
    return mr;
}
#endif
