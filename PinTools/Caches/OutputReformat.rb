#!/usr/bin/ruby

fin = File.open(ARGV[0])

headers = ["Load Hits", "Load Misses", "Load Accesses", "Load Miss Rate",
           "Store Hits", "Store Misses", "Store Accesses", "Store Miss Rate",
           "Total Hits", "Total Misses", "Total Accesses", "Total Miss Rate"
          ]
puts headers.join("\t")

fin.each_line do |line|
  line.match("\\A\\w+[^:]*"){|m| print "\n#{m}"}
  line.match("\s\\d.*") {|m| print "#{m}"}
end
puts
