//
//  TraceReader.h
//  Footprint_Sharing
//
//  Created by yechencheng on 6/17/15.
//  Copyright (c) 2015 yechencheng. All rights reserved.
//

#ifndef Footprint_Sharing_TraceReader_h
#define Footprint_Sharing_TraceReader_h

#include <fstream>
#include <iostream>
#include <stdint.h>
using namespace std;

int64_t FileSize(const char* filename){
    streampos begin,end;
    ifstream myfile (filename, ios::binary);
    begin = myfile.tellg();
    myfile.seekg (0, ios::end);
    end = myfile.tellg();
    myfile.close();
    return end - begin;
}

Trace* ReadTrace(const char* filename){
    ifstream in(filename, ios::in);
    int64_t fl = FileSize(filename);
    Trace* T = new Trace(fl/sizeof(int64_t), *new string(filename));
    char a[sizeof(int64_t)];
    while(in){
        in.read(a, sizeof(int64_t));
        T->push(*(int64_t*)a);
    }
    return T;
}

#endif
