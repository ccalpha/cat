#!/usr/bin/ruby


def GenerateFootprint(prefix, dataCount, numAccesses = 100002, numH = 256/8*7, numL = 256/8)
  freq = dataCount.size

  freq.times{|i|
    fout = File.open("#{prefix}.array.#{i}", 'w')
    fout.write("#{numAccesses}\t#{numH/2}\t#{numAccesses}\t#{numAccesses}\n")
    numAccesses.times{|w|
      sum = 0
      freq.times{ |j|
        sum += ((w+1-j).to_f/freq).ceil
      }
      sum /= freq.to_f
      sum = [sum, dataCount[i].to_f].min
      fout.write("#{w+1}\t#{sum}\t0.0\n")
    }
    fout.close
  }
end

NumH = 256/8*7
NumL = 256/8
#Test Case 1
#DataCount = [NumH/2, NumH/2, NumH/2, NumH/2, NumH/4, NumH/4]
#GenerateFootprint("0", DataCount)

#Test Case 2
DataCount = [NumH/2, NumH/2, NumL/4, NumL/4, NumH/2, NumH/2]
GenerateFootprint("1", DataCount)
