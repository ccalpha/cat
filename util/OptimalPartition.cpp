#include <iostream>
#include <string>
#include <algorithm>
#include <numeric>

#include <boost/program_options.hpp>

#include "footprint.h"
#include "OptimalPartition.h"
#include "general.h"

using namespace std;

int main(int argc, char **argv){
    double c;
    double roundto;
    vector<string> names;
    
    namespace po = boost::program_options;
    po::options_description desc("Allowed Options");
    desc.add_options()
        ("help,h", "This message")
        ("capacity,c", po::value<double>(&c)->required(), "capacity of cache")
        ("roundto,r", po::value<double>(&roundto)->default_value(128), "chunk size")
        ("footprint,f", po::value<vector<string>>(), "footprints")
        ;
    po::positional_options_description p;
    p.add("footprint", -1);
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    if(vm.count("help")){
        cout << desc << endl;
        return 0;
    }
    po::notify(vm);
    
    vector<FPDist> fpds;
    for(auto i : vm["footprint"].as<vector<string>>())
        fpds.push_back(FPDist(i));

    vector<double> rt = OptimalPartition(fpds, c, roundto);
    PrintVector(rt, "Opt Partition: ");
    for(int i = 0; i < rt.size(); i++)
        rt[i] = fpds[i].GetMissRatio(rt[i])*fpds[i].access_count;
    PrintVector(rt, "Opt Miss Count: ");
    double amc = accumulate(rt.begin(), rt.end(), 0);
    cout << "Accumulated Miss Count : " << amc << endl;
    return 0;
}
