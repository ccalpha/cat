#pragma once
#include <vector>
#include <algorithm>
#include <functional>
#include <numeric>

#include "TraceReader.h"

using namespace std;

class InterleaveEngine{
    protected:
        vector<TraceReader*> trs;
        vector<double> acrate;
        InterleaveEngine(){}
    public:
        virtual MemRef* GetNext() = 0;
        InterleaveEngine(vector<TraceReader*> _trs, vector<double> _acrate){
            trs = _trs;
            acrate = _acrate;
        }
};

class UniformInterleave : public InterleaveEngine{
    private:
        vector<double> pt;
        vector<int> cpt;
        vector<MemRef*> mpt;
        uint64_t flag;
        uint8_t cnti;
        uint64_t scale;
    public:
        UniformInterleave(vector<TraceReader*> _trs){
            vector<double> x(_trs.size(), 1.0);
            UniformInterleave(_trs, x);
        }

        UniformInterleave(vector<TraceReader*> _trs, vector<double> _acrate) : InterleaveEngine(_trs, _acrate){
            pt = vector<double>(trs.size(), 0.0);
            cpt = vector<int>(trs.size(), 0);
            flag = 1;
            flag <<= trs.size();
            flag--;
            double sum = accumulate(acrate.begin(), acrate.end(), 0.0);
            for(auto &i : acrate)
                i /= sum;
            for(auto i : trs)
                mpt.push_back(i->GetChunk(0));
            cnti = 0;
            scale = 1;
        }

        MemRef* GetNext(){
            while(flag){
                for(; cnti < trs.size(); cnti++){
                    if((flag>>cnti)&1){
                        pt[cnti] += acrate[cnti];
                        if(pt[cnti] > trs[cnti]->GetChunkSize()){
                            pt[cnti] = 0;
                            cpt[cnti]++;
                            if(cpt[cnti] == trs[cnti]->GetNumChunk()){
                                flag ^= ((uint64_t)1 << cnti);
                                continue;
                            }
                            delete mpt[cnti];
                            mpt[cnti] = trs[cnti]->GetChunk(cpt[cnti]);
                        }
                        if(pt[cnti] == 0 || floor(pt[cnti]) != floor(pt[cnti]-acrate[cnti]))
                            return (mpt[cnti])+int(floor(pt[cnti]));
                    }
                }
                cnti = 0;
            }
            return NULL;
        }

        uint8_t GetNextID(){
            return cnti;
        }
};

class SimpleInterleavingEngine{
    public:
        vector<double> ars;
        virtual uint32_t GetNext() = 0;
        SimpleInterleavingEngine(vector<double> _ars);
};

SimpleInterleavingEngine::SimpleInterleavingEngine(vector<double> _ars){
    ars = _ars;
};

class UniformSimpleInterleave : public SimpleInterleavingEngine{
    private:
        uint32_t pos;
        vector<double> rrcounter;
    public:
        UniformSimpleInterleave(vector<double> _ars) : SimpleInterleavingEngine(_ars){
            pos = -1;
            rrcounter.resize(ars.size(), 0.0);
        }
        uint32_t GetNext(){
            while(1){
                pos++;
                if(pos == ars.size()) pos = 0;
                rrcounter[pos] += ars[pos];
                if(floor(rrcounter[pos]-ars[pos]) != floor(rrcounter[pos])){
                    return pos;
                }
            }
            return -1;
        }
};
