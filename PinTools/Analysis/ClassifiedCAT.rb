#!/usr/bin/ruby

#usage : selection.rb numofcorun time
names = File.readlines("names.txt").map{ |x| x.strip}
#candidates = (0..names.size-1).to_a
candidates = [1,2,3,4,9,10,15,18,22,24]#,19,5,12,17,8,20]
candidates.each{|x| puts names[x]}
nco = ARGV[0].to_i

candidates = candidates.combination(nco).to_a
ROOT="../Traces_continous"

prng = Random.new(17329489) #1111111th prime

def CheckCover(alloc)
    for i in alloc
        if(i == 0)
            return false;
        end
    end
    return true
end

def GetContinuousAlloc(prng, nway, nprog)
    alloc = [0,nway-1]
    for i in 0...nprog-1
        alloc << prng.rand(0..nway-1)
        alloc << prng.rand(0..nway-1)
    end
    alloc.shuffle!(random: prng) #alloc might not cover all of the ways

    rt = Array.new(nway, 0)
    for i in 0...nprog
        if(alloc[i*2] > alloc[i*2+1])
            alloc[i*2], alloc[i*2+1] = alloc[i*2+1], alloc[i*2]
        end
        for j in alloc[i*2]..alloc[i*2+1]
            rt[j] |= (1<<i)
        end
    end

    mask = []
    for i in 0...nprog
        mask << 2**(1+alloc[i*2+1])-(2**alloc[i*2])
    end
    return rt,mask
    #return rt
end

cnt = 0
for gid in candidates
    for i in 0...2
        #continuous allocator
        alloc = GetContinuousAlloc(prng, 20, nco)

        while(CheckCover(alloc[0]) == false)
            alloc = GetContinuousAlloc(prng, 20, nco)
        end
        puts "working on co-run group : #{gid.join(" ")}"
        alloc_hex = []
        alloc[1].each{|x| alloc_hex << "0x#{x.to_s(16)}"}
        puts "allocation : #{alloc_hex.join(" ")}"
        system("echo #{gid.join(" ")} >> groups")
        system("echo #{alloc[0].join(" ")} >> alloc_share")
        next
        system("echo \"#{ROOT}/#{names[gid[0]]}.trace\n#{ROOT}/#{names[gid[1]]}.trace\" > traces_random")
        system("echo \"#{alloc.join("\t")}\" > alloc_random")

        system("./runtest.sh CAT_Simple -t ./traces_random -a ./alloc_random -c 20971520 >> CAT_Class_20M")
        #threads = []
        #threads << Thread.new{system("./runtest.sh CAT_Simple -t ./traces_random -a ./alloc_random -c 20971520 >> CAT_Class_20M")}
        #threads << Thread.new{system("./runtest.sh CAT_Simple -t ./traces_random -a ./alloc_random -c 10485760 >> CAT_Class_10M")}
        #threads << Thread.new{system("./runtest.sh CAT -t ./traces_random -a ./alloc_random -c 5242880 >> CAT_5M")}
        #system("./runtest.sh CAT -t ./traces_random -a ./alloc_random -c 5242880 >> CAT_5M")


        #threads.each { |thr| thr.join }
    end
end
