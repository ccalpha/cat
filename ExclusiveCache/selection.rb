#!/usr/bin/ruby

#usage : selection.rb numofcorun
nco = ARGV[0].to_i

names = []
File.readlines("names.txt").each{|x| names << x.strip}
mrs = File.readlines("./missrate").map{|x| x.to_f}

candidates = (0..names.size-1).to_a
#candidates = (0..9).to_a
candidates = [1,2,3,4,5,9,10,15,18,19,22,24] 
#candidates = [1,2,3,4,9,10,15,18,22,24]
candidates = candidates.combination(nco).to_a

ac=File.readlines("AccessRate3")

#ROOT="../PinTools/SelectedFP"
ROOT="../PinTools/Intel_FullTrace/"

cnt = 0
for i in candidates
    s = i.join(" ")
    cmd = ""
    for j in i
        cmd += " #{ROOT}/#{names[j]}.footprint"
    end
    puts "working on co-run group : #{s}"
    system("./PredictExclusive -H 32768 -L 131072 #{cmd} -a #{ac[cnt].strip}") 
    #system("../util/NaturalPartition -c 196608 -a #{ac[cnt].strip} -f #{cmd}") 
    #for j in i
    #system("../util/ShowFootprint -c #{196608*(mrs[j]/(mrs[i[0]]+mrs[i[1]]))} -f #{ROOT}/#{names[j]}.footprint")
    #system("../util/ShowFootprint -c 98304 -f #{ROOT}/#{names[j]}.footprint")
    #system("../util/ShowFootprint -c 76458 -f #{ROOT}/#{names[j]}.footprint")
    #end
    cnt=cnt+1
end
