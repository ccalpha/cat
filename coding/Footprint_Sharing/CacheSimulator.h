//
//  CacheSimulator.h
//  Footprint_Sharing
//
//  Created by yechencheng on 5/10/15.
//  Copyright (c) 2015 yechencheng. All rights reserved.
//

#ifndef __Footprint_Sharing__CacheSimulator__
#define __Footprint_Sharing__CacheSimulator__

#include <stdio.h>
#include "footprint.h"
#include "LRUStack.h"

class CacheStack{
public:
    virtual int64_t push(int64_t datum, int64_t pushTo = 0) = 0;  //-1:no miss, other:evictee
    virtual ~CacheStack() {};
};

//Two program, one occupy ds+ss cache, the other one ss.
class PSCache : public CacheStack{
private:
    LRUStack *LD;
    LRUStack *LS;
public:
    int64_t ss;
    int64_t ds;
    PSCache(int64_t _ss, int64_t _ds) : ss(_ss), ds(_ds){
        LD = new LRUStack(ds);
        LS = new LRUStack(ss);
    }
    ~PSCache(){
	delete LD;
	delete LS;
    }
    int64_t push(int64_t datum, int64_t dedicate){
        int64_t IsFromLS = 0;
        if(dedicate){
            if(LD->IsInStack(datum) || !LD->IsFull()){
                return LD->push(datum);
            }
            else if(LS->IsInStack(datum)){
                LS->push(datum);
                LS->RemoveHead();
                IsFromLS = LRUAccess::DataHit;
            }
            datum = LD->push(datum);
            if(datum == LRUAccess::DataFill || datum == LRUAccess::DataHit)
                return datum;
        }
        int64_t ret = LS->push(datum);
        if(IsFromLS == LRUAccess::DataHit) return LRUAccess::DataHit;
        return ret;
    }
};

class LRUCache : public CacheStack{
private:
    LRUStack *L;
public:
    int64_t size;
    LRUCache(int64_t _size) : size(_size){
        L = new LRUStack(size);
    }
    ~LRUCache(){
	delete L;
    }
    int64_t push(int64_t datum, int64_t pushTo = 0){
        return L->push(datum);
    }
};

//return miss ratio
double RandomInterleave(CacheStack *CS, Trace *T[], int64_t N, int64_t *pushTo = NULL, int64_t *hits = NULL);

double RoundRobinInterleave(CacheStack *CS, Trace *T[], int64_t N, int64_t *pushTo = NULL);

double ContinuousInterleave(CacheStack *CS, Trace *T[], int64_t N, int64_t *pushTo = NULL);
#endif /* defined(__Footprint_Sharing__CacheSimulator__) */
