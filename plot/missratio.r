#!/usr/bin/env Rscript

library(ggplot2)
library(scales)

data <- read.table("catmissratio.data",header=T)
colnames(data) = c("ID","Name","Ways","Loads","Stores","L3Misses","Prediction")
data$Measured = data$L3Misses/(data$Loads+data$Stores)

mrcurve <- ggplot(data)+
    geom_line(aes(x=1:nrow(data),y=Prediction+1e-6,color="a",linetype="a"),size=2)+
    geom_line(aes(x=1:nrow(data),y=Measured,color="b",linetype="b"),size=2)+
    scale_color_manual(name=element_blank(), values=c('#3399FF','#FF6600'),labels=c("Prediction","Measurement"))+
    scale_linetype_manual(name=element_blank(), values=c("dashed", "solid"),labels=c("Prediction","Measurement"))+
    xlab(label="Benchmarks")+
    ylab(label="Miss Ratio")+
    #scale_y_log10(labels=percent)+
    scale_x_continuous(breaks=seq(1,nrow(data),by=20),labels=data$Name[seq(1,nrow(data),by=20)],expand=c(0.01,0))+
    scale_y_continuous(labels=percent,limits=c(0,0.2))+
    theme(axis.text.x=element_text(angle=90,hjust=1,vjust=1.5))+
    theme(text=element_text(size=48))+
    theme(legend.position="top")+
    theme(legend.key.width=unit(1.5,"cm"))+
    theme(axis.ticks.length=unit(0.25,"cm"))
  
mrcurve <- mrcurve + 
  geom_vline(aes(xintercept=21),size=1,linetype="dotted")+
  geom_vline(aes(xintercept=40),size=1,linetype="dotted")+
  geom_segment(aes(x=21,y=0.1,xend=40,yend=0.12),size=0.5,linetype="solid",arrow=arrow(length=unit(0.3,"cm")))+
  geom_label(aes(x=21,y=0.092,label="1.25MB"),size=6,fontface="italic")+
  geom_label(aes(x=40,y=0.129,label="25MB"),size=6,fontface="italic")+
  geom_vline(aes(xintercept=281),size=1,linetype="dotted")+
  geom_vline(aes(xintercept=300),size=1,linetype="dotted")+
  geom_segment(aes(x=281,y=0.1,xend=300,yend=0.12),size=0.5,linetype="solid",arrow=arrow(length=unit(0.3,"cm")))+
  geom_label(aes(x=281,y=0.092,label="1.25MB"),size=6,fontface="italic")+
  geom_label(aes(x=300,y=0.129,label="25MB"),size=6,fontface="italic")+
  

pdf("Rplots.pdf", 24,8)
mrcurve
dev.off()
