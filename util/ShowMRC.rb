#!/usr/bin/ruby

for i in (0...2**24).step(2**12)
    system("../util/ShowFootprint -c #{i} #{ARGV[0]} | sed -e \"s/miss ratio #{i} //g\"")
end
