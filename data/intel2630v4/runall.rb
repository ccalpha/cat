#!/usr/bin/ruby

require 'fileutils'

dirs = File.readlines("dirs.txt").map{|x| x.strip}
commands = File.readlines("commands.txt").map{|x| x.strip}
names = File.readlines("names.txt").map{|x| x.strip}

pwd = Dir.pwd

Events = "-e r5381d0 -e r5320d1 -e r5382d0 -e \'cpu/config=0x5301b7,config1=0x3fbc008fff/\'"
w = 0xfffff
nw = 20

#selected = ["milc","leslie3d", "gamess"]
selected = ["gamess"]
#selected = ["milc"]
while w != 0
    for i in 0...dirs.length
        #if not selected.include?(names[i])
        #    next
        #end
        
        Dir.chdir(dirs[i])
        #puts Dir.pwd
        name = "#{names[i]}.W#{nw}.data"
        system("~/cat/util/ShowFootprint -c #{nw*20480} ~/cat/PinTools/Caches/output/#{names[i]}.fp")
        #system("perf stat -r 3 #{Events} -o #{name} -B rdtset -r 4 -c 4 -t \'cpu=4;l3=0x#{w.to_s(16)}\' #{commands[i]}")
        #puts("perf stat -r 3 #{Events} -o #{name} -B rdtset -r 4 -c 4 -t \'cpu=4;l3=0x#{w.to_s(16)}\' #{commands[i]}")
        #FileUtils.move("#{name}", "#{pwd}/#{name}")
    end
    w = w>>1
    nw = nw-1
end
