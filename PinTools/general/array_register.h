#ifndef ARRAY_REGISTER_H
#define ARRAY_REGISTER_H

#include <iostream>
#include <map>
#include <algorithm>
#include <stdint.h>

/*
   Suppose Arrays are disjoint
   */

using namespace std;

class ArrayRegister{
    private:
        typedef map<ADDRINT, ADDRINT>::iterator map_itr;
        map<ADDRINT, ADDRINT> arrays;  //begin, length
        map<ADDRINT, ADDRINT> arraysID;
        ADDRINT size;
        ADDRINT callee;

    public:
        vector<ADDRINT> callees;
        vector<ADDRINT> sizes;

        int GetNumberOfArray(){ return arrays.size(); }
        ArrayRegister(){
            callees.push_back(0);
            sizes.push_back(0);
        }
        bool IsInsideArray(ADDRINT addr, ADDRINT begin, ADDRINT length){
            return addr >= begin && addr < begin+length;
        }

        void BeforeMalloc(ADDRINT _size, ADDRINT _callee = 0){
            size = _size;
            callee = _callee;
        }

        vector<ADDRINT> PreProcessAddr(ADDRINT &addr){
            vector<ADDRINT> ret;
            if(addr == 0) return ret;
            //align
            if(addr&127){
                ADDRINT sz = (addr&127);
                if(size <= sz) return ret;
                addr -= sz;
                addr += 128;
            }

            map_itr it = arrays.upper_bound(addr);
            while(it != arrays.end() && addr+size > it->first){
                //cerr << "Array Overlap 1 : " << addr << "(" << size << ")" << "\t";
                //cerr << it->first << "(" << it->second << ")" << endl;
                ret.push_back(GetID(it->first));
                FreeArray(it->first);
                ++it;
            }

            while(it != arrays.begin()){
                --it;
                if(it->first+it->second > addr){
                    //cerr << "Array Overlap 2 : " << addr << "(" << size << ")" << "\t";
                    //cerr << it->first << "(" << it->second << ")" << endl;
                    ret.push_back(GetID(it->first));
                    FreeArray(it->first);
                }
                else
                    break;
            }
            return ret;
        }

        ADDRINT AfterMalloc(ADDRINT &addr){
            if(addr == 0) return 0;
            PreProcessAddr(addr);

            int n = callees.size();
            callees.push_back(callee);
            sizes.push_back(size);
            arrays[addr] = size;
            arraysID[addr] = n;
            return size;
        }

        void FreeArray(ADDRINT addr){
            if(addr == 0) return;
            arrays.erase(addr);
            arraysID.erase(addr);
        }

        ADDRINT GetID(ADDRINT addr){
            map_itr it = arrays.upper_bound(addr);
            if(it == arrays.begin()){
                return 0; //in no registed array
            }
            --it;

            if(IsInsideArray(addr, it->first, it->second))
                return arraysID[it->first];
            else
                return 0;
        }
};

class MergeEngine{
    private:
        ArrayRegister ar;
        vector<int> mID;  //id of group of array
        vector<ADDRINT> sizes;  //size of each group, ar.sizes are sizes of each array
        vector<ADDRINT> callees;  //callee of group
        int _cnt; //number of groups
        map<ADDRINT, int> splitArrays;
        ADDRINT large;
        ADDRINT small;
        ADDRINT size;
        ADDRINT callee;
    public:
        MergeEngine(){
            large = (1*1024*1024)/2;
            small = (1*1024*1024)/16;
            //the first array should be reserved for non-malloced address
            _cnt = 1;
            mID.push_back(0);
            sizes.push_back(0);
            callees.push_back(0);
        }
        int GetNumberOfArray(){
            return _cnt;
        }
        void BeforeMalloc(ADDRINT _size, ADDRINT _callee = 0){
            size = _size;
            callee = _callee;
            ar.BeforeMalloc(_size, _callee);
        }
        void PreProcessAddr(ADDRINT addr){
            vector<ADDRINT> ret = ar.PreProcessAddr(addr);
            for(unsigned int i = 0; i < ret.size(); i++)
                sizes[mID[ret[i]]] -= sizes[ret[i]];

        }
        void AfterMalloc(ADDRINT addr){
            if(addr == 0) return;
            //PreProcessAddr(addr);
            /*
               if array size is too samll, merge it with previous arraies which have the
               same callee. The threshold is *small*. Otherwise, split large array to chunks
               which are *large*
               */
            if(size > large){
                int n = size / large;
                size %= large;
                splitArrays[addr] = n + (size==0);
                for(int i = 0; i < n; i++, addr+=large){
                    ar.BeforeMalloc(large, callee);
                    ar.AfterMalloc(addr);
                    int n = mID.size();
                    mID.push_back(n);
                    mID[n] = _cnt++;
                    sizes.push_back(large);
                    callees.push_back(ar.callees[n]);
                }
                ar.BeforeMalloc(size, callee);
            }
            if(size){
                ar.AfterMalloc(addr);
                int n = mID.size();
                mID.push_back(-1);
                sizes.push_back(0);
                callees.push_back(ar.callees[n]);

                if(ar.sizes[n] < small){
                    for(int i = _cnt-1; i > 0; i--){
                        if(callees[i] == ar.callees[n] && sizes[i] < small){
                            mID[n] = i;
                            break;
                        }
                    }
                }
                if(mID[n] == -1){
                    mID[n] = _cnt++;
                }
                sizes[mID[n]] += ar.sizes[n];
            }
        }

        void FreeArray(ADDRINT addr){
            if(addr == 0) return;
            if(splitArrays.find(addr) != splitArrays.end()){
                int n = splitArrays[addr];
                splitArrays.erase(splitArrays.find(addr));
                for(int i = 0; i < n-1; i++, addr += large){
                    int id = ar.GetID(addr);
                    sizes[mID[id]] -= ar.sizes[id];
                    ar.FreeArray(addr);
                }
            }
            int id = ar.GetID(addr);
            sizes[mID[id]] -= ar.sizes[id];
            ar.FreeArray(addr);
        }

        ADDRINT GetID(ADDRINT addr){
            ADDRINT x = ar.GetID(addr);
            return mID[x];
            return 1;
        }
};

class PageEngine{
    private:
        map<ADDRINT, ADDRINT> idx;
        int log2chunksize;
        ADDRINT chunksize;

        ADDRINT size;
        ADDRINT callee;
        vector<ADDRINT> sizes;
        vector<ADDRINT> callees;
    public:
        PageEngine(){
            log2chunksize = 16;
            chunksize = 1 << log2chunksize;
        }
        int GetNumberOfArray(){
            return sizes.size();
        }
        ADDRINT GetID(ADDRINT addr){
            addr >>= log2chunksize;
            if(idx.find(addr) == idx.end()) return 0;
            return idx[addr];
        }
        void BeforeMalloc(ADDRINT _size, ADDRINT _callee = 0){
            size = _size;
            callee = _callee;
        }

        void AlignArrayToChunk(ADDRINT &addr){
            size += addr & (chunksize-1);
            addr = addr >> log2chunksize << log2chunksize;
            if(size < chunksize) size = chunksize;
            else size = size >> log2chunksize << log2chunksize;
        }

        void AfterMalloc(ADDRINT addr){
            AlignArrayToChunk(addr);
            while(size >= chunksize){
                if(GetID(addr) == 0){
                    idx[addr>>log2chunksize] = sizes.size();
                    sizes.push_back(chunksize);
                    callees.push_back(callee);
                }
                addr += chunksize;
                size -= chunksize;
            }
        }

        void FreeArray(ADDRINT addr){
        }
};

//typedef PageEngine ArrayEngine;
//typedef MergeEngine ArrayEngine;
typedef ArrayRegister ArrayEngine;
#endif
