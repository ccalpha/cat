#include <iostream>
#include <ga/GASimpleGA.h>
#include <ga/std_stream.h>
#include <ga/GA1DBinStrGenome.h>

using namespace std;

float Objective(GAGenome &g){
  GA1DBinaryStringGenome & genome = (GA1DBinaryStringGenome &)g;
  int cnt = 0;
  for(int i = 0; i < genome.length(); i++)
    cnt += genome.gene(i);
  return cnt;
}

int main(){
  GA1DBinaryStringGenome genome(128, Objective);
  GASimpleGA ga(genome);
  ga.populationSize(32);
  ga.nGenerations(2048);
  ga.pMutation(0.001);
  ga.pCrossover(0.9);
  ga.evolve();
  const GA1DBinaryStringGenome &g = (const GA1DBinaryStringGenome&)(ga.statistics().bestIndividual());

  cout << "The GA found:\n" << g << endl;
  cout << "Best Population : " << ga.statistics().bestPopulation() << endl;
  cout << "Minimal Miss Ratio : " << ga.statistics().maxEver() << endl;
}
