//#include "pin.H"
#include "pin_cache.H"
#include <vector>
#include <cstdlib>
#include <ctime>
#include <cassert>
#include <iostream>
#include <numeric>
#include <functional>
#include <cmath>
#include <cstring>

#include "array_register.h"
#include "../../general/general.h"

class _LRUSet{
    protected:
        CACHE_TAG *tags;
        UINT64 *times;
        UINT64 access_time;
        UINT32 associativity;
    public:
        _LRUSet(UINT32 _associativity){
            associativity = _associativity;
            tags = new CACHE_TAG[associativity];
            times = new UINT64[associativity];
            for(UINT32 i = 0; i < associativity; i++){
                times[i] = 0;
                tags[i] = 0;
            }
            access_time = 1;
        }
        VOID SetAssociativity(UINT32 _associativity){
            associativity = _associativity;
        }
        UINT32 GetAssociativity(){
            return associativity;
        }
        virtual BOOL Find(CACHE_TAG tag, int IsTransient){
            ++access_time;
            for(UINT32 i = 0; i < associativity; i++){
                if(tags[i] == tag){
                    times[i] = IsTransient ? 1 : access_time;
                    return true;
                }
            }
            return false;
        }

        UINT32 GetLRUIdx(){
            UINT64 tmp = times[0];
            UINT32 idx = 0;
            for(UINT32 i = 0; i < associativity; i++){
                if(times[i] < tmp){
                    idx = i;
                    tmp = times[i];
                }
            }
            return idx;
        }

        UINT32 GetMRUIdx(){
            UINT64 tmp = times[0];
            UINT32 idx = 0;
            for(UINT32 i = 0; i < associativity; i++){
                if(times[i] > tmp){
                    idx = i;
                    tmp = times[i];
                }
            }
            return idx;
        }

        virtual CACHE_TAG Replace(CACHE_TAG tag, int IsMRU){
            UINT32 idx = GetLRUIdx();
            times[idx] = IsMRU ? access_time : 1;
            CACHE_TAG x = tags[idx];
            tags[idx] = tag;
            return x;
        }

        VOID Flush(){
            for(UINT32 i = 0; i < associativity; i++){
                tags[i] = 0;
                times[i] = 0;
            }
        }
};

class ParLRUSet : public _LRUSet{
    private:
        vector<UINT32> *pars;
    public:
        ParLRUSet(UINT32 _associativity, vector<UINT32> *_pars) : _LRUSet(_associativity){
            pars = _pars;
        }
        //using _LRUSet::Find;
        //using _LRUSet::Replace;
        virtual BOOL Find(CACHE_TAG tag, int par){
            ++access_time;
            for(UINT32 i = pars->at(par); i < pars->at(par+1); i++){
                if(tags[i] == tag){
                    times[i] = access_time;
                    return true;
                }
            }
            return false;
        }

        UINT32 GetLRUIdx(int par){
            UINT32 idx = pars->at(par);
            UINT64 tmp = times[idx];
            for(UINT32 i = idx+1; i < pars->at(par+1); i++){
                if(times[i] < tmp){
                    idx = i;
                    tmp = times[i];
                }
            }
            return idx;
        }

        CACHE_TAG Replace(CACHE_TAG tag, int par){
            UINT32 idx = GetLRUIdx(par);
            times[idx] = access_time;
            tags[idx] = tag;
            return tag;
        }
};

class ParEngin{
    private:
        vector<UINT32> *pars;
    public:
        UINT32 par;
        ParEngin(vector<UINT32> *_pars){
            pars = _pars;
            par = 0;
        }
        UINT32 GetPar(ADDRINT addr){
            return par;
        }
};



template <UINT32 STORE_ALLOCATION, class LRUSet = _LRUSet>
class LRUCACHE : public CACHE_BASE{
    private:
        std::vector<LRUSet> sets;
        INT64 IsLIP;
    public:
        LRUCACHE(std::string name, UINT32 cacheSize, UINT32 lineSize, UINT32 associativity) : CACHE_BASE(name, cacheSize, lineSize, associativity)
    {
        //cout << associativity << "\t" << NumSets() << endl;
        for(UINT32 i = NumSets(); i > 0; i--)
            sets.push_back(LRUSet(associativity));
    }
        virtual bool IsMRU(ADDRINT addr){
            return true;
        }

        virtual bool IsTransient(ADDRINT addr){
            return false;
        }

        bool Access(ADDRINT addr, UINT32 size, ACCESS_TYPE accessType){
            const ADDRINT highAddr = addr + size;
            bool allHit = true;
            const ADDRINT lineSize = LineSize();
            const ADDRINT notLineMask = ~(lineSize - 1);
            do
            {
                CACHE_TAG tag;
                UINT32 setIndex;
                SplitAddress(addr, tag, setIndex);
                LRUSet & set = sets[setIndex];
                bool localHit = set.Find(tag, IsTransient(addr));
                allHit &= localHit;
                if ( (! localHit) && (accessType == ACCESS_TYPE_LOAD || STORE_ALLOCATION == CACHE_ALLOC::STORE_ALLOCATE))
                {
                    set.Replace(tag, IsMRU(addr));
                }
                addr = (addr & notLineMask) + lineSize;
            }
            while (addr < highAddr);
            _access[accessType][allHit]++;
            return allHit;
        }
        bool AccessSingleLine(ADDRINT addr, ACCESS_TYPE accessType){
            CACHE_TAG tag;
            UINT32 setIndex;

            SplitAddress(addr, tag, setIndex);
            LRUSet & set = sets[setIndex];

            bool hit = set.Find(tag, IsTransient(addr));
            //if(setIndex == 0){
            //cout << tag << " " << hit << endl;
            //}
            // on miss, loads always allocate, stores optionally
            if ( (! hit) && (accessType == ACCESS_TYPE_LOAD || STORE_ALLOCATION == CACHE_ALLOC::STORE_ALLOCATE))
            {
                set.Replace(tag, IsMRU(addr));
            }

            _access[accessType][hit]++;

            return hit;
        }
        void Flush(){
            for (INT32 index = NumSets(); index >= 0; index--) {
                sets[index].Flush();
            }
            IncFlushCounter();
        }
        void ResetStats(){
            for (UINT32 accessType = 0; accessType < ACCESS_TYPE_NUM; accessType++){
                _access[accessType][false] = 0;
                _access[accessType][true] = 0;
            }
            IncResetCounter();
        }
};

template <UINT32 STORE_ALLOCATION>
class DIPCACHE : public LRUCACHE<STORE_ALLOCATION>{
    private:
        INT64 IsLIP;
        UINT32 Possibility;
        UINT32 LIPSet;
        UINT32 BIPSet;
    public:
        DIPCACHE(std::string name, UINT32 cacheSize, UINT32 lineSize, UINT32 associativity, UINT32 _Possibility = 32, UINT32 _LIPSet = 32, UINT32 _BIPSet = 32) : LRUCACHE<STORE_ALLOCATION>(name, cacheSize, lineSize, associativity){
            IsLIP = 0;
            Possibility = _Possibility;
            LIPSet = _LIPSet;
            BIPSet = _BIPSet;
            srand(time(NULL));
        }
        bool IsTransient(ADDRINT addr){
            return false;
        }

        bool IsMRU(ADDRINT addr){
            CACHE_TAG tag;
            UINT32 setIndex;
            CACHE_BASE::SplitAddress(addr, tag, setIndex);

            if(setIndex < LIPSet){
                IsLIP++;
                return true;
            }
            else if(setIndex < LIPSet+BIPSet){
                IsLIP--;
                if(rand()%Possibility)
                    return false;
                else
                    return true;
            }
            else{
                if(IsLIP < 0) return true;
                if(rand() % Possibility) return false;
                return true;
            }
        }
};

class TransientMask{
    private:
        ArrayEngine *ary_reg;
        vector<int> masks;
    public:
        TransientMask(ArrayEngine *_ary_reg){
            ary_reg = _ary_reg;
        }

        void init(string MaskFile){
            ifstream fin(MaskFile.c_str());
            int a;
            while(fin >> a)
                masks.push_back(a);
        }

        bool IsTransient(ADDRINT addr){
            int idx = ary_reg->GetID(addr);
            return masks[idx];
        }

        int GetPar(ADDRINT addr){
            return IsTransient(addr);
        }
};

template <UINT32 STORE_ALLOCATION>
class TransientCACHE : public LRUCACHE<STORE_ALLOCATION>{
    private:
        TransientMask *tm;
    public:
        TransientCACHE(std::string name, UINT32 cacheSize, UINT32 lineSize, UINT32 associativity, TransientMask *_tm) : LRUCACHE<STORE_ALLOCATION>(name, cacheSize, lineSize, associativity){
            tm = _tm;
        }

        bool IsMRU(ADDRINT addr){
            return !tm->IsTransient(addr);
        }
        bool IsTransient(ADDRINT addr){
            return tm->IsTransient(addr);
        }
};

template <UINT32 STORE_ALLOCATION>
class TLRUTest : public LRUCACHE<STORE_ALLOCATION>{
    public:
        bool MyIsTransient;
        TLRUTest(std::string name, UINT32 cacheSize, UINT32 lineSize, UINT32 associativity) : LRUCACHE<STORE_ALLOCATION>(name, cacheSize, lineSize, associativity){
            MyIsTransient = true;
        }
        bool IsMRU(ADDRINT addr){
            return !MyIsTransient;
        }
        bool IsTransient(ADDRINT addr){
            return MyIsTransient;
        }
};

template<UINT32 STORE_ALLOCATION, class PE = ParEngin>
class WayParLRU : public CACHE_BASE{
    private:
        PE *pe;
        vector<UINT32> *pars;
        vector<ParLRUSet> sets;
    public:
        WayParLRU(std::string name, UINT32 cacheSize, UINT32 lineSize, UINT32 associativity, PE *_pe, vector<UINT32> *_pars) : CACHE_BASE(name, cacheSize, lineSize, associativity){
            pe = _pe;
            pars = _pars;
            for(UINT32 i = NumSets(); i > 0; i--)
                sets.push_back(ParLRUSet(associativity, pars));
        }

        bool Access(ADDRINT addr, UINT32 size, ACCESS_TYPE accessType){
            const ADDRINT highAddr = addr + size;
            bool allHit = true;
            const ADDRINT lineSize = LineSize();
            const ADDRINT notLineMask = ~(lineSize - 1);
            do
            {
                CACHE_TAG tag;
                UINT32 setIndex;
                SplitAddress(addr, tag, setIndex);

                ParLRUSet & set = sets[setIndex];
                bool localHit = set.Find(tag, pe->GetPar(addr));
                allHit &= localHit;
                if ( (! localHit) && (accessType == ACCESS_TYPE_LOAD || STORE_ALLOCATION == CACHE_ALLOC::STORE_ALLOCATE))
                {
                    set.Replace(tag, pe->GetPar(addr));
                }
                addr = (addr & notLineMask) + lineSize;
            }
            while (addr < highAddr);
            _access[accessType][allHit]++;
            return allHit;
        }
        bool AccessSingleLine(ADDRINT addr, ACCESS_TYPE accessType){
            CACHE_TAG tag;
            UINT32 setIndex;

            SplitAddress(addr, tag, setIndex);
            ParLRUSet & set = sets[setIndex];

            bool hit = set.Find(tag, pe->GetPar(addr));
            //if(setIndex == 0){
            //cout << tag << " " << hit << endl;
            //}
            // on miss, loads always allocate, stores optionally
            if ( (! hit) && (accessType == ACCESS_TYPE_LOAD || STORE_ALLOCATION == CACHE_ALLOC::STORE_ALLOCATE))
            {
                set.Replace(tag, pe->GetPar(addr));
            }

            _access[accessType][hit]++;

            return hit;
        }
        void Flush(){
            for (INT32 index = NumSets(); index >= 0; index--) {
                sets[index].Flush();
            }
            IncFlushCounter();
        }
        void ResetStats(){
            for (UINT32 accessType = 0; accessType < ACCESS_TYPE_NUM; accessType++){
                _access[accessType][false] = 0;
                _access[accessType][true] = 0;
            }
            IncResetCounter();
        }
};

class CATSet{
    private:
        CACHE_TAG *tags;
        uint8_t *progid;
        uint64_t *times; //useless, LRU stack
        uint64_t accesstime; //useless
        uint32_t associativity;
        uint64_t *alloc;
    public:
        CATSet(uint32_t _associativity, uint64_t *_alloc){
            associativity = _associativity;
            alloc = _alloc;    //also can new an array for each set
            tags = new CACHE_TAG[associativity];
            progid = new uint8_t[associativity];
            times = new uint64_t[associativity];
            for(uint32_t i = 0; i < associativity; i++){
                times[i] = 0;
                progid[i] = 255;
            }
            accesstime = 0;
        }

        bool Find(CACHE_TAG t, uint8_t pid = 0){
            ++accesstime;
            for(uint32_t i = 0; i < associativity; i++){
                if(progid[i] == pid && tags[i] == t){
                    times[i] = accesstime;
                    return true;
                }
            }
            return false;
        }

        bool FindAndRemove(CACHE_TAG t, uint8_t pid = 0){
            for(uint32_t i = 0; i < associativity; i++){
                if(progid[i] == pid && tags[i] == t){
                    times[i] = 0;
                    progid[i] = 0;
                    tags[i] = 0;
                    return true;
                }
            }
            return false;
        }

        uint32_t GetAssociativity(){
            return associativity;
        }
        void SetAssociativity(uint32_t _asso){
            associativity = _asso;
        }
        bool Replace(CACHE_TAG tag, uint8_t pid = 0){
            uint32_t id = 0;
            uint32_t stime = times[0];
            if(alloc){
                for(uint32_t i = 1; i < associativity; i++){
                    if(ContainBit(alloc[i], pid) && stime > times[i]){
                        stime = times[i];
                        id = i;
                    }
                }
            }
            else{
                for(uint32_t i = 1; i < associativity; i++){
                    if(stime > times[i]){
                        stime = times[i];
                        id = i;
                    }
                }
            }
            bool rt = pid == progid[id];
            tags[id] = tag;
            progid[id] = pid;
            times[id] = accesstime;
            return rt;
        }

        void Flush(){
            for(uint32_t i = 0; i < associativity; i++){
                times[i] = 0;
                tags[i] = 0;
            }
        }
};

template<UINT32 STORE_ALLOCATION>
class CATCACHE : public CACHE_BASE{
    private:
        vector<CATSet> sets;
        uint64_t *alloc;
        uint64_t occu_count[256];
    public:
        CATCACHE(std::string name, UINT32 cacheSize, UINT32 lineSize, UINT32 associativity) : CACHE_BASE(name, cacheSize, lineSize, associativity){
            alloc = NULL;
            for(UINT32 i = NumSets(); i > 0; i--)
                sets.push_back(CATSet(associativity, alloc));
            memset(occu_count, 0, sizeof(occu_count));
        }

        CATCACHE(std::string name, UINT32 cacheSize, UINT32 lineSize, UINT32 associativity, vector<uint64_t> _alloc) : CACHE_BASE(name, cacheSize, lineSize, associativity){
            alloc = new uint64_t[associativity];
            for(uint32_t i = 0; i < associativity; i++)
                alloc[i] = _alloc[i];
            for(UINT32 i = NumSets(); i > 0; i--)
                sets.push_back(CATSet(associativity, alloc));
            memset(occu_count, 0, sizeof(occu_count));
        }

        bool Access(uint8_t pid, ADDRINT addr, UINT32 size, ACCESS_TYPE accessType){
            const ADDRINT highAddr = addr + size;
            bool allHit = true;
            const ADDRINT lineSize = LineSize();
            const ADDRINT notLineMask = ~(lineSize - 1);
            do
            {
                CACHE_TAG tag;
                UINT32 setIndex;
                SplitAddress(addr, tag, setIndex);

                CATSet & set = sets[setIndex];
                bool localHit = set.Find(tag, pid);
                allHit &= localHit;
                if ( (! localHit) && (accessType == ACCESS_TYPE_LOAD || STORE_ALLOCATION == CACHE_ALLOC::STORE_ALLOCATE))
                {
                    if(set.Replace(tag, pid))
                        occu_count[pid]++;
                }
                addr = (addr & notLineMask) + lineSize;
            }
            while (addr < highAddr);
            _access[accessType][allHit]++;
            return allHit;
        }

        bool AccessSingleLine(uint8_t pid, ADDRINT addr, ACCESS_TYPE accessType){
            CACHE_TAG tag;
            UINT32 setIndex;

            SplitAddress(addr, tag, setIndex);
            CATSet & set = sets[setIndex];

            bool hit = set.Find(tag, pid);
            if ( (! hit) && (accessType == ACCESS_TYPE_LOAD || STORE_ALLOCATION == CACHE_ALLOC::STORE_ALLOCATE))
            {
                if(set.Replace(tag, pid))
                    occu_count[pid]++;
            }

            _access[accessType][hit]++;

            return hit;
        }

        bool RemoveSingleLine(uint8_t pid, ADDRINT addr, ACCESS_TYPE accessType){
            if(addr == 0)
                return false;
            CACHE_TAG tag;
            UINT32 setIndex;
            SplitAddress(addr, tag, setIndex);
            CATSet &set = sets[setIndex];
            bool hit = set.FindAndRemove(tag, pid);
            if(hit){
                _access[accessType][!hit]--;
                _access[accessType][hit]++;
            }
            return hit;
        }

        void Flush(){
            for (INT32 index = NumSets(); index >= 0; index--) {
                sets[index].Flush();
            }
            IncFlushCounter();
        }
        void ResetStats(){
            for (UINT32 accessType = 0; accessType < ACCESS_TYPE_NUM; accessType++){
                _access[accessType][false] = 0;
                _access[accessType][true] = 0;
            }
            IncResetCounter();
        }

        uint64_t* GetOccuCount(){
            return occu_count;
        }
};


template <UINT32 STORE_ALLOCATION, class LRUSet = _LRUSet>
class LRUFilter : public CACHE_BASE{
    private:
        std::vector<LRUSet> sets;
    public:
        LRUFilter(std::string name, UINT32 cacheSize, UINT32 lineSize, UINT32 associativity) : CACHE_BASE(name, cacheSize, lineSize, associativity)
    {
        for(UINT32 i = NumSets(); i > 0; i--)
            sets.push_back(LRUSet(associativity));
    }
        bool Access(ADDRINT addr, ACCESS_TYPE accessType, ADDRINT &victim){
            return AccessSingleLine(addr, accessType, victim);
        }

        bool AccessSingleLine(ADDRINT addr, ACCESS_TYPE accessType, ADDRINT &victim){
            CACHE_TAG tag;
            UINT32 setIndex;

            SplitAddress(addr, tag, setIndex);
            LRUSet & set = sets[setIndex];

            bool hit = set.Find(tag, false);
            if ( (! hit) && (accessType == ACCESS_TYPE_LOAD || STORE_ALLOCATION == CACHE_ALLOC::STORE_ALLOCATE))
            {
                victim = (ADDRINT)(set.Replace(tag, true));
                victim <<= 6; // restore the cache block address
            }

            _access[accessType][hit]++;

            return hit;
        }
        void Flush(){
            for (INT32 index = NumSets(); index >= 0; index--) {
                sets[index].Flush();
            }
            IncFlushCounter();
        }
        void ResetStats(){
            for (UINT32 accessType = 0; accessType < ACCESS_TYPE_NUM; accessType++){
                _access[accessType][false] = 0;
                _access[accessType][true] = 0;
            }
            IncResetCounter();
        }
};
