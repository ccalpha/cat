library(ggplot2)
library(data.table)

data<-read.table("./2classified.data", header = TRUE)
attach(data)
aggdata<-aggregate(data, by=list(Class0, Class1), FUN=mean, na.rm=TRUE)
print(aggdata)
detach(data)

