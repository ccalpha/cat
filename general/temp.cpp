#include "footprint_reborn.h"

using namespace std;

int main(){
    FPDist<> x;
    x.OnReference(111);
    x.OnReference(123);
    x.OnReference(111);
    
    PreciseFPDist<> y;
    y.OnReference(111);
    y.OnReference(123);
    y.OnReference(111);
    
    return 0;
}
