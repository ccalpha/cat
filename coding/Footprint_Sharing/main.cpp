//
//  main.cpp
//  Footprint_Sharing
//
//  Created by yechencheng on 5/6/15.
//  Copyright (c) 2015 yechencheng. All rights reserved.
//

#include <iostream>
#include <climits>
#include "footprint.h"
#include "TraceGenerator.h"
#include "CacheSimulator.h"
#include "TraceReader.h"

using namespace std;

bool LRUSimEnabled = false;

//Two streams, verify FP
void TestCase1(){
    cout << "Test case 1 : " << endl;
    int64_t n = 1000;
    Trace **T = new Trace*[2];
    T[0] = StreamTrace(n, 9);
    T[1] = RandomTrace(n, 8);
    CacheStack *CSLRU = new LRUCache(16);
    cout << "Sim : " << RandomInterleave(CSLRU, T, 2) << endl;

    FPDist **d = new FPDist*[2];
    d[0] = GetFootprint(T[0]);
    d[1] = GetFootprint(T[1]);
    cout << "FP : " << (*d[0]+*d[1]).GetMissRatio(16) << endl;

    cout << "Test case 1 over" << endl;

}

void TestCase2(int64_t n = 10000, int64_t m = 300, int64_t ssize = 16, int64_t dsize = 112){
    cout << "Test case 1 : " << endl;
    ssize = 16;
    dsize = 112;

    //setup trace
    Trace **T = new Trace*[2];
    T[1] = RandomTrace(n, m);
    T[0] = StreamTrace(n, m);
    int64_t *TraceType = new int64_t[2];
    TraceType[0] = 0;   //Trace which only impact shared parts
    TraceType[1] = 1;   //Trace which occupy dedicated parts

    //setup fp
    FPDist **d = new FPDist*[2];
    d[0] = GetFootprint(T[0]);
    d[1] = GetSFootprint(T[1], dsize);

    //simulate results of split stack
    CacheStack *PSLRU = new PSCache(ssize, dsize);
    double rt = RandomInterleave(PSLRU, T, 2, TraceType);
    cout << "Stack (All) : " << rt << endl;

    //Figure out whole miss ratio with access rate(AR[1] is the aceeses that evcited into L)
    //Wrong AR, since LFP internally remove the data kept in H
    FPDist *td = GetFootprint(T[1]);
    int64_t *AR = new int64_t[2];
    AR[0] = T[0]->N;
    AR[1] = T[1]->N * td->GetMissRatio(dsize);
    cout << "PSFP(AR) : " << GetMissRateAR(d, 2, ssize, AR) << endl;

    //Another AR, do not consider the data evicted into L
    //Right AR
    AR[0] = T[0]->N;
    AR[1] = T[1]->N;
    double *pos = new double[2];
    cout << "PSFP(AR,N) : " << GetMissRateAR(d, 2, ssize, AR, pos) << endl;

}

//Compare LFP with LRU
void TestCase3(int64_t ssize, int64_t dsize, Trace* t){
    cout << "Test Case : Comparing LFP with LRU, Single Program" << endl;

    LRUCache L(ssize+dsize);
    int64_t hit_count = 0;
    for(int64_t i = 0; i < t->N; i++){
        int64_t rt = L.push(t->A[i]);
        if(rt == LRUAccess::DataHit)
            hit_count++;
    }
    cout << "LRU Miss Ratio : " << 1- hit_count / (double)t->N << endl;

    FPDist *fd = GetLFootprint(t, dsize);
    double mr = fd->GetMissRatio(ssize);
    cout << "LFP Miss Ratio : " << mr << endl;
}

//Compare LFP with HOTL
void TestCase4(int64_t ssize, int64_t dsize, Trace*t){
    cout << "Test Case : Comparing LFP with FP, Single Program" << endl;
    FPDist *fd = GetHOTLFootprint(t);
    cout << "HOTL Miss Ratio  : " << fd->GetMissRatio(ssize+dsize) << endl;
    FPDist *lfd = GetLFootprint(t, dsize);
    cout << "LFP Miss Ratio : " << lfd->GetMissRatio(ssize) << endl;
        //FPDist *nfd = GetLFootprint_Naive(t, dsize);


    //cout << "Naive LFP Miss Ratio : " << nfd->GetMissRatio(ssize) << endl;
}

void TestPSLRU(int64_t ssize, int64_t dsize, int64_t nt, Trace** ts){
    CacheStack *PS = new PSCache(ssize, dsize);
    int64_t *pushTo = new int64_t[nt];
    fill(pushTo, pushTo + nt, 0);
    pushTo[0] = 1;
    int64_t hits[nt];
    double mr = RandomInterleave(PS, ts, nt, pushTo, hits);
    //double mr = RoundRobinInterleave(PS, ts, nt, pushTo);
    
    cout << "Sim Miss Ratio : " << mr << endl;
    cout << "Individual Misses : ";
    for(int i = 0; i < nt; i++)
        cout << " " << 1 - hits[i] / (double)ts[i]->N;
    cout << endl;
    delete PS;
}

//Compare LFP with simulation(exclusive cache)
void TestCase5(int64_t ssize, int64_t dsize, int64_t nt, Trace** ts){
    FPDist *fps[nt];

    fps[0] = GetSFootprint2(ts[0], dsize);
    //fps[0] = GetLFootprint(ts[0], dsize);
    //fps[0] = GetSFootprint(ts[0], dsize);
    //fps[0] = GetLFootprint_Naive(ts[0], dsize);
    //AdjustWindowLFootprint(fps[0], dsize);

    for(int64_t i = 1; i < nt; i++){
       fps[i] = GetHOTLFootprint(ts[i]);
    }

    int64_t AR[nt];
    double epos[nt];
    for(int i = 0; i < nt; i++)
        AR[i] = fps[i]->N;

    int64_t misses = 0;
    int64_t acces = ts[0]->N+ts[1]->N;
    misses += min(ts[0]->getM(), dsize);
    misses += min(ssize, max(ts[0]->getM()-dsize, (int64_t)0)+ts[1]->getM());
    misses += min(ts[1]->getM(), dsize);
    misses = 0;
    
    double mrl = GetMissRateAR(fps, nt, ssize, AR, epos);
    cout << "LFP Miss Ratio : " << mrl+misses/(double)acces << endl;
    cout << "Individual Misses :";
    for(int i = 0; i < nt; i++){
        int64_t p = epos[i];
        cout << " " << (fps[i]->FP[p+1] - fps[i]->FP[p]) / (fps[i]->W[p+1] - fps[i]->W[p]);
        //cout << " " << fps[i]->FP[p] << " " << fps[i]->W[p] << endl;
    }
    cout << endl;
}

//Validate HOTL and FP
void TestCase6(int64_t ssize, int64_t dsize, Trace *t){
    FPDist *fd1 = GetFootprint(t);
    FPDist *fd2 = GetHOTLFootprint(t);
    for(int64_t i = 0; i < t->getM(); i++){
        if(abs(fd1->FP[i] - fd2->FP[i]) > 10e-7){
            cout << fd1->FP[i] << " " << fd2->FP[i] << endl;
        }
    }
    cout << "Comparing HOTL and FP Over" << endl;
}

//Compare HOTL with simulation(composible)
void TestCase7(int64_t ssize, int64_t dsize, int64_t nt, Trace** ts){
    FPDist *fps[nt];
    for(int64_t i = 0; i < nt; i++){
        fps[i] = GetHOTLFootprint(ts[i]);
    }

    int64_t AR[nt];
    double epos[nt];
    for(int i = 0; i < nt; i++)
        AR[i] = ts[i]->N;
    double mrl = GetMissRateAR(fps, nt, dsize + ssize, AR, epos);
    cout << "HOTL Miss Ratio : " << mrl << endl;
    cout << "Individual Misses : ";
    for(int i = 0; i < nt; i++){
        cout << " " << (*fps[i])[epos[i]+1] - (*fps[i])[epos[i]];
    }
    cout << endl;
}

//Test negative value in LFP
void TestCase8(int64_t ssize, int64_t dsize, int64_t nt, Trace **ts){
    bool flag = true;
    int cnt = 0;
    Trace *t = nullptr;
    FPDist *f = nullptr;

    dsize = 3;
    while(flag && cnt < 100){
        t = RandomTrace(10, 5);
        f = GetLFootprint(t, dsize);

        for(int i = 0; i < f->M; i++){
            if(f->FP[i] < 0){
                flag = false;
                break;
            }
        }
        cnt++;
    }
    if(flag == false){
        for(int i = 0; i < t->N; i++)
            cout << t->A[i] << " ";
        cout << endl;
        OutputFPDist(f);
    }
}

//Case for negative values in LFP
void TestCase9(){
    int n = 10;
    int h = 3;
    Trace t(n);
    //int64_t a[] = {4, 0, 1, 3, 1, 3, 4, 0, 0, 2};
    int64_t a[] = {9,6,9,6,6,6,9,7,8,9};

    for(int i = 0; i < n; i++)
        t.A = a;
    FPDist *f = GetLFootprint(&t, h);
    OutputFPDist(f);
}

//Test CCFP -- PASSED
void TestCase10(int64_t nt, Trace** ts){
    for(int64_t i = 0; i < nt; i++){
        cout << ts[i]->name << endl;
        //OutputTrace(ts[i]);
        //int64_t a[10] = {0,1,2,0,1,1,0,0,1,0};
        //ts[i]->A = a;
        FPDist *cfp = GetCFootprint(ts[i]);
        FPDist *hfp = GetHOTLFootprint(ts[i]);
        //FPDist *hfp = GetFootprint(ts[i]);
        for(int64_t j = 0; j < cfp->M; j++){
            if(abs((cfp->FP[j] - hfp->FP[j]) / hfp->FP[j]) > 1e-5){
                cerr << "ERROR : " << j << " " << cfp->W[j] << " " << cfp->FP[j] << " " << hfp->FP[j] << endl;
                break;
            }

        }
    }
}

void TestCase11(int64_t ssize, int64_t dsize, int64_t nt, Trace** ts){
    for(int64_t i = 0; i < nt; i++){
        cout << ts[i]->name << endl;
        OutputTrace(ts[i]);
        int64_t a[] = {0,1,0,1,0,1,0,2,2,2};
        ts[i]->A = a;

        FPDist *sfp2 = GetSFootprint2(ts[i], dsize);
        FPDist *sfp = GetSFootprint(ts[i], dsize);
        for(int64_t j = 0; j < sfp->M; j++){
            if(abs((sfp2->FP[j] - sfp->FP[j]) / sfp->FP[j]) > 1e-5){
                cerr << "ERROR : " << j << " " << sfp->W[j] << " " << sfp2->FP[j] << " " << sfp->FP[j] << endl;
                break;
            }
        }
    }
}

void TestCase12(Trace* ts){
    int64_t H = 57344;
    FPDist *sfp = GetSFootprint2(ts, H);
    FPDist *fp = GetHOTLFootprint(ts);
    /*
    for(int64_t i = 0; i < 100; i++)
        cout << sfp->FP[i] << " " << fp->FP[i] << endl;
     */
    double val = sfp->FP[0] + H;
    int64_t pos = upper_bound(fp->FP, (fp->FP)+fp->M, val) - fp->FP;
    if(abs(fp->FP[pos-1] - val) < abs(fp->FP[pos] - val))
        pos--;
    cout << pos << endl;
    double err = 0;
    double lmin = INT_MAX, lmax = 0;
    for(int64_t i = 0, j = pos; j < fp->M; i++, j++){
        double x = 1-(sfp->FP[i]+H)/fp->FP[j];
        lmin = min(x, lmin);
        lmax = max(x, lmax);
        err += abs(x);
    }
    cout << err << " " << err / (ts->N-pos) << " " << lmin << " " << lmax << endl;
}

void TestCase13(int64_t ssize, int64_t dsize, int64_t nt, Trace** ts){
    FPDist *fps[2];
    fps[0] = GetHOTLFootprint(ts[0]);
    fps[1] = GetHOTLFootprint(ts[1]);
    FPDist sfp(fps[0]->N);
    
    int64_t pos = upper_bound(fps[0]->FP, fps[0]->FP+fps[0]->M, fps[0]->FP[0]+dsize) - fps[0]->FP;
    --pos;
    for(int64_t i = 0, j = pos; j < sfp.M; i++, j++){
        sfp.W[i] = fps[0]->W[i];
        sfp.FP[i] = fps[0]->FP[j] - dsize;
    }
    sfp.M -= (pos+1);
    fps[0] = &sfp;
    
    int64_t AR[nt];
    double epos[nt];
    for(int i = 0; i < nt; i++)
        AR[i] = fps[i]->N;
    //FPDist *tmp = GetHOTLFootprint(ts[0]);
    //AR[0] = ts[0]->N * tmp->GetMissRatio(dsize);
    
    double mrl = GetMissRateAR(fps, nt, ssize, AR, epos);
    cout << "LFP Miss Ratio : " << mrl << endl;
    cout << "Individual Misses : " << endl;
    for(int i = 0; i < nt; i++){
        cout << " " << (*fps[i])[epos[i]+1] - (*fps[i])[epos[i]];
        //cout << " " << fps[i]->FP[p] << " " << fps[i]->W[p] << endl;
    }
    cout << endl;
}

//Compare VFP with simulation(exclusive cache)
void TestCase14(int64_t ssize, int64_t dsize, int64_t nt, Trace** ts){
    FPDist *fps[nt];
    fps[0] = GetVFootprint(ts[0]);
    ((VFPDist*)fps[0])->init((double)dsize);
    for(int64_t i = 1; i < nt; i++){
        fps[i] = GetHOTLFootprint(ts[i]);
    }
    
    int64_t AR[nt];
    double epos[nt];
    for(int i = 0; i < nt; i++)
        AR[i] = fps[i]->N;
    
    double mrl = GetMissRateAR(fps, nt, ssize, AR, epos);
    cout << "VFP Miss Ratio : " << mrl << endl;
    cout << "Individual Misses :";
    for(int i = 0; i < nt; i++){
        cout << " " << (*fps[i])[epos[i]+1] - (*fps[i])[epos[i]];
    }
    cout << endl;
    
    delete fps[0];
}

//Show inconsisitency of footprint
void TestCase15(int64_t ssize, int64_t dsize, int64_t nt, Trace** ts){
    FPDist *fps[nt];
    for(int64_t i = 0; i < nt; i++){
        fps[i] = GetHOTLFootprint(ts[i]);
    }
    
    for(int i = 0; i < nt; i++){
        OutputTrace(ts[i]);
        for(int j = 2; j < fps[i]->M; j++){
            if(fps[i]->FP[j]-fps[i]->FP[j-1] > fps[i]->FP[j-1]-fps[i]->FP[j-2]){
                cout << fps[i]->FP[j] << " " << fps[i]->FP[j-1] << " " << fps[i]->FP[j-2] << endl;
            }
        }
    }
}

//Show if there is a constant offset between VFP and ILFP
void TestCase16(int64_t ssize, int64_t dsize, int64_t nt, Trace** ts){
    for(int64_t i = 0; i < nt; i++){
        //VFPDist x(*GetHOTLFootprint(ts[i]));
        //x.init(dsize);
        FPDist x(*GetHOTLFootprint(ts[i]));
        FPDist y(*GetSFootprint2(ts[i], dsize));
        int cnt = 0;
        for(int i = 0; i < y.M; i++){
            if(abs(x[i] - y[i]) > 1e-7){
                cnt++;
                if(cnt % 10000 != 0) continue;
                cout << i << " " << x[i] << " " << y[i] << " " << x.GetFT(y[i]) << endl;
            }
        }
    }
}

//measure precision of footprints, tune lfp
void TestCase17(Trace *t){
    const int len = 26;
    const int64_t sizes[len] = {8192,16384,24576,32768,40960,49152,57344,65536,73728,81920,90112,98304,106496,114688,122880,131072,196608,262144,327680,393216,458752,524288,589824,655360,720896,786432};
    
    for(int i = 0; i < len; i++){
        cout << "Size : " << sizes[i] << endl;
        
        CacheStack *s = new LRUCache(sizes[i]);
        int64_t misses = 0;
        for(int64_t i = 0; i < t->N; i++){
            if(s->push((*t)[i]) != LRUAccess::DataHit)
                misses++;
        }
        misses -= t->getM();
        cout << "\tLRU : " << misses / (double)t->N << endl;
        delete s;
        
        FPDist *fp = GetHOTLFootprint(t);
        cout << "\tFP : " << fp->GetMissRatio(sizes[i]) << endl;
        
        cout << "\tLFP : " << endl;
        for(int64_t j = 1024; j < sizes[i] && j < t->getM(); j += 1024){
            FPDist *lfp = GetLFootprint(t, j);
            cout << "\t\t" << j << " " << lfp->GetMissRatio(sizes[i]-j) << endl;
            delete lfp;
        }
    }
}


// ssize, dsize, trace1, trace2, trace3 ....
int main(int argc, const char * argv[]) {
    //int64_t n = atoi(argv[1]);
    //int64_t m = atoi(argv[2]);

    int64_t ssize = 1 * (1<<13);
    int64_t dsize = 7 * (1<<13); //cacheline = 128

    ssize = atoi(argv[1]);
    dsize = atoi(argv[2]);

    //ssize = 65536;
    //dsize = 32768;

    ssize = 8192;
    dsize = 57344;
    
    //dsize = 3584;
    //ssize = 512;
    //dsize = 448;
    //dsize = 357;
    //ssize = 123;

    //TestCase8(1, 1, 1, NULL);
    //TestCase9();
    //return 0;

    int64_t nt = argc - 3;
    
    Trace **ts = new Trace*[nt];
    int64_t off = 0;
    
    for(int ii = 3; ii < 3+nt; ii++){
        int i = ii-3;
        ts[i] = ReadTrace(argv[ii]);
        //ts[i]->N = 100000;
        //ts[i] = RandomTrace(10, 3);
        ts[i]->NormalizeAddr(off);
        off += ts[i]->getM();
        cout << ts[i]->name << " " << ts[i]->N << " " << ts[i]->getM() << endl;
        //TestCase12(ts[i]);
        TestCase17(ts[i]);
        
    }
    return 0;
    
    Trace *tts[nt];
    if(LRUSimEnabled){
        for(int i = 0; i < nt; i++){
            tts[i] = new Trace(*ts[i]);
            tts[i]->AddrOffset(off);
            off += tts[i]->getM();
        }
    }
    else{
        for(int i= 0; i < nt; i++)
            tts[i] = ts[i];
    }
    
    //TestCase11(ssize, dsize, nt, ts);
    cout << "H : " << dsize << " L : " << ssize << endl;
    for(int i = 0; i < nt; i++)
        for(int j = 0; j < nt; j++){
            Trace *x[2];
            x[0] = ts[i];
            x[1] = tts[j];
            
            /*
            x[0] = new Trace(*ts[i]);
            x[1] = new Trace(*ts[j]);
            x[1]->AddrOffset(x[0]->getM());
            */
            
            cout << x[0]->name << " ";
            cout << x[1]->name << endl;
            
            //TestCase15(ssize, dsize, 2, x);
            //TestPSLRU(ssize, dsize, 1, x);
            //TestCase5(ssize, dsize, 2, x);
            TestCase14(ssize, dsize, 2, x);
            //TestCase7(ssize, dsize, 2, x);
            //TestCase13(ssize, dsize, 2, x);
            //delete x[0];
            //delete x[1];
        }
}
