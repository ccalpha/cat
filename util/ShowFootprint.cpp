#include <iostream>
#include <string>
#include <boost/program_options.hpp>

#include "footprint_reborn.h"
#include "footprintManager_reborn.h"
//#include "footprint.h"
//#include "footprintManager.h"
#include "general.h" 

using namespace std;
double QueryFootprint(FPDist &fp, char type, double val){
   switch(type){
       case 'w': return FPUtil::GetFP(&fp, val);break;
       case 't': return FPUtil::GetFT(&fp, val);break;
       case 'c': return FPUtil::GetMissRatio(&fp, val); break; 
//        case 'c' : return fp.GetMissRatioColdMiss(val)*64-fp.GetMissRatio(val)*63;break;
       default: cout << "Unknown type : " << type << endl; return -1; break;
   }
}

int main(int argc, char** argv){
    namespace po = boost::program_options;
    
    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "show help messages")
        ("query,q", "query mode: [w|t] value")
        ("window,w", po::value< vector<double> >(), "windows to footprint")
        ("filltime,t", po::value< vector<double> >(), "cache size to fill time")
        ("cache,c", po::value<vector<double>>(), "cache lines to miss ratio, cold misses counted")
        ("footprint,f", po::value<string>()->required(), "file of footprint")
        ("showinfo,s", "show information of footprint")
        //("reusetime,R", "the file of reuse time histogram")
        ("raw,r", "translate raw file to human readable formation(TODO)")
    ;
    po::positional_options_description p;
    p.add("footprint", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    if(vm.count("help")){
        cout << desc << endl;
        return 0;
    }
    po::notify(vm);
    cout.precision(10);
    if(vm.count("translate")){
        cout << "To be implemented" << endl;
    }

    FPDist &fp = *(FPDist::deserialize(vm["footprint"].as<string>()));
    if(vm.count("showinfo")){
        cout << "Number of Accesses : " << FPUtil::GetNAccesses(&fp) << endl;
        cout << "Number of Data : " << FPUtil::GetNData(&fp) << endl;
    }

    if(vm.count("cache")){
        for(auto i : vm["cache"].as<vector<double>>())
            cout << "miss ratio " << i << " " << QueryFootprint(fp, 'c', i) << endl;
    }

    if(vm.count("window")){
        for(auto i : vm["window"].as< vector<double> >())
            cout << "footprint " << i << " " << QueryFootprint(fp, 'w', i) << endl;
    }

    if(vm.count("filltime")){
        for(auto i : vm["filltime"].as<vector<double> >())
            cout << "filltime " << i << " " << QueryFootprint(fp, 't', i) << endl;
    }

    if(vm.count("query")){
        string type;
        double value;
        while(cin >> type >> value){
            char t = type[0];

            switch(t){
                case 'w': cout << "window ";break;
                case 't': cout << "filltime ";break;
                case 'c': cout << "missratio "; break;
                default: cout << "Unknow type : " << type << endl;break;
            }
            cout << value << " " << QueryFootprint(fp, t, value) << endl; 
        }
    }

    if(vm.count("raw")){
        cout << "TODO" << endl;
    }
    return 0;
}
