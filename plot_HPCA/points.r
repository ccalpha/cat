library(ggplot2)
library(data.table)
library("gridExtra")
library("cowplot")
fontsize <- 32
gcc <- read.table("./gcc.wss", header=FALSE)
gcc <- gcc[1000:2000,]
gcc <- melt(gcc, V1)

fancy_scientific <- function(l) {
     # turn in to character string in scientific notation
     l <- format(l, scientific = TRUE)
     l <- gsub("0\\.0e\\+00","0",l)
     # quote the part before the exponent to keep all the digits
     #l <- gsub("e\\+", "e", l)
     l <- gsub("e\\+0", "e\\+", l)
     # turn the 'e+' into plotmath format
     #l <- gsub("e", "%*%10^", l)
     # return this as an expression
     #parse(text=l)
     text=l
}

gcc_plot <- ggplot(gcc) +
  geom_line(aes(x=seq(1000,1000-1+length(value)),y=value, color=factor(2, labels=c("gcc")))) +
  labs(x="Accesses(x1M)", y="Working set size") +
  theme_gray()+
  theme(text = element_text(size=fontsize))+
  theme(legend.justification=c(0,1),legend.position=c(0,1)) +
  theme(aspect.ratio=0.5)+
  theme(axis.text.y = element_text(angle = 45))+
  scale_y_continuous(labels=fancy_scientific)+ 
  guides(color=guide_legend(title = NULL))




soplex <- read.table("./soplex.wss", header=FALSE)
soplex <- soplex[1000:2000,]
soplex <- melt(soplex, V1)

soplex_plot <- ggplot(soplex) +
  geom_line(aes(x=seq(1000,1000-1+length(value)),y=value, color=factor(2, labels=c("soplex")))) +
  labs(x="Accesses(x1M)", y=element_blank()) +
  theme_gray()+
  theme(text = element_text(size=fontsize))+
  theme(legend.justification=c(0,1),legend.position=c(0,1)) +
  theme(aspect.ratio=0.5)+
  theme(axis.text.y = element_text(angle = 45))+
  scale_y_continuous(labels=fancy_scientific)+ 
  guides(color=guide_legend(title = NULL))


lbm <- read.table("./lbm.wss", header=FALSE)
lbm <- lbm[1000:2000,]
lbm <- melt(lbm, V1)

lbm_plot <- ggplot(lbm) +
  geom_line(aes(x=seq(1000,1000-1+length(value)),y=value, color=factor(2, labels=c("lbm")))) +
  labs(x="Accesses(x1M)", y=element_blank()) +
  theme_gray()+
  theme(text = element_text(size=fontsize))+
  theme(legend.justification=c(0,1),legend.position=c(0,1)) +
  theme(aspect.ratio=0.5)+
  theme(axis.text.y = element_text(angle = 45))+
  scale_y_continuous(labels=fancy_scientific, breaks=c(1e+5, 1.5e+5, 2e+5))+ 
  guides(color=guide_legend(title = NULL))



wrf <- read.table("./wrf.wss", header=FALSE)
wrf <- wrf[1000:2000,]
wrf <- melt(wrf, V1)

wrf_plot <- ggplot(wrf) +
  geom_line(aes(x=seq(1000,1000-1+length(value)),y=value, color=factor(2, labels=c("wrf")))) +
  labs(x="Accesses(x1M)", y=element_blank()) +
  theme_gray()+
  theme(text = element_text(size=fontsize))+
  theme(legend.justification=c(0,1),legend.position=c(0,1)) +
  theme(aspect.ratio=0.5)+
  theme(axis.text.y = element_text(angle = 45))+
  scale_y_continuous(labels=fancy_scientific)+ 
  guides(color=guide_legend(title = NULL))

#grid.arrange(soplex_plot, gcc_plot, lbm_plot, wrf_plot, ncol=2)
#grid.arrange(gcc_plot, soplex_plot, ncol = 2)
#grid.arrange(lbm_plot, wrf_plot, ncol = 2)
gcc_plot
soplex_plot
lbm_plot
wrf_plot
