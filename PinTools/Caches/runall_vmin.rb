#!/usr/bin/ruby

require 'thread'

dirs = File.readlines("dirs.txt")
commands = File.readlines("commands.txt")
names = File.readlines("names_test.txt")
#times = File.readlines("solo_time")
#ft = File.readlines("../Traces/fp_test/filltime_8M")

target = "/localdisk/cye/cat/PinTools/vmin/sampling_result_3"
PIN_ROOT = "/localdisk/cye/pin"
TOOL_S = "/localdisk/cye/cat/PinTools/Caches/obj-intel64/SimulatorVMIN.so"
#TOOL_S = "/localdisk/cye/cat/PinTools/Caches/obj-intel64/SimulatorFilter.so"
ROOT = "/localdisk/cye/cat/PinTools/Caches"

queue = Queue.new
#for i in [3]
#for i in [5,13,22,23]
for i in 0...dirs.length
    #check if it is read
    #if(names[i].start_with?("#") || names[i].start_with?("$"))
    #    next
    #end

#    threads << Thread.new(i) do |i|
        #find the length
        length = 0
        tmp = `grep "Misses : " ../vmin/sampling_result/#{names[i].strip}.str | tail -n 1`.split[2]
        length += tmp.to_i
        tmp = `grep "Write back : " ../vmin/sampling_result/#{names[i].strip}.str | tail -n 1`.split[3]
        length += tmp.to_i

        #queue << "cd #{dirs[i].strip}; #{PIN_ROOT}/pin -t #{TOOL_S} -o #{target}/#{names[i].strip}.str -m 1000000000 -b 10000000000 -w 8192 -- #{commands[i].strip}"
       # system("cd #{dirs[i].strip}; #{PIN_ROOT}/pin -t #{TOOL_S} -b 10000000000 -w 8192 -o #{target}/#{names[i].strip}.result -l #{length} -d 8192 -f #{ROOT}/../vmin/filltimes/#{names[i].strip}.fts  -- #{commands[i].strip}")
        #next

        queue << "cd #{dirs[i].strip}; #{PIN_ROOT}/pin -t #{TOOL_S} -b 10000000000 -w 8192 -o #{target}/#{names[i].strip}.result -l #{length} -d 4096 -f #{ROOT}/../vmin/filltimes/#{names[i].strip}.fts  -- #{commands[i].strip}"
#    end

    next

    if(ft[i].to_f == 1e20)
        next
    end
    system("echo #{ft[i].strip} > #{target}/ftlist")
    system("cd #{dirs[i].strip}; #{PIN_ROOT}/pin -t #{TOOL_T} -o #{target}/#{names[i].strip}.ftr -f #{target}/ftlist -- #{commands[i].strip}")
end

threads = []
4.times do |i|
    threads << Thread.new do
        while(e = queue.pop(true) rescue nil)
            puts e
            pid = spawn("#{e}")
            Process.wait pid
        end
    end
end

threads.each{|t| t.join}
