#include "RTSampling.h"
#include "TraceReader.h"
#include "footprint.h"

int main(int argc, char **argv){
    SimpleTraceReader trace("../PinTools/Traces_continous/lbm.trace");
    FPDist fpdist;
    RTSampling rts(atoi(argv[1]));

    fpdist.InitCalculation();
    for(size_t i = 0; i < trace.GetNumRef(); i++){
        uint64_t addr = trace.GetNextRef() >> 6;

        uint64_t atime = rts.Access(addr, i);
        if(atime){ //update upon hit
            fpdist.Access(addr, i);
            //cout << "Hit or Filling : " << addr << endl;
        }
        if(atime || (i & 0xfffff) == 0){
            uint64_t updated_addr = rts.Update(i);
            if(updated_addr){
                fpdist.Access(updated_addr, i);
                //cout << "Update : " << updated_addr << endl;
            }
        }
    }
    fpdist.FinishCalculation();
    fpdist.OutputToFile("footprint");
    return 0;
}
