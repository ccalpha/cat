#include <iostream>
#include <string>
#include <sstream>
#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters

#include "pin_cache.H"
#include "../../general/footprint.h"
#include "../general/array_register.h"

#define MALLOC "malloc"
#define CALLOC "acalloc"
#define FREE "free"
#define REALLOC "arealloc"

KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "FootprintDist", "Output file");
KNOB<UINT64> KnobRefCount(KNOB_MODE_WRITEONCE, "pintool", "n", "1000000000000", "Reference count of Program");
KNOB<UINT64> KnobLineSize(KNOB_MODE_WRITEONCE, "pintool", "l", "7", "log2 of cache line size, default 7");

vector<FPDist*> fpds;
//ArrayRegister ary_reg;
//MergeEngine ary_reg;
ArrayEngine ary_reg;
UINT64 refCount = 0;
UINT64 floor2CacheLine = 7;
UINT64 mallocCount = 0;

LOCALFUN VOID Fini(int code, VOID * v)
{
  cout << "Number of Arrays : " << ary_reg.GetNumberOfArray() << endl;
//ofstream fout(KnobOutput.Value().c_str());
//fout << "Number of Arrarys : " << ary_reg.GetNumberOfArray() << endl;
//fout.close();

  for(unsigned int i = 0; i < fpds.size(); i++){
    fpds[i]->FinishCalculation(refCount);
    fpds[i]->OutputToFile(KnobOutput.Value().c_str());
    //std::stringstream ss;
    //ss << KnobOutput.Value() << "." << i;
    //fpds[i]->OutputToFile(ss.str());
  }
}


LOCALFUN VOID MemRefMulti(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
  refCount++;
  //return ;
  int id = ary_reg.GetID(addr);
  while((int)fpds.size() <= id){
    fpds.push_back(new FPDist(KnobRefCount.Value()));
  }
  fpds[id]->Access(addr >> floor2CacheLine, refCount);
}

LOCALFUN VOID MemRefSingle(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
  refCount++;
  //return ;
  int id = ary_reg.GetID(addr);
  while((int)fpds.size() <= id){
    fpds.push_back(new FPDist(KnobRefCount.Value()));
  }
  fpds[id]->Access(addr >> floor2CacheLine, refCount);

}

LOCALFUN VOID Instruction(INS ins, VOID *v)
{
    if (INS_IsMemoryRead(ins) && INS_IsStandardMemop(ins))
    {
        const UINT32 size = INS_MemoryReadSize(ins);
        const AFUNPTR countFun = (size <= 4 ? (AFUNPTR) MemRefSingle : (AFUNPTR) MemRefMulti);

        // only predicated-on memory instructions access D-cache
        INS_InsertPredicatedCall(
            ins, IPOINT_BEFORE, countFun,
            IARG_MEMORYREAD_EA,
            IARG_MEMORYREAD_SIZE,
            IARG_UINT32, CACHE_BASE::ACCESS_TYPE_LOAD,
            IARG_END);
    }

    if (INS_IsMemoryWrite(ins) && INS_IsStandardMemop(ins))
    {
        const UINT32 size = INS_MemoryWriteSize(ins);
        const AFUNPTR countFun = (size <= 4 ? (AFUNPTR) MemRefSingle : (AFUNPTR) MemRefMulti);

        // only predicated-on memory instructions access D-cache
        INS_InsertPredicatedCall(
            ins, IPOINT_BEFORE, countFun,
            IARG_MEMORYWRITE_EA,
            IARG_MEMORYWRITE_SIZE,
            IARG_UINT32, CACHE_BASE::ACCESS_TYPE_STORE,
            IARG_END);
    }
}

VOID MallocBefore(ADDRINT callee, ADDRINT size)
{
    ary_reg.BeforeMalloc(size);
}

VOID FreeBefore(ADDRINT ptr){
  ary_reg.FreeArray(ptr);
}

VOID MallocAfter(ADDRINT ret)
{
  ary_reg.AfterMalloc(ret);
}

VOID CallocBefore(ADDRINT callee, ADDRINT size, ADDRINT num){
  ary_reg.BeforeMalloc(size * num);
}

VOID ReallocBefore(ADDRINT callee, ADDRINT ptr, ADDRINT size){
  ary_reg.FreeArray(ptr);
  ary_reg.BeforeMalloc(size);
}

VOID Image(IMG img, VOID *v)
{
  RTN mallocRtn = RTN_FindByName(img, MALLOC);
  if (RTN_Valid(mallocRtn))
  {
      RTN_Open(mallocRtn);

      // Instrument malloc() to print the input argument value and the return value.
      RTN_InsertCall(mallocRtn, IPOINT_BEFORE, (AFUNPTR)MallocBefore,
                     IARG_ADDRINT, RTN_Address(mallocRtn),
                     IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                     IARG_END);
      RTN_InsertCall(mallocRtn, IPOINT_AFTER, (AFUNPTR)MallocAfter,
                     IARG_FUNCRET_EXITPOINT_VALUE, IARG_END);
      RTN_Close(mallocRtn);
  }

  // Find the free() function.
  RTN freeRtn = RTN_FindByName(img, FREE);
  if (RTN_Valid(freeRtn))
  {
      RTN_Open(freeRtn);
      // Instrument free() to print the input argument value.
      RTN_InsertCall(freeRtn, IPOINT_BEFORE, (AFUNPTR)FreeBefore,
                     IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                     IARG_END);
      RTN_Close(freeRtn);
  }

  RTN callocRtn = RTN_FindByName(img, CALLOC);
  if(RTN_Valid(callocRtn)){
    RTN_Open(callocRtn);
    RTN_InsertCall(callocRtn, IPOINT_BEFORE, (AFUNPTR)CallocBefore,
        IARG_ADDRINT, RTN_Address(callocRtn),
        IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
        IARG_FUNCARG_ENTRYPOINT_VALUE, 1,
        IARG_END);
    RTN_InsertCall(callocRtn, IPOINT_AFTER, (AFUNPTR)MallocAfter,
                       IARG_FUNCRET_EXITPOINT_VALUE, IARG_END);
    RTN_Close(callocRtn);
  }

  RTN reallocRtn = RTN_FindByName(img, REALLOC);
  if(RTN_Valid(reallocRtn)){
    RTN_Open(reallocRtn);
    RTN_InsertCall(reallocRtn, IPOINT_BEFORE, (AFUNPTR)ReallocBefore,
        IARG_ADDRINT, RTN_Address(reallocRtn),
        IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
        IARG_FUNCARG_ENTRYPOINT_VALUE, 1,
        IARG_END);
    RTN_InsertCall(reallocRtn, IPOINT_AFTER, (AFUNPTR)MallocAfter,
                       IARG_FUNCRET_EXITPOINT_VALUE, IARG_END);
    RTN_Close(reallocRtn);
  }
}

GLOBALFUN int main(int argc, char *argv[])
{
    PIN_InitSymbols();
    PIN_Init(argc, argv);
    floor2CacheLine = KnobLineSize.Value();

    IMG_AddInstrumentFunction(Image, 0);
    INS_AddInstrumentFunction(Instruction, 0);
    PIN_AddFiniFunction(Fini, 0);

    // Never returns
    PIN_StartProgram();

    return 0; // make compiler happy
}
