#include <iostream>
#include <vector>
#include <set>

#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters

#include "pin_cache.H"
#include "../general/Caches.h"
#include "../general/CacheConfig.h"
#include "../general/array_register.h"
#include "../../general/TraceReader.h"
#include "../../general/ArrayRW.h"

KNOB<string> KnobTrace(KNOB_MODE_WRITEONCE, "pintool", "t", "trace", "trace file");
KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "TransientLRU", "Output file");
KNOB<string> KnobMaskFile(KNOB_MODE_WRITEONCE, "pintool", "m", "FPDist.mask", "mask file for transient addr");
KNOB<UINT64> KnobLineSize(KNOB_MODE_WRITEONCE, "pintool", "l", "6", "log2 of cache line size, default 6");
KNOB<string> KnobArrays(KNOB_MODE_WRITEONCE, "pintool", "a", "array", "array file");

//MergeEngine ary_reg;
ArrayEngine ary_reg;
UINT64 floor2CacheLine = 6;
std::ofstream OutputFile;
UINT64 ins_count = 0;
UINT64 ref_count = 0;
std::set<ADDRINT> wss_count;
TransientMask tmask(&ary_reg);


//LOCALFUN ITLB::CACHE itlb("ITLB", ITLB::cacheSize, ITLB::lineSize, ITLB::associativity);
//LOCALVAR DTLB::CACHE dtlb("DTLB", DTLB::cacheSize, DTLB::lineSize, DTLB::associativity);
//LOCALVAR IL1::CACHE il1("L1 Instruction Cache", IL1::cacheSize, IL1::lineSize, IL1::associativity);
//LOCALVAR DL1::CACHE dl1("L1 Data Cache", DL1::cacheSize, DL1::lineSize, DL1::associativity);
LOCALVAR TransientCACHE<UL2::allocation> ul2("L2 Unified Cache : Transient LRU", UL2::cacheSize, UL2::lineSize, UL2::associativity, &tmask);
LOCALVAR TransientCACHE<UL3::allocation> ul3("L3 Unified Cache : Transient LRU", UL3::cacheSize, UL3::lineSize, UL3::associativity, &tmask);

LOCALFUN VOID Fini(int code, VOID * v)
{
    cout << ins_count << "\t" << ref_count << "\t" << wss_count.size() << "\n";
    //OutputFile << itlb;
    //OutputFile << dtlb;
//    OutputFile << il1;
//    OutputFile << dl1;
    cout << ul2;
    //OutputFile << ul3;
}

LOCALFUN VOID Ul2Access(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    // second level unified cache
    //const BOOL ul2Hit =
    ul2.Access(addr, size, accessType);

    // third level unified cache
    //if ( ! ul2Hit) ul3.Access(addr, size, accessType);
}

LOCALFUN VOID MemRefSingle(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType = CACHE_BASE::ACCESS_TYPE_LOAD)
{
    ref_count++;
    wss_count.insert(addr);
    Ul2Access(addr, size, accessType);
}


VOID MallocBefore(ADDRINT callee, ADDRINT size)
{
    ary_reg.BeforeMalloc(size);
}

VOID FreeBefore(ADDRINT ptr){
  ary_reg.FreeArray(ptr);
}

VOID MallocAfter(ADDRINT ret)
{
  ary_reg.AfterMalloc(ret);
}

VOID CallocBefore(ADDRINT callee, ADDRINT size, ADDRINT num){
  ary_reg.BeforeMalloc(size * num);
}

VOID ReallocBefore(ADDRINT callee, ADDRINT ptr, ADDRINT size){
  ary_reg.FreeArray(ptr);
  ary_reg.BeforeMalloc(size);
}

void PrepareArrays(){
    ArrayReader ai(KnobArrays.Value());
    while(ai.GetNext()){
        switch(ai.info[0]){
            case MALLOC: MallocBefore(ai.info[1], ai.info[2]); break;
            case RETURN: MallocAfter(ai.info[1]); break;
            //case FREE: FreeBefore(ai.info[1]); break;
            //case REALLOC: ReallocBefore(ai.info[1], ai.info[2], ai.info[3]); break;
            //case CALLOC: CallocBefore(ai.info[1], ai.info[2], ai.info[3]); break;
        }
    }
}

GLOBALFUN int main(int argc, char *argv[])
{
    PIN_InitSymbols();
    PIN_Init(argc, argv);
    floor2CacheLine = KnobLineSize.Value();
    tmask.init(KnobMaskFile.Value());

    PrepareArrays();
    TraceReader trace(KnobTrace.Value());
    trace.ShowInfo();
    //for(int i = 1; i < 2; i++){
    for(int i = 0; i < trace.GetNumChunk(); i++){
      cout << "Working on chunk " << i << endl;
      MemRef* refs = trace.GetChunk(i);
      for(uint64_t j = 0; j < trace.GetChunkSize(); j++){
        MemRefSingle(refs[j].address, refs[j].size);
      }
      ul2.Flush();
      delete refs;
    }
    Fini(0, NULL);

    return 0; // make compiler happy
}
