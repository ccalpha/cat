#pragma once

#include <iostream>
#include <vector>
#include "footprint.h"

using namespace std;

class CAT{
private:
    double c;   //capacity of one way
    vector<size_t> allocations;
    vector<FPDist> fpds;
    vector<VFPDist> vfpds;
    vector<double> occupancies;
    vector<double> AddingWay(size_t allocation);
    vector<double> accessrate;
public:
    CAT(double _c) : c(_c) {};
    CAT(double _c, vector<FPDist> fps);
    CAT(double _c, vector<FPDist> fps, vector<double> _ar);
    void InitOccupancies(vector<double> x);
    void AddProgram(FPDist _fp);
    vector<double> AddWay(size_t allocation);
    vector<double> GetOccupancies();
    vector<double> GetMissRatio();
    double GetMissCount();
    double GetOverallMissRatio();
    vector<double> TryAddOccupancy(vector<double> occu);
    vector<double> TryAddWay(size_t allocation);
    int GetNumProgs();
    void clear();
};

double CAT::GetOverallMissRatio(){
    vector<double> x = GetMissRatio();
    double total_access = 0;
    double total_miss = 0;
    for(int i = 0; i < x.size(); i++){
        total_access += fpds[i].access_count;
        total_miss += fpds[i].access_count*x[i];
    }
    return total_miss / total_access;
}

CAT::CAT(double _c, vector<FPDist> fps, vector<double> ar) : c(_c){
    accessrate = ar;
    for(auto &i : fps){
        AddProgram(i);
    }
}

CAT::CAT(double _c, vector<FPDist> fps) : CAT(_c, fps, vector<double>(fps.size(), 1.0)){
}

double CAT::GetMissCount(){
    vector<double> x = GetMissRatio();
    double sum = 0;
    for(int i = 0; i < x.size(); i++)
        sum += fpds[i].access_count*x[i];
    return sum;
}

void CAT::InitOccupancies(vector<double> x){
    occupancies = x;
}

//return additional occupancy of the comming way
vector<double> CAT::AddingWay(size_t allocation){
    int len = GetNumProgs();
    vector<FPDist*> f;
    vector<double> ar;
    for(size_t i = 0; i < len; i++){
        if(allocation & (1<<i)) {
            vfpds[i].init(occupancies[i]);
            f.push_back(&(vfpds[i]));
            ar.push_back(accessrate[i]);
        }
    }
    double ft = FTofComposedFP(c, f, ar);
    vector<double> rt;
    for(int i=0, j=0; i < len; i++){
        if(allocation & (1<<i)){
            rt.push_back(f[j]->GetFP(ft*ar[j]));
            j++;
        }
        else
            rt.push_back(0);
    }
    return rt;
}

vector<double> CAT::GetMissRatio(){
    vector<double> rt;
    for(int i = 0; i < fpds.size(); i++){
        double mr = fpds[i].GetMissRatio(occupancies[i]);
        rt.push_back(mr);
    }
    return rt;
}

//return comming occupancy
vector<double> CAT::TryAddWay(size_t allocation){
    return AddingWay(allocation);
}

//return miss ratios
vector<double> CAT::TryAddOccupancy(vector<double> occu){
    for(int i = 0;i < occu.size(); i++)
        occupancies[i] += occu[i];
    vector<double> rt = GetMissRatio();
    for(int i = 0;i < occu.size(); i++)
        occupancies[i] -= occu[i];
    return rt;
}

void CAT::clear(){
    //Doesn't reset fps
    occupancies.clear();
    allocations.clear();
    for(int i = 0; i < fpds.size(); i++)
        occupancies.push_back(0);
}

int CAT::GetNumProgs(){
    return fpds.size();
}

void CAT::AddProgram(FPDist _fp){
    fpds.push_back(_fp);
    VFPDist _vfp(_fp);
    vfpds.push_back(_vfp);
    occupancies.push_back(0);
}

//return occupancy
vector<double> CAT::AddWay(size_t allocation){
    allocations.push_back(allocation);
    vector<double> oc = AddingWay(allocation);
    for(int i = 0; i < GetNumProgs(); i++)
        occupancies[i] += oc[i];
    return occupancies;
}

vector<double> CAT::GetOccupancies(){
    return occupancies;
}
