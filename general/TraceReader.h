#ifndef __TraceReader_H
#define __TraceReader_H

#include <iostream>
#include <fstream>
#include <stdint.h>
#include <string>
#include <assert.h>

using namespace std;

struct MemRef{
  uint64_t address;
  uint32_t size;
};

class TraceReader{
private:
    ifstream fin;
    uint64_t maxSize;
    uint64_t chunkSize;
    double srate;
    uint64_t numChunk;
    uint64_t numInst;
    string name;
public:
    TraceReader(string _name);
    uint64_t GetNumInst();
    int GetNumChunk();
    uint64_t GetChunkSize();
    ~TraceReader();
    MemRef* GetChunk(int idx);
    void ShowInfo();
};

void TraceReader::ShowInfo(){
    cout << "Trace File : " << name << endl;
    cout << "Max Size : " << maxSize << endl;
    cout << "Sampling Rate : " << srate << endl;
    cout << "Size of Chunks : " << chunkSize << endl;
    cout << "Number of Chunks : " << numChunk << endl;
    cout << "Number of Instructions of Original Program: " << numInst << endl;
}

TraceReader::TraceReader(string _name){
    name = _name;
    fin.open(name.c_str());
    assert(fin.good() && "Open Trace File Error");
    fin.seekg(-40, ios_base::end);
    fin.read((char*)&maxSize, sizeof(maxSize));
    fin.read((char*)&chunkSize, sizeof(chunkSize));
    fin.read((char*)&srate, sizeof(srate));
    fin.read((char*)&numChunk, sizeof(numChunk));
    fin.read((char*)&numInst, sizeof(numInst));
}

uint64_t TraceReader::GetNumInst(){
    return numInst;
}

int TraceReader::GetNumChunk(){
    return numChunk;
}

uint64_t TraceReader::GetChunkSize(){
  return chunkSize;
}

MemRef* TraceReader::GetChunk(int idx){
  assert((uint64_t)idx < numChunk && "no such many chunks");

  fin.seekg(chunkSize*(sizeof(MemRef::address) + sizeof(MemRef::size))*idx);
  MemRef* ret = new MemRef[chunkSize];
  for(uint64_t i = 0; i < chunkSize; i++){
    fin.read((char*)&(ret[i].address), sizeof(ret[i].address));
    fin.read((char*)&(ret[i].size), sizeof(ret[i].size));
  }
  return ret;
}

TraceReader::~TraceReader(){
  fin.close();
}

//simple traces, only data and number of references
class SimpleTraceReader{
private:
    uint64_t ref_cnt;
    uint64_t skipped;
    ifstream fin;
public:
    string fname;
        SimpleTraceReader(string _fname);
        uint64_t GetNextRef();
        uint64_t GetNumRef();
        void ShowInfo();
        void ResetRefStream();
};

SimpleTraceReader::SimpleTraceReader(string _fname){
    fname = _fname;
    fin.open(fname.c_str());
    assert(fin.good() && "File open error");
    fin.seekg(-16, ios_base::end);
    fin.read((char*)&ref_cnt, sizeof(ref_cnt));
    fin.read((char*)&skipped, sizeof(skipped));
    fin.seekg(0);
}

uint64_t SimpleTraceReader::GetNextRef(){
    uint64_t x;
    fin.read((char*)&x, sizeof(x));
    return x;
}

uint64_t SimpleTraceReader::GetNumRef(){
    return ref_cnt;
}

void SimpleTraceReader::ShowInfo(){
    cout << "Number of references : " << ref_cnt << endl;
    cout << "Skipped references : " << skipped << endl;
}

void SimpleTraceReader::ResetRefStream(){
    fin.seekg(0);
}

#endif
