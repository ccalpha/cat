#include "pin.H"
#include <iostream>
#include <fstream>

/* ===================================================================== */
/* Names of malloc and free */
/* ===================================================================== */
#if defined(TARGET_MAC)
#define MALLOC "_malloc"
#define FREE "_free"
#define CALLOC "_calloc"
#define REALLOC "_realloc"
#else
#define MALLOC "malloc"
#define FREE "free"
#define CALLOC "calloc"
#define REALLOC "realloc"
#endif

/* ===================================================================== */
/* Global Variables */
/* ===================================================================== */

std::ofstream TraceFile;

/* ===================================================================== */
/* Commandline Switches */
/* ===================================================================== */

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool",
    "o", "malloctrace.out", "specify trace file name");

/* ===================================================================== */


/* ===================================================================== */
/* Analysis routines                                                     */
/* ===================================================================== */

VOID MallocBefore(ADDRINT callee, ADDRINT size)
{
    TraceFile << "malloc(" << callee << ") " << size << endl;
}

VOID CallocBefore(ADDRINT callee, ADDRINT size, ADDRINT num){
  TraceFile << "calloc(" << callee << ") " << size << " " << num << " " << size*num << endl;
}

VOID ReallocBefore(ADDRINT callee, ADDRINT ptr, ADDRINT size){
  TraceFile << "realloc(" << callee << ") " << ptr << " " << size << endl;
}

VOID FreeBefore(ADDRINT ptr){
  TraceFile << "free(" << ptr << ")" << endl;
}

VOID MallocAfter(ADDRINT callee, ADDRINT ret){
  TraceFile << "\treturn(" << callee << ") " << ret << endl;
}

VOID CallocAfter(ADDRINT callee, ADDRINT ret){
  TraceFile << "\treturn(" << callee << ") " << ret << endl;
}

VOID ReallocAfter(ADDRINT callee, ADDRINT ret){
  TraceFile << "\treturn(" << callee << ") " << ret << endl;
}


VOID Image(IMG img, VOID *v)
{

  RTN mallocRtn = RTN_FindByName(img, MALLOC);
  if (RTN_Valid(mallocRtn)){
      RTN_Open(mallocRtn);

      // Instrument malloc() to print the input argument value and the return value.
      RTN_InsertCall(mallocRtn, IPOINT_BEFORE, (AFUNPTR)MallocBefore,
                     IARG_ADDRINT, RTN_Address(mallocRtn),
                     IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                     IARG_END);
      RTN_InsertCall(mallocRtn, IPOINT_AFTER, (AFUNPTR)MallocAfter,
                     IARG_ADDRINT, RTN_Address(mallocRtn),
                     IARG_FUNCRET_EXITPOINT_VALUE, IARG_END);
      RTN_Close(mallocRtn);
  }

  // Find the free() function.
  RTN freeRtn = RTN_FindByName(img, FREE);
  if (RTN_Valid(freeRtn)){
      RTN_Open(freeRtn);
      // Instrument free() to print the input argument value.
      RTN_InsertCall(freeRtn, IPOINT_BEFORE, (AFUNPTR)FreeBefore,
                     IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                     IARG_END);
      RTN_Close(freeRtn);
  }

  RTN callocRtn = RTN_FindByName(img, CALLOC);
  if(RTN_Valid(callocRtn)){
    RTN_Open(callocRtn);
    RTN_InsertCall(callocRtn, IPOINT_BEFORE, (AFUNPTR)CallocBefore,
        IARG_ADDRINT, RTN_Address(callocRtn),
        IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
        IARG_FUNCARG_ENTRYPOINT_VALUE, 1,
        IARG_END);
    RTN_InsertCall(callocRtn, IPOINT_AFTER, (AFUNPTR)CallocAfter,
                   IARG_ADDRINT, RTN_Address(callocRtn),
                   IARG_FUNCRET_EXITPOINT_VALUE, IARG_END);
    RTN_Close(callocRtn);
  }

  RTN reallocRtn = RTN_FindByName(img, REALLOC);
  if(RTN_Valid(reallocRtn)){
    RTN_Open(reallocRtn);
    RTN_InsertCall(reallocRtn, IPOINT_BEFORE, (AFUNPTR)ReallocBefore,
        IARG_ADDRINT, RTN_Address(reallocRtn),
        IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
        IARG_FUNCARG_ENTRYPOINT_VALUE, 1,
        IARG_END);
    RTN_InsertCall(reallocRtn, IPOINT_AFTER, (AFUNPTR)ReallocAfter,
                   IARG_ADDRINT, RTN_Address(reallocRtn),
                   IARG_FUNCRET_EXITPOINT_VALUE, IARG_END);
    RTN_Close(reallocRtn);
  }
}

/* ===================================================================== */

VOID Fini(INT32 code, VOID *v)
{
    TraceFile.close();
}

/* ===================================================================== */
/* Print Help Message                                                    */
/* ===================================================================== */

INT32 Usage()
{
    cerr << "This tool produces a trace of calls to malloc." << endl;
    cerr << endl << KNOB_BASE::StringKnobSummary() << endl;
    return -1;
}

/* ===================================================================== */
/* Main                                                                  */
/* ===================================================================== */

int main(int argc, char *argv[])
{
    // Initialize pin & symbol manager
    PIN_InitSymbols();
    if( PIN_Init(argc,argv) )
    {
        return Usage();
    }

    // Write to a file since cout and cerr maybe closed by the application
    TraceFile.open(KnobOutputFile.Value().c_str());
    TraceFile << hex;
    TraceFile.setf(ios::showbase);

    // Register Image to be called to instrument functions.
    IMG_AddInstrumentFunction(Image, 0);
    PIN_AddFiniFunction(Fini, 0);

    // Never returns
    PIN_StartProgram();

    return 0;
}

/* ===================================================================== */
/* eof */
/* ===================================================================== */
