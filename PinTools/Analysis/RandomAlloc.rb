#!/usr/bin/ruby

#usage : selection.rb numofcorun time
names = []
File.readlines("names.txt").each{|x| names << x.strip}
candidates = (0..names.size-1).to_a
nco = ARGV[0].to_i

candidates = candidates.combination(nco).to_a
ROOT="../Traces_SampledRef"

prng = Random.new(17329489) #1111111th prime

def CheckCover(alloc)
    for i in alloc
        if(i == 0)
            return false;
        end
    end
    return true
end

def GetContinuousAlloc(prng, nway, nprog)
    alloc = [0,nway-1]
    for i in 0...nprog-1
        alloc << prng.rand(0..nway-1)
        alloc << prng.rand(0..nway-1)
    end
    alloc.shuffle!(random: prng) #alloc might not cover all of the ways     
    
    rt = Array.new(nway, 0)
    for i in 0...nprog
        if(alloc[i*2] > alloc[i*2+1])
            alloc[i*2], alloc[i*2+1] = alloc[i*2+1], alloc[i*2]
        end
        for j in alloc[i*2]..alloc[i*2+1]
            rt[j] |= (1<<i)
        end
    end
    return rt
end

groups = []
allocs = []
for i in 0...20
    for j in 0...names.size
        for k in j+1...names.size
            groups << [j,k]
            alloc = GetContinuousAlloc(prng, 20, nco)
            while(CheckCover(alloc) == false)
                alloc = GetContinuousAlloc(prng, 20, nco)
            end
            allocs << alloc
        end
    end
end

def func(x, groups, allocs, names)
        f = File.open("info_#{x}", 'w')
        f.sync = true
        for i in 2030*x...2030*(x+1)
            f.write("group : ")
            f.puts(groups[i].join(" "))
            f.write("allocation : ")
            f.puts(allocs[i].join(" "))
            system("echo \"#{ROOT}/#{names[groups[i][0]]}.trace\n#{ROOT}/#{names[groups[i][1]]}.trace\" > traces_#{x}")
            system("echo \"#{allocs[i].join(" ")}\" > alloc_#{x}")
            #system("./runtest.sh CAT -t ./traces_#{x} -a ./alloc_#{x} -c 20971520 >> CAT_#{x}")
            system("./runtest.sh CAT -t ./traces_#{x} -a ./alloc_#{x} -c 5242880 >> CAT_#{x}")
        end
end

threads = []
4.times{ |x|
    puts "#{2030*x} #{2030*(x+1)}"
    threads << Thread.new{
        func(x, groups, allocs, names)
    }
}

threads.each{|x| x.join}
exit

for i in 0...1000
    gid = prng.rand(candidates.length-1)
    alloc = []
    
    alloc = GetContinuousAlloc(prng, 20, nco)
    while(CheckCover(alloc) == false)
        alloc = GetContinuousAlloc(prng, 20, nco)
    end
    puts "working on co-run group : #{candidates[gid].join(" ")}"
    puts "allocation : #{alloc.join(" ")}"

    system("echo \"#{ROOT}/#{names[candidates[gid][0]]}.trace\n#{ROOT}/#{names[candidates[gid][1]]}.trace\" > traces_random")
    system("echo \"#{alloc.join("\t")}\" > alloc_random")
    next  
    threads = []
    threads << Thread.new{system("./runtest.sh CAT -t ./traces_random -a ./alloc_random -c 20971520 >> CAT_20M")}
    threads << Thread.new{system("./runtest.sh CAT -t ./traces_random -a ./alloc_random -c 10485760 >> CAT_10M")}
    threads << Thread.new{system("./runtest.sh CAT -t ./traces_random -a ./alloc_random -c 5242880 >> CAT_5M")}
    
    threads.each { |thr| thr.join }
end
