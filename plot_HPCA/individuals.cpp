#include <iostream>
#include <map>
#include <fstream>
#include <string>

using namespace std;

int main(){
    ifstream fin("coruns_2.data");
    string x, y;
    double a, b, c;
    map<string, double> mr;
    map<string, double> vfp;
    map<string, double> error;

    while(fin>>x){
        fin >> y >> a >> b >> c;
        if(mr.find(x) == mr.end()){
            mr[x] = 0;
            vfp[x] = 0;
            error[x] = 0;
        }
        if(mr.find(y) == mr.end()){
            mr[y] = 0;
            vfp[y] = 0;
            error[y] = 0;
        }
        mr[x] += a;
        vfp[x] += b;
        error[x] += c;

        mr[y] += a;
        vfp[y] += b;
        error[y] += c;
    }

    for(auto i : mr){
        cout << i.first << " " << mr[i.first]/27 << " " << vfp[i.first]/27 << " " << error[i.first]/27 << endl;
    }
    return 0;
}
