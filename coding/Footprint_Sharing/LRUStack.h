//
//  LRUStack.h
//  Footprint_Sharing
//
//  Created by yechencheng on 5/10/15.
//  Copyright (c) 2015 yechencheng. All rights reserved.
//

#ifndef Footprint_Sharing_LRUStack_h
#define Footprint_Sharing_LRUStack_h

#include <map>
#include <algorithm>

using namespace std;

enum LRUAccess{DataHit = -1, DataFill = -2};

class LRUStack{
private:
    int64_t size;
    int64_t *A;
    int64_t *next;
    int64_t *previous;
    
    int64_t head;
    int64_t tail;
    
    map<int64_t, int64_t> pos;
    
    int64_t cnt;
    
    void MoveToHead(int64_t p){
        if(head == p) return;
        
        next[previous[p]] = next[p];
        if(tail != p)
            previous[next[p]] = previous[p];
        else
            tail = previous[p];
        
        previous[p] = -1;
        next[p] = head;
        previous[head] = p;
        head = p;
    }
    
public:
    LRUStack(int64_t _size) : size(_size){
        A = new int64_t[size];
        next = new int64_t[size];
        previous = new int64_t[size];
        fill(next, next+size, -1);
        fill(previous, previous+size, -1);
        head = tail = -1;
        
        cnt = 0;
    }
    ~LRUStack(){
    	delete A;
	delete next;
	delete previous;
    }    
    int64_t push(int64_t data){
        if(size == 0)
            return data;
        
        if(pos.count(data) != 0 && A[pos[data]] == data){
            MoveToHead(pos[data]);
            return LRUAccess::DataHit;
        }
        if(cnt < size){
            if(cnt == 0){
                A[0] = data;
                previous[0] = next[0] = -1;
                head = tail = 0;
                pos[data] = 0;
            }
            else{
                int64_t p = 0;
                while(p < size){
                    if(previous[p] == -1 && head != p)
                        break;
                    p++;
                }
            
                A[p] = data;
                previous[p] = -1;
                next[p] = head;
                previous[head] = p;
                head = p;
                pos[data] = p;
            }
            cnt++;
            return LRUAccess::DataFill;
        }
        else{
            //remove tail
            int64_t pb = tail;
            tail = previous[tail];
            previous[pb] = -1;
            next[tail] = -1;
            int64_t retval = A[pb];
            pos.erase(retval);  //probably bad performance
            cnt--;
            push(data);
            return retval;
        }
    }
    
    int64_t GetHead(){
        return A[head];
    }
    
    int64_t GetTail(){
        return A[tail];
    }
    
    //Some algorithm need to seris connect LRU stack
    void RemoveHead(){
        pos.erase(A[head]);
        head = next[head];
        if(head != -1){
            next[previous[head]] = -1;
            previous[head] = -1;
        }
        else tail = -1;
        cnt--;
    }
    
    bool IsInStack(int64_t datum){
        if(pos.count(datum) != 0 && A[pos[datum]] != datum)
            return false;
        return pos.count(datum) != 0 && A[pos[datum]] == datum;
    }
    
    bool IsFull(){
        return cnt >= size;
    }
};

#endif
