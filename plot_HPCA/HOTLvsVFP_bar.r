library(ggplot2)
#library(reshape)
library(data.table)
library("gridExtra")
library("cowplot")
require(scales)



corun2<- read.table("./2group_classes.data", header=TRUE)

corun2dat <- corun2
corun2dat["Config"] <- NA
corun2dat$Config <- paste(corun2dat$C1, "I\n ", corun2dat$C2, "M\n ", corun2dat$C3, "L", sep="")
corun2dat <- subset(corun2dat, select = c(L3MissRatio, VFP, VFP_AbsError, FP_AbsError, Config))

corun2dat_melt <- melt(corun2dat, "Config")
plot_2corun <- ggplot(corun2dat_melt) +
    geom_bar(aes(Config, value, group=variable,
                 fill = factor(variable, labels=c("Actual", "VFP", "VFP Error", "HOTL Error"))),
         position = "dodge",
         stat="identity") +
    scale_y_continuous(labels=percent)+
    theme_gray() +
    theme(legend.justification=c(0,1),
        legend.position=c(0,1),
        text = element_text(size=20),
        plot.margin=unit(c(0.5,.1,1,.1), "cm")) +
    labs(x="Intensity categories of 2 benchmark co-run groups", y="Miss Ratio") +
    guides(fill=guide_legend(title = NULL))+
    scale_fill_manual(values=c("#FF6600", "#66CC66", "#0033CC", "#00CCFF"))
    #+coord_fixed(ratio = 80)
plot_2corun<-ggdraw(switch_axis_position(plot_2corun, axis = 'y', keep = 'y'))

corun3<- read.table("./3group_classes.data", header=TRUE)
corun3dat <- corun3
corun3dat["Config"] <- NA
corun3dat$Config <- paste(corun3dat$C1, "I\n ", corun3dat$C2, "M\n ", corun3dat$C3, "L", sep="")
corun3dat <- subset(corun3dat, select = c(L3MissRatio, VFP, VFP_AbsError, FP_AbsError, Config))

corun3dat_melt <- melt(corun3dat, "Config")
plot_3corun <- ggplot(corun3dat_melt) +
geom_bar(aes(Config, value, group=variable,
                 fill = factor(variable, labels=c("Actual", "VFP", "VFP Error", "HOTL Error"))),
         position = "dodge",
         stat="identity") +
    scale_y_continuous(labels=percent)+
    theme_gray() +
    theme(legend.justification=c(0,1),
        legend.position=c(0,1),
        text = element_text(size=20),
        plot.margin=unit(c(.1,.1,.1,.1), "cm")) +
    labs(x="Intensity categories of 3 benchmark co-run groups", y="Miss Ratio") +
    guides(fill=guide_legend(title = NULL)) +
    scale_fill_manual(values=c("#FF6600", "#66CC66", "#0033CC", "#00CCFF"))
    #+coord_fixed(ratio = 80)
plot_3corun<-ggdraw(switch_axis_position(plot_3corun, axis = 'y', keep = 'y'))


tt <- ttheme_default(colhead=list(fg_params = list(parse=TRUE)))

corun2dat_table <- read.table("./grouped_calsses_2_table.data", header=FALSE)
corun3dat_table <- read.table("./grouped_calsses_3_table.data", header=FALSE)
tbl2=tableGrob(corun2dat_table, rows=NULL, theme=tt)
colnames(tbl2)<-NULL
tbl3=tableGrob(corun3dat_table, rows=NULL, theme=tt)

#grid.arrange(plot_2corun, tbl2, ncol = 1)
#grid.arrange(plot_3corun, tbl3, ncol = 1)
grid.arrange(plot_2corun, plot_3corun, ncol=1)

total<-rbind(corun2, corun3)
total["Config"] <- NA
total$Config <- paste(total$C1, "I\n ", total$C2, "M\n ", total$C3, "L", sep="")
#total$Config <- paste(total$C1, "I", total$C2, "M", total$C3, "L", sep="")
total <- subset(total, select = c(Config, VFP_RelError))
total_melt <- melt(total, "Config")

total_melt$variable <- c(2,2,2,2,2,2,3,3,3,3,3,3,3,3,3)
plot_total <- ggplot(total_melt) +
      geom_bar(aes(Config, value, fill=factor(variable, labels = c("2 benchmarks", "3 benchmakrs"))), stat="identity") +
      coord_fixed(ratio = 8) +
      scale_y_continuous(labels=percent)+
      theme_gray() +
      theme(legend.justification=c(1,1),
          legend.position=c(1,1),
          text = element_text(size=20),
          panel.border = element_blank(),
          plot.margin=unit(c(.1,.1,.1,.1), "cm")) +
      guides(fill=guide_legend(title = NULL)) +
      labs(x="Intensity categories of co-run groups", y="Average Relative Error")+
      scale_fill_manual(values=c("#FF6600", "#66CC66"))

plot(plot_total)
