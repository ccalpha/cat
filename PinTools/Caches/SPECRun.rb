#!/usr/bin/ruby
require 'thread'
require 'optparse'
require 'pp'
require 'ostruct'

actions = ["footprint", "footprint_array", "LRU", "DIP", "TLRU", "mask", "malloc", "ReuseTime"]
dirs = File.readlines("dirs.txt")
commands = File.readlines("commands.txt");
names = File.readlines("names.txt").map{|x| x.strip}.to_a
#ids = (0..dirs.length-1).to_a
ids=[1,2,3,4,9,10,15,18,22,24]
class OptParse
    def self.parse(args)
        options = OpenStruct.new
        options.library = []
        options.inplace = false
        options.transfer_type = :auto
        options.verbose = false

        opt_parser = OptionParser.new do |opts|
            opts.banner = "Usage : command.rb [--list b1,b2,...] --action a1,a2,a3"
            opts.on("--list x,y,z", Array, "list of program to be executed") do |list|
                options.list = list
            end
            opts.on("--action x,y,z", Array, "list of actions to be applyed") do |action|
                options.action = action
            end

            opts.on("-f", "--force", "force to rebuild pin tools") do |f|
                options.f = f;
            end

        end
        opt_parser.parse!(args)
        options
    end
end

options = OptParse.parse(ARGV)

if options.f != nil
    system("make clean; make -j")
end

act = options.action
if options.action == nil
    act = actions
end

act.each{ |a|
    if actions.index(a) == nil
        puts "Actions should be #{actions}"
        exit
    end
}

if options.list != nil
    ids = options.list.map{|i| i.to_i}
end

#options has been parsed
for i in (0..ids.length-1).to_a.reverse
    if dirs[i].strip.start_with?("#")
        puts "Removed : #{dirs[i]}"
        ids.delete(i)
    end
end

PIN_ROOT=`echo $PIN_ROOT`.strip
CAT_ROOT="/home/cye/cat/"


queue = Queue.new

if act.index("ReuseTime") != nil
    for i in ids
        queue << "cd #{dirs[i].strip} ; #{PIN_ROOT}/pin -t #{CAT_ROOT}/PinTools/Caches/obj-intel64/ReuseTime.so -o #{CAT_ROOT}/PinTools/result/#{i}.RT -- #{commands[i].strip}"
    end
end

if act.index("LRU") != nil
    for i in ids
        queue << "cd #{dirs[i].strip} ; #{PIN_ROOT}/pin.sh -t #{CAT_ROOT}/PinTools/Caches/obj-intel64/LRU.so -o #{CAT_ROOT}/PinTools/result/#{i}.LRU -- #{commands[i].strip}"
    end
end

if act.index("DIP") != nil
    for i in ids
        queue << "cd #{dirs[i].strip} ; #{PIN_ROOT}/pin -t #{CAT_ROOT}/PinTools/Caches/obj-intel64/DIP.so -o #{CAT_ROOT}/PinTools/result/#{i}.DIP -- #{commands[i].strip}"
    end
end

if act.index("footprint") != nil
    for i in ids
        queue << "cd #{dirs[i].strip} ; #{PIN_ROOT}/pin -t #{CAT_ROOT}/PinTools/Caches/obj-intel64/footprint.so -l 6 -o #{CAT_ROOT}/PinTools/result/#{names[i]}.footprint -- #{commands[i].strip}"
    end
end

if act.index("malloc") != nil
    for i in ids
        queue << "cd #{dirs[i].strip} ; #{PIN_ROOT}/pin -t #{CAT_ROOT}/PinTools/Caches/obj-intel64/malloctrace.so -o #{CAT_ROOT}/PinTools/result/#{i}.malloc -- #{commands[i].strip}"
    end
end

if act.index("footprint_array") != nil
    for i in ids
        queue << "rm #{CAT_ROOT}/PinTools/result/#{i}.array.* -f ; cd #{dirs[i].strip} ; #{PIN_ROOT}/pin -t #{CAT_ROOT}/PinTools/Caches/obj-intel64/footprint_array.so -l 6 -o #{CAT_ROOT}/PinTools/result/#{i}.array -- #{commands[i].strip}"
    end
end

if act.index("mask") != nil
    for i in ids
        queue << "/localdisk/cye/cat/TransientMask/TransientMask --config Intel -m #{CAT_ROOT}/PinTools/result/#{i}.mask -f #{CAT_ROOT}/PinTools/result/#{i}.array"
    end
end

if act.index("TLRU") != nil
    for i in ids
        queue << "cd #{dirs[i].strip} ; #{PIN_ROOT}/pin -t #{CAT_ROOT}/PinTools/Caches/obj-intel64/TransientLRU.so -l 6 -o #{CAT_ROOT}/PinTools/result/#{i}.TLRU -m #{CAT_ROOT}/PinTools/result/#{i}.mask -- #{commands[i].strip}"
    end
end


threads = []
4.times do |i|
    threads << Thread.new do
        while (e = queue.pop(true) rescue nil)
            puts e
            pid = spawn("#{e}")
            Process.wait pid
        end
    end
end

threads.each{|t| t.join}
