#include <iostream>
#include <string>
#include <cstdlib>

#include "../general/randomArray.h"
#include "../general/roi.h"

using namespace std;


int main(int argc, char** argv){
    cout << "Usage streamrandom SSize(M) Rsize itr" << endl;
    int Ssize = atoi(argv[1]) << 20;
    int Rsize = atoi(argv[2]) << 20;
    int itr = atoi(argv[3]);
    
    Ssize /= sizeof(uint32_t);
    Rsize /= sizeof(uint32_t);
    uint32_t* streams = new uint32_t[Ssize];
    for(int i = 0; i < Ssize; i++)
        streams[i] = i;
    uint32_t* rands = InplaceRandomArray(Rsize);
    
    ROIBegin();
    volatile uint64_t sum = 0;
    for(int i = 0; i < itr; i++){
        for(int j = 0; j < Ssize; j++)
            sum += streams[j];
        for(int j = rands[0]; j ; j = rands[j])
            sum += j;
    }
    ROIEnd();
    cout << sum << endl;
    return 0;
}
