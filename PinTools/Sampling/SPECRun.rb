#!/usr/bin/ruby
require 'thread'
require 'optparse'
require 'pp'
require 'ostruct'

dirs = File.readlines("dirs.txt")
commands = File.readlines("commands.txt");
names = File.readlines("names.txt");

PIN_ROOT=`echo $PIN_ROOT`.strip
#CAT_ROOT="/home/sc2221051/cat"
CAT_ROOT="/localdisk/cye/cat"
queue = Queue.new

for i in [1,2,3,4,9,10,15,18,22,24]
#for i in 0..dirs.length-1
    #queue << "cd #{dirs[i].strip} ; #{PIN_ROOT}/pin.sh -t #{CAT_ROOT}/PinTools/Sampling/obj-intel64/sampling.so -c 33554432 -size 858993459  -o #{CAT_ROOT}/PinTools/Traces/#{names[i].strip}.trace -a #{CAT_ROOT}/PinTools/Traces/#{names[i].strip}.array  -- #{commands[i].strip}"
    #queue << "cd #{dirs[i].strip} ; #{PIN_ROOT}/pin.sh -t #{CAT_ROOT}/PinTools/Sampling/obj-intel64/sampling.so -c 33554432 -size 8589934592  -o #{CAT_ROOT}/PinTools/Traces/#{names[i].strip}.trace -a #{CAT_ROOT}/PinTools/Traces/#{names[i].strip}.array  -- #{commands[i].strip}"
    queue << "cd #{dirs[i].strip} ; #{PIN_ROOT}/pin -t #{CAT_ROOT}/PinTools/Sampling/obj-intel64/sampling_continous.so -o #{CAT_ROOT}/PinTools/Traces_continous/#{names[i].strip}.trace -- #{commands[i].strip}"
end

threads = []
1.times do |i|
    threads << Thread.new do
        while (e = queue.pop(true) rescue nil)
            pid = spawn("#{e}")
            Process.wait pid
            puts "Finished : #{e}"
        end
    end
end

threads.each{|t| t.join}
