#include <iostream>
#include <vector>
#include <queue>
#include <cstdlib>

#include <boost/functional/hash.hpp>
#include <boost/program_options.hpp>

#include "footprint.h"
#include "CAT.h"
#include "general.h"

using namespace std;

vector<int> Discretize(vector<double> ocu, int tu = 128){
    vector<int> rt;
    for(int i = 0; i < ocu.size(); i++){
        double x = round(ocu[i] / tu) * tu;
        rt.push_back((int)x);
    }
    return rt;
}

size_t HashFunc(vector<int> ocu, map<size_t, vector<int> > &HashTab){
    //std::size_t seed = time(NULL);
    size_t seed = 0;
    for(int i = 0; i < ocu.size(); i++)
        boost::hash_combine(seed, ocu[i]);
    while(HashTab.find(seed) != HashTab.end()){
        vector<int> x = HashTab[seed];
        if(x == ocu)
            return seed;
        boost::hash_combine(seed, 0);
    }
//    HashTab[seed] = ocu;
    return seed;
}

//return best allocation
vector<size_t> ApproximateDP(int nways, double c, vector<FPDist> &fps, int roundTo = 128, int upper=10000){
    map<size_t, vector<int> > *HashTab = new map<size_t, vector<int> >[nways+1];
    map<size_t, double> *DPmcnt = new map<size_t, double> [nways+1];
    map<size_t, pair<size_t, size_t > > *PreAlloc = new map<size_t, pair<size_t, size_t> > [nways+1]; //alloc and id of predecessor
    DPmcnt[0][0] = 0;
    HashTab[0][0] = vector<int>(fps.size(), 0);
    PreAlloc[0][0] = make_pair(0,0);
    size_t nchoice = 1;
    nchoice <<= fps.size();
     
    for(int i = 0; i < nways; i++){
        int next_i = i + 1;
        CAT mycat(c, fps);
        priority_queue<pair<double, size_t> > priq;
        for(auto j : HashTab[i]){
            vector<double> tmpj;
            for(auto _j : j.second)
                tmpj.push_back(_j);
            mycat.InitOccupancies(tmpj);
            for(size_t k = 1; k < nchoice; k++){
                vector<double> ocu = mycat.TryAddWay(k);
                //PrintVector(ocu, "increased ocu : ");
                for(int _j = 0; _j < ocu.size(); _j++)
                    ocu[_j] += tmpj[_j];
                vector<int> docu = Discretize(ocu);
                size_t id = HashFunc(docu, HashTab[next_i]);
                double mcnt = 0;
                for(size_t p = 0; p < docu.size(); p++)
                    mcnt += fps[p].GetMissRatio(docu[p]) * fps[p].access_count;
                //cout << i << " " << k << " " << mcnt << " " << id << endl;
                //PrintVector(docu);
                if(DPmcnt[next_i].find(id) != DPmcnt[next_i].end())
                    DPmcnt[next_i][id] = min(DPmcnt[next_i][id], mcnt);
                else{
                    //solve inconsistence between DPmcnt and priq
                    if(priq.size()){
                        auto tp = priq.top();
                        while(1){
                            if(DPmcnt[next_i].find(tp.second) == DPmcnt[next_i].end()){
                                priq.pop();
                                continue;
                            }
                            else if(DPmcnt[next_i][tp.second] != tp.first){
                                priq.pop();
                                tp.first = DPmcnt[next_i][tp.second];
                                priq.push(tp);
                                continue;
                            }
                            else
                                break;
                        }
                    }

                    //if there are too many allocations, forget currently worst one
                    if(priq.size() == upper){
                        if(mcnt >= priq.top().first)
                            continue;   //if current one is the worst, forget and continue
                        else{
                            auto x = priq.top();
                            priq.pop();

                            //***Forget it will cause error!!! TOBE fixed. Probabaly the possibility of
                            //encountering error is pretty low that can be neglected(depends 
                            //on the possiblity of conflict of hashing)
                            HashTab[next_i].erase(x.second); 

                            DPmcnt[next_i].erase(x.second);
                            PreAlloc[next_i].erase(x.second);
                        }
                    }
                    priq.push(make_pair(mcnt, id));
                    HashTab[next_i][id] = docu;
                    DPmcnt[next_i][id] = mcnt;
                    PreAlloc[next_i][id] = make_pair(k, j.first);
                }
            }
        }
    }
    double mmcnt = 1e40;
    size_t mid = 0;
    for(auto i : DPmcnt[nways]){
        if(mmcnt > i.second){
            mmcnt = i.second;
            mid = i.first;
        }
    }
    cout << "Minimal Aggregated Miss Count : " << mmcnt <<  endl;
    vector<size_t> rt;
    for(int i = nways; i > 0; i--){
        if(PreAlloc[i].find(mid) == PreAlloc[i].end())
            cerr << "Error : Can't find solution, inconsistent between PreAlloc and DPmcnt" << endl;
        rt.push_back(PreAlloc[i][mid].first);
        mid = PreAlloc[i][mid].second;
    }
    reverse(rt.begin(), rt.end());
    return rt;
}

// command nways c fp1 fp2 ..
int main(int argc, char **argv){
    int nways;
    double c;
    double roundto;
    int upper;
    namespace po = boost::program_options;
    po::options_description desc("Allowed Options");
    desc.add_options()
        ("help,h", "produce help message")
        ("nways,w", po::value<int>(&nways)->required(), "[required] number of ways")
        ("capacity,c", po::value<double>(&c)->required(), "[required] capacity of each way")
        ("roundto,r", po::value<double>(&roundto)->default_value(128), "round to how many cache line")
        ("upper,u", po::value<int>(&upper)->default_value(10000), "number of stats evaluated for each way")
        ("footprint,f", po::value< vector<string> >(), "footprints of programs")
        ;
    po::positional_options_description p;
    p.add("footprint", -1);
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    if(vm.count("help")){
        cout << desc << endl;
        return 0;
    }
    po::notify(vm);

    vector<FPDist> fps;
    for(auto i : vm["footprint"].as<vector<string> >())
        fps.push_back(FPDist(i)); 
    vector<size_t> alloc = ApproximateDP(nways, c, fps, roundto, upper);
    PrintVector(alloc, "Allocation : ");
    return 0;
}
