#pragma once

#include "footprint.h"

void NormalizeAR(vector<double> &ar){
    if(ar[0] < 1.0) return;

    double x = 0;
    accumulate(ar.begin(), ar.end(), x);
    for(auto &i : ar)
        i /= x;
}

class FPManager{
public:
	double GetMissRatio(FPDist &fp, double c);
	double GetMissRatioColdMiss(FPDist &fp, double c);
	virtual double GetFP(FPDist &fp, double w);
	virtual double GetFT(FPDist &fp, double c);
	double GetWSS(FPDist &fp);
	void serilize(FPDist &fp);
	void deserilize(FPDist &fp);
	double ComposedFP(double c, vector<FPDist*> fpds, vector<double> &ar);
	vector<double> CompsoedFT(double c, vector<FPDist*> fpds, vector<double> &ar);
};

class VFPManager : public FPManager{
public:
    double GetFP(VFPDist &fp, double w);
    double GetFT(VFPDist &fp, double c);
	double GetVFP(VFPDist &VFP, double h, double w);
	double GetVFT(VFPDist &VFP, double h, double w);
    double ComposedFP(double c, vector<VFPDist*> vfps, vector<double> &ar);
    vector<double> ComposedFT(double c, vector<VFPDist*> vfps, vector<double> &ar);
};

double FPManager::GetMissRatio(FPDist &fp, double c){
	if(c == 0) return 1;
	double tc = GetFT(fp, c);
	return GetFP(fp, tc+1)-GetFP(fp, tc);
}

double FPManager::GetMissRatioColdMiss(FPDist &fp, double c){
	double mr = GetMissRatio(fp, c);
	return mr+fp.GetWSS()/fp.naccess;
}

//<! fp(w) = [fp(a)-fp(b)] *(w-b)/(a-b) + fp(b)
double FPManager::GetFP(FPDist &fp, double w){
	int64_t id = upper_bound(fp.W, fp.W+fp.M, w) - fp.W;
	double *FP = fp.FP;
	double *W = fp.W;

	if(id == fp.M) return FP[M-1];
	double ret;
	if(id == 0) ret = w/W[0]*Fp[0];
	else{
		assert(FP[id] >= 0 && FP[id-1] >= 0 && "Invalide FP Value");
		ret = FP[id-1]+(w-W[id-1])/(W[id]-W[id-1]) * (FP[id]-FP[id-1]);
	}
	return min(ret, FP[M-1]);

}

//<! ft(c) = (c-b)/(a-b)*[ft(a)-ft(b)] + ft(b) 
double FPManager::GetFT(FPDist &fp, double c){
	assert(c <= fp.GetWSS());
	if(c >= fp.GetWSS()) return 1e20;
	int64_t id = upper_bound(fp.FP, fp.FP+fp.M, c) - fp.FP;
	if(id == 0) return 0;
	double ret = fp.W[id-1];
	ret += (c-fp.FP[id-1])/(fp.FP[id]-fp.FP[id-1]) * (fp.W[id]-fp.W[id-1]);
	return ret;
}

double FPManager::GetWSS(FPDist &fp){
	return fp.GetWSS();
}

double FPManager::serilize(FPDist &fp, string filename){
	ofstream fout;
	if(!VerifyFile(filename)){
		fout.open(filename.c_str());
		fout.write((char*)&verifycode, sizeof(verifycode));
		fout.write((char*)&M, sizeof(M));
		fout.write((char*)&access_count, sizeof(access_count));
		fout.write((char*)&data_count, sizeof(data_count));
		fout.write((char*)&N, sizeof(N));
		fout.write((char*)W, sizeof(W[0])*M);
	}   
	else{
		fout.open(filename.c_str(), ios_base::app);
	}   
	fout.write((char*)FP, sizeof(FP[0])*M);
	fout.close();
}

double FPManager::deserilize(FPDist &fp, string filename){
	isgood = true;
	ifstream fin(filename.c_str());
	assert(fin.good());
	uint64_t vf;
	fin.read((char*)&vf, sizeof(verifycode));
	fin.read((char*)&M, sizeof(M));
	fin.read((char*)&access_count, sizeof(access_count));
	fin.read((char*)&data_count, sizeof(data_count));
	fin.read((char*)&N, sizeof(N));
	W = new int64_t[M];
	fin.read((char*)W, sizeof(W[0])*M);
	fin.seekg(idx*sizeof(FP[0])*M, ios_base::cur);
	FP = new double[M];
	fin.read((char*)FP, sizeof(FP[0])*M);
	if(!fin.good()){
		isgood = false;
	}   
	fin.close();
}

//<! ar should be normalized, input fill time w, return composed fill time, normalized to access rate ar.
double FPManager::ComposedFP(double w, vector<FPDist*> fps, vector<double> &ar){
    double rt = 0;
    for(int i = 0; i < fps.size(); i++)
        rt += GetFP(*(fps[i]), w*ar[i]);
    return rt;
}

//<! input cach size(c), return composed fill times of fps, ar can be unormalized. 
vector<double> FPManager::ComposedFT(double c, vector<FPDist*> fps, vector<double> &ar){
    NormalizeAR(ar);
    double l = 0, h = 1e20;
    while(h-l > 1e-6){
        double mid = (l+h)/2;
        double mc = ComposedFP(mid, fps, ar);
        if(mc < c) l = mid;
        else r = mid;
    }
    vector<double> rt;
    for(int i = 0; i < fps.size(); i++)
        rt.push_back(GetFP(*(fps[i]), l*ar[i]));
    return rt;
}

double VFPManager::GetFP(VFPDist &vfp, double w){
    return GetVFP(vfp, w);
}

double VFPManager::GetFT(VFPDist &vfp, double c){
    return GetVFT(vfp, c);
}

double VFPManager::GetVFP(VFPDist &VFP, double h, double w){
	if(VFP.h != h) VFP.init(h);
	return max(FPManager::GetFP(VFP, w+th) - h, 0.0);
}

double VFPManager::GetVFT(VFPDist &VFP, double h, double c){
	if(VFP.h != h) VFP.init(h);
	if(c < VFP.data_count) return 1e20;
	return FPManager::GetFT(VFP, c+h) - VFP.th;
}

//<! input fill time w, return composed vfp, **ar should be normalized**
double VFPManager::ComposedVFP(double w, vector<VFPDist*> vfps, vector<double> &ar){
    double rt = 0;
    for(int i = 0; i < vfps.size(); i++)
        rt += GetFP(*(vfps[i]), w*ar[i]);
    return rt;
}

vector<double> VFPManager::ComposedVFT(double c, vector<VFPDist*> vfps, vector<double> &ar){
    NormalizeAR(ar);
    double l = 0, h = 1e20;
    while(h-l > 1e-6){
        double mid = (l+h)/2;
        double mc = ComposedVFP(mid, vfps, ar);
        if(mc < c) l = mid;
        else r = mid;
    }
    vector<double> rt;
    for(int i = 0; i < fps.size(); i++)
        rt.push_back(GetVFP(*(vfps[i]), l*ar[i]));
    return rt;
}


