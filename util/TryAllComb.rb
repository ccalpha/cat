#!/usr/bin/ruby

FP_HOME = "../PinTools/Intel_FullTrace"
list = File.readlines("./lists").map{|x| x.strip}
ar = File.readlines("./AccessRate").map{|x| x.strip}

a = (0..list.length-1).to_a
a = a.combination(2).to_a

cnt = 0
for i in 0...list.length
    for j in i+1...list.length
        #system("./NaturalPartition -c 196608 -a #{ar[cnt]} -f #{FP_HOME}/#{list[i]}.footprint #{FP_HOME}/#{list[j]}.footprint")
        cnt = cnt+1
    end
end
