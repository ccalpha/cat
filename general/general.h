#pragma once

#include <vector>
#include <string>
using namespace std;

template <class T>
void PrintVector(vector<T> x, string prefix = "", string suffix = ""){
	cout << prefix;
	for(auto i : x)
		cout << i << " ";
	cout << suffix << endl;
}

template<class X, class Y>
bool ContainBit(X x, Y y){
	return (x>>y)&1;
}


template<typename T>
void Read(istream &is, T &x){
	is.read((char*)&x, sizeof(T));
}

template<typename T>
void Write(ostream &os, T x){ 
	os.write((char*)&x, sizeof(T));
}
