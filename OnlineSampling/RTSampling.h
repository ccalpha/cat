#pragma once

#include <iostream>
#include <map>
#include <stdint.h>
#include <algorithm>

using namespace std;

class RTSampling{
private:
    int SetSize;
    map<uint64_t, uint64_t> watch_points;
    uint64_t *hash;
public:
    RTSampling(int _SetSize = 1024); 
    uint64_t Access(uint64_t addr, uint64_t inst_counter); // return last accesses time, 0 if not found
    uint64_t Update(uint64_t inst_counter); //return the updated address or 0
};

RTSampling::RTSampling(int _SetSize){
    SetSize = _SetSize;
    hash = new uint64_t[1024];  //randomly choose an address to watch instead of the next upcomming data
}

uint64_t RTSampling::Access(uint64_t addr, uint64_t inst_counter){
    if(watch_points.size() < SetSize){
        watch_points[addr] = inst_counter;
        return inst_counter;
    }

    if(watch_points.find(addr) == watch_points.end())
        return 0;

    uint64_t rt = watch_points[addr];
    watch_points.erase(addr);
    return rt;
}

uint64_t RTSampling::Update(uint64_t inst_counter){
    if(watch_points.size() < SetSize) return 0;

    auto max = max_element(watch_points.begin(), watch_points.end(), watch_points.value_comp());
    if(inst_counter - max->second > 1500000){
        uint64_t rt = max->first;
        watch_points.erase(max);
        return rt;
    }
    return 0;
}
