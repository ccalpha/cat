#!/usr/bin/ruby

file = ARGV[0]
nc = ARGV[1].to_i
x = File.readlines(file).map{|x| x=x.strip}

cnt = 0
data = []
for i in x
    data << i
    cnt=cnt+1
    if(cnt % nc == 0)
        puts data.join(" ")
        data = []
    end
end

