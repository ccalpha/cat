\section{Model}
\label{sec:model}

To illustrate the model, we define a variant of the footprint
metric $\fp$ proposed by Xiang et. al. in ASPLOS13. Given a
\emph{memory trace}, a subsequence of the trace is called a
\emph{window}. The length of a window is the length of the
subsequence. The set of the data (cachelines) accessed in
a window is called the \emph{working set} of the window. The
cardinality of a window's working set is called \emph{working set size},
which is denoted as \emph{$\WSS$}. In ASPLOS13, Xiang et. al.
used a metric footprint, $\fp(x)$, to denote the average $\WSS$ of all windows
of length $x$ and derived a quantitative relation between
working set size and LRU miss ratio.

To model a split LRU stack, we need to extend the notion
footprint. Suppose the LRU stack is split into two parts,
the higher one of $H$ entries and the lower of $L$ entries.
The \emph{most-recently-used working set} (MWS) is the set
of $H$ data (possibly less than $H$ if $\WSS$ is less than $H$)
that are most recently used in the working set of a window
while the \emph{least-recently-used working set} (LWS) is the
rest of the data in the working set. The cardinality of
LWS is denoted as \emph{least-recently-used working set size}
(L$\WSS$). Similar to $\fp(x)$, we define the metric least-recently-used
footprint, $\lfp(x)$, as the average L$\WSS$ of all windows of length
$x$. Figure~\ref{fig:notion_illu} illustrates these notions.

\begin{figure}[h]
\centering
\includegraphics[scale=0.23,trim=4 4 4 4,clip]{figures/notion_illustration}
\caption{Illustration of the notion, most-recently-used working set,
leastly-recently-used working set and least-recently-used footprint.}
\label{fig:notion_illu}
\end{figure}

Back to the problem of last-level cache sharing in an exclusive cache
hierarchy, we can model a process's pressure on the last level cache
using $\lfp(x)$. In ASPLOS13, Xiang et. al. proposed a composable approach
using $\fp(x)$ to predict the miss ratio of the shared cache on which
multiple processes run. The strength of their method is the composability
where the locality of each process can be derived from one pass of the memory trace
and the miss ratio of shared cache can be composed from individual process's
profiled locality. Their method can be similarly applied on our notion of $\lfp(x)$ and the
resulted method will be evaluated in Section Evaluation. Our contribution
of this work is an algorithm that derives $\lfp(x)$, for different $H$
and all $x$, \emph{in one pass of the memory trace}. The details of the
algorithm are presented in Section~\ref{sec:algorithm}.

\section{Algorithm}
\label{sec:algorithm}

Suppose $H$ is fixed in this section. If the window length $x$ is fixed,
it is trivial to derive $\lfp(x)$. The challenge lies in how to derive
$\lfp(x)$ in one pass of the memory trace not for one value of $x$, but
for all values of $x$. To begin with, we introduce the big picture
of the algorithm and the notations used in the rest of the section.

While scanning the trace, to calculate for all values of $x$,
we record a few histograms and calculate the target $\lfp(x)$ from
the profiled histograms. A histogram is a mapping from window
lengths to the count of the windows that are of the corresponding
length and satisfy some properties. The definitions of these
windows and the notations of their histograms are given in Table~\ref{tbl:notion}.

\begin{table*}[ht!]
%\tabcolsep=0.11cm
  \centering
  \caption{Notions}
  \vspace{2mm}
%  \resizebox{0.48\textwidth}{!}{
  \begin{tabular}{|c|l|c|l|}
    \hline
    & \textbf{Defintion} & \textbf{Notation} & \textbf{Examples}\\
    \hline
    \hline
    \textbf{H-maximal window} & Window with $\WSS=H$ and any of its properly & MW &\ding{182}\ding{183}\ding{184}\ding{185} in Figure~\ref{fig:example_trace_H3}\\
     & enclosing windows has $\WSS>H$ (Definition~\ref{def:mw}) &  & \\
    \hline
    \textbf{H-overlap window} & Overlapped part of two adjacent H-maximal  & OV &\ding{186}\ding{187}\ding{188} in Figure~\ref{fig:example_trace_H3}\\
     & windows for the same value of $H$ (Definition~\ref{def:ov}) &  &\\
    \hline
    \textbf{H-reuse window}  & Definition~\ref{def:rw} & RW &\ding{182}\ding{183}\ding{184}\ding{185} in Figure~\ref{fig:example_trace_H2} (b)\\
    \hline
    \hline
    \textbf{MW}($H$,$x$) & Histogram of H-maximal windows of $\WSS=H$ &  & Table~\ref{tbl:histo1}, Table~\ref{tbl:histo3}\\
    \hline
    \textbf{OV}($H$,$x$) & Histogram of H-overlap windows for a given $H$ &  & Table~\ref{tbl:histo2}\\
    \hline
    \textbf{RW}($H$,$x$) & Histogram of H-reuse windows of $\WSS=H$ &  & Table~\ref{tbl:histo4}\\
    \hline
    \textbf{RW}($\leq H$,$x$) & Histogram of H-reuse windows of $\WSS\leq H$ &  & Table~\ref{tbl:histo4}\\
    & (i.e. RW(1,$x$) + RW(2,$x$) + ... + RW($H$,$x$)) & &\\
    \hline
    \hline
    \textbf{N} & the length of the entire trace & & 12, for the trace in Figure~\ref{fig:example_trace_H3}\\
    \hline
    \textbf{M} & the total amount of data of the entire trace & & 4, for the trace in Figure~\ref{fig:example_trace_H3}\\
    \hline
  \end{tabular}
%  }
  \label{tbl:notion}
\end{table*}

\begin{table*}[ht!]
\begin{tabular}{c|c}
\begin{minipage}{0.48\textwidth}
\centering
\includegraphics[scale=0.23,trim=4 4 4 4,clip]{figures/example_trace_H3}
\captionof{figure}{Example}
\label{fig:example_trace_H3}
\end{minipage}
&
\begin{minipage}{0.48\textwidth}
\centering
\includegraphics[scale=0.23,trim=4 4 4 4,clip]{figures/example_trace_H2}
\captionof{figure}{Example}
\label{fig:example_trace_H2}
\end{minipage}
\\
\end{tabular}
\end{table*}

The first observation we made is that, for a given window,
$L$\WSS$=\max($\WSS$-H, 0)$. Therefore given window length, to determine
the average L$\WSS$ (i.e. $\lfp$), we need to distinguish the
windows whose $\WSS$ is no more than $H$. And the
total sum of L$\WSS$ can be derived from \textbf{1)} the sum of $\WSS$, \textbf{2)}
the count of the windows with $\WSS\leq H$ and \textbf{3)}
the accumulated sum of their $\WSS$.

\[
\lfp(x)=\frac{\sum\limits_{w}\WSS(w)-\sum\limits_{\WSS(w)> H}H-\sum\limits_{\WSS(w)\leq H}\WSS(w)}{N-x+1}
\]

Note than the first term can be derived from the footprint metric $\fp$,
that is, $\sum\limits_{w}$\WSS$(w)$ is $\fp(x)\times (N-x+1)$. The rest of the section
describes the computation of the other two terms.

The second observation is that there is an algorithm that
scans the trace once and counts the length-$x$ windows of
$\WSS\leq H$ for all possible values of $x$. To understand this algorithm,
we first define a class of special windows called \emph{H-maximal windows}.

\begin{definition}
H-maximal window is a window with $\WSS=H$ and any window that properly
encloses it has $\WSS>H$.
\label{def:mw}
\end{definition}

Intuitively, H-maximal window denotes a \emph{maximal} span in the memory
trace which can contain $H$ distinct data.

We claim one important property of H-maximal window as below. The proof
is omitted due to space constraint.

\begin{lemma}
A window has $\WSS\leq H$ if and only if it is enclosed by some H-maximal window.
\end{lemma}

Due to above property, counting the windows with $\WSS\leq H$ is equivalent
to counting the windows enclosed by the union of H-maximal windows.
In the rest of this section, we use the example in Figure~\ref{fig:example_trace_H3}
as illustration.

\begin{table}[ht!]
%\tabcolsep=0.11cm
  \centering
  \caption{$H$ = 3, MW(3,$x$)}
  \vspace{2mm}
  \resizebox{0.48\textwidth}{!}{
  \begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|}
    \hline
    \textbf{Window Length} & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12 \\
    \hline
    \textbf{Windows}       &   &   &\ding{182}\ding{185}&   &   &\ding{183}\ding{184}&   &   &   &    &    &    \\
    \hline
    \textbf{Window Count}  & 0 & 0 & 2 & 0 & 0 & 2 & 0 & 0 & 0 & 0 & 0 & 0 \\
    \hline
  \end{tabular}
  }
  \label{tbl:histo1}
\end{table}

\begin{table}[ht!]
%\tabcolsep=0.11cm
  \centering
  \caption{$H$ = 3, OV(3,$x$)}
  \vspace{2mm}
  \resizebox{0.48\textwidth}{!}{
  \begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|}
    \hline
    \textbf{Window Length} & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12 \\
    \hline
    \textbf{Windows}       &   & \ding{186}\ding{187}\ding{188}&   &   &   &   &   &   &   &    &    &    \\
    \hline
    \textbf{Window Count}  & 0 & 3 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
    \hline
  \end{tabular}
  }
  \label{tbl:histo2}
\end{table}

Note that all H-maximal windows can be detected in one pass over trace.
By scanning the trace once, we are able to build a histogram ($MW(H,x)$) of H-maximal windows
over window length. The histogram for $H=3$ of the example is given in Table~\ref{tbl:histo1}. In addition,
we maintain a second histogram ($OV(H,x)$) for the overlapped part
between two \emph{adjacent} H-maximal windows over window length.

\begin{definition}
A window is called an H-overlap window if it is an overlapped part
between two adjacent H-maximal windows.
%\textcolor{red}{should we define adjacent?}
%\textcolor{red}{Should we emphasize that $\WSS$(H-overlap windows) != H? Definition of OV(H,x) in Table1 would be a little ambiguity}
\label{def:ov}
\end{definition}

For example, the window from $b$ to $c$ (\ding{186}) is the overlapped part
between two adjacent H-maximal windows \ding{182} and \ding{183}.
The H-overlap window's histogram for $H=3$ is presented in Table~\ref{tbl:histo2}.
A length-$T$ H-maximal window encloses $T-x+1$ length-$x$
windows. \emph{If all H-maximal windows are disjoint}, the count
of length-$x$ windows enclosed by H-maximal windows can be read solely
from the first histogram by accumulating the count of windows
within each H-maximal window. Because H-maximal windows overlap,
some length-$x$ windows are over counted. Te overcount of these windows
can be read from the second histogram.
We claim following theorem and omit its proof due to space constraint.

\begin{theorem}
The count of length-$x$ windows with $\WSS$ $\leq H$ is
$$\sum_{t=x}^{N}MW(H,t)\times (t-x+1) - \sum_{t=x}^{N}OV(H,t)\times (t-x+1)$$
\end{theorem}

For $x=4$ and $H=3$, in the example trace, we want to count the length-$4$
windows with $\WSS$ $\leq 3$. The first term in above theorem is $2\times (6-4+1)=6$
and the second term is $0$. Therefore there are $6$ windows satisfying the
condition. They are $bcdd$, $cddc$, $ddcb$, $cbaa$, $baab$ and $aabc$.
Readers of interest can verify the formula with different $x$.

We need the final piece of the puzzle in computing $\lfp$, the accumulated sum of
$\WSS$ over the length-$x$ windows with $\WSS<H$. A naive way to compute this
value is through counting the windows of $\WSS=H$ for all possible valueso of $H$ using the
method described above and summing over their $\WSS$. Doing do requires maintaining
$\mathcal{O}(H)$ histograms. Upon every memory access, $2H$ histograms
needs to be updated, which is $\mathcal{O}(H)$ operations per access. Instead, we made
the third observation, that the necessary information in the $2H$ histograms can be represented
using 4 histograms, which can be obtained in $\mathcal{O}(1)$ operations per access (as
in Figure~\ref{fig:opt_part}).

\begin{figure}[h]
\centering
\includegraphics[scale=0.23,trim=4 4 4 4,clip]{figures/opt_part}
\caption{The optimized approach over the naive approach.}
\label{fig:opt_part}
\end{figure}

\begin{definition}
A window is an H-reuse window, if it is an H-maximal window satisfying either of following conditions:
\begin{itemize}
\item either end is the trace's boundary (e.g. \ding{182} and \ding{185} in Figure~\ref{fig:example_trace_H2} (b)).
\item it is a reuse window (e.g. \ding{183} and \ding{184} in Figure~\ref{fig:example_trace_H2} (b)).
\end{itemize}
\label{def:rw}
\end{definition}

\noindent Note that Xiang et. al. profiled the histogram of H-reuse windows
for all values of $H$ ($RW(\leq \infty, x)$) to calculate the metric of footprint
($\fp(x)$) in ASPLOS13.

\begin{lemma}
An H-maximal window is either an H-overlap window or an H-reuse window.
\label{lemma:reuse}
\end{lemma}

\begin{lemma}
MW(H,x) = OV(H+1,x) + RW(H,x)
\label{lemma:relation}
\end{lemma}

Theorem~\ref{lemma:relation} can be verified by looking at Figure~\ref{fig:example_trace_H3} and
Figure~\ref{fig:example_trace_H2} (a), where \ding{186}, \ding{187} and \ding{188} in Figure
\ref{fig:example_trace_H3} (OV(3,x)) correspond to \ding{183}, \ding{185} and \ding{187}
in Figure~\ref{fig:example_trace_H2} (a) (MW(2,x)). \ding{182}, \ding{184}, \ding{186} and
\ding{188} are exactly the H-reuse windows for $H=2$ (RW(2,x)).

%comment(cc) : because H-maximal windows is monotonic increasing,
%meaning if window a is larger than b, then start point of a should
%be on the right of start point of b, and end point of a is the same.
%Otherwise, the overlap rule does not work.
%for example, three window, [1,10], [2,8], [3, 12], theorem does not work.

%comment(Hao) : by definition, if a enclose b, $\WSS$(a) > H, which
%is contradictory to the fact that a is an H-maximal window

\begin{table}[ht!]
%\tabcolsep=0.11cm
  \centering
  \caption{$H$ = 2, MW(2,$x$)}
  \vspace{2mm}
  \resizebox{0.48\textwidth}{!}{
  \begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|}
    \hline
    \textbf{Window Length} & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12 \\
    \hline
    \textbf{Windows}       &   &\ding{182}\ding{183}\ding{185}\ding{187}\ding{188}&   &\ding{184}\ding{186}&   &   &   &   &    &    &    &   \\
    \hline
    \textbf{Window Count}  & 0 & 5 & 0 & 2 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
    \hline
  \end{tabular}
  }
  \label{tbl:histo3}
\end{table}

\begin{table}[ht!]
%\tabcolsep=0.11cm
  \centering
  \caption{$H$ = 1, RW(1,$x$) (RW($\leq$ 1, $x$))}
  \vspace{2mm}
  \resizebox{0.48\textwidth}{!}{
  \begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|}
    \hline
    \textbf{Window Length} & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12 \\
    \hline
    \textbf{Windows}       &\ding{182}\ding{185}&\ding{183}\ding{184}&   &    &    &   &   &    &    &    &   &\\
    \hline
    \textbf{Window Count}  & 2 & 2 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
    \hline
  \end{tabular}
  }
  \label{tbl:histo4}
\end{table}

We claim the following theorem.

\begin{theorem}
The sum of $\WSS$ of the length-$x$ windows with $\WSS$ $\leq H$ is
\begin{align*}
  &\sum\limits_{t=x}^{N}MW(H,t)\times (t-x+1)\times H  \\
- &\sum\limits_{t=x}^{N}OV(H,t)\times (t-x+1)\times H  \\
- &\sum\limits_{t=x}^{N}MW(H-1,t)\times(t-x+1) \\
- &\sum\limits_{t=x}^{N}RW(\leq H-2,t)\times(t-x+1)
\end{align*}
\end{theorem}

We walk through two cases on the example trace to demonstrate the above calculation.

\begin{itemize}
\item \textbf{$x=3$ and $H=3$}. From Table~\ref{tbl:histo1}, we read the first term is
$3\times MW(3,3)\times (3-3+1) + 3\times MW(3,6)\times (6-3+1)$, which is $30$.
The second term is $0$. The third term, read from Table~\ref{tbl:histo3}, is
$MW(2,4)\times (4-3+1)$, which is $4$. The fourth term is $0$. Therefore above
calculation yields $26$. In the example trace,
there are 6 length-$3$ windows of $\WSS$ $3$ ($abc$, $bcd$, $dcb$, $cba$, $abc$ and $bcd$)
and 4 length-$3$ windows of $\WSS$ $2$ ($cdd$, $ddc$, $baa$ and $aab$). The results match.

\item \textbf{$x=5$ and $H=3$}. Similarly, From Table~\ref{tbl:histo1}, we read
the first term as $3\times MW(3, 6)\times (6-5+1)$. Other three terms are all $0$.
So we get the result as $12$. Note that there are 4 length-$5$ windows of $\WSS$ $\leq 3$,
$bcddc$, $cddcb$, $cbaab$ and $baabc$. Their sum of $\WSS$ is $12$.

\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[scale=0.23,trim=4 4 4 4,clip]{figures/roadmap}
\caption{The roadmap for calculating $\lfp(x)$ for a given $H$.}
\label{fig:roadmap}
\end{figure}

To put it together, there is a linear algorithm to derive $\lfp(x)$ for a given $H$.
The calculation flow is shown in Figure~\ref{fig:roadmap}.
To derive for different $H$, we simply maintain multiple copies of the profile, one
for each target $H$. The possible values of $H$ can not be too many. In the problem of
modeling exclusive shared cache, $H$ is the typical private cache capacity. In
the problem of modeling cache partitioning, $H$ is the typical cache ways allocated
for one core.

%comment(cc) : do we need a conclusion for lfp(H,x) ? show lfp(H,x) = SumOfWSS/CountOfWindows
