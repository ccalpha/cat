#include<iostream>
#include <algorithm>
using namespace std;

int *mask;
int *tag;
int H1;
int H2;
int L;
int C;

//True = miss
bool Access(int x, int pid){
    for(int i = 0; i < C; i++){
        if(tag[i] == x){
            tag[i] = 0;
            break;
        }
    }

    for(int i = 0; i < C && x; i++){
        if(mask[i] & (1<<pid))
            swap(x, tag[i]);
    }
    return x != 0;
}

void OutputCacheStat(){
    for(int i = 0; i < C; i++)
        cout << tag[i] << " ";
    cout << endl;
}

//command H1 H2 L
int main(int argc, char **argv){
    H1 = atoi(argv[1]);
    H2 = atoi(argv[2]);
    L = atoi(argv[3]);
    C = H1+H2+L;
    cout << H1 << " " << H2 << " " << L << endl;
    
    mask = new int[C];
    tag = new int[C];
    fill(mask, mask+C, 3);
    fill(tag, tag+C, 0);
    fill(mask, mask+H1, 1);
    fill(mask+H1, mask+H1+H2, 2);
    
    int A = 0;
    int B = 100;
    int misses = 0;
    for(int i = 0; i < 100000; i++){
        misses += Access((A%5)+1, 0);
        misses += Access(B, 1);
        A++;
        B++;
        if(i <= 200)
            OutputCacheStat();
    }
    cout << misses << " " << 200000 << endl;
}
