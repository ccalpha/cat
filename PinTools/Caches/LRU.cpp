#include <iostream>

#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters

#include "pin_cache.H"
#include "../general/Caches.h"
#include "../general/CacheConfig.h"
#include <set>

KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "stdout", "Output file");
std::ostream *fout;

LOCALFUN ITLB::CACHE itlb("ITLB", ITLB::cacheSize, ITLB::lineSize, ITLB::associativity);
LOCALVAR DTLB::CACHE dtlb("DTLB", DTLB::cacheSize, DTLB::lineSize, DTLB::associativity);
LOCALVAR IL1::CACHE il1("L1 Instruction Cache", IL1::cacheSize, IL1::lineSize, IL1::associativity);
LOCALVAR DL1::CACHE dl1("L1 Data Cache", DL1::cacheSize, DL1::lineSize, DL1::associativity);
LOCALVAR LRUCACHE<UL2::allocation> ul2("L2 Unified Cache : LRU", UL2::cacheSize, UL2::lineSize, UL2::associativity);
LOCALVAR LRUCACHE<UL3::allocation> ul3("L3 Unified Cache : LRU", UL3::cacheSize, UL3::lineSize, UL3::associativity);

UINT64 ins_count = 0;
UINT64 ref_count = 0;
std::set<ADDRINT> wss_count;

LOCALFUN VOID Fini(int code, VOID * v)
{
    *fout << ins_count << "\t" << ref_count << "\t" << wss_count.size() << "\n";

//    *fout << itlb;
//    *fout << dtlb;
    *fout << il1;
    *fout << dl1;
    *fout << ul2;
    //*fout << ul3;
}



LOCALFUN VOID Ul2Access(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    // second level unified cache
    //const BOOL ul2Hit =
    ul2.Access(addr, size, accessType);

    // third level unified cache
    //if ( ! ul2Hit) ul3.Access(addr, size, accessType);
}


LOCALFUN VOID InsRef(ADDRINT addr)
{
    ins_count++;
    const UINT32 size = 1; // assuming access does not cross cache lines
    const CACHE_BASE::ACCESS_TYPE accessType = CACHE_BASE::ACCESS_TYPE_LOAD;

    // ITLB
    //itlb.AccessSingleLine(addr, accessType);

    // first level I-cache
    const BOOL il1Hit = il1.AccessSingleLine(addr, accessType);
    // second level unified Cache
    if ( ! il1Hit) Ul2Access(addr, size, accessType);
}

LOCALFUN VOID MemRefMulti(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    ref_count++;
    //wss_count.insert(addr);
    // DTLB
    //dtlb.AccessSingleLine(addr, CACHE_BASE::ACCESS_TYPE_LOAD);

    // first level D-cache
    //const BOOL dl1Hit = dl1.Access(addr, size, accessType);

    // second level unified Cache
    //if ( ! dl1Hit) Ul2Access(addr, size, accessType);
}

LOCALFUN VOID MemRefSingle(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    ref_count++;
    //wss_count.insert(addr);
    // DTLB
    //dtlb.AccessSingleLine(addr, CACHE_BASE::ACCESS_TYPE_LOAD);

    // first level D-cache
    //const BOOL dl1Hit = dl1.AccessSingleLine(addr, accessType);
    // second level unified Cache
    //if ( ! dl1Hit) Ul2Access(addr, size, accessType);
}


LOCALFUN VOID Instruction(INS ins, VOID *v)
{
    // all instruction fetches access I-cache
    INS_InsertCall(
        ins, IPOINT_BEFORE, (AFUNPTR)InsRef,
        IARG_INST_PTR,
        IARG_END);

    if (INS_IsMemoryRead(ins) && INS_IsStandardMemop(ins))
    {
        const UINT32 size = INS_MemoryReadSize(ins);
        const AFUNPTR countFun = (size <= 4 ? (AFUNPTR) MemRefSingle : (AFUNPTR) MemRefMulti);

        // only predicated-on memory instructions access D-cache
        INS_InsertPredicatedCall(
            ins, IPOINT_BEFORE, countFun,
            IARG_MEMORYREAD_EA,
            IARG_MEMORYREAD_SIZE,
            IARG_UINT32, CACHE_BASE::ACCESS_TYPE_LOAD,
            IARG_END);
    }

    if (INS_IsMemoryWrite(ins) && INS_IsStandardMemop(ins))
    {
        const UINT32 size = INS_MemoryWriteSize(ins);
        const AFUNPTR countFun = (size <= 4 ? (AFUNPTR) MemRefSingle : (AFUNPTR) MemRefMulti);

        // only predicated-on memory instructions access D-cache
        INS_InsertPredicatedCall(
            ins, IPOINT_BEFORE, countFun,
            IARG_MEMORYWRITE_EA,
            IARG_MEMORYWRITE_SIZE,
            IARG_UINT32, CACHE_BASE::ACCESS_TYPE_STORE,
            IARG_END);
    }
}

GLOBALFUN int main(int argc, char *argv[])
{
    PIN_Init(argc, argv);
    if(KnobOutput.Value() == "stdout")
      fout = &(std::cout);
    else
      fout = new ofstream(KnobOutput.Value().c_str());

    INS_AddInstrumentFunction(Instruction, 0);
    PIN_AddFiniFunction(Fini, 0);

    // Never returns
    PIN_StartProgram();

    return 0; // make compiler happy
}
