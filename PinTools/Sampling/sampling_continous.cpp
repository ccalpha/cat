#include <pin.H>
#include <ctime>
#include <cstdlib>
#include <map>
#include <iostream>
#include <fstream>
#include <assert.h>
typedef UINT32 CACHE_STATS; // type of cache hit/miss counters
#include "pin_cache.H"

using namespace std;

KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "SampledTrace", "Output file of trace");
KNOB<UINT64> KnobSample(KNOB_MODE_WRITEONCE, "pintool", "sample", "4000000000", "Number of references sampled(1 billion by default)"); 
KNOB<UINT64> KnobSkip(KNOB_MODE_WRITEONCE, "pintool", "skip", "8192", "reference skipped");

UINT64 sinst = 0;
UINT64 skipped = 0;
UINT64 instcount = 0;
ofstream traceout;


LOCALFUN VOID MemRef(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType){
    if(skipped){
        --skipped;
        return;
    }
    if(sinst-- == 0)
        PIN_ExitApplication(0);
    instcount++;
    traceout.write((char*)&addr, sizeof(addr));
}

LOCALFUN VOID Instruction(INS ins, VOID *v){
    if(INS_IsStandardMemop(ins) && (INS_IsMemoryRead(ins) || INS_IsMemoryWrite(ins))){
        INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)MemRef,
                INS_IsMemoryRead(ins)?IARG_MEMORYREAD_EA : IARG_MEMORYWRITE_EA,
                INS_IsMemoryRead(ins)?IARG_MEMORYREAD_SIZE : IARG_MEMORYWRITE_SIZE,
                IARG_UINT32, CACHE_BASE::ACCESS_TYPE_LOAD,
                IARG_END);
    }
}

LOCALFUN VOID Fini(int code, VOID * v){
    cout << "Finish Sample" << endl;
    traceout.write((char*)&instcount, sizeof(instcount));
    skipped = KnobSkip.Value();
    traceout.write((char*)&skipped, sizeof(skipped));
    traceout.close();
}

LOCALFUN VOID Init(){
    cout << "BEGIN TO SAMPLE" << endl;
    sinst = KnobSample.Value();
    skipped = KnobSkip.Value();
    traceout.open(KnobOutput.Value().c_str(), ofstream::out);
    assert(traceout.good());
}

GLOBALFUN int main(int argc, char *argv[])
{
    PIN_InitSymbols();
    PIN_Init(argc, argv);
    Init();
    //IMG_AddInstrumentFunction(Image, 0);
    INS_AddInstrumentFunction(Instruction, 0);
    PIN_AddFiniFunction(Fini, 0);

    PIN_StartProgram();

    return 0; // make compiler happy
}
