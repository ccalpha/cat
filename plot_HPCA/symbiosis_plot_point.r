library(ggplot2)
library(data.table)
library("gridExtra")
library("cowplot")

sym <- read.table("./symbiosis.data", header=TRUE)
attach(sym)
sym <- sym[order(OPT),]
sym$ID <- seq(0,length(OPT)-1)
detach(sym)

sym <- melt(sym, id=c("ID"))

ggplot(sym) +
  geom_point(aes(x=ID,y=value, color=factor(variable, labels=c("OPT", "VFP", "DI")))) +
  labs(x="Accesses(x1M)", y="Working set size") +
  theme_gray()+
  theme(text = element_text(size=14))+
  theme(legend.justification=c(0,1),legend.position=c(0,1)) +
  guides(color=guide_legend(title = NULL))
