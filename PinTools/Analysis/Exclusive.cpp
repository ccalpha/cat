#include <iostream>

#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters

#include "pin_cache.H"
#include "../general/Caches.h"
#include "../general/CacheConfig.h"
#include "../../general/TraceReader.h"
#include "../../general/general.h"

#include <vector>

KNOB<string> KnobTrace(KNOB_MODE_WRITEONCE, "pintool", "t", "traces", "list of traces");
KNOB<string> KnobAR(KNOB_MODE_WRITEONCE, "pintool", "a", "", "list of access rates");
KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "stdout", "Output file");

std::ostream *fout;

vector<double> accessRate;
vector<TraceReader*> trl;
vector<LRUCACHE<UL2::allocation>*> ul2s;
LOCALVAR LRUCACHE<UL3::allocation> ul3("L3 Unified Cache : LRU", UL3::cacheSize, UL3::lineSize, UL3::associativity);

vector<UINT64> count_access;

LOCALFUN VOID InitProcess(){
    ifstream fin(KnobTrace.Value().c_str());
    assert(fin.good() && "List file error");
    string tmp;
    while(fin >> tmp){
        trl.push_back(new TraceReader(tmp));
        count_access.push_back(0);
    }
    fin.close();
    for(auto i : trl)
        i->ShowInfo();
    for(uint32_t i = 0; i < trl.size(); i++)
        ul2s.push_back(new LRUCACHE<UL2::allocation>("L2 Dedicated Cache : LRU", UL2::cacheSize, UL2::lineSize, UL2::associativity));
    if(KnobAR.Value() == ""){
        for(uint32_t i = 0; i < trl.size(); i++)
            accessRate.push_back((double)(trl[i]->GetNumChunk()));
    }
    else{
        fin.open(KnobAR.Value().c_str());
        double tval;
        while(fin >> tval)
            accessRate.push_back(tval);
        fin.close();
    }
    double sum = accumulate(accessRate.begin(), accessRate.end(), 0.0);
    for(auto &i : accessRate)
        i /= sum;
    PrintVector(accessRate, "Access Rates: ");
}

LOCALFUN VOID Fini(int code, VOID * v)
{
    PrintVector(count_access, "Accesses Count : ");
    *fout << "Info of L2 Caches\n";
    for(auto i : ul2s)
        *fout << *i;
    *fout << "Info of L3 Caches\n";
    *fout << ul3;
}


LOCALFUN VOID Ul2Access(int pid, ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType = CACHE_BASE::ACCESS_TYPE_LOAD)
{
    //cout << pid << " " << addr << endl;
    count_access[pid]++;
    // second level unified cache
    //const BOOL ul2Hit =
    //cout << addr << endl;
    BOOL l2hit = ul2s[pid]->Access(addr, size, accessType);
    if(!l2hit)
        ul3.Access(addr, size, accessType);

    // third level unified cache
    //if ( ! ul2Hit) ul3.Access(addr, size, accessType);
}

LOCALFUN VOID MemRefSingle(int pid, ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType = CACHE_BASE::ACCESS_TYPE_LOAD)
{
    // DTLB
    //dtlb.AccessSingleLine(addr, CACHE_BASE::ACCESS_TYPE_LOAD);

    // first level D-cache
    //const BOOL dl1Hit = dl1.AccessSingleLine(addr, accessType);
    // second level unified Cache
    //if ( ! dl1Hit)
      Ul2Access(pid, addr, size, accessType);
}


GLOBALFUN int main(int argc, char *argv[])
{
    PIN_Init(argc, argv);
    if(KnobOutput.Value() == "stdout")
      fout = &(std::cout);
    else
      fout = new ofstream(KnobOutput.Value().c_str());
    InitProcess();
    //Interleave based on access rate
    vector<uint32_t> ref_id;
    vector<MemRef*> ref_ptrs;
    vector<uint64_t> addr_id;

    for(uint32_t i = 0; i < trl.size(); i++){
        ref_id.push_back(0);
        ref_ptrs.push_back(trl[i]->GetChunk(0));
        addr_id.push_back(0);
    }

    UINT64 flags = 1;
    flags <<= trl.size();
    flags--;
    vector<double> counters(trl.size(), 0.0);
    while(flags){
        for(uint32_t i = 0; i < accessRate.size(); i++){
            if(!((flags>>i)&1))
                continue;
            counters[i] += accessRate[i];
            if(floor(counters[i]) != floor(counters[i]-accessRate[i])){
                if(addr_id[i] >= trl[i]->GetChunkSize()){ //the end of this chunk, go to next one
                    //update addr id
                    addr_id[i] = 0;
                    delete ref_ptrs[i];

                    //update chunk id
                    ref_id[i]++;
                    if(ref_id[i] >= (uint32_t)trl[i]->GetNumChunk()){
                        flags &= ~((UINT64)1 << i);
                        cout << "Program " << i << " Finished" << endl;
                        continue;
                    }

                    //update chunk ref
                    ref_ptrs[i] = trl[i]->GetChunk(ref_id[i]);
                }
                //access
                MemRefSingle(i, (ref_ptrs[i])[addr_id[i]].address, (ref_ptrs[i])[addr_id[i]].size);
                addr_id[i]++;
            }
        }
    }

    Fini(0, NULL);
/*
    for(int i = 0; i < trace.GetNumChunk(); i++){
      cout << "Working on chunk " << i << endl;
      MemRef* refs = trace.GetChunk(i);
      for(uint64_t j = 0; j < trace.GetChunkSize(); j++){
        MemRefSingle(refs[j].address, refs[j].size);
      }
      delete refs;
    }
*/
    return 0; // make compiler happy
}
