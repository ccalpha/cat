#!/usr/bin/ruby

#groups = File.readlines("groups").to_a
#allocs = File.readlines("allocs").to_a
names = File.readlines("../PinTools/general/names.txt").to_a
FPDir = "../PinTools/Traces_SampledRef/"

allocs = []
for i0 in 0...20
    for j0 in 0...20
        for i1 in i0...20
            for j1 in j0...20
                a = Array.new(20,0)
                
                for i in i0..i1
                    a[i] |= 1
                end
                for i in j0..j1
                    a[i] |= 2
                end
                
                flag = true
                for i in a
                    if(i == 0)
                        flag = false
                        break
                    end
                end

                if(flag == false)
                    next
                end

                allocs << a.join(" ")
            end
        end
    end
end


for i in 0...names.length
    for j in i+1...names.length
        puts "#{i} #{j}"
        fps = "#{FPDir}/#{names[i].strip}.footprint #{FPDir}/#{names[j].strip}.footprint"
        mmr = 1.0
        malloc = ""
        for k in allocs
            File.open("alloc", "w"){|f|
                f.write(k)
            }
            mr = `./PredictCAT -c 81920 -a alloc #{fps} | grep "Overall Miss Ratio" `
            mr = mr.split[4].to_f
            if(mr < mmr)
                mmr = mr
                malloc = k
            end
        end
        puts mmr
        puts malloc
    end
end
