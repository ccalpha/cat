#include <iostream>
#include <iomanip>
#include <vector>
#include <map>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

map<int, double> opt;

map<int, double> vfp_missratio;
map<int, double> vfp_deg;
map<int, double> vfp_mr;

map<int, double> fp_missratio;
map<int, double> fp_deg;
map<int, double> fp_mr;

map<int, int> vfp_backtrace;
map<int, int> fp_backtrace;

class _data{
public:
    int GID;
    int ID[3];
    double mr;
    double vfp;
    double fp;
    double deg;
    int mask;
};

_data data[1024];

int mrid[1024];
double missrate[1024];

class _individuals{
public:
    int nprog;
    int id[1024];
    double missrate[1024];   //must be sorted by decesending
    _individuals(){
        nprog = 0;
        ifstream fin("./missrate.data");
        while(fin >> id[nprog])
            fin >> missrate[nprog++];
    }
    double GetDeg(int mask, int num=2){
        int ids[num];
        fill(ids, ids+num, 0);
        int cnt = 0;
        for(int i = 0; i < nprog; i++){
            if(mask & (1<<i)){
                ids[cnt++] |= (1<<i);
                cnt %= num;
            }
        }
        double rt = 0;
        for(int i = 0; i < num; i++){
            if(opt.find(ids[i]) == opt.end())
                cout << "ERROR : " << hex << ids[i] << endl;
            rt += opt[ids[i]];
        }
        return rt;
    }
}individuals;

string deserilize(int mask){
    ostringstream os;
    for(int i = 0; i < 32; i++)
        if(mask & (1<<i))
            os << i << ",";
    string rt = os.str();
    rt.pop_back();
    return rt;
}

int main(){

    int ssize = 4;
    ifstream fin("./group_4_degradation.data");
    int cnt = 0;
    while(fin >> data[cnt].GID){
        data[cnt].mask = 0;
        for(int i = 0; i < ssize; i++){
            fin >> data[cnt].ID[i];
            data[cnt].mask |= 1<<data[cnt].ID[i];
        }
        fin >> data[cnt].mr;
        fin >> data[cnt].vfp;
        fin >> data[cnt].fp;
        fin >> data[cnt].deg;
        cnt++;
    }
    for(int i = 0; i < cnt; i++){
        opt[data[i].mask] = data[i].deg;
            
        vfp_missratio[data[i].mask] = data[i].vfp;
        vfp_deg[data[i].mask] = data[i].deg;
        vfp_mr[data[i].mask] = data[i].mr;

        fp_missratio[data[i].mask] = data[i].fp;
        fp_deg[data[i].mask] = data[i].deg;
        fp_mr[data[i].mask] = data[i].mr;

        vfp_backtrace[data[i].mask] = 0;
        fp_backtrace[data[i].mask] = 0;
    }

    vector<int> ms;
    for(auto j : opt)
        ms.push_back(j.first);
    
    for(int i = 0; i < cnt; i++){
        for(auto j : ms){
            if(data[i].mask & j)
                continue;
            int k = data[i].mask | j;

            if(opt.find(k) != opt.end()){
                opt[k] = min(data[i].deg+opt[j], opt[k]);

                if(vfp_missratio[k] > data[i].vfp + vfp_missratio[j]){
                    vfp_missratio[k] = data[i].vfp + vfp_missratio[j];
                    vfp_deg[k] = data[i].deg + vfp_deg[j];
                    vfp_mr[k] = data[i].mr + vfp_mr[j];
                    vfp_backtrace[k] = data[i].mask;
                }

                if(fp_missratio[k] > data[i].fp + fp_missratio[j]){
                    fp_missratio[k] = data[i].fp + fp_missratio[j];
                    fp_deg[k] = data[i].deg + fp_deg[j];
                    fp_mr[k] = data[i].mr + fp_mr[j];
                    fp_backtrace[k] = data[i].mask;
                }
            }else{
                opt[k] = data[i].deg + opt[j];

                vfp_missratio[k] = data[i].vfp + vfp_missratio[j];
                vfp_deg[k] = data[i].deg + vfp_deg[j];
                vfp_mr[k] = data[i].mr + vfp_mr[j];
                vfp_backtrace[k] = data[i].mask;

                fp_missratio[k] = data[i].fp + fp_missratio[j];
                fp_deg[k] = data[i].deg + fp_deg[j];
                fp_mr[k] = data[i].mr + fp_mr[j];
                fp_backtrace[k] = data[i].mask;
            }
        }
    }

    int cntid = 0;
    for(auto i : opt){
        if(__builtin_popcount(i.first) == ssize)
            continue;
        cout  << deserilize(i.first) << " " << opt[i.first] << " " << vfp_deg[i.first] << " " << individuals.GetDeg(i.first) << " " << fp_deg[i.first];
        cout << " " << deserilize(vfp_backtrace[i.first]) << " " << deserilize(fp_backtrace[i.first]);
        cout << endl;
        //cout << setprecision(10) << vfp_mr[i.first] << " " << fp_mr[i.first] << endl;    
    }
    return 0;
}
