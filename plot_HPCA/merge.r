library(data.table)

data<-read.table("./FX8120_3corun", header=TRUE)
classes<-read.table("./Classes", header=TRUE)

data<-data.table(data)
classes<-data.table(classes)
data <- merge(data, classes, by.x="ID1", by.y="ID", all.x=TRUE)
colnames(data)[which(names(data) == "Class")] = "Class1"
data <- merge(data, classes, by.x="ID2", by.y="ID", all.x=TRUE)
colnames(data)[which(names(data) == "Class")] = "Class2"
data <- merge(data, classes, by.x="ID3", by.y="ID", all.x=TRUE)
colnames(data)[which(names(data) == "Class")] = "Class3"

data[,Name.x:=NULL]
data[,Name.y:=NULL]
data[,Name:=NULL]

#write.table(data, "./out")

