#What is this?

library(data.table)

options(digits=8)
coruns <- read.table("./coruns_3.data", header=TRUE)
coruns <- coruns[which(coruns$ID1 != 19 & coruns$ID2 != 19 & coruns$ID3 != 19), ]
coruns <- coruns[which(coruns$ID1 != 22 & coruns$ID2 != 22 & coruns$ID3 != 22), ]

individuals <- read.table("./individuals.data", header=TRUE)

coruns<-data.table(coruns)
individuals <- data.table(individuals)

mix <- coruns
mix <- merge(mix, individuals, by.x = "ID1", by.y = "GroupID", all.x = TRUE)
names(mix)[names(mix)=="AR"] <- "AR1"
mix <- merge(mix, individuals, by.x = "ID2", by.y = "GroupID", all.x = TRUE)
names(mix)[names(mix)=="AR"] <- "AR2"
mix <- merge(mix, individuals, by.x = "ID3", by.y = "GroupID", all.x = TRUE)
names(mix)[names(mix)=="AR"] <- "AR3"

write.table(mix, "./filtered_3.data");
