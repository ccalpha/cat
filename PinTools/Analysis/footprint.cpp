#include <iostream>

#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters

#include "pin_cache.H"
#include "../../general/footprint.h"
#include "../../general/TraceReader.h"

KNOB<string> KnobTrace(KNOB_MODE_WRITEONCE, "pintool", "t", "trace", "trace file");
KNOB<double> KnobCacheSize(KNOB_MODE_WRITEONCE, "pintool", "c", "16384", "number of cache blocks");
KNOB<UINT64> KnobLineSize(KNOB_MODE_WRITEONCE, "pintool", "l", "6", "log2 of cache line size, default 7");
FPDist *fpd;

LOCALFUN VOID Fini(double mr, double mrc)
{
  cout << "number of data and accesses : " << fpd->GetDataCount() << " " << fpd->access_count << endl;
  cout << "Number of cache blocks : " << KnobCacheSize.Value() << endl;
  cout << "Size of cache block : " << KnobLineSize.Value() << endl;
  cout << "Miss ratio without cold misses : " << mr*100 << "%" << endl;
  cout << "MissRatio with cold misses : " << mrc*100 << "%" << endl;
  //fpd->OutputToFile(KnobOutput.Value());
  delete fpd;
}

UINT64 floor2CacheLine = 6;

LOCALFUN VOID MemRefSingle(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType = CACHE_BASE::ACCESS_TYPE_LOAD)
{
  fpd->Access(addr >> floor2CacheLine);
}

GLOBALFUN int main(int argc, char *argv[])
{
    PIN_Init(argc, argv);
    fpd = new FPDist();
    floor2CacheLine = KnobLineSize.Value();


    TraceReader trace(KnobTrace.Value());
    trace.ShowInfo();

    int CacheSize = KnobCacheSize.Value();
    double mr = 0, mrc = 0;
    fpd->InitCalculation();
    for(int i = 0; i < trace.GetNumChunk(); i++){
    //for(int i = 0; i < 2; i++){
    // fpd->StartNewChunk();
    //fpd->InitCalculation();
      cout << "Working on chunk " << i << endl;
      MemRef* refs = trace.GetChunk(i);
      for(uint64_t j = 0; j < trace.GetChunkSize(); j++)
        MemRefSingle(refs[j].address, refs[j].size);
    //fpd->FinishCalculation();
    //mr = fpd->GetMissRatio(CacheSize);
    //cout << mr << endl;
      //mrc += fpd->GetMissRatioColdMiss(CacheSize);
      delete refs;
    }
    //Fini(mr/trace.GetNumChunk(), mrc/trace.GetNumChunk());
    //return 0;
    
    fpd->FinishCalculation();
    string rawname = KnobTrace.Value();
    int lastindex = rawname.find_last_of(".");
    rawname = rawname.substr(0, lastindex);
    fpd->OutputToFile(rawname+".footprint");
    mr = fpd->GetMissRatio(CacheSize);
    mrc = fpd->GetMissRatioColdMiss(CacheSize);
    Fini(mr, mrc);
    return 0; // make compiler happy
}
