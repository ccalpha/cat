#ifndef __MassArray_H
#define __MassArray_H

#include "DPTransientMask.h"

inline double SumOfFPDS(double w){
  double ret = 0;
  for(int i = 0; i < fpds.size(); i++)
    ret += (*fpds[i])[w];
  return ret;
}

inline double MinOfFPDS(double w){
  double ret = 1e128;
  for(int i = 0; i < fpds.size(); i++)
    ret = min(ret, (*fpds[i])[w]);
  return ret;
}

inline int IsValidation(double w, double c){
  if(SumOfFPDS(w) < c)
    return -1;  //w is too small
  if(MinOfFPDS(w) > c)
    return 1; //w is too large
  return 0;
}

inline int Validate(double w, double c, double &l, double &r, double rw){
  int stat = IsValidation(w, c);
  switch(stat){
    case 0 : return 0; break;
    case 1 : r = rw; break;
    case -1 : l = rw; break;
  }
  return 0;
}

/*
given th and w, return minimal miss ratio
Granularity is for cache size, probabality we can use larger granularity
*/
double MinMissRatioLarge(double h, double l, double th, double w, int *sol = NULL, double g = 64.0){
  int hblock = floor(h / g);
  int lblock = floor(l / g);
  int cblock = hblock+lblock;
  const double MAX = 2.0;
  int n = fpds.size();
  //double DP[n][hblock+n+1][cblock+n+1];
  double **DP[n];
  //cout << "Size of H and L : " << h << " " << l << " " << th << " " << w << endl;
  for(int i = 0; i < n; i++){
    DP[i] = new double*[hblock+n+1];
    for(int j = 0; j < hblock+n+1; j++){
      DP[i][j] = new double[cblock+n+1];
      fill(DP[i][j], DP[i][j]+cblock+n+1, MAX);
    }
  }

  if(GetIdx((*fpds[0])[th], g) < hblock && GetIdx((*fpds[0])[th+w], g) < cblock)
    DP[0][GetIdx((*fpds[0])[th], g)][GetIdx((*fpds[0])[th+w], g)] = (*fpds[0])[th+w+1]-(*fpds[0])[th+w];
  if(GetIdx((*fpds[0])[w], g) < hblock)
    DP[0][0][GetIdx((*fpds[0])[w], g)] = (*fpds[0])[w+1] - (*fpds[0])[w];

  for(int i = 0; i < n-1; i++){
    for(int j = 0; j < hblock; j++){
      for(int k = 0; k < cblock; k++){
        if(DP[i][j][k] == MAX) continue;
        //cout << "Progressing : " << i << " " << j << " " << k << " : " << DP[i][j][k] << endl;

        int x = j+GetIdx((*fpds[i+1])[th], g);
        int y = k+GetIdx((*fpds[i+1])[th+w], g);
        if(x < hblock+n && y < cblock+n){
          double &nxt1 = DP[i+1][x][y];
          nxt1 = min(nxt1, DP[i][j][k] + (*fpds[i+1])[th+w+1]-(*fpds[i+1])[th+w]);
        }
        x = k+GetIdx((*fpds[i+1])[w], g);
        if(x < cblock+n){
          double &nxt2 = DP[i+1][j][x];
          nxt2 = min(nxt2, DP[i][j][k] + (*fpds[i+1])[w+1]-(*fpds[i+1])[w]);
        }
      }
    }
  }
  double ret = MAX;
  for(int i = hblock - n; i < hblock + n; i++)
    for(int j = cblock - n; j < cblock + n; j++)
      ret = min(ret, DP[n-1][i][j]);
  if(ret == MAX) return ret;

  //looking for solution
  if(sol == NULL)
    sol = new int[fpds.size()];
  double answ = ret;
  int preh = -1;
  int prec = cblock;
  for(int i = hblock-n; i < hblock+n; i++){
    for(int j = cblock-n; j < cblock+n; j++){
      if(answ == DP[n-1][i][j]){
        preh = i;
        prec = j;
        break;
      }
    }
    if(preh != -1)
      break;
  }

  for(int i = n-1; i > 0; i--){
    if(abs(DP[i-1][preh][prec-GetIdx((*fpds[i])[w], g)] + (*fpds[i])[w+1] - (*fpds[i])[w] - answ) < 1e-7){
      prec -= GetIdx((*fpds[i])[w], g);
      answ -= (*fpds[i])[w+1] - (*fpds[i])[w];
      sol[i] = 1;
    }
    else{
      preh -= GetIdx((*fpds[i])[th], g);
      prec -= GetIdx((*fpds[i])[th+w], g);
      answ -= (*fpds[i])[th+w+1] - (*fpds[i])[th+w];
      sol[i] = 0;
    }
  }
  if(preh == 0 && prec == (*fpds[0])[w])
    sol[0] = 1;
  else
    sol[0] = 0;
  return ret;
}

double MassArrays(double h, double l, int *ret=NULL, double g = 64.0){
    double c = h + l;
    double fw=100002;

    double l1 = 0, r1 = fpds[0]->W[fpds[0]->M-1];
    double l2, r2;
    while(r1 -l1 > 1e2){ //find th
      cout << "l1 : " << l1 << "\t" << r1 << endl;
      double mid1 = (l1+r1)/2;
      double midmid1 = (mid1+r1)/2;
      //cout << "1 : " << l1 << " " << r1 << endl;
      if(Validate(mid1, h, l1, r1, mid1)) continue;
      if(Validate(midmid1, h, l1, r1, midmid1)) continue;
      //cout << "2 : " << l1 << " " << r1 << endl;
      //break;

      //value for mid1
      l2 = 0, r2 = fpds[0]->W[fpds[0]->M-1];
      double vmid1 = 2.0;
      while(r2 -l2 > 1e2){ //find w for th=mid1
        double mid2 = (l2+r2)/2;
        double midmid2 = (mid2+r2)/2;
        //cout << "l21 : " << l2 << "\t" << r2 << " : " << mid2 << " " << midmid2 << endl;
        if(MinOfFPDS(mid1+mid2) > c) {r2 = mid2; continue;}
        if(SumOfFPDS(mid2) < c) {l2 = mid2; continue;}
        if(MinOfFPDS(mid1+midmid2) > c) {r2 = midmid2; continue;}
        if(SumOfFPDS(midmid2) < c) {l2 = midmid2; continue;}
        vmid1 = MinMissRatioLarge(h, l, mid1, mid2, ret, g);
        if(vmid1 < MinMissRatioLarge(h, l, mid1, midmid2, ret, g))
          r2 = midmid2;
        else
          l2 = mid2;
        //cout << "l22 : " << l2 << "\t" << r2 << " : " << mid2 << " " << midmid2 << endl;
      }
      fw = r2;

      //value for midmid1
      l2 = 0, r2 = fpds[0]->W[fpds[0]->M-1];
      double vmidmid1;
      while(r2 -l2 > 1e2){ //find w for th=midmid1
        double mid2 = (l2+r2)/2;
        double midmid2 = (mid2+r2)/2;
        //cout << "l23 : " << l2 << "\t" << r2 << " : " << mid2 << " " << midmid2 << endl;

        if(MinOfFPDS(midmid1+mid2) > c) {r2 = mid2; continue;}
        if(SumOfFPDS(mid2) < c) {l2 = mid2; continue;}
        if(MinOfFPDS(midmid1+midmid2) > c) {r2 = midmid2; continue;}
        if(SumOfFPDS(midmid2) < c) {l2 = midmid2; continue;}

        vmidmid1 = MinMissRatioLarge(h, l, midmid1, mid2, ret, g);
        if(vmid1 < MinMissRatioLarge(h, l, midmid1, midmid2, ret, g))
          r2 = midmid2;
        else
          l2 = mid2;
        //cout << "l24 : " << l2 << "\t" << r2 << " : " << mid2 << " " << midmid2 << endl;
      }

      if(MinOfFPDS(mid1+fw) > c) {r1 = mid1; continue;}
      if(SumOfFPDS(fw) < c) {l1 = mid1; continue;}
      if(MinOfFPDS(midmid1+r2) > c) {r1 = midmid1; continue;}
      if(SumOfFPDS(r2) < c) {l1 = midmid1; continue;}

      //compare
      if(vmid1 < vmidmid1){
        r1 = midmid1;
      }else{
        fw = r2;
        l1 = mid1;
      }
    }
    return MinMissRatioLarge(h, l, r1, fw, ret, g);
}

double ExhustDP(double h, double l, double th, double w, int *sol){
  if(IsValidation(th, h)) return 2.0;
  if(IsValidation(th+w, h+l)) return 2.0;
  double ret = 2;
  for(int i = 0; i < ceil(th); i++)
    for(int j = 0; j < ceil(w); j++){
      double t = MinMissRatioLarge(h, l, (double)i, (double)j, sol, 1.0);
      ret = min(ret, t);
      cout << i << " " << j << " " << t << endl;
    }
  return ret;
}
#endif
