#include <iostream>
#include "../general/TraceWriter.h"

using namespace std;

void GenerateStream(string filename, uint64_t len, uint64_t itr, uint32_t num_chunk=4){
    TraceWriter tw(filename, len*itr, num_chunk);
    for(int i = 0; i < num_chunk; i++){
      for(int i = 0; i < itr; i++){
          uint64_t addr = 0x10000000;
          for(int j = 0; j < len; j++){
              tw.Write(addr, 4);
              addr += 4;
          }
      }
    }
}

int main(int argc, char** argv){
    GenerateStream("genstream.trace", 1<<20, 4);
    return 0;
}
