#include <fstream>
#include <string>
#include <stdint.h>
#include <assert.h>
#include <algorithm>

using namespace std;

class ReuseTimeHistogram{
public:
    uint64_t L; //Trace length, except cold misses
    uint64_t W; //Number of windows
    uint64_t M; //Working set size
    uint64_t *windows;
    uint64_t *rts;

    ReuseTimeHistogram();
    ReuseTimeHistogram(string s);
    void InputFromFile(string s);
    double GetMissRatio(uint64_t rt);    
};

ReuseTimeHistogram::ReuseTimeHistogram(){
    assert(0 && "Haven't been implemented yet");
}

ReuseTimeHistogram::ReuseTimeHistogram(string s){
    InputFromFile(s);
}

void ReuseTimeHistogram::InputFromFile(string s){
    ifstream fin(s);
    assert(fin.good());
    fin >> L;
    fin >> M;
    fin >> W;
    L += M;

    windows = new uint64_t[W];
    rts = new uint64_t[W];

    for(uint64_t i = 0; i < W; i++){
        fin >> windows[i] >> rts[i];
    }
    fin.close();
}

double ReuseTimeHistogram::GetMissRatio(uint64_t rt){
    uint64_t pos = upper_bound(windows, windows+W, rt) - windows;
    double hits = rts[pos-1];
    hits += (rt-windows[pos-1])/(double)(windows[pos]-windows[pos-1])*(rts[pos]-rts[pos-1]);
    return 1- hits/L;
}
