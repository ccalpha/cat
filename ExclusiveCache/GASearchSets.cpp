#include <iostream>
#include <fstream>
#include <set>
#include <string>
#include <vector>

#include <ga/ga.h>
/*
#include <ga/GASimpleGA.h>
#include <ga/std_stream.h>
#include <ga/GA1DBinStrGenome.h>
#include <ga/GA1DArrayGenome.h>
#include <ga/GAAllele.h>
*/

using namespace std;

const int nprog = 29;
double abs_error[nprog][nprog];
double rel_error[nprog][nprog];
int minprog[] = {9,10,1}; //Minimal number of programs of each class

double Evaluate(vector<int> &id, double x[nprog][nprog]){
    int cnt[3] = {0,0,0};
    for(int i = 0; i < id.size(); i++)
        cnt[id[i]]++;
    
    double rt[3][3];
    for(int i = 0; i < 3; i++)
        for(int j = 0; j < 3; j++)
            rt[i][j] = 0;

    for(int i =0; i < nprog; i++)
        for(int j = i+1; j < nprog; j++)
            rt[id[i]][id[j]] += x[i][j];

// weights for group-group: 0-0 0-1 0-2 1-1 1-2 2-2
    const double weights[6] = {128, 32, 4, 96, 4, 1 };
    double value = 0;
    value += weights[0]*(rt[0][0])/(cnt[0]*(cnt[0]-1)/2);
    value += weights[1]*(rt[0][1]+rt[1][0])/(cnt[0]*cnt[1]);
    value += weights[2]*(rt[0][2]+rt[2][0])/(cnt[0]*cnt[2]);
    value += weights[3]*(rt[1][1]/(cnt[1]*(cnt[1]-1)/2));
    value += weights[4]*(rt[1][2]+rt[2][1])/(cnt[1]*cnt[2]);
    value += weights[5]*(rt[2][2])/(cnt[2]*(cnt[2]-1)/2);
    return value;
}

float Objective(GAGenome &g){
//    GA1DBinaryStringGenome & genome = (GA1DBinaryStringGenome &)g;
    GA1DArrayAlleleGenome<int> &genome = (GA1DArrayAlleleGenome<int> &)g;
    vector<int> groups;
    for(int i = 0; i < genome.length(); i++){
            groups.push_back(genome.gene(i));
    }
    int a[] = {0,0,0};
    for(int i = 0; i < genome.length(); i++){
        a[groups[i]]++;
    }
    for(int i = 0; i < 3; i++){
        if(a[i] < minprog[i])
            return 0;
    }
    double value = Evaluate(groups, rel_error);
    return 1e4-value;
}

vector<int> GeneticAlgorithm(){

    int aset[] = {0,1,2};    
    GAAlleleSet<int> alleles(3, aset);
    GA1DArrayAlleleGenome<int> genome(nprog, alleles, Objective);
    //genome.initializer(GA1DArrayAlleleGenome<int>::UniformInitializer);
    //genome.mutator(GA1DArrayAlleleGenome<int>::FlipMutator);
    //genome.crossover(GA1DArrayGenome<int>::OnePointCrossover);
    
    GASteadyStateGA ga(genome);
    GASigmaTruncationScaling trunc;
    ga.scaling(trunc);
    ga.set(gaNpopulationSize, 128);
    ga.set(gaNpCrossover, 0.6); 
    ga.set(gaNpMutation, 0.001);
    ga.set(gaNnGenerations, 1024*512*200);
    ga.set(gaNpReplacement, 0.25);
    ga.evolve(); 
    
    /*
       ga.populationSize(128);
       ga.nGenerations(1024*256);
       ga.pMutation(0.001);
       ga.pCrossover(0.9);
       ga.evolve();
       */
    const GA1DArrayAlleleGenome<int> &g = (const GA1DArrayAlleleGenome<int>&)(ga.statistics().bestIndividual());
    cout << "Best Population : " << ga.statistics().bestPopulation() << endl;
    cout << "Minimal Relative Error : " << 1e4-ga.statistics().maxEver() << endl;
    vector<int> rt;
    for(int i = 0; i < g.length(); i++)
            rt.push_back(g.gene(i));
    return rt;
}


void OutputInfo(vector<int> &groups){
    ifstream fnamein("names.txt");
    string names[nprog];
    for(int i = 0; i < nprog; i++){
        fnamein >> names[i];
    }
    fnamein.close();

    int cnt[3] = {0,0,0};
    for(int i = 0; i < groups.size(); i++)
        cnt[groups[i]]++;
    cout << "Size of groups : ";
    for(int i = 0; i < 3; i++)
        cout << cnt[i] << " ";
    cout << endl;
    
    cout << "Group ID: ";
    for(int i = 0; i < groups.size(); i++)
        cout << groups[i] << " ";
    cout << endl;
    for(int i = 0; i < groups.size(); i++)
        cout << names[i] << " ";
    cout << endl;

    double rerr[3][3];
    double aerr[3][3];
    double dm[3][3];
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            rerr[i][j] = 0;
            aerr[i][j] = 0;
            dm[i][j] = cnt[i]*cnt[j];
        }
        dm[i][i] = (cnt[i]-1)*cnt[i]/2.0;
    }

    for(int i = 0; i < groups.size(); i++)
        for(int j = i+1; j < groups.size(); j++){
            rerr[groups[i]][groups[j]] += rel_error[i][j]; 
            aerr[groups[i]][groups[j]] += abs_error[i][j];
        }

    for(int i = 0; i < 3; i++)
        for(int j = i+1; j < 3; j++){
            rerr[i][j] += rerr[j][i];
            aerr[i][j] += aerr[j][i];
        }

    cout << "Relative Error : " << endl;
    for(int i = 0; i < 3; i++){
        for(int j = i; j < 3; j++)
            cout << rerr[i][j]/dm[i][j] << " ";
        cout << endl;
    }

    cout << "Abs Error : " << endl;
    for(int i = 0; i < 3; i++){
        for(int j = i; j < 3; j++)
            cout << aerr[i][j]/dm[i][j] << " ";
        cout << endl;
    }
}

int main(int argc, char**argv){
    ifstream fin("result_error");
    int num = 0;
    string a, b;
    double c, d;
    for(int i = 0; i < nprog; i++)
        for(int j = i+1; j < nprog; j++){
            fin >> a >> b >> abs_error[i][j] >> rel_error[i][j];
            abs_error[j][i] = abs_error[i][j];
            rel_error[j][i] = rel_error[i][j];
        }
    fin.close();

    vector<int> rt = GeneticAlgorithm();


    OutputInfo(rt);

    return 0;
}
