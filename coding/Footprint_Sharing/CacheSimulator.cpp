//
//  CacheSimulator.cpp
//  Footprint_Sharing
//
//  Created by yechencheng on 5/10/15.
//  Copyright (c) 2015 yechencheng. All rights reserved.
//

#include "CacheSimulator.h"
#include <random>
#include <algorithm>
#include <iostream>

using namespace std;

//Useless, functional test
double Test(CacheStack *CS){
    const int64_t sz = 6;
    int64_t data[sz] = {1, 0, 2, 3, 1, 2};
    int64_t pushTo[sz] = {1, 0, 1, 1, 1, 1};
    for(int64_t i = 0; i < sz; i++){
        cout << CS->push(data[i], pushTo[i]) << endl;
    }
    return 1.0;
}

//Toy simulator : hand write PSstack, d = 2, s = 1
int64_t S[3] = {-1, -1, -1};
int64_t verify(int64_t data, int64_t p){
    if(p == 0){
        if(S[2] == -1){
            S[2] = data;
            return -2;
        }
        if(S[2] == data)
            return -1;
        else{
            int64_t ret = S[2];
            S[2] = data;
            return ret;
        }
    }
    else{
        if(S[0] == data) return -1;
        if(S[1] == data) { swap(S[0], S[1]); return -1;}
        if(S[2] == data) { swap(S[0], S[2]); swap(S[1], S[2]); return -1;}

        if(S[0] == -1) { S[0] = data; return -2;}
        if(S[1] == -1) { S[1] = data; verify(data, 1); return -2;}
        if(S[2] == -1) { S[2] = data; verify(data, 1); return -2;}
                swap(S[2], S[1]);
        swap(S[1], S[0]);
        swap(S[0],data);
        return data;
    }
}

double RandomInterleave(CacheStack *CS, Trace *T[], int64_t N, int64_t *pushTo, int64_t *hits){
    if(pushTo == NULL){
        pushTo = new int64_t[N];
        fill(pushTo, pushTo+N, 0);
    }
    
    int64_t s = 0;
    int64_t ns[N];
    for(int64_t i = 0; i < N; i++){
        ns[i] = T[i]->N;
        s += ns[i];
    }
    int64_t totalAcc = s;
    
    if(hits == NULL)
        hits = new int64_t[N];
    fill(hits, hits+N, 0);
    
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> dis;

    while(s > 0){
        int64_t p = dis(gen) % s;

        for(int64_t i = 0; i < N; i++){
            if(p <= ns[i]){
                p = CS->push(T[i]->A[T[i]->N-ns[i]], pushTo[i]);
                if(p == LRUAccess::DataHit) hits[i]++;
                ns[i]--;
                break;
            }
            else
                p -= ns[i];
        }
        s--;
    }
    int64_t Misses = totalAcc;
    for(int i = 0; i < N; i++)
        Misses -= hits[i];
    return Misses / (double)totalAcc;
}

double RoundRobinInterleave(CacheStack *CS, Trace *T[], int64_t N, int64_t *pushTo){
    if(pushTo == NULL){
        pushTo = new int64_t[N];
        fill(pushTo, pushTo+N, 0);
    }
    
    int64_t s = 0;
    int64_t ns[N];
    for(int64_t i = 0; i < N; i++){
        ns[i] = T[i]->N;
        s += ns[i];
    }
    int64_t totalAcc = s;
    int64_t Misses = 0;
    while(s != 0){
        for(int64_t i = 0; i < N; i++){
            if(ns[i] != 0){
                int64_t ret = CS->push(T[i]->A[T[i]->N-ns[i]], pushTo[i]);
                if(ret != -1) Misses++;
                ns[i]--;
                s--;
            }
        }
    }
    return Misses / (double)totalAcc;
}

double ContinuousInterleave(CacheStack *CS, Trace *T[], int64_t N, int64_t *pushTo){
    if(pushTo == NULL){
        pushTo = new int64_t[N];
        fill(pushTo, pushTo+N, 0);
    }
    int64_t s = 0;
    int64_t ns[N];
    for(int64_t i = 0; i < N; i++){
        ns[i] = T[i]->N;
        s += ns[i];
    }
    int64_t totalAcc = s;
    
    int64_t Misses = 0;
    for(int64_t i = 0; i < N; i++){
        for(int64_t j = 0; j < T[i]->N; j++)
            if(CS->push(T[i]->A[j], pushTo[i]) != -1)
                Misses++;
    }
    return Misses / (double)totalAcc;
}
