#pragma once

#include <vector>
#include <algorithm>
#include "footprint.h"

using namespace std;

//c is the number of cache line, round to 128 cache line
//return opt partition
vector<double> OptimalPartition(vector<FPDist> &fps, double c, double round=1){
    size_t nchunk = c/round;
    size_t nprog = fps.size();

    const double dmax = 1e40;
    double mcount[nprog][nchunk+1];
    int prev[nprog][nchunk+1];

    for(size_t i = 0; i < nprog; i++)
        fill(mcount[i], mcount[i]+nchunk+1, dmax);

    for(size_t i = 0; i <= nchunk; i++){
        mcount[0][i] = fps[0].GetMissRatio(i*round)*fps[0].access_count;
        prev[0][i] = i;
    }
    for(size_t i = 0; i < nprog-1; i++){
        FPDist &next_fps = fps[i+1];
        for(size_t j = i+1; j <= nchunk; j++){
            for(size_t k = nchunk-j; k >= 1; k--){
                double mc = next_fps.GetMissRatio(k*round) * next_fps.access_count;
                mc += mcount[i][j];
                if(mcount[i+1][j+k] > mc){
                    mcount[i+1][j+k] = mc;
                    prev[i+1][j+k] = k;
                }
            }
        }
    }
    vector<double> rt;
    for(int i = nprog-1, p = nchunk; i >= 0; i--){
        rt.push_back(prev[i][p]*round);
        p -= prev[i][p];
    }
    reverse(rt.begin(), rt.end());
    return rt;
}
