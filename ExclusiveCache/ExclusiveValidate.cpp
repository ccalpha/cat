#include<iostream>
#include <algorithm>
using namespace std;

int *mask;
int *tag;
int H1;
int H2;
int L;
int C;

//True = miss
bool Access(int x, int pid){
    for(int i = 0; i < C; i++){
        if(tag[i] == x){
            tag[i] = 0;
            break;
        }
    }

    for(int i = 0; i < C && x; i++){
        if(mask[i] & (1<<pid))
            swap(x, tag[i]);
    }
    return x != 0;
}

void OutputCacheStat(){
    for(int i = 0; i < C; i++)
        cout << tag[i] << " ";
    cout << endl;
}

void CountOccupancy(int &cnt){
    for(int i = 0; i < C; i++)
        if(tag[i] < 100 && tag[i])
            cnt++;
}

//command H1 H2 L
int main(int argc, char **argv){
    H1 = atoi(argv[1]);
    H2 = atoi(argv[2]);
    L = atoi(argv[3]);
    C = H1+H2+L;
    cout << H1 << " " << H2 << " " << L << endl;

    mask = new int[C];
    tag = new int[C];
    fill(tag, tag+C, 0);
    fill(mask, mask+C, 3);
    fill(mask, mask+H1, 1);
    fill(mask+H1, mask+H1+H2, 2);

    int A = 1;
    int add = 1;
    int B = 100;
    int misses[2] = {0,0};
    int occu = 0;
    for(int i = 0; i < 10000000; i++){
        misses[0] += Access(A, 0);
        CountOccupancy(occu);
        if(i <= 200){
            //cout << A << " : ";
            //OutputCacheStat();
            cout << occu << endl;
            occu = 0;
        }
        misses[1] += Access(B, 1);
        CountOccupancy(occu);
        if(i <= 200){
            //cout << B << " : ";
            //OutputCacheStat();
            cout << occu << endl;
            occu = 0;
        }
        A += add;
        if(A % 7 == 0) { add = -add; A += add;}
        B++;
    }
    cout << misses[0]+misses[1] << " " << 200000 << endl;
    cout << misses[0] << " " << misses[1] << endl;
    cout << occu << endl;
}
