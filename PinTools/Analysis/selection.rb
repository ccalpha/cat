#!/usr/bin/ruby

#usage : selection.rb numofcorun time
names = []
File.readlines("names.txt").each{|x| names << x.strip}
nco = ARGV[0].to_i
candidates = (0..names.size-1).to_a
candidates = [1,2,3,4,5,9,10,15,18,19,22,24]

candidates = candidates.combination(nco).to_a
ROOT="../Traces_continous"

ars = []
File.readlines("./AccessRate3").each{|x| ars << x.strip}

j = 0.to_i
for i in candidates
    system("echo #{ars[j]} > ar")
    j = j+1
    if(i.include?(19) || i.include?(5))
        next
    end
    s = i.join(" ")
    puts "working on co-run group : #{s}"
    system("echo \"#{ROOT}/#{names[i[0]]}.trace\n#{ROOT}/#{names[i[1]]}.trace\n#{ROOT}/#{names[i[2]]}.trace\" > list")
    #system("echo \"#{ROOT}/#{names[i[0]]}.trace\" > list")
    system("cat ar")
    system("time ./runtest.sh Exclusive_Simple.so -t ./list -a ar")
end
