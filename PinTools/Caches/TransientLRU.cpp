#include <iostream>
#include <vector>
#include <set>

#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters

#include "pin_cache.H"
#include "../general/Caches.h"
#include "../general/CacheConfig.h"
#include "../general/array_register.h"

#define MALLOC "malloc"
#define CALLOC "calloc"
#define FREE "free"
#define REALLOC "realloc"

KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "TransientLRU", "Output file");
KNOB<string> KnobMaskFile(KNOB_MODE_WRITEONCE, "pintool", "m", "FPDist.mask", "mask file for transient addr");
KNOB<UINT64> KnobLineSize(KNOB_MODE_WRITEONCE, "pintool", "l", "7", "log2 of cache line size, default 7");

//MergeEngine ary_reg;
ArrayRegister ary_reg;
UINT64 floor2CacheLine = 7;
std::ofstream OutputFile;
UINT64 ins_count = 0;
UINT64 ref_count = 0;
std::set<ADDRINT> wss_count;
TransientMask tmask(&ary_reg);


LOCALFUN ITLB::CACHE itlb("ITLB", ITLB::cacheSize, ITLB::lineSize, ITLB::associativity);
LOCALVAR DTLB::CACHE dtlb("DTLB", DTLB::cacheSize, DTLB::lineSize, DTLB::associativity);
LOCALVAR IL1::CACHE il1("L1 Instruction Cache", IL1::cacheSize, IL1::lineSize, IL1::associativity);
LOCALVAR DL1::CACHE dl1("L1 Data Cache", DL1::cacheSize, DL1::lineSize, DL1::associativity);
LOCALVAR TransientCACHE<UL2::allocation> ul2("L2 Unified Cache : Transient LRU", UL2::cacheSize, UL2::lineSize, UL2::associativity, &tmask);
LOCALVAR TransientCACHE<UL3::allocation> ul3("L3 Unified Cache : Transient LRU", UL3::cacheSize, UL3::lineSize, UL3::associativity, &tmask);

LOCALFUN VOID Fini(int code, VOID * v)
{
    OutputFile << ins_count << "\t" << ref_count << "\t" << wss_count.size() << "\n";
    //OutputFile << itlb;
    //OutputFile << dtlb;
    OutputFile << il1;
    OutputFile << dl1;
    OutputFile << ul2;
    //OutputFile << ul3;
}

LOCALFUN VOID Ul2Access(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    // second level unified cache
    //const BOOL ul2Hit =
    ul2.Access(addr, size, accessType);

    // third level unified cache
    //if ( ! ul2Hit) ul3.Access(addr, size, accessType);
}


LOCALFUN VOID InsRef(ADDRINT addr)
{
    ins_count++;
    const UINT32 size = 1; // assuming access does not cross cache lines
    const CACHE_BASE::ACCESS_TYPE accessType = CACHE_BASE::ACCESS_TYPE_LOAD;

    // ITLB
    //itlb.AccessSingleLine(addr, accessType);

    // first level I-cache
    const BOOL il1Hit = il1.AccessSingleLine(addr, accessType);

    // second level unified Cache
    if ( ! il1Hit) Ul2Access(addr, size, accessType);
}

LOCALFUN VOID MemRefMulti(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    ref_count++;
    wss_count.insert(addr);
    // DTLB
    //dtlb.AccessSingleLine(addr, CACHE_BASE::ACCESS_TYPE_LOAD);

    // first level D-cache
    const BOOL dl1Hit = dl1.Access(addr, size, accessType);

    // second level unified Cache
    if ( ! dl1Hit) Ul2Access(addr, size, accessType);
}

LOCALFUN VOID MemRefSingle(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    ref_count++;
    wss_count.insert(addr);
    // DTLB
    //dtlb.AccessSingleLine(addr, CACHE_BASE::ACCESS_TYPE_LOAD);

    // first level D-cache
    const BOOL dl1Hit = dl1.AccessSingleLine(addr, accessType);

    // second level unified Cache
    if ( ! dl1Hit) Ul2Access(addr, size, accessType);
}


LOCALFUN VOID Instruction(INS ins, VOID *v)
{
    // all instruction fetches access I-cache
    INS_InsertCall(
        ins, IPOINT_BEFORE, (AFUNPTR)InsRef,
        IARG_INST_PTR,
        IARG_END);

    if (INS_IsMemoryRead(ins) && INS_IsStandardMemop(ins))
    {
        const UINT32 size = INS_MemoryReadSize(ins);
        const AFUNPTR countFun = (size <= 4 ? (AFUNPTR) MemRefSingle : (AFUNPTR) MemRefMulti);

        // only predicated-on memory instructions access D-cache
        INS_InsertPredicatedCall(
            ins, IPOINT_BEFORE, countFun,
            IARG_MEMORYREAD_EA,
            IARG_MEMORYREAD_SIZE,
            IARG_UINT32, CACHE_BASE::ACCESS_TYPE_LOAD,
            IARG_END);
    }

    if (INS_IsMemoryWrite(ins) && INS_IsStandardMemop(ins))
    {
        const UINT32 size = INS_MemoryWriteSize(ins);
        const AFUNPTR countFun = (size <= 4 ? (AFUNPTR) MemRefSingle : (AFUNPTR) MemRefMulti);

        // only predicated-on memory instructions access D-cache
        INS_InsertPredicatedCall(
            ins, IPOINT_BEFORE, countFun,
            IARG_MEMORYWRITE_EA,
            IARG_MEMORYWRITE_SIZE,
            IARG_UINT32, CACHE_BASE::ACCESS_TYPE_STORE,
            IARG_END);
    }
}

VOID MallocBefore(ADDRINT callee, ADDRINT size)
{
    ary_reg.BeforeMalloc(size);
}

VOID FreeBefore(ADDRINT ptr){
  ary_reg.FreeArray(ptr);
}

VOID MallocAfter(ADDRINT ret)
{
  ary_reg.AfterMalloc(ret);
}

VOID CallocBefore(ADDRINT callee, ADDRINT size, ADDRINT num){
  ary_reg.BeforeMalloc(size * num);
}

VOID ReallocBefore(ADDRINT callee, ADDRINT ptr, ADDRINT size){
  ary_reg.FreeArray(ptr);
  ary_reg.BeforeMalloc(size);
}

VOID Image(IMG img, VOID *v)
{
  RTN mallocRtn = RTN_FindByName(img, MALLOC);
  if (RTN_Valid(mallocRtn))
  {
      RTN_Open(mallocRtn);

      // Instrument malloc() to print the input argument value and the return value.
      RTN_InsertCall(mallocRtn, IPOINT_BEFORE, (AFUNPTR)MallocBefore,
                     IARG_ADDRINT, RTN_Address(mallocRtn),
                     IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                     IARG_END);
      RTN_InsertCall(mallocRtn, IPOINT_AFTER, (AFUNPTR)MallocAfter,
                     IARG_FUNCRET_EXITPOINT_VALUE, IARG_END);
      RTN_Close(mallocRtn);
  }

  // Find the free() function.
  RTN freeRtn = RTN_FindByName(img, FREE);
  if (RTN_Valid(freeRtn)){
      RTN_Open(freeRtn);
      // Instrument free() to print the input argument value.
      RTN_InsertCall(freeRtn, IPOINT_BEFORE, (AFUNPTR)FreeBefore,
                     IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                     IARG_END);
      RTN_Close(freeRtn);
  }

  RTN callocRtn = RTN_FindByName(img, CALLOC);
  if(RTN_Valid(callocRtn)){
    RTN_Open(callocRtn);
    RTN_InsertCall(callocRtn, IPOINT_BEFORE, (AFUNPTR)CallocBefore,
        IARG_ADDRINT, RTN_Address(callocRtn),
        IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
        IARG_FUNCARG_ENTRYPOINT_VALUE, 1,
        IARG_END);
    RTN_InsertCall(callocRtn, IPOINT_AFTER, (AFUNPTR)MallocAfter,
                       IARG_FUNCRET_EXITPOINT_VALUE, IARG_END);
    RTN_Close(callocRtn);
  }

  RTN reallocRtn = RTN_FindByName(img, REALLOC);
  if(RTN_Valid(reallocRtn)){
    RTN_Open(reallocRtn);
    RTN_InsertCall(reallocRtn, IPOINT_BEFORE, (AFUNPTR)ReallocBefore,
        IARG_ADDRINT, RTN_Address(reallocRtn),
        IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
        IARG_FUNCARG_ENTRYPOINT_VALUE, 1,
        IARG_END);
    RTN_InsertCall(reallocRtn, IPOINT_AFTER, (AFUNPTR)MallocAfter,
                       IARG_FUNCRET_EXITPOINT_VALUE, IARG_END);
    RTN_Close(reallocRtn);
  }
}

GLOBALFUN int main(int argc, char *argv[])
{
    PIN_InitSymbols();
    PIN_Init(argc, argv);
    floor2CacheLine = KnobLineSize.Value();
    OutputFile.open(KnobOutput.Value().c_str());
    tmask.init(KnobMaskFile.Value());
    /*
    int nums[6] = {1024, 1024, 7168, 7168, 7168, 7168};
    int starts[6] = {0};
    int ends[6];
    int cnts[6] = {0};
    ends[0] = nums[0];
    for(int i = 1; i < 6; i++){
      starts[i] = starts[i-1] + nums[i-1];
      ends[i] = ends[i-1] + nums[i];
      cnts[i] = starts[i];
    }
    bool MyIsTransient[6] = {0, 0, 0, 0, 1, 1};

    for(int i = 0; i < 1000000; i++){
      for(int j = 0; j < 6; j++){
        ul2.MyIsTransient = MyIsTransient[j];
        ul2.AccessSingleLine(64 * cnts[j]++, CACHE_BASE::ACCESS_TYPE_LOAD);
        if(cnts[j] == ends[j])
          cnts[j] = starts[j];
      }
    }
    */

    IMG_AddInstrumentFunction(Image, 0);
    INS_AddInstrumentFunction(Instruction, 0);
    PIN_AddFiniFunction(Fini, 0);

    PIN_StartProgram();

    return 0; // make compiler happy
}
