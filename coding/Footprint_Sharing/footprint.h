//
//  footprint.h
//  Footprint_Sharing
//
//  Created by yechencheng on 5/6/15.
//  Copyright (c) 2015 yechencheng. All rights reserved.
//

#ifndef Footprint_Sharing_footprint_h
#define Footprint_Sharing_footprint_h

#include <map>
#include <set>
#include <algorithm>
#include <stdint.h>
#include <string>
#include <cstring>
#include <cmath>
#include <iostream>

using namespace std;

enum direction {forward, backward};


//distribution of footprint, only selected windows are figured out.
class FPDist{
private:
    void GetW(){
        //for now, all windows are selected
        M = N;
        W = new int64_t[M];
        for(int64_t i = 0; i < N; i++){
            W[i] = i+1;
        }
    }
public:
    int64_t *W;
    double *FP;
    int64_t N;  //number of windows
    int64_t M;  //number of choosen windows
    
    FPDist(int64_t _N){
        N = _N;
        GetW();
        FP = new double[M];
    }
    FPDist(FPDist &x){
        M = x.M;
        N = x.N;
        W = new int64_t[M];
        FP = new double[M];
        memcpy(W, x.W, M*sizeof(int64_t));
        memcpy(FP, x.FP, M*sizeof(double));
    }
    
    virtual ~FPDist(){
        delete W;
        delete FP;
    }
    
    //mr = fp(w+1)-fp(w), fp(w)=c
    virtual double GetMissRatio(double c, int64_t *rtp = NULL){
        double tc = GetFT(c);
        return operator[](tc+1) - operator[](tc);
    }
    
    FPDist operator+(const FPDist& lhs){
        FPDist retval(max(lhs.M, M));
        retval.N = N + lhs.N;
        
        int64_t pos = 0;
        for(int64_t i = 0, j = 0; i < lhs.M && j < M; ){
            if(lhs.W[i] == W[j]){
                retval.W[pos] = lhs.W[i] + W[j];
                retval.FP[pos] = lhs.FP[i] + FP[j];
                i++;
                j++;
                pos++;
            }
            else if(lhs.W[i] > W[j])
                j++;
            else i++;
        }
        
        
        retval.M = pos;
        return retval;
    }
    
    virtual double operator[](const double w){
        int64_t id = upper_bound(W, W+M, floor(w)) - W;        
        double ret = FP[id-1];
        ret += (w-W[id-1])/(W[id]-W[id-1])*(FP[id]-FP[id-1]);
        return ret;
    }
    
    virtual double GetFT(const double c){
        int64_t id = upper_bound(FP, FP+M, c) - FP;
        double ret = W[id-1];
        ret += (c-FP[id-1])/(FP[id]-FP[id-1])*(W[id]-W[id-1]);
        return ret;
    }
};

class VFPDist : public FPDist{
private:
    double h = -1;
    double th = -1;
public:
    VFPDist(FPDist f) : FPDist(f){
    }
    
    void init(double _h){
        h = _h;
        th = GetFT(h);
    }
    
    double GetVFP(double _h, double w){
        if(_h != h){
            init(_h);
        }
        return FPDist::operator[](w+th) - h;
    }
    double GetVFT(double _h, double c){
        if(_h != h){
            init(_h);
        }
        double ret = FPDist::GetFT(c+h) - th;
        return ret;
    }
    
    double operator[](const double w){
        return FPDist::operator[](w+th) - h;
    }
    
    double GetVFT(const double c){
        return FPDist::GetFT(c+h) - th;
    }
};

//class for trace
static int TraceNameIDX = 0;
class Trace{
private:
    
    int64_t M = -1;
    map<int64_t, int64_t> *DistinctAccess = NULL;
    
    map<int64_t, int64_t>* GetDistincAccess(){
        if(DistinctAccess != NULL)
            return DistinctAccess;
        DistinctAccess = new map<int64_t, int64_t>();
        for(int64_t i = 0; i < N; i++){
            if(DistinctAccess->find(A[i]) == DistinctAccess->end())
                (*DistinctAccess)[A[i]] = DistinctAccess->size();
        }
        return DistinctAccess;
    }
    map<int64_t, int64_t> dis;
    int64_t cnt;
public:
    string name;
    int64_t *A;
    int64_t N;
    int64_t *fd;
    int64_t *bw;
    
    int64_t operator[](int64_t idx){
        return A[idx];
    }
    
    Trace(int64_t _N, string _name = ""){
        N = _N;
        A = new int64_t[N];
        cnt = 0;
        fd = NULL;
        bw = NULL;
        name = _name;
        if(name == "")
            name = to_string(TraceNameIDX++);
    }
    
    Trace(const Trace& t){
        N = t.N;
        M = -1;
        DistinctAccess = NULL;
        cnt = t.cnt;
        fd = NULL;
        bw = NULL;
        name = t.name;
        A = new int64_t[t.N];
        for(int64_t i = 0; i < N; i++)
            A[i] = t.A[i];
    }
    
    ~Trace(){
        delete A;
        delete fd;
        delete bw;
        delete DistinctAccess;
    }
    
    int64_t* GetForward();
    int64_t* GetBackward();
    
    int64_t getM(){
        if(M != -1) return M;
        GetDistincAccess();
        M = DistinctAccess->size();
        return M;
    }
    int64_t getN(){
        return N;
    }
    void push(int64_t datum){
        if(dis.find(datum) == dis.end())
            dis[datum] = dis.size();
        A[cnt++] = dis[datum];
    }
    
    void NormalizeAddr(int64_t off = 0){
        GetDistincAccess();
        for(int64_t i = 0; i < N; i++){
            A[i] = (*DistinctAccess)[A[i]] + off;
        }
    }
    
    void AddrOffset(int64_t off){
        for(int64_t i = 0; i < N; i++)
            A[i] += off;
    }
};


int64_t* GetNext(Trace *T, direction d);
FPDist* GetFootprint(Trace *T);
FPDist* GetHOTLFootprint(Trace *T);
FPDist* GetSFootprint(Trace *T, int64_t DSize);
double GetMissRateAR(FPDist* d[], int64_t N, int64_t csize, int64_t *AR = NULL, double *epos=NULL);
FPDist* GetLFootprint(Trace *T, int64_t H);
FPDist* GetLFootprint_Naive(Trace *T, int64_t H);
void AdjustWindowLFootprint(FPDist *fpd, int64_t dsize);
FPDist* GetSFootprint2(Trace *t, int64_t H);
FPDist* GetCFootprint(Trace *t);
VFPDist* GetVFootprint(Trace *t);

int64_t VerifyLemma3(Trace *T, int64_t H);
int64_t VerfiyHaoAlgo(Trace *T, int64_t H);

#endif
