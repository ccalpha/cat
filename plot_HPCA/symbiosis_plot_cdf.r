library(ggplot2)
require(scales)
library(data.table)
library("gridExtra")
library("cowplot")


sym<- read.table("./symbiosis_3_2.data", header=TRUE)
attach(sym)
sym$ID <- seq(0,length(OPT)-1)
detach(sym)

sym_melt<- melt(sym, id=c("ID"))
Scheduling <- factor(sym_melt$variable, labels=c("OPT", "VFP", "DI", "HOTL"))
ggplot(sym_melt) +
  stat_ecdf(aes(value, color = Scheduling, linetype= Scheduling), size=1.2)+
  scale_x_continuous(labels=percent)+
  scale_y_continuous(labels=percent)+
  geom_vline(xintercept = median(sym$VFP), linetype="longdash") +
  theme_gray() +
  theme(legend.justification=c(1,0),
      legend.position=c(1,0),
      text = element_text(size=28))+
  theme(aspect.ratio=0.7)+
  theme(axis.text.y = element_text(angle = 45))+
  theme(legend.key.width = unit(2, "cm"))+
  labs(x='', y='Cumulative Percent')

  sym<- read.table("./symbiosis_4_2.data", header=TRUE)
  attach(sym)
  sym$ID <- seq(0,length(OPT)-1)
  detach(sym)

  sym_melt<- melt(sym, id=c("ID"))
  Scheduling <- factor(sym_melt$variable, labels=c("OPT", "VFP", "DI", "HOTL"))
  ggplot(sym_melt) +
    stat_ecdf(aes(value, color = Scheduling, linetype= Scheduling), size=1.2)+
    scale_x_continuous(labels=percent)+
    scale_y_continuous(labels=percent)+
    geom_vline(xintercept = median(sym$VFP), linetype="longdash") +
    theme_gray() +
    theme(legend.justification=c(1,0),
        legend.position=c(1,0),
        text = element_text(size=28))+
    theme(aspect.ratio=0.7)+
    theme(axis.text.y = element_text(angle = 45))+
    theme(legend.key.width = unit(2, "cm"))+
    labs(x='', y='')

  #  labs(x="Aggregated Slowdown", y="Cumulative Percent")
