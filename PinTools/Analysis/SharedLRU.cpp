#include <iostream>

#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters

#include "pin_cache.H"
#include "../general/Caches.h"
#include "../general/CacheConfig.h"
#include "../../general/TraceReader.h"
#include "../../general/general.h"

#include "../../general/Interleaving.h"

KNOB<string> KnobTrace(KNOB_MODE_WRITEONCE, "pintool", "t", "traces", "list of traces");
KNOB<string> KnobAR(KNOB_MODE_WRITEONCE, "pintool", "r", "", "list of access rates");
KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "stdout", "Output file");
std::ostream *fout;

LOCALVAR LRUCACHE<UL3::allocation> ul3("Shared L3 : LRU", UL3::cacheSize, UL3::lineSize, UL3::associativity);

vector<TraceReader*> trl;
vector<double> accressRate;

vector<uint64_t> count_access;
vector<uint64_t> count_misses;

LOCALFUN VOID Init(){
    vector<uint64_t> alloc;

    //inital cache
    //read traces
    ifstream fin(KnobTrace.Value().c_str());
    string tmp;
    while(fin>>tmp){
        trl.push_back(new TraceReader(tmp));
    }
    fin.close();

    //read ar
    if(KnobAR.Value() != ""){
        fin.open(KnobAR.Value().c_str());
        accressRate.clear();
        double x;
        while(fin >> x)
            accressRate.push_back(x);
        fin.close();
    }
    else{
        for(auto i : trl)
            accressRate.push_back(i->GetNumChunk());
    }
    count_access = vector<uint64_t>(trl.size(), 0);
    count_misses = vector<uint64_t>(trl.size(), 0);
}

LOCALFUN VOID Fini(int code, VOID * v)
{
    PrintVector(count_access, "Accesses Count : ");
    PrintVector(count_misses, "Misses Count : ");
    *fout << "Info of L3 Caches\n";
    *fout << ul3;
}

GLOBALFUN int main(int argc, char *argv[])
{
    PIN_Init(argc, argv);
    if(KnobOutput.Value() == "stdout")
      fout = &(std::cout);
    else
      fout = new ofstream(KnobOutput.Value().c_str());
    Init();
    
    UniformInterleave ui(trl, accressRate);
    MemRef* p;
    uint64_t inst_count = 0;
    while((p = ui.GetNext()) != NULL){
        inst_count++;
        if((inst_count>>25<<25)==inst_count)
            cout << inst_count << endl;
        uint8_t pid = ui.GetNextID();
        count_access[pid]++;
        if(!ul3.Access(p->address, p->size, CACHE_BASE::ACCESS_TYPE_LOAD))
            count_misses[pid]++;
    }
    Fini(0, NULL);
    return 0;
}
