#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <boost/program_options.hpp>

#include "footprint.h"
#include "CAT.h"
#include "general.h"

using namespace std;

CAT* GetCAT(size_t way_size, vector<FPDist> &fps, vector<size_t> &alloc, vector<double> &ar){
    CAT *cat = new CAT((double)way_size,fps);
    for(auto i : alloc){
        cat->AddWay(i);
        //vector<double> ocu = cat->GetOccupancies();
        //PrintVector(ocu, "ocu : ");
        //double sum = accumulate(ocu.begin(), ocu.end(), 0.0);
        //cout << sum << endl;
    }
    return cat;
}

//command H L fp1 fp2 ...
int main(int argc, char** argv){
    size_t C;

    namespace po = boost::program_options;
    po::options_description desc("Allowed Options");
    desc.add_options()
        ("help,h", "This message")
        ("cache,c", po::value<size_t>(&C)->required(), "Capacity of cache")
        ("footprint,f", po::value<vector<string>>(), "footprints")
        ("allocation,a", po::value<string>()->required(), "allocations")
        ("accessrate,r", po::value<vector<double>>()->multitoken(), "individual access rates")
    ;
    po::positional_options_description p;
    p.add("footprint", -1);
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    if(vm.count("help")){
        cout << desc << endl;
        return 0;
    }
    po::notify(vm);

    size_t nft = vm["footprint"].as<vector<string>>().size();
    vector<FPDist> fpds;
    for(auto i : vm["footprint"].as<vector<string>>()){
        fpds.push_back(FPDist(i));
    }
    
    vector<double> ar;
    if(vm.count("accessrate"))
        ar = vm["accessrate"].as<vector<double>>();
    else{
        for(auto &i : fpds)
            //ar.push_back(i.access_count);
            ar.push_back(0);
    }
    if(ar.size() != 0 && ar.size() != nft){
        cerr << "Number of access rates and footprints are not the same" << endl;
        return 1;
    }
    
    vector<size_t> alloc;
    ifstream fin(vm["allocation"].as<string>().c_str());
    size_t x;
    while(fin >> x)
        alloc.push_back(x);
    assert((C%alloc.size()==0)&& "Cache size must be times of number of ways");

    CAT *cat = GetCAT(C/alloc.size(), fpds, alloc, ar);
    cout << "Overall Miss Ratio : " << cat->GetOverallMissRatio() << endl;
    cout << "Individual Miss Ratio : ";
    vector<double> mrs = cat->GetMissRatio();
    PrintVector(mrs);
    vector<double> ocu = cat->GetOccupancies();
    PrintVector(ocu, "Occupancy : ");
    return 0;
}
