#include <iostream>

#include "pin.H"

typedef UINT32 CACHE_STATS; // type of cache hit/miss counters

#include "pin_cache.H"
#include "../general/Caches.h"
#include "../general/CacheConfig.h"
#include "/localdisk/cye/vmin/simulator/simulator.cpp"
#include "/localdisk/cye/vmin/simulator/tracetools.cpp"
#include <set>
#include <boost/timer.hpp>
#include <queue>
#include <string>

KNOB<string> KnobOutput(KNOB_MODE_WRITEONCE, "pintool", "o", "stdout", "Output file");
KNOB<UINT64> KnobLength(KNOB_MODE_WRITEONCE, "pintool", "l", "10000", "length of the memory trace");
KNOB<UINT64> KnobDRAM(KNOB_MODE_WRITEONCE, "pintool", "d", "262144", "size of dram for Hotness Policy");
KNOB<string> KnobFT(KNOB_MODE_WRITEONCE, "pintool", "f", "fts", "list of filltimes");
KNOB<double> KnobExcutionTime(KNOB_MODE_WRITEONCE, "pintool", "t", "1.0", "execution time(s)");
KNOB<UINT64> KnobBound(KNOB_MODE_WRITEONCE, "pintool", "b", "10000000000", "accesses up to b");
KNOB<UINT64> KnobWarmup(KNOB_MODE_WRITEONCE, "pintool", "w", "0", "accesses skipped as warmup");

//LOCALFUN ITLB::CACHE itlb("ITLB", ITLB::cacheSize, ITLB::lineSize, ITLB::associativity);
//LOCALVAR DTLB::CACHE dtlb("DTLB", DTLB::cacheSize, DTLB::lineSize, DTLB::associativity);
//LOCALVAR IL1::CACHE il1("L1 Instruction Cache", IL1::cacheSize, IL1::lineSize, IL1::associativity);
LOCALVAR DL1::CACHE dl1("L1 Data Cache", DL1::cacheSize, DL1::lineSize, DL1::associativity);
LOCALVAR LRUCACHE<UL2::allocation> ul2("L2 Unified Cache : LRU", UL2::cacheSize, UL2::lineSize, UL2::associativity);
LOCALVAR LRUFilter<UL3::allocation> ul3("L3 Unified Cache : LRU", UL3::cacheSize, UL3::lineSize, UL3::associativity);

UINT64 ins_count = 0;
UINT64 ref_count = 0;
std::set<ADDRINT> wss_count;
std::set<UINT64> dirty;

UINT64 misses_count = 0;
UINT64 writeback_count = 0;

boost::timer mytimer;

Simulator mem_sim;
vtime_t vtime_mem = 0;
vtime_t trace_len;
vtime_t dram_space;
UINT64 bound = 0;
UINT64 warmup = 0;

addr_t Addr2BlockID(addr_t addr) { return ((addr_t)addr) >> BLOCK_OFFSET; }
addr_t Addr2PageID(addr_t addr) { return ((addr_t)addr) >> PAGE_OFFSET; }
addr_t BlockID2PageID(addr_t block_id) { return block_id >> (PAGE_OFFSET - BLOCK_OFFSET); }

void Init(){
    trace_len = KnobLength.Value();
    dram_space = KnobDRAM.Value();
    bound = KnobBound.Value();
    warmup = KnobWarmup.Value();

    vector<vector<vtime_t>> fts;
    ifstream fin(KnobFT.Value());
    double x;
    double y;
    while(fin>>x){
        fin >> y;
        //cout << x << " " << y << endl;
        if(x >= 1e19)
            break;
        vector<vtime_t> tmp;
        tmp.push_back((vtime_t)x);
        tmp.push_back((vtime_t)y);
        fts.push_back(tmp);
    }
    fin.close();

    //vtime_t taus_arr[] = {}; /*{ft(16MB), ft(8*16MB)}, {ft(32MB), ft(8*32MB)}, {64, 8*64}, {128, 256, 512, 1024}  */
    mem_sim.SetCosts(15, 55, 15, 90, 10000000, 10000000);

    float n_mem = 8232654;

    // Jake added this bit.
    for (size_t i = 0; i != fts.size(); ++i) {
      for (size_t j = 0; j != fts[i].size(); ++j) {
	fts[i][j] = fts[i][j] * n_mem/10000000;
      }
    }

    mem_sim.SetHTaus(fts, R);
    mem_sim.SetAccessesIn10ms(trace_len/(KnobExcutionTime.Value()*100));
    mem_sim.SetDRAMSpace(dram_space);
}
/*
LOCALFUN VOID OutputInfo(){
    
    *fout << "Instruments : " << ins_count << "\n";
    *fout << "References : " << ref_count << "\n";
    *fout << "Working set size : " << wss_count.size() << "\n";
    *fout << "Misses : " << misses_count << "\n";
    *fout << "Write back : " << writeback_count << "\n";
    
}
*/

LOCALFUN VOID Fini(int code, VOID * v)
{
    cout << "Ref Count : " << ref_count << endl;
    //*fout << "*****END*****\n";
    string output = KnobOutput.Value();
    mem_sim.ComputeHResult(HPolicy::HVMIN, trace_len, wss_count.size());
    mem_sim.ComputeHResult(HPolicy::HWS, trace_len, wss_count.size());
    mem_sim.ComputeHResult(HPolicy::HEAT, trace_len, wss_count.size());
    //mem_sim.OutputResults(HPolicy::HVMIN, output);
    //mem_sim.OutputResults(HPolicy::HWS, output);
    //mem_sim.OutputResults(HPolicy::HEAT, output);
    mem_sim.OutputHResults(output);
    /*
    OutputInfo();
    *fout << itlb;
    *fout << dtlb;
    *fout << dl1;
    *fout << ul2;
    *fout << ul3;
    fout->flush();
    fout->close();
    */
}


LOCALFUN VOID Ul2Access(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    // second level unified cache
    const BOOL ul2Hit = ul2.Access(addr, size, accessType);

    // third level unified cache
    if ( ! ul2Hit) {
        ADDRINT victim = 0;
        if(!ul3.AccessSingleLine(addr, accessType, victim)){
            //misses_count++;
            mem_sim.access = Access((addr_t)BlockID2PageID(Addr2BlockID(addr)), ++vtime_mem, R);
            mem_sim.ProcessAccessHVMIN();
            mem_sim.ProcessAccessHWS(trace_len);
            mem_sim.ProcessAccessHEAT();
            if(dirty.find(victim) != dirty.end()){
                dirty.erase(victim);
                //writeback_count++;
                mem_sim.access = Access((addr_t)BlockID2PageID(Addr2BlockID(victim)), ++vtime_mem, W);
                mem_sim.ProcessAccessHVMIN();
                mem_sim.ProcessAccessHWS(trace_len);
                mem_sim.ProcessAccessHEAT();
            }
        }
    }
}


LOCALFUN VOID MemRefSingle(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType)
{
    wss_count.insert(addr>>6);
    ref_count++;
    
    if(ref_count < warmup) //skip warmup
        return;

    if(ref_count == bound)
        PIN_ExitApplication(0);
    /*
    const UINT64 mymod = 1000000000;
    
    
    //about 2 min per billion
    if(ref_count %  mymod == 0){
        cout << ref_count << " " << mytimer.elapsed() << endl;
        mytimer.restart();
        *fout << "Ref_count : " << ref_count << endl;
    }
    */

    if(accessType == CACHE_BASE::ACCESS_TYPE_STORE)
        dirty.insert(addr>>6<<6);
    const BOOL dl1Hit = dl1.AccessSingleLine(addr, accessType);
    if ( ! dl1Hit) Ul2Access(addr, size, accessType);
}

LOCALFUN VOID MemRefMulti(ADDRINT addr, UINT32 size, CACHE_BASE::ACCESS_TYPE accessType){
    ADDRINT up = addr + size; 
    do{
        MemRefSingle(addr, size, accessType);
        addr = (addr >> 6 << 6) + 64;
    }while(addr < up);
}

LOCALFUN VOID Instruction(INS ins, VOID *v)
{

    if (INS_IsMemoryRead(ins) && INS_IsStandardMemop(ins))
    {
        const UINT32 size = INS_MemoryReadSize(ins);
        const AFUNPTR countFun = (size <= 4 ? (AFUNPTR) MemRefSingle : (AFUNPTR) MemRefMulti);

        INS_InsertPredicatedCall(
            ins, IPOINT_BEFORE, countFun,
            IARG_MEMORYREAD_EA,
            IARG_MEMORYREAD_SIZE,
            IARG_UINT32, CACHE_BASE::ACCESS_TYPE_LOAD,
            IARG_END);
    }

    if (INS_IsMemoryWrite(ins) && INS_IsStandardMemop(ins))
    {
        const UINT32 size = INS_MemoryWriteSize(ins);
        const AFUNPTR countFun = (size <= 4 ? (AFUNPTR) MemRefSingle : (AFUNPTR) MemRefMulti);

        INS_InsertPredicatedCall(
            ins, IPOINT_BEFORE, countFun,
            IARG_MEMORYWRITE_EA,
            IARG_MEMORYWRITE_SIZE,
            IARG_UINT32, CACHE_BASE::ACCESS_TYPE_STORE,
            IARG_END);
    }
}

GLOBALFUN int main(int argc, char *argv[])
{
    PIN_Init(argc, argv);
    Init();
    INS_AddInstrumentFunction(Instruction, 0);
    PIN_AddFiniFunction(Fini, 0);

    // Never returns
    PIN_StartProgram();

    return 0; // make compiler happy
}
