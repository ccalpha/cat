/*
counting the number of identical occupancy of allocations
*/
#include <iostream>
#include <map>
#include <vector>
#include <cmath>
#include <boost/functional/hash.hpp>

#include "TraceReader.h"
#include "footprint.h"
#include "CAT.h"
#include "general.h"

using namespace std;

//truncate to 8k
vector<int> Discretize(vector<double> ocu, int tu = 128){
    vector<int> rt;
    for(int i = 0; i < ocu.size(); i++){
        double x = round(ocu[i]) / tu;
        rt.push_back((int)x);
    }
    return rt;
}

map<size_t, vector<int> > HashTab;
map<size_t, double> MinMR;

size_t HashFunc(vector<int> ocu){
    std::size_t seed = time(NULL);
    for(int i = 0; i < ocu.size(); i++)
        boost::hash_combine(seed, ocu[i]);
    while(HashTab.find(seed) != HashTab.end()){
        vector<int> x = HashTab[seed];
        if(x == ocu)
            return seed;
        boost::hash_combine(seed, 0);
    }
    HashTab[seed] = ocu;
    return seed;
}

//c is the capacity of each way
size_t TryAll(int nways, int nprogs, double c, vector<FPDist> fps){
    CAT mycat(c);
    for(int i = 0; i < fps.size(); i++)
        mycat.AddProgram(fps[i]);

    size_t up = 1;
    up <<= nways*nprogs;

    size_t mask = (1<<nprogs)-1;
    for(size_t i = 0; i < up; i++){
        if(i % (up>>1) == 0)
            cout << i << " : " << HashTab.size() << endl;
        //check if all ways are allocated
        size_t x = i;
        bool flag = false;
        for(int j = 0; j < nways; j++, x>>=nprogs){
            if(x & mask)
                continue;
            flag = true;
            break;
        }
        if(flag) continue;
        x = i;
        for(int j = 0; j < nways; j++, x>>=nprogs){
            size_t y = x & mask;
            mycat.AddWay(y);
        }
        vector<double> rt = mycat.GetOccupancies();
        //PrintVector(rt);
        vector<int> irt = Discretize(rt);
        //PrintVector(irt);
        size_t id = HashFunc(irt);
        if(MinMR.find(id) == MinMR.end())
            MinMR[id] = mycat.GetMissCount();
        else
            MinMR[id] = min(MinMR[id], mycat.GetMissCount());
        mycat.clear();
    }
    return HashTab.size();
}

int main(int argc, char** argv){
    cout << "Usage : command nways nprograms c footprint1 footprint2" << endl;
    int nways = atoi(argv[1]);
    int nprogs = atoi(argv[2]);
    double c = atoi(argv[3]);
    assert(nprogs+4 <= argc && "wrong parameters");
    vector<FPDist> fpds;

    for(int i = 4; i < argc; i++){
        string str(argv[i]);
        FPDist f(str);
        fpds.push_back(f);
    }
    size_t rt = TryAll(nways, nprogs, c, fpds);
    cout << "Number of identical occupancy : " << HashTab.size() << endl;
    
    double mmr = 1e40;
    for(auto i : MinMR)
        mmr = min(mmr, i.second);
    cout << "Minimal miss ratio count : " << mmr << endl;
    return 0;
}
