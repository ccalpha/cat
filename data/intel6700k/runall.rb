#!/usr/bin/ruby

require 'fileutils'

dirs = File.readlines("dirs.txt").map{|x| x.strip}
commands = File.readlines("commands.txt").map{|x| x.strip}
names = File.readlines("names.txt").map{|x| x.strip}

pwd = Dir.pwd

#Events = "-e r5381d0 -e r5320d1 -e 'cpu/config=0x5301b7,config1=0x3fbc000091/'"
#Events = "-e r5381d0 -e r5320d1 -e r5382d0 -e 'cpu/config=0x5301b7,config1=0x3fbc000091/' -e 'cpu/config=0x5301bb,config1=0x3fbc008fff/'"
Events = "-e r5381d0 -e r5320d1 -e 'cpu/config=0x5301b7,config1=0x3fbc008fff/'"

selected = ["milc","leslie3d", "gamess"]

for i in 0...dirs.length
    if not selected.include?(names[i])
        next
    end
    Dir.chdir(dirs[i])
    puts Dir.pwd
    system("perf stat -r 3 #{Events} -o #{names[i]}.data -B #{commands[i]}")
    FileUtils.move("#{names[i]}.data", "#{pwd}/#{names[i]}.data")
end
