#!/usr/bin/ruby

groups = File.readlines("random_groups").to_a
allocs = File.readlines("random_allocs").to_a
names = File.readlines("../PinTools/general/names.txt").to_a
FPDir = "../PinTools/Intel_FullTrace"

arate = File.readlines("accessrate").to_a.map{|x| x.strip}

for i in 0...groups.length
    fps = ""
    for j in groups[i].split
        fps = fps + " #{FPDir}/#{names[j.to_i].strip}.footprint"
    end
    system("echo #{allocs[i].strip.reverse} > alloc")
    #system("./PredictCAT -c 327680 -a alloc #{fps}")
    
    system("./PredictCAT -c 163840 -a alloc -r #{arate[i]} -f #{fps}")
    #system("./PredictCAT -c 327680 -a alloc -r #{arate[i]} #{fps}")
    #puts("./PredictCAT -c 327680 -a alloc #{fps}")
end
