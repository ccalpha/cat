#ifndef __footprint_h
#define __footprint_h

#include <iomanip>
#include <fstream>
#include <algorithm>
#include <map>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <numeric>
#include <string>
#include <cassert>
#include <set>
#include <vector>
#include "stdint.h"
using namespace std;

//distribution of footprint, only selected windows are figured out.
class FPDist{
    private:
        void GetW(){
            M = 0;
            for(int64_t i = 256, step = 1; i <= N; step <<= 1){
                M += 256;
                i += step * 256;
            }
            W = new int64_t[M];
            for(int64_t i = 0, j = 256, step = 1; i < M; i+=256){
                for(int k = 0; k < 256; k++, j += step){
                    W[i+k] = j;
                }
                step <<= 1;
            }
        }
    public:
        int64_t *W; //sizes of windows
        double *FP; // footprints
        int64_t N;  //number of windows
        int64_t M;  //number of choosen windows
        int64_t access_count; //number of accesses
        double data_count;
        int verifycode;

        FPDist(int64_t _N = 100000000000ll, int64_t _access_count = 0){
            if(verifycode == 0) verifycode = time(NULL);
            access_count = _access_count;
            N = _N;
            GetW();
            FP = new double[M];
            fill(FP, FP+M, 0.0);
            InitCalculation();
        }
        FPDist(const FPDist &x){
            M = x.M;
            N = x.N;
            access_count = x.access_count;
            data_count = x.data_count;
            chunk_count = x.chunk_count;
            W = new int64_t[M];
            FP = new double[M];
            memcpy(W, x.W, M*sizeof(int64_t));
            memcpy(FP, x.FP, M*sizeof(double));
        }

        virtual ~FPDist(){
            delete[] W;
            delete[] FP;
        }

    private:
        int64_t *cnt1;  // i*rt[i]-wss
        int64_t *cnt2; // histogram of rt[i]
        map<uint64_t, uint64_t> prev;
        int64_t sum_wss;
        int64_t wss_count;
        int64_t wss_ptr;
        int64_t wss_pre;
        int64_t chunk_count;
    public:
        void ShowInfo(){
            cout << "Showing information of footprint" << endl;
            cout << "Accesses count : " << access_count << endl;
            cout << "Length of trace : " << N << endl;
            cout << "Number of selected windows : "<< M << endl;
            cout << "Number of data : " << data_count << endl;
            cout << "Number of chunks : " << chunk_count << endl;
            cout << endl;
        }

        double GetDataCount(){
            return data_count;
        }
        void Clear(){
            delete cnt1;
            delete cnt2;
            access_count = 0;
            sum_wss = 0;
            wss_count = 0;
            wss_ptr = 0;
            wss_pre = 0;
            prev.clear();
            chunk_count = 0;
        }

        void InitCalculation(){
            chunk_count = 0;
            if(cnt1 != NULL){
                Clear();
            }
            cnt1 = new int64_t[M];
            cnt2 = new int64_t[M];
            fill(cnt1, cnt1+M, 0);
            fill(cnt2, cnt2+M, 0);
        }

        //**IMPORTANT** the number of reference of each chunk should be the same.
        //**IMPORTANT** same as FinishCalculation, if the trace is time preserving trace, ref_count shoule be given
        void StartNewChunk(int64_t ref_count = 0){
            ref_count = max(access_count, ref_count);
            ref_count++; //if use access_count in the loop, remove this, since Access() will increase access_count.
            for(map<uint64_t, uint64_t>::iterator it = prev.begin(); it != prev.end(); it++){
                if(it == prev.begin()){
                    Access(it->first, ref_count); // to update wss
                }
                else{
                    UpdateCount(it->first, ref_count);
                }
            }
            chunk_count++;
            access_count = 0;
            sum_wss = 0;
            wss_count = 0;
            wss_ptr = 0;
            wss_pre = 0;
            data_count += prev.size();
            prev.clear();
          
        }

        /*
         1. figure out reuse time rt
         2. let W[idx] is the closet window larger than rt
         3. cnt2 is the histogram of rt
         4. cnt1 is rt*numberof(rt)
         */
        map<int64_t, uint64_t> rts;
        void UpdateCount(uint64_t addr, int64_t ref_count){
            uint64_t prev_access = prev[addr];
            uint64_t rt = ref_count-prev_access+1;
/*
            int64_t idx = upper_bound(W, W+M, rt) - W;
            if(idx) cnt2[idx-1]++;
            if(idx < M) cnt1[idx] += rt;
            return;
*/
            int64_t idx = lower_bound(W, W+M, rt) - W;
            cnt2[idx]++;  //FTW(i)
            cnt1[idx] += rt-1;

        }

        //cnt1 is wss and FTW[1..w], cnt2 is FTW[w+1..n], wss_ptr = current window
        void Access(uint64_t addr, int64_t ref_count){
          access_count++;
          if(prev.find(addr) == prev.end())
            prev[addr] = 0;
          int64_t prev_access = prev[addr];
          if(prev_access) //deal FTW
              UpdateCount(addr, ref_count);
          prev[addr] = ref_count;

            
          ref_count++;  //shift the wss value to right
            while(wss_ptr<M && W[wss_ptr] < ref_count){
              cnt1[wss_ptr] -= wss_count*(W[wss_ptr]-wss_pre);
              wss_pre = W[wss_ptr];
              wss_ptr++;
            }
            cnt1[wss_ptr] -= (ref_count-1-wss_pre)*wss_count;
            if(!prev_access)
              wss_count++;
            cnt1[wss_ptr] -= wss_count;
            wss_pre = ref_count;
           
/*
            //deal wss
            while(wss_ptr < M && W[wss_ptr] <= ref_count){
                cnt1[wss_ptr] -= sum_wss + (W[wss_ptr]-wss_pre)*wss_count;
                wss_pre = W[wss_ptr];
                sum_wss = 0;
                wss_ptr++;
            }

            sum_wss += wss_count * (ref_count - wss_pre);
            if(!prev_access){
                wss_count++;
                prev[addr] = ref_count;
            }
            sum_wss += wss_count;
            wss_pre = ref_count+1;
*/
        }

        void Access(uint64_t addr){
            Access(addr, access_count+1);
        }

        void FinishCalculation(int64_t ref_count = 0, int64_t _chunk_count = 0){
          if(chunk_count < 1)
              chunk_count = 1;
          if(_chunk_count) chunk_count = _chunk_count;
          if(!ref_count) ref_count = access_count;
          data_count = (data_count + prev.size()) / (double)chunk_count;
          //Deal with last accesses
          ref_count++;
          for(map<uint64_t, uint64_t>::iterator it = prev.begin(); it != prev.end(); it++){
              if(it == prev.begin()){
                  Access(it->first, ref_count); // to update wss
                  access_count--;
              }
              else{
                  UpdateCount(it->first, ref_count);
              }
          }
          ref_count--;

          for(int64_t i = 1; i < M; i++)
              cnt1[i] += cnt1[i-1];
          for(int64_t i = M; i >= 0; i--)
              cnt2[i] += cnt2[i+1];
          
          for(int64_t i = 0; i < M; i++){
              if(W[i] > ref_count){
                  M = i;
                  break;
              }
              FP[i] = (cnt1[i] + cnt2[i+1]*W[i]) / (double)(ref_count-W[i]+1);
              FP[i] /= chunk_count;
          }
          N = ref_count;
        }
};

class VFPDist : public FPDist{
    private:
        double h;
        double th;
    public:
        VFPDist(FPDist f) : FPDist(f){
            h = -1;
            th = -1;
        }
        double GetH(){ return h;}
        double GetTH() { return th;}
        void init(double _h){
            h = _h;
            th = FPDist::GetFT(h);
        }
};

#endif
