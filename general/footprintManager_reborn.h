#pragma once

#include <fstream>
#include <string>
#include <algorithm>
#include <numeric>
#include <cassert>
#include "footprint_reborn.h"

void NormalizeAR(vector<double> &ar){
    if(ar[0] < 1.0) return;

    double x = 0;
    accumulate(ar.begin(), ar.end(), x);
    for(auto &i : ar)
        i /= x;
}

class FPUtil{
public:
	static double GetMissRatio(FPDist *fp, double c);
    //static double GetMissRatio(FPDist *fp, HashHist<uint64_t> *rthist, double c);
	static double GetMissRatioColdMiss(FPDist *fp, double c);
	static double GetFP(FPDist *fp, double w);
	static double GetFT(FPDist *fp, double c);
	static double GetNData(FPDist *fp);
    static double GetNAccesses(FPDist *fp);
	static void serialize(FPDist *fp, string filename = "footprint");
	static FPDist* deserialize(string filename = "footprint");
};


class VFPUtil : public FPUtil{
public:
	static double GetVFP(VFPDist *VFP, double h, double w);
	static double GetVFT(VFPDist *VFP, double h, double c);
};


class FPManager{
private:
    FPDist *fp;
public:
    double GetMissRatio(double c){return FPUtil::GetMissRatio(fp, c);}
    double GetMissRatioColdMiss(double c){return FPUtil::GetMissRatioColdMiss(fp, c);}
    double GetFP(double w){return FPUtil::GetFP(fp, w);}
    double GetFT(double c){return FPUtil::GetFT(fp, c);}
    double GetNData(){return FPUtil::GetNData(fp);}
    void serialize(string filename = "footprint"){FPUtil::serialize(fp, filename);}
    void deserialize(string filename = "footprint"){fp = FPUtil::deserialize(filename);}
    FPDist* GetFPDist() {return fp;}
};

class VFPManager : public FPManager{
private:
    VFPDist *vfp;
    double h;
    double th;
public:
    VFPManager(){ h = 0; th = 0; }
    double GetFP(double w){return GetVFP(h, w);}
    double GetFT(double c){return GetVFT(h, c);}
    void InitTH(double _h);
    double GetVFP(double _h, double w);
    double GetVFT(double h, double c);
    VFPDist* GetVFPDist() { return vfp;}
};

void VFPManager::InitTH(double _h){
    if(h == _h) return;
    h = _h;
    th = FPUtil::GetFT(vfp, h);
}

double VFPManager::GetVFP(double _h, double w){
	InitTH(_h);
    return max(FPUtil::GetFP(vfp, w+th)-h, 0.0);
}

double VFPManager::GetVFT(double _h, double c){
    InitTH(_h);
	if(c < vfp->GetNData()) return 1e20;
	return FPUtil::GetFT(vfp, c+h)-th;
}

double FPUtil::GetMissRatio(FPDist *fp, double c){
	if(c == 0) return 1;
	double tc = GetFT(fp, c);
    //cout << fp->GetNMisses(tc)/(double)fp->GetNAccesses() << " " << GetFP(fp, tc+1)-GetFP(fp, tc) << endl;
    //return fp->GetNMisses(tc)/(double)fp->GetNAccesses();
	return GetFP(fp, tc+1)-GetFP(fp, tc);
}


double FPUtil::GetMissRatioColdMiss(FPDist *fp, double c){
	double mr = GetMissRatio(fp, c);
	return mr+fp->GetNData()/fp->GetNAccesses();
}

double FPUtil::GetFP(FPDist *fp, double w){
	return (*fp)[w];
}

//<! ft(c) = (c-b)/(a-b)*[ft(a)-ft(b)] + ft(b) 
double FPUtil::GetFT(FPDist *fp, double c){
    if(c >= fp->GetNData())
        return fp->GetNAccesses();
    double l = 0, h = fp->GetNAccesses();
    while(h-l>1e-6){
        double mid = (l+h)/2;
        if(GetFP(fp,mid) < c)
            l = mid;
        else
            h = mid;
    }
    cout << l << endl;
    return l;
    //return fp->FPHist.GetXValue(c);
}

double FPUtil::GetNData(FPDist *fp){
	return fp->GetNData();
}

double FPUtil::GetNAccesses(FPDist *fp){
    return fp->GetNAccesses();
}

void FPUtil::serialize(FPDist *fp, string filename){
	ofstream fout;
	fout.open(filename.c_str());
    fp->serialize(fout);
	fout.close();
}



FPDist* FPUtil::deserialize(string filename){
	ifstream fin(filename.c_str());
	assert(fin.good());
    FPDist *_fp = new FPDist();
    _fp->deserialize(fin);
	fin.close();
    return _fp;
}

//<! ar should be normalized, input fill time w, return composed fill time, normalized to access rate ar.
double ComposedFP(double w, vector<FPManager*> fps, vector<double> &ar){
    NormalizeAR(ar);
    double rt = 0;
    for(size_t i = 0; i < fps.size(); i++)
        rt += fps[i]->GetFP(w*ar[i]);
    return rt;
}

//<! input cach size(c), return composed fill times of fps, ar can be unormalized. 
double ComposedFT(double c, vector<FPManager*> fps, vector<double> &ar){
    NormalizeAR(ar);
    double l = 0, h = 1e20;
    while(h-l > 1e-6){
        double mid = (l+h)/2;
        double mc = ComposedFP(mid, fps, ar);
        if(mc < c) l = mid;
        else h = mid;
    }
    return l;
}

double VFPUtil::GetVFP(VFPDist *VFP, double h, double w){
    double th = FPUtil::GetFT(VFP, h);
	return max(FPUtil::GetFP(VFP, w+th) - h, 0.0);
}

double VFPUtil::GetVFT(VFPDist *VFP, double h, double c){
	if(c < VFP->GetNData()) return 1e20;
    double th = FPUtil::GetFT(VFP, h);
	return FPUtil::GetFT(VFP, c+h) - th;
}
