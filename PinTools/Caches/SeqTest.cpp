#include "pin.H"
typedef UINT32 CACHE_STATS;
#include "pin_cache.H"
#include "Caches.h"
#include "CacheConfig.h"
#include <iostream>

using namespace std;

DIPCACHE<UL3::allocation> dip_dip("DIP_DIP", 1*MEGA, 64, 16);
LRUCACHE<UL3::allocation> lru_dip("LRU_DIP", 1*MEGA, 64, 16);
DIPCACHE<UL3::allocation> dip_power8("DIP_Power8", 8*MEGA, 128, 8);
LRUCACHE<UL3::allocation> lru_power8("LRU_Power8", 8*MEGA, 128, 8);


int main(int argc, char*argv[]){
        PIN_Init(argc, argv);
        for(int itr = 0; itr < 10; itr++)
        for(int i = 0; i < 16*MEGA; i += 4){
                dip_dip.Access(i, 4, CACHE_BASE::ACCESS_TYPE_LOAD);
                lru_dip.Access(i, 4, CACHE_BASE::ACCESS_TYPE_LOAD);
                dip_power8.Access(i, 4, CACHE_BASE::ACCESS_TYPE_LOAD);
                lru_power8.Access(i, 4, CACHE_BASE::ACCESS_TYPE_LOAD);
        }

        cout << dip_dip;
        cout << lru_dip;
        cout << dip_power8;
        cout << lru_power8;
        return 0;
}
