#include <iostream>
#include <fstream>
#include <stdint.h>

#include "ReuseTime.h"
using namespace std;

//command ReuseTimeFile ReuseTime
int main(int argc, char **argv){
    string s(argv[1]);
    ReuseTimeHistogram rth(s);
    uint64_t rtime = atoll(argv[2]);
    cout.precision(5);
    cout << "Miss Ratio : " << rth.GetMissRatio(rtime) << endl;
    return 0;
}
