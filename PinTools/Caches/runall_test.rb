#!/usr/bin/ruby

require 'thread' 

CAT_ROOT = "#{Dir.home}/cat"
dirs = File.readlines("#{CAT_ROOT}/general/dirs.txt").map{|x| x.strip}
commands = File.readlines("#{CAT_ROOT}/general/commands.txt").map{|x| x.strip}
names = File.readlines("#{CAT_ROOT}/general/names.txt").map{|x| x.strip}
#ft = File.readlines("../Traces/fp_test/filltime_8M")

PIN_ROOT = ENV["PIN_ROOT"]
TOOL_S = "#{CAT_ROOT}/PinTools/Caches/obj-intel64/SimulatorFilter.so"
TOOL_F = "#{CAT_ROOT}/PinTools/Caches/obj-intel64/footprint.so"
TOOL_T = "#{CAT_ROOT}/PinTools/Caches/obj-intel64/FillTimeFilter.so"
TOOL_WSS = "#{CAT_ROOT}/PinTools/Caches/obj-intel64/CountWSS.so"

OUTPUT = "#{CAT_ROOT}/PinTools/Caches/output"

queue = Queue.new
threads = []
for i in [10]
#for i in 0...dirs.length
    if File.exist? "#{OUTPUT}/#{names[i].strip}.fp"
        puts names[i]
        next
    end

    puts("cd #{dirs[i].strip}; #{PIN_ROOT}/pin -t #{TOOL_F} -o #{OUTPUT}/#{names[i].strip}.fp -- #{commands[i].strip}")
#    system("cd #{dirs[i].strip}; #{PIN_ROOT}/pin -t #{TOOL_F} -o #{OUTPUT}/#{names[i].strip}.fp -- #{commands[i].strip}")


#    next

    #queue << "cd #{dirs[i].strip}; #{PIN_ROOT}/pin -t #{TOOL_S} -m 1000000 -o #{target}/#{names[i].strip}.cstr -- #{commands[i].strip}"
#    queue << "cd #{dirs[i].strip}; #{PIN_ROOT}/pin -t #{TOOL_WSS} -o /localdisk/cye/cat/PinTools/Caches/#{names[i]}.wss -- #{commands[i]}"
    queue << "cd #{dirs[i].strip}; time #{PIN_ROOT}/pin -t #{TOOL_F} -o #{OUTPUT}/#{names[i].strip}.fp -- #{commands[i].strip}"
end

#puts threads.length
4.times do |i|
    threads << Thread.new do
        while(e = queue.pop(true) rescue nil)
            puts e
            pid = spawn("#{e}")
            Process.wait pid
        end
    end
end
threads.each{|t| t.join}
